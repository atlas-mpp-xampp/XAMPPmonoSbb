# $Id: CMakeLists.txt 783761 2016-11-11 16:50:45Z nkoehler $
################################################################################
# Package: XAMPPmonoH
################################################################################


# Declare the package name:
atlas_subdir( XAMPPmonoH )

# Extra dependencies, based on the environment:
set( extra_dep )
if( XAOD_STANDALONE )
   set( extra_dep Control/xAODRootAccess )
else()
   set( extra_dep Control/AthenaBaseComps GaudiKernel )
endif()

option( USE_XGBOOST "Enable compilation of XGBoost elements " OFF )

if( USE_XGBOOST )
  # Temporary directory for XGBoost build results:
  set( _xgboostBuildDir ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/XGBoostBuild )

  # Set up the variables that the users can pick up XGBoost with:
  set( XGBOOST_INCLUDE_DIRS
    $<BUILD_INTERFACE:${_xgboostBuildDir}/include>
    $<INSTALL_INTERFACE:include> )
  set( XGBOOST_LIBRARIES
    ${_xgboostBuildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}xgboost${CMAKE_SHARED_LIBRARY_SUFFIX}
    ${_xgboostBuildDir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}rabit${CMAKE_STATIC_LIBRARY_SUFFIX}
    ${_xgboostBuildDir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}dmlc${CMAKE_STATIC_LIBRARY_SUFFIX} )

  # Set up the build of XGBoost for the build area:
  ExternalProject_Add( XGBoost
    PREFIX ${CMAKE_BINARY_DIR}
    INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
    GIT_REPOSITORY https://github.com/dmlc/xgboost.git
    GIT_TAG 78ec77f
    BUILD_IN_SOURCE 1
    BUILD_COMMAND $(MAKE)
    INSTALL_COMMAND
    ${CMAKE_COMMAND} -E make_directory ${_xgboostBuildDir} &&
    ${CMAKE_COMMAND} -E make_directory ${_xgboostBuildDir}/lib &&
    ${CMAKE_COMMAND} -E make_directory ${_xgboostBuildDir}/include &&
    ${CMAKE_COMMAND} -E create_symlink ${CMAKE_BINARY_DIR}/src/XGBoost/include/xgboost ${_xgboostBuildDir}/include/xgboost &&
    ${CMAKE_COMMAND} -E create_symlink ${CMAKE_BINARY_DIR}/src/XGBoost/rabit/include/rabit ${_xgboostBuildDir}/include/rabit &&
    ${CMAKE_COMMAND} -E create_symlink ${CMAKE_BINARY_DIR}/src/XGBoost/dmlc-core/include/dmlc ${_xgboostBuildDir}/include/dmlc &&
    ${CMAKE_COMMAND} -E copy_if_different lib/${CMAKE_SHARED_LIBRARY_PREFIX}xgboost${CMAKE_SHARED_LIBRARY_SUFFIX} ${_xgboostBuildDir}/lib &&
    ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_STATIC_LIBRARY_PREFIX}rabit${CMAKE_STATIC_LIBRARY_SUFFIX} ${_xgboostBuildDir}/lib &&
    ${CMAKE_COMMAND} -E copy_if_different dmlc-core/${CMAKE_STATIC_LIBRARY_PREFIX}dmlc${CMAKE_STATIC_LIBRARY_SUFFIX} ${_xgboostBuildDir}/lib &&
    ${CMAKE_COMMAND} -E copy_directory ${_xgboostBuildDir}/ <INSTALL_DIR>
    BUILD_BYPRODUCTS ${XGBOOST_LIBRARIES} )

  # Install XGBoost
  install( DIRECTORY ${_xgboostBuildDir}/
    DESTINATION . USE_SOURCE_PERMISSIONS )

  # Clean up:
  unset( _xgboostBuildDir )

endif() # if USE_XGBOOST

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   MetTriggerSFUncertainties
   SMShapeUncertainties
   Control/AthToolSupport/AsgTools
   Event/xAOD/xAODCore
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODJet
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODTrigMissingET
   Event/xAOD/xAODMuon
   Event/xAOD/xAODTau
   Event/xAOD/xAODTracking
   Event/xAOD/xAODTruth
   Event/xAOD/xAODParticleEvent 
   Event/xAOD/xAODLuminosity 
   PhysicsAnalysis/AnalysisCommon/AssociationUtils
   PhysicsAnalysis/AnalysisCommon/PATInterfaces
   PhysicsAnalysis/AnalysisCommon/PMGTools
   PhysicsAnalysis/MCTruthClassifier
   PhysicsAnalysis/SUSYPhys/SUSYTools 
   Reconstruction/Jet/JetJvtEfficiency
   Reconstruction/Jet/JetSubStructureUtils
   Trigger/TrigAnalysis/TrigDecisionTool
   PRIVATE
   XAMPPbase   
   FlavorTagDiscriminants
   AsgExternal/Asg_Test
   Control/xAODRootAccess
   Control/AthContainers
   DataQuality/GoodRunsLists
   Event/EventPrimitives
   Event/FourMomUtils
   Event/xAOD/xAODBTagging
   Event/xAOD/xAODBase
   Event/xAOD/xAODCaloEvent
   Event/xAOD/xAODCutFlow
   Event/xAOD/xAODPrimitives
   PhysicsAnalysis/AnalysisCommon/IsolationSelection
   PhysicsAnalysis/AnalysisCommon/PileupReweighting
   PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonShowerShapeFudgeTool
   PhysicsAnalysis/ElectronPhotonID/IsolationCorrections
   PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection
   PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
   PhysicsAnalysis/JetMissingEtID/JetSelectorTools
   PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces
   PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections
   PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections
   PhysicsAnalysis/MuonID/MuonSelectorTools
   PhysicsAnalysis/TauID/TauAnalysisTools
   Reconstruction/Jet/JetCPInterfaces
   Reconstruction/Jet/JetCalibTools
   Reconstruction/Jet/JetInterface
   Reconstruction/Jet/JetResolution
   Reconstruction/Jet/JetUncertainties
   Reconstruction/Jet/JetMomentTools
   Reconstruction/Jet/BoostedJetTaggers
   Reconstruction/MET/METInterface
   Reconstruction/MET/METUtilities
   Reconstruction/tauRecTools
   Tools/PathResolver
   Trigger/TrigAnalysis/TriggerMatchingTool
   Trigger/TrigConfiguration/TrigConfInterfaces
   Trigger/TrigConfiguration/TrigConfxAOD
   ${extra_deps} )

# External dependencies:
find_package( Boost )
find_package( ROOT COMPONENTS Core Tree RIO Hist Physics )

# Libraries in the package:
atlas_add_library( XAMPPmonoHLib
   XAMPPmonoH/*.h Root/*.cxx
   PUBLIC_HEADERS XAMPPmonoH
   INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${XGBOOST_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${Boost_LIBRARIES} ${XGBOOST_LIBRARIES} ${ROOT_LIBRARIES} AsgTools xAODCore xAODEgamma xAODEventInfo xAODRootAccess FlavorTagDiscriminants
   xAODJet xAODMissingET xAODMuon xAODTau xAODTracking xAODTruth xAODParticleEvent xAODLuminosity SUSYToolsLib xAODCutFlow
   XAMPPbaseLib 
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${BOOST_LIBRARIES} 
   MetTriggerSFUncertaintiesLib SMShapeUncertaintiesLib
   AssociationUtilsLib PATInterfaces TrigDecisionToolLib PMGToolsLib
   MCTruthClassifierLib JetJvtEfficiencyLib JetSubStructureUtils
   AthContainers EventPrimitives FourMomUtils xAODBTagging xAODBase
   xAODPrimitives IsolationSelectionLib PileupReweightingLib
   ElectronEfficiencyCorrectionLib ElectronPhotonFourMomentumCorrectionLib
   ElectronPhotonSelectorToolsLib ElectronPhotonShowerShapeFudgeToolLib
   IsolationCorrectionsLib PhotonEfficiencyCorrectionLib JetSelectorToolsLib
   FTagAnalysisInterfacesLib MuonEfficiencyCorrectionsLib
   MuonMomentumCorrectionsLib MuonSelectorToolsLib TauAnalysisToolsLib
   JetCPInterfaces JetCalibToolsLib JetInterface JetResolutionLib
   JetUncertaintiesLib JetMomentToolsLib METInterface METUtilitiesLib
   PathResolver TriggerMatchingToolLib TrigConfInterfaces TrigConfxAODLib
   xAODTrigMissingET xAODMetaData GoodRunsListsLib BoostedJetTaggersLib
   PRIVATE_LINK_LIBRARIES xAODTrigger PathResolver )

if( USE_XGBOOST )
  add_dependencies( XAMPPmonoHLib XGBoost )
  target_compile_definitions( XAMPPmonoHLib PUBLIC USE_XGBOOST )
endif()

if( NOT XAOD_STANDALONE )
   atlas_add_component( XAMPPmonoH
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaBaseComps GaudiKernel GoodRunsLists
      AthenaKernel AthAnalysisBaseCompsLib xAODEventInfo xAODMuon xAODPrimitives xAODCutFlow
      xAODJet xAODBTagging xAODEgamma xAODMissingET xAODTracking xAODTau xAODParticleEvent
      TauAnalysisToolsLib xAODCore AthContainers AsgTools xAODBase xAODCutFlow xAODRootAccess xAODTruth
      xAODMetaData PATInterfaces PathResolver SUSYToolsLib XAMPPbaseLib XAMPPmonoHLib
      MetTriggerSFUncertaintiesLib SMShapeUncertaintiesLib)
endif()

atlas_add_dictionary( XAMPPmonoHDict
   XAMPPmonoH/XAMPPmonoHDict.h
   XAMPPmonoH/selection.xml
   LINK_LIBRARIES XAMPPmonoHLib )


# Install files from the package:
atlas_install_data( data/* )
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )

# Install python packages
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
include(SetupPythonPackage)

## Install rootpy
#setup_python_package( rootpy
#   "https://files.pythonhosted.org/packages/55/e5/07a19ef0bb1111039b69202ba30550cc65c6d295bb17a81861b5994bd3c2/rootpy-1.0.1.tar.gz"
#   "3146957733f56f4981cd2f5162774f66"
#   PATCH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/patch-setup.cfg setup.cfg
#)
#
## Install seaborn plotting
#setup_python_package( seaborn
#   "https://files.pythonhosted.org/packages/7a/bf/04cfcfc9616cedd4b5dd24dfc40395965ea9f50c1db0d3f3e52b050f74a5/seaborn-0.9.0.tar.gz"
#   "b50cfee47ab6c7d8e07646e805523e14"
#   PATCH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/patch-setup.cfg setup.cfg
#)
