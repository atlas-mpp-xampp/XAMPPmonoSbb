#include <XAMPPmonoH/LikelihoodOrdering.h>

#include <cmath>

namespace XAMPP {

    float METz(TLorentzVector lepton, TLorentzVector metVec, int type) {
        float pTauZ = lepton.Pz() / 1000.;  //[GeV]
        float pTauY = lepton.Py() / 1000.;
        float pTauX = lepton.Px() / 1000.;
        float energyTau = lepton.E() / 1000.;
        float METx = metVec.Px() / 1000.;
        float METy = metVec.Py() / 1000.;

        float poleMassW = 80.4;  //[GeV]
        float A = poleMassW * poleMassW / 2. + pTauX * METx + pTauY * METy;

        float center = A * pTauZ / (energyTau * energyTau - pTauZ * pTauZ);
        float discriminant =
            A * A * pTauZ * pTauZ - (energyTau * energyTau - pTauZ * pTauZ) * (energyTau * energyTau * (METx * METx + METy * METy) - A * A);

        float METz = 0.;
        if (discriminant > 0)
            METz = 1000 * (center + type * TMath::Sqrt(discriminant) / (energyTau * energyTau - pTauZ * pTauZ));  //[MeV]
        else
            METz = 1000 * center;

        return METz;
    }

    float LBW(float mass, float poleMass, float width) {
        return 2 * TMath::Log(pow((mass * mass - poleMass * poleMass), 2) + pow((poleMass * width), 2));
    }

    float LChi2(float mass, float poleMass, float width) { return pow((mass - poleMass), 2) / (width * width); }

    float CalculateLikelihood(std::vector<float>& measuredMasses, int Njets) {
        const float poleMassW = 80.4;    //[GeV]
        const float poleMassT = 172.5;   //[GeV]
        const float widthW = 2.085;      //[GeV]
        const float widthT = 1.41;       //[GeV]
        const float diffSigmaHad = 9.;   // uncertainty on the massTh - massWh
        const float diffSigmaLep = 18.;  // uncertainty on the massTh - massWh
        const float diffMass = poleMassT - poleMassW;

        const float massQ1Q2 = measuredMasses[0];
        const float massQ1Q2Bh = (Njets == 4) ? measuredMasses[1] : measuredMasses[2];

        float likelihood =
            LBW(massQ1Q2, poleMassW, widthW) + LBW(massQ1Q2Bh, poleMassT, widthT) + LChi2(diffMass, massQ1Q2Bh - massQ1Q2, diffSigmaHad);

        if (Njets == 4) {
            return likelihood;  // += LChi2(diffMass, massQ1Q2Bh-massQ1Q2,
                                // diffSigmaHad);
        } else if (Njets > 4) {
            const float massQ3MET = measuredMasses[1];
            const float massQ3METBl = measuredMasses[3];

            return likelihood += LBW(massQ3METBl, poleMassT, widthT) + LBW(massQ3MET, poleMassW, widthW) +
                                 LChi2(diffMass, massQ3METBl - massQ3MET, diffSigmaLep);
        } else
            return 0;
    }

    float EllipticCut(float mtop, float mW, int N_btag) {
        float theta = 40. * (TMath::Pi() / 180.);
        float x_0 = 0;
        float y_0 = 0;
        float a = 0;
        float b = 0;

        if (N_btag == 1) {
            // optimised parameters for 1 tag of the ellipse
            x_0 = 174.9;  // center, mtop
            y_0 = 82.8;   // center, mW
            a = 44.;      // major semi-axis
            b = 24.;      // minor semi-axis
        } else if (N_btag == 2) {
            // optimised parameters for 2 tag of the ellipse
            x_0 = 172.;  // center, mtop
            y_0 = 83.3;  // center, mW
            a = 56.;     // major semi-axis
            b = 28.;     // minor semi-axis
        }

        float eccentricity = TMath::Sqrt(a * a - b * b);

        float x_1 = x_0 + eccentricity * TMath::Cos(theta);
        float y_1 = y_0 + eccentricity * TMath::Sin(theta);
        float x_2 = x_0 - eccentricity * TMath::Cos(theta);
        float y_2 = y_0 - eccentricity * TMath::Sin(theta);

        float d1 = TMath::Sqrt(pow((mtop - x_1), 2) + pow((mW - y_1), 2));
        float d2 = TMath::Sqrt(pow((mtop - x_2), 2) + pow((mW - y_2), 2));

        return (2 * a - (d1 + d2));
    }

    float LikelihoodOrderingFourJets(std::vector<const xAOD::Jet*> jetsForHbbReco, TLorentzVector tlv_complete_j0,
                                     TLorentzVector tlv_complete_j1, int btag) {
        int Njets = jetsForHbbReco.size();
        TLorentzVector tempWh, tempTh;
        std::vector<float> measuredMasses;
        int indexBh = 0;
        int indexQ1 = 0;
        int indexQ2 = 0;
        float tempLogL;
        float minLogL = -1.;

        for (int l = 1; l < Njets; l++) {
            // index l: combinatorics for 2nd bjet in 1tag events
            if (btag == 2 && l < Njets - 1) continue;
            for (int i = btag; i < Njets - 1; i++) {
                if (btag == 1 && i == l) continue;
                for (int j = i + 1; j < Njets; j++) {
                    if (btag == 1 && j == l) continue;

                    tempWh = jetsForHbbReco.at(i)->p4() + jetsForHbbReco.at(j)->p4();
                    // b-jet from hadronic W: j0
                    tempTh = tempWh + tlv_complete_j0;
                    measuredMasses.push_back(tempWh.M() / 1000.);
                    measuredMasses.push_back(tempTh.M() / 1000.);
                    tempLogL = CalculateLikelihood(measuredMasses, Njets);
                    if (minLogL == -1.) minLogL = tempLogL;

                    if (tempLogL <= minLogL) {
                        minLogL = tempLogL;
                        indexBh = 0;
                        indexQ1 = i;
                        indexQ2 = j;
                    }
                    measuredMasses.clear();

                    // b-jet from hadronic W: j1 (2tag) or l (1tag)
                    if (btag == 1)
                        tempTh = tempWh + jetsForHbbReco.at(l)->p4();
                    else if (btag == 2)
                        tempTh = tempWh + tlv_complete_j1;
                    measuredMasses.push_back(tempWh.M() / 1000.);
                    measuredMasses.push_back(tempTh.M() / 1000.);
                    tempLogL = CalculateLikelihood(measuredMasses, Njets);

                    if (tempLogL <= minLogL) {
                        minLogL = tempLogL;
                        if (btag == 1)
                            indexBh = l;
                        else if (btag == 2)
                            indexBh = 1;
                        indexQ1 = i;
                        indexQ2 = j;
                    }
                    measuredMasses.clear();
                }  // over j
            }      // over i
        }          // over l

        if (indexQ1 == indexQ2 || indexQ1 == indexBh || indexQ2 == indexBh) return -1;

        TLorentzVector tlv_Wh = jetsForHbbReco.at(indexQ1)->p4() + jetsForHbbReco.at(indexQ2)->p4();
        TLorentzVector tlv_Th;

        if (btag == 2) {
            if (indexBh == 0)
                tlv_Th = tlv_Wh + tlv_complete_j0;
            else if (indexBh == 1)
                tlv_Th = tlv_Wh + tlv_complete_j1;
        } else if (btag == 1) {
            if (indexBh == 0)
                tlv_Th = tlv_Wh + tlv_complete_j0;
            else
                tlv_Th = tlv_Wh + jetsForHbbReco.at(indexBh)->p4();
        }
        return EllipticCut(tlv_Th.M() / 1000., tlv_Wh.M() / 1000., btag);
    }
    float LikelihoodOrdering(std::vector<const xAOD::Jet*> jetsForHbbReco, TLorentzVector tlv_complete_j0, TLorentzVector tlv_complete_j1,
                             TLorentzVector metVec, const xAOD::Muon* muon, int btag, NLepton nlep) {
        int Njets = jetsForHbbReco.size();

        if (Njets < 4 || btag == 0)
            return 0;
        else if (Njets == 4)
            return LikelihoodOrderingFourJets(jetsForHbbReco, tlv_complete_j0, tlv_complete_j1, btag);
        else {
            TLorentzVector tempWh, tempTh;
            TLorentzVector tempTau, totalMetVec;
            TLorentzVector tempWl, tempTl;
            int indexQ1 = 0;
            int indexQ2 = 0;
            //           int indexQ3 = 0;
            int indexBh = 0;
            //           int indexBl = 0;
            float minLogL = -1.;
            float tempMinL;
            std::vector<float> measuredMasses;

            for (int l = 1; l < Njets; l++) {
                // index l: combinatorics for 2nd bjet in 1tag events
                if (btag == 2 && l < Njets - 1) continue;
                for (int i = btag; i < Njets - 1; i++) {
                    if (btag == 1 && i == l) continue;
                    for (int j = i + 1; j < Njets; j++) {
                        if (btag == 1 && j == l) continue;
                        tempWh = jetsForHbbReco.at(i)->p4() + jetsForHbbReco.at(j)->p4();
                        for (int k = btag; k < Njets; k++) {
                            // index k: combinatorics for tau jet in 0 lepton, 2 lepton regions.
                            // in 1 lepton region, no need of combinatorics, muon plays role of
                            // tau jet.
                            if (nlep == NLepton::OneLepton && k < Njets - 1) continue;
                            if (nlep != NLepton::OneLepton && (k == i || k == j)) continue;
                            if (nlep != NLepton::OneLepton && btag == 1 && k == l) continue;

                            if (nlep == NLepton::OneLepton)
                                tempTau = muon->p4();
                            else
                                tempTau = jetsForHbbReco.at(k)->p4();

                            float metVecZ = 0.;
                            // z-component of pTV needed for invariance mass of leptonic top.
                            // in 2 lepton region, four-momentum of Z is pTV.
                            if (nlep != NLepton::TwoLepton) {
                                metVecZ = METz(tempTau, metVec,
                                               1);  //+ solution from quadratic equation
                                totalMetVec.SetPxPyPzE(metVec.Px(), metVec.Py(), metVecZ, metVec.E());
                            } else
                                totalMetVec = metVec;

                            tempWl = tempTau + totalMetVec;

                            //////////////combinatorics on b-jets
                            // bjet from hadronic top: j0
                            tempTh = tempWh + tlv_complete_j0;
                            if (btag == 2)
                                tempTl = tempWl + tlv_complete_j1;
                            else if (btag == 1)
                                tempTl = tempWl + jetsForHbbReco.at(l)->p4();

                            measuredMasses.push_back(tempWh.M() / 1000.);
                            measuredMasses.push_back(tempWl.M() / 1000.);
                            measuredMasses.push_back(tempTh.M() / 1000.);
                            measuredMasses.push_back(tempTl.M() / 1000.);
                            tempMinL = CalculateLikelihood(measuredMasses, Njets);
                            if (minLogL == -1.) minLogL = tempMinL;

                            if (tempMinL <= minLogL) {
                                minLogL = tempMinL;
                                indexQ1 = i;
                                indexQ2 = j;
                                //                       indexQ3 = k;
                                indexBh = 0;
                                //                       if(btag == 2) indexBl = 1;
                                //                       else if(btag == 1) indexBl = l;
                            }
                            measuredMasses.clear();

                            // bjet from hadronic top: j1 (2tag) or l (1tag)
                            if (btag == 2)
                                tempTh = tempWh + tlv_complete_j1;
                            else if (btag == 1)
                                tempTh = tempWh + jetsForHbbReco.at(l)->p4();
                            tempTl = tempWl + tlv_complete_j0;

                            measuredMasses.push_back(tempWh.M() / 1000.);
                            measuredMasses.push_back(tempWl.M() / 1000.);
                            measuredMasses.push_back(tempTh.M() / 1000.);
                            measuredMasses.push_back(tempTl.M() / 1000.);
                            tempMinL = CalculateLikelihood(measuredMasses, Njets);

                            if (tempMinL <= minLogL) {
                                minLogL = tempMinL;
                                indexQ1 = i;
                                indexQ2 = j;
                                //                       indexQ3 = k;
                                if (btag == 2)
                                    indexBh = 1;
                                else if (btag == 1)
                                    indexBh = l;
                                //                       indexBl = 0;
                            }
                            measuredMasses.clear();

                            //- solution from quadratic function, pTV_z
                            if (nlep != NLepton::TwoLepton) {
                                metVecZ = METz(tempTau, metVec, -1);  //- solution
                                totalMetVec.SetPxPyPzE(metVec.Px(), metVec.Py(), metVecZ, metVec.E());
                            } else
                                totalMetVec = metVec;

                            tempWl = tempTau + totalMetVec;

                            //////combinatorics on b-jets
                            // bjet from hadronic top: j0
                            tempTh = tempWh + tlv_complete_j0;
                            if (btag == 2)
                                tempTl = tempWl + tlv_complete_j1;
                            else if (btag == 1)
                                tempTl = tempWl + jetsForHbbReco.at(l)->p4();
                            measuredMasses.push_back(tempWh.M() / 1000.);
                            measuredMasses.push_back(tempWl.M() / 1000.);
                            measuredMasses.push_back(tempTh.M() / 1000.);
                            measuredMasses.push_back(tempTl.M() / 1000.);
                            tempMinL = CalculateLikelihood(measuredMasses, Njets);

                            if (tempMinL <= minLogL) {
                                minLogL = tempMinL;
                                indexQ1 = i;
                                indexQ2 = j;
                                //                       indexQ3 = k;
                                indexBh = 0;
                                //                       if(btag == 2) indexBl = 1;
                                //                       else if(btag == 1) indexBl = l;
                            }
                            measuredMasses.clear();

                            // bjet from hadronic top: j1 (2tag) or l (1tag)
                            if (btag == 2)
                                tempTh = tempWh + tlv_complete_j1;
                            else if (btag == 1)
                                tempTh = tempWh + jetsForHbbReco.at(l)->p4();
                            tempTl = tempWl + tlv_complete_j0;
                            measuredMasses.push_back(tempWh.M() / 1000.);
                            measuredMasses.push_back(tempWl.M() / 1000.);
                            measuredMasses.push_back(tempTh.M() / 1000.);
                            measuredMasses.push_back(tempTl.M() / 1000.);
                            tempMinL = CalculateLikelihood(measuredMasses, Njets);

                            if (tempMinL <= minLogL) {
                                minLogL = tempMinL;
                                indexQ1 = i;
                                indexQ2 = j;
                                //                       indexQ3 = k;
                                if (btag == 2)
                                    indexBh = 1;
                                else if (btag == 1)
                                    indexBh = l;
                                //                       indexBl = 0;
                            }
                            measuredMasses.clear();
                        }
                    }
                }
            }
            TLorentzVector tlv_Wh, tlv_Th;
            tlv_Wh = jetsForHbbReco.at(indexQ1)->p4() + jetsForHbbReco.at(indexQ2)->p4();

            if (btag == 2) {
                if (indexBh == 0)
                    tlv_Th = tlv_Wh + tlv_complete_j0;
                else if (indexBh == 1)
                    tlv_Th = tlv_Wh + tlv_complete_j1;
            } else if (btag == 1) {
                if (indexBh == 0)
                    tlv_Th = tlv_Wh + tlv_complete_j0;
                else
                    tlv_Th = tlv_Wh + jetsForHbbReco.at(indexBh)->p4();
            }
            return EllipticCut(tlv_Th.M() / 1000., tlv_Wh.M() / 1000., btag);
        }
    }
}  // namespace XAMPP
