#ifndef XAMPPmonoH_LINKDEF_H
#define XAMPPmonoH_LINKDEF_H

#include <XAMPPmonoH/MonoHAnalysisHelper.h>
#include <XAMPPmonoH/MonoHTruthAnalysisHelper.h>

#include <XAMPPmonoH/MonoHAnalysisConfig.h>
#include <XAMPPmonoH/MonoHTruthSelector.h>
#include <XAMPPmonoH/MonoHJetSelector.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class XAMPP::MonoHAnalysisHelper;
#pragma link C++ class XAMPP::MonoHTruthAnalysisHelper;
#pragma link C++ class XAMPP::MonoHAnalysisConfig;
#pragma link C++ class XAMPP::MonoHTruthAnalysisConfig;
#pragma link C++ class XAMPP::MonoHTruthSelector;
#pragma link C++ class XAMPP::MonoHJetSelector;
#endif

#endif
