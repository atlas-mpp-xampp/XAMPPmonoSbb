#include <XAMPPmonoH/MonoHAnalysisConfig.h>

namespace XAMPP {
    MonoHAnalysisConfig::MonoHAnalysisConfig(std::string Analysis) : AnalysisConfig(Analysis) {
        declareProperty("doProduction", m_doproduction = true);
        declareProperty("doBlinding", m_blinding = true);
        declareProperty("doMETTrigger", m_doMETTrigger = true);
        declareProperty("doSingleElecTrigger", m_doSingleElecTrigger = true);
        declareProperty("doSingleMuonTrigger", m_doSingleMuonTrigger = true);
        declareProperty("doMETTriggerCalibration", m_doMETTriggerCalibration = false);
        declareProperty("doPFlowElecCleaning", m_doPFlowElecCleaning = false);
        declareProperty("doCONFselection", m_doCONFselection = false);
        declareProperty("doFullSkimming", m_doFullSkimming = false);
    }

    StatusCode MonoHAnalysisConfig::initializeCustomCuts() {
        // --------------------------------------------------------------------
        // 0L_SR_Resolved cutflow
        // --------------------------------------------------------------------
        CutFlow SRResolvedCutFlow("0L_SR_Resolved");

        Cut* PFlowElecCleaning = NewCut("PFlow Electron veto ", Cut::CutType::CutInt, m_doproduction);
        if (m_doPFlowElecCleaning) {
            if (!PFlowElecCleaning->initialize("DFCommonCut", "==1")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(PFlowElecCleaning);
        }
        // skimming cuts
        // --------------------------------------------------------------------
        Cut* IsMETTrigPassed = NewCut("IsMETTrigPassed", Cut::CutType::CutChar, m_doproduction);
        if (m_doMETTrigger) {
            if (!IsMETTrigPassed->initialize("IsMETTrigPassed", "==1")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(IsMETTrigPassed);
        }

        // lepton veto
        Cut* BaselineElectronVeto = NewCut("0 baseline electrons", Cut::CutType::CutInt, m_doproduction);
        if (!BaselineElectronVeto->initialize("N_BaselineElectrons", "==0")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(BaselineElectronVeto);

        Cut* BaselineMuonVeto = NewCut("0 baseline muons", Cut::CutType::CutInt, m_doproduction);
        if (!BaselineMuonVeto->initialize("N_BaselineMuons", "==0")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(BaselineMuonVeto);

        // tau veto
        Cut* TauVeto = NewCut("Tau Veto", Cut::CutType::CutInt, m_doproduction);
        if (!TauVeto->initialize("N_SignalTaus", "==0")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(TauVeto);

        // extended tau veto
        Cut* Extended_TauVeto = NewCut("Extended Tau Veto", Cut::CutType::CutInt, m_doproduction);
        if (!Extended_TauVeto->initialize("N_TausExtended_Resolved", "==0")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(Extended_TauVeto);

        // MET > 150 GeV
        Cut* MetTST150 = NewCut("MetTST>150", Cut::CutType::CutXAMPPmet, m_doproduction);
        if (!MetTST150->initialize("MetTST", ">150000.")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(MetTST150);

        // 2 or more small-R central jets
        Cut* TwoJets = NewCut(">=2 jets", Cut::CutType::CutInt,
                              m_doproduction);  // set to true that we slim the tree, or between
                                                // both cutflows will be applied
        if (!TwoJets->initialize("N_Jets04", ">=2")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(TwoJets);

        // N_BJets_04 >= 2, slimming cut
        Cut* MoreThanOneBJet = NewCut(">=2 b-tags", Cut::CutType::CutInt, m_doproduction);
        if (!MoreThanOneBJet->initialize("N_BJets_04", ">= 2")) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(MoreThanOneBJet);

        // m_jj > 40 GeV
        Cut* HiggsMassResolved = NewCut("mjj > 40", Cut::CutType::CutFloat, m_doproduction);
        if (!HiggsMassResolved->initialize("m_jj", ">40000.")) return StatusCode::FAILURE;
        // m_jj_corr > 40 GeV
        Cut* HiggsMassResolvedCorr = NewCut("mjj_corr > 40", Cut::CutType::CutFloat, m_doproduction);
        if (!HiggsMassResolvedCorr->initialize("m_jj_corr", ">40000.")) return StatusCode::FAILURE;
        // skimming cut: either m_jj or m_jj_corr (= muon-in-jet corrected value of m_jj) must be > 40 GeV
        SRResolvedCutFlow.push_back(HiggsMassResolved->combine(HiggsMassResolvedCorr, Cut::Combine::OR));

        // blinding (only applied to data, not MC), only write events with m_jj > 200 GeV
        // to ntuples. Always applied as a skimming cut, meaning that this cut does actually remove events
        // and is not only for bookkeeping purposes (otherwise it would be pointless)
        // As we are currently not sure, if m_jj should be replaced by the muon-in-jet corrected variable, the skimming is applied as an OR
        // of both
        Cut* BlindingLowResolved = NewCut("blinding: mjj<0", Cut::CutType::CutFloat, true);
        if (!BlindingLowResolved->initialize("m_jj", "<0.")) return StatusCode::FAILURE;
        Cut* BlindingLowResolvedCorr = NewCut("blinding: mjj_corr<0", Cut::CutType::CutFloat, true);
        if (!BlindingLowResolvedCorr->initialize("m_jj_corr", "<0.")) return StatusCode::FAILURE;
        Cut* BlindingLowResolvedStandardAndCorr = BlindingLowResolved->combine(BlindingLowResolvedCorr, Cut::Combine::OR);
        Cut* BlindingHighResolved = NewCut("blinding: mjj>200", Cut::CutType::CutFloat, true);
        if (!BlindingHighResolved->initialize("m_jj", ">200000.")) return StatusCode::FAILURE;
        Cut* BlindingHighResolvedCorr = NewCut("blinding: mjj_corr>200", Cut::CutType::CutFloat, true);
        if (!BlindingHighResolvedCorr->initialize("m_jj_corr", ">200000.")) return StatusCode::FAILURE;
        Cut* BlindingHighResolvedStandardAndCorr = BlindingHighResolved->combine(BlindingHighResolvedCorr, Cut::Combine::OR);
        if (m_blinding && isData())
            SRResolvedCutFlow.push_back(BlindingLowResolvedStandardAndCorr->combine(BlindingHighResolvedStandardAndCorr, Cut::Combine::OR));

        // non skimming cuts
        // --------------------------------------------------------------------

        // common cuts for resolved selection

        // Mass window used in statistical evaluation (based on m_jj histogram)
        Cut* HiggsMassWindowLowResolved = NewCut("mjj > 50", Cut::CutType::CutFloat, false);
        if (!HiggsMassWindowLowResolved->initialize("m_jj", ">50000.")) return StatusCode::FAILURE;
        Cut* HiggsMassWindowHighResolved = NewCut("mjj < 280", Cut::CutType::CutFloat, false);
        if (!HiggsMassWindowHighResolved->initialize("m_jj", "<280000.")) return StatusCode::FAILURE;
        Cut* HiggsMassWindowResolved = HiggsMassWindowLowResolved->combine(HiggsMassWindowHighResolved, Cut::Combine::AND);
        SRResolvedCutFlow.push_back(HiggsMassWindowResolved);

        // anti-QCD cut
        Cut* DeltaPhiMin = NewCut("|DeltaPhiMin3|>20deg", Cut::CutType::CutFloat, m_doFullSkimming);
        if (!DeltaPhiMin->initialize("AbsDeltaPhiMin3", Form(">=%f", TMath::Pi() * 1. / 9.))) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(DeltaPhiMin);

        // cut on object based met significance ro further reduce QCD background
        Cut* METSig12 = NewCut("METSig>12", Cut::CutType::CutFloat, m_doFullSkimming);
        if (!METSig12->initialize("MetTST_Significance_noPUJets_noSoftTerm", Form(">=%f", 12.))) return StatusCode::FAILURE;
        SRResolvedCutFlow.push_back(METSig12);

        // -------------------------------
        // Definition of cuts used in CONF
        // Note: The cuts are only defined here so that the cuts can be accessed by the CR cutflows, too.
        // But they are initialized and added to the cutflow only if the flag doCONFselection is set to true
        // -------------------------------
        Cut* Jet1Pt = NewCut("JetBLight1BPt > 45", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* Jet2Pt = NewCut("JetBLight2BPt > 45", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* JetPt;
        Cut* DeltaRJJ = NewCut("DeltaRjj", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* bJetVeto = NewCut("bJetVeto", Cut::CutType::CutInt, m_doFullSkimming);
        Cut* HtCut = NewCut("HtCut", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* N_TwoJets = NewCut("N_Jets=2", Cut::CutType::CutInt, m_doFullSkimming);
        Cut* N_SigSumTwoJets = NewCut("sigjet012ptsum>120", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* N_SigSum120;
        Cut* MoreJets = NewCut("N_Jets>2", Cut::CutType::CutInt, m_doFullSkimming);
        Cut* N_SigSumMoreJets = NewCut("sigjet012ptsum>150", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* N_SigSum150;
        Cut* AbsDeltaPhiJJ = NewCut("|DeltaPhiJJ|<140deg", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* AbsDeltaPhiMetJJ = NewCut("|DeltaPhiMetJJ|>120deg", Cut::CutType::CutFloat, m_doFullSkimming);

        // --------------------------------------
        // Definition of new event selection cuts
        // Note: The cuts are only defined here so that the cuts can be accessed by the CR cutflows, too.
        // But they are initialized and added to the cutflow only if the flag doCONFselection is set to false (default)
        // --------------------------------------
        Cut* ptjj100 = NewCut("pt_jj > 100000", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* ptjjcorr100 = NewCut("pt_jj_corr > 100000", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* mT_METclosestBJet = NewCut("mT_METclosestBJet > 170000", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* mT_METfurthestBJet = NewCut("mT_METfurthestBJet > 200000", Cut::CutType::CutFloat, m_doFullSkimming);

        // initialize and add the CONF cuts only when flag doCONFselection is enabled
        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            // Either one of the two Higgs candidate jets should have pT > 45 GeV
            if (!Jet1Pt->initialize("Jet_BLight1BPt", ">=45000.")) return StatusCode::FAILURE;
            if (!Jet2Pt->initialize("Jet_BLight2BPt", ">=45000.")) return StatusCode::FAILURE;
            JetPt = Jet1Pt->combine(Jet2Pt, Cut::Combine::OR);
            SRResolvedCutFlow.push_back(JetPt);

            // dRjj < 1.8 cut (jets of dijet system need to be pointed somewhat in the
            // same direction)
            if (!DeltaRJJ->initialize("DeltaRJJ", "<1.8 ")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(DeltaRJJ);

            // bJetVeto: reject events with more than 2 central b-jets -> check if they
            // are central
            if (!bJetVeto->initialize("N_BJets_04", "<=2")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(bJetVeto);

            // HtRatio cut: The leading three jets should carry the largest fraction of the total pT sum in the event
            if (!HtCut->initialize("HtRatioResolved", "<=0.37")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(HtCut);

            // jet pt sum cut
            if (!N_TwoJets->initialize("N_Jets04", "==2")) return StatusCode::FAILURE;
            if (!N_SigSumTwoJets->initialize("sigjet012ptsum", ">120000.")) return StatusCode::FAILURE;
            N_SigSum120 = N_SigSumTwoJets->combine(N_TwoJets, Cut::Combine::AND);
            if (!MoreJets->initialize("N_Jets04", "> 2")) return StatusCode::FAILURE;
            if (!N_SigSumMoreJets->initialize("sigjet012ptsum", ">150000.")) return StatusCode::FAILURE;
            N_SigSum150 = N_SigSumMoreJets->combine(MoreJets, Cut::Combine::AND);
            // N_SigSum150->SetName("sigjet012ptsum>120(150)");
            SRResolvedCutFlow.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));

            // anti-QCD cuts
            if (!AbsDeltaPhiJJ->initialize("AbsDeltaPhiJJ", Form("<=%f", TMath::Pi() * 7. / 9.))) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(AbsDeltaPhiJJ);

            if (!AbsDeltaPhiMetJJ->initialize("AbsDeltaPhiMetJJ", Form(">=%f", TMath::Pi() * 2. / 3.))) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(AbsDeltaPhiMetJJ);
        } else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            // pt_jj > 100 GeV; note that for MET > 350 GeV a cut of pt_jj > 300 GeV should be used
            // --> this cut of pt_jj > 300 GeV is considered only in XAMPPplotting
            if (!ptjj100->initialize("pt_jj", ">100000.")) return StatusCode::FAILURE;
            if (!ptjjcorr100->initialize("pt_jj_corr", ">100000.")) return StatusCode::FAILURE;
            // skimming cut: either pt_jj or pt_jj_corr (= muon-in-jet corrected pt_jj value) must be > 100 GeV
            SRResolvedCutFlow.push_back(ptjj100->combine(ptjjcorr100, Cut::Combine::OR));

            // ttbar reduction variables mT(MET, closest b-jet) and mT(MET, furthest b-jet)
            if (!mT_METclosestBJet->initialize("mT_METclosestBJet", ">170000.")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(mT_METclosestBJet);

            if (!mT_METfurthestBJet->initialize("mT_METfurthestBJet", ">200000.")) return StatusCode::FAILURE;
            SRResolvedCutFlow.push_back(mT_METfurthestBJet);
        }

        ATH_CHECK(AddToCutFlows(SRResolvedCutFlow));

        // --------------------------------------------------------------------
        // 0L_SR_Merged CutFlow
        // --------------------------------------------------------------------
        CutFlow SRMergedCutFlow("0L_SR_Merged");

        // only when we are running with PFLow jets
        if (m_doPFlowElecCleaning) SRMergedCutFlow.push_back(PFlowElecCleaning);

        // skimming cuts
        // --------------------------------------------------------------------
        if (m_doMETTrigger) { SRMergedCutFlow.push_back(IsMETTrigPassed); }
        SRMergedCutFlow.push_back(BaselineElectronVeto);
        SRMergedCutFlow.push_back(BaselineMuonVeto);
        SRMergedCutFlow.push_back(TauVeto);

        // extended tau veto merged
        Cut* Extended_TauVeto_Merged = NewCut("Extended Tau Veto", Cut::CutType::CutInt, m_doproduction);
        if (!Extended_TauVeto_Merged->initialize("N_TausExtended_Merged", "==0")) return StatusCode::FAILURE;
        SRMergedCutFlow.push_back(Extended_TauVeto_Merged);

        // MET > 150 GeV
        SRMergedCutFlow.push_back(MetTST150);

        // At least one large-R jet
        Cut* OneFatJet = NewCut(">=1 fat-jets", Cut::CutType::CutInt,
                                m_doproduction);  // set to true that we slim the tree, or between
                                                  // both cutflows will be applied
        if (!OneFatJet->initialize("N_Jets10", ">=1")) return StatusCode::FAILURE;
        SRMergedCutFlow.push_back(OneFatJet);

        // At least two associated track jets
        Cut* N_associated_trkJets_geq2 = NewCut("N_associated_trkJets>=2", Cut::CutType::CutInt, m_doFullSkimming);
        if (!N_associated_trkJets_geq2->initialize("N_associated_Jets02", ">=2")) return StatusCode::FAILURE;
        // SRMergedCutFlow.push_back(N_associated_trkJets_geq2);

        // The leading two track jet should pass the OR
        Cut* TrackJet_1passOR = NewCut("TrackJet_1passOR = true", Cut::CutType::CutBool, m_doproduction);
        if (!TrackJet_1passOR->initialize("TrackJet_1passOR", "== true")) return StatusCode::FAILURE;
        Cut* TrackJet_2passOR = NewCut("TrackJet_2passOR = true", Cut::CutType::CutBool, m_doproduction);
        if (!TrackJet_2passOR->initialize("TrackJet_2passOR", "== true")) return StatusCode::FAILURE;
        Cut* TrackJet_passOR = TrackJet_1passOR->combine(TrackJet_2passOR, Cut::Combine::AND);
        // SRMergedCutFlow.push_back(TrackJet_passOR);

        // At least two b-tagged associated track jet OR large R jet passes loosest Xbb score for 25% anti topness
        Cut* NbtaggedTrkJets_geq2 = NewCut(">= 2 b-tagged track jets", Cut::CutType::CutInt, m_doproduction);
        if (!NbtaggedTrkJets_geq2->initialize("N_BTags_associated_02", ">= 2")) return StatusCode::FAILURE;

        Cut* trkJet_cuts=N_associated_trkJets_geq2->combine(TrackJet_passOR, Cut::Combine::AND)->combine(NbtaggedTrkJets_geq2, Cut::Combine::AND);

        Cut* largeR_Xbb_AT25_80WP = NewCut("Large R Xbb AT25 80WP tagged", Cut::CutType::CutFloat, m_doproduction);
        if (!largeR_Xbb_AT25_80WP->initialize("NNXbb_CombScore_25", ">= 1.2")) return StatusCode::FAILURE;

        Cut* largeR_btagging = trkJet_cuts->combine(largeR_Xbb_AT25_80WP, Cut::Combine::OR);

        SRMergedCutFlow.push_back(largeR_btagging);

        // mJ > 40 GeV
        Cut* HiggsMassMerged = NewCut("mJ > 40", Cut::CutType::CutFloat, false);
        if (!HiggsMassMerged->initialize("m_J", ">40000.")) return StatusCode::FAILURE;
        // mJ_corr > 40 GeV
        Cut* HiggsMassMergedCorr = NewCut("mJ_corr > 40", Cut::CutType::CutFloat, false);
        if (!HiggsMassMergedCorr->initialize("m_J_corr", ">40000.")) return StatusCode::FAILURE;
        // skimming cut: either m_J or m_J_corr must be > 40 GeV
        SRMergedCutFlow.push_back(HiggsMassMerged->combine(HiggsMassMergedCorr, Cut::Combine::OR));

        // blinding (only applied to data, not MC), only write events with m_J > 200 GeV
        // to ntuples. Always applied as a skimming cut, meaning that this cut does actually remove events
        // and is not only for bookkeeping purposes (otherwise it would be pointless)
        // As in the resolved region, the blinding is applied as on OR of the standard m_J cuts and the mij cuts
        Cut* BlindingLowMerged = NewCut("blinding: mJ<0", Cut::CutType::CutFloat, true);
        if (!BlindingLowMerged->initialize("m_J", "<0.")) return StatusCode::FAILURE;
        Cut* BlindingLowMergedCorr = NewCut("blinding: mJ_corr<0", Cut::CutType::CutFloat, true);
        if (!BlindingLowMergedCorr->initialize("m_J_corr", "<0.")) return StatusCode::FAILURE;
        Cut* BlindingLowMergedStandardAndCorr = BlindingLowMerged->combine(BlindingLowMergedCorr, Cut::Combine::OR);
        Cut* BlindingHighMerged = NewCut("blinding: mJ>200", Cut::CutType::CutFloat, true);
        if (!BlindingHighMerged->initialize("m_J", ">200000.")) return StatusCode::FAILURE;
        Cut* BlindingHighMergedCorr = NewCut("blinding: mJ_corr>200", Cut::CutType::CutFloat, true);
        if (!BlindingHighMergedCorr->initialize("m_J_corr", ">200000.")) return StatusCode::FAILURE;
        Cut* BlindingHighMergedStandardAndCorr = BlindingHighMerged->combine(BlindingHighMergedCorr, Cut::Combine::OR);
        if (m_blinding && isData())
            SRMergedCutFlow.push_back(BlindingLowMergedStandardAndCorr->combine(BlindingHighMergedStandardAndCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        // Mass window used in statistical evaluation (based on m_J histogram)
        Cut* HiggsMassWindowLowMerged = NewCut("mJ_corr> 50", Cut::CutType::CutFloat, false);
        if (!HiggsMassWindowLowMerged->initialize("m_J_corr", ">50000.")) return StatusCode::FAILURE;
        Cut* HiggsMassWindowHighMerged = NewCut("mJ_corr < 270", Cut::CutType::CutFloat, false);
        if (!HiggsMassWindowHighMerged->initialize("m_J_corr", "<270000.")) return StatusCode::FAILURE;
        Cut* HiggsMassWindowMerged = HiggsMassWindowLowMerged->combine(HiggsMassWindowHighMerged, Cut::Combine::AND);
        SRMergedCutFlow.push_back(HiggsMassWindowMerged);

        // common merged event selection cuts

        // common merged anti-QCD cuts
        SRMergedCutFlow.push_back(DeltaPhiMin);

        // -------------------------------------------------------------
        // Define b-jet veto and HTcut - only used in the CONF selection
        // -------------------------------------------------------------
        Cut* BJetVeto_trkJets = NewCut("BJetVeto", Cut::CutType::CutInt, m_doFullSkimming);
        Cut* HtRatioCut_merged = NewCut("HtCut", Cut::CutType::CutFloat, m_doFullSkimming);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            // bJet veto merged -> no b-tagged track jets outside dR=1.0 wrt leading fat
            // jet
            if (!BJetVeto_trkJets->initialize("N_BTags_not_associated_02", "==0")) return StatusCode::FAILURE;
            SRMergedCutFlow.push_back(BJetVeto_trkJets);

            // HTcut - Leading large-R jet should carry largest amount of total pT sum in the event
            if (!HtRatioCut_merged->initialize("HtRatioMerged", "<=0.57")) return StatusCode::FAILURE;
            SRMergedCutFlow.push_back(HtRatioCut_merged);
        }

        ATH_CHECK(AddToCutFlows(SRMergedCutFlow));

        // --------------------------------------------------------------------
        // 1L_CR_Resolved cut flow
        // --------------------------------------------------------------------
        CutFlow CR1ResolvedCutFlow("1L_CR_Resolved");

        if (m_doPFlowElecCleaning) CR1ResolvedCutFlow.push_back(PFlowElecCleaning);

        // skimming cuts
        // --------------------------------------------------------------------
        Cut* IsSingleMuonTrigPassed = NewCut("IsSingleMuonTrigPassed", Cut::CutType::CutChar, m_doproduction);
        Cut* IsSingleMuonTrigMatched = NewCut("IsSingleMuonTrigMatched", Cut::CutType::CutChar, m_doproduction);
        if (m_doMETTrigger && !(m_doMETTriggerCalibration)) { CR1ResolvedCutFlow.push_back(IsMETTrigPassed); }
        if (m_doSingleMuonTrigger) {
            if (!IsSingleMuonTrigPassed->initialize("IsSingleMuonTrigPassed", "==1")) return StatusCode::FAILURE;
            if (!IsSingleMuonTrigMatched->initialize("IsSingleMuonTrigMatched", "==1")) return StatusCode::FAILURE;
            if (m_doMETTriggerCalibration) CR1ResolvedCutFlow.push_back(IsSingleMuonTrigPassed);
            if (m_doMETTriggerCalibration) CR1ResolvedCutFlow.push_back(IsSingleMuonTrigMatched);
        }

        // Veto on electrons
        CR1ResolvedCutFlow.push_back(BaselineElectronVeto);

        // Exactly one muon
        Cut* OneBaselineMuon = NewCut("1 baseline muon", Cut::CutType::CutInt, m_doproduction);
        if (!OneBaselineMuon->initialize("N_BaselineMuons", "==1")) return StatusCode::FAILURE;
        Cut* OneSignalMuonPt7 = NewCut("1 signal muon", Cut::CutType::CutInt, m_doproduction);
        if (!OneSignalMuonPt7->initialize("N_SignalMuons", "==1")) return StatusCode::FAILURE;
        Cut* LeadingMuonPt25 = NewCut("pt_mu1 > 25 GeV", Cut::CutType::CutFloat, m_doproduction);
        if (!LeadingMuonPt25->initialize("pt_mu1", ">=25000.")) return StatusCode::FAILURE;
        Cut* OneSignalMuon = OneSignalMuonPt7->combine(LeadingMuonPt25, Cut::Combine::AND);
        Cut* OneMuon = OneSignalMuon->combine(OneBaselineMuon, Cut::Combine::AND);
        CR1ResolvedCutFlow.push_back(OneMuon);

        // After lepton cuts all other cuts are the same as in the 0lep channel

        // tau veto
        CR1ResolvedCutFlow.push_back(TauVeto);
        CR1ResolvedCutFlow.push_back(Extended_TauVeto);

        // METlepInvis > 150 GeV, i.e. MET with muon set as invisible in MET calculation
        Cut* MetTST150lepInvis = NewCut("MetTSTlepInvis>150", Cut::CutType::CutXAMPPmet, m_doproduction);
        if (!MetTST150lepInvis->initialize("MetTSTlepInvis", ">150000.")) return StatusCode::FAILURE;
        CR1ResolvedCutFlow.push_back(MetTST150lepInvis);

        CR1ResolvedCutFlow.push_back(TwoJets);
        CR1ResolvedCutFlow.push_back(MoreThanOneBJet);
        CR1ResolvedCutFlow.push_back(HiggsMassResolved->combine(HiggsMassResolvedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------
        CR1ResolvedCutFlow.push_back(HiggsMassWindowResolved);
        // anti-QCD cut for resolved selection
        CR1ResolvedCutFlow.push_back(DeltaPhiMin);

        // Object based METlepInvis cut
        Cut* METSig12_lepInvis = NewCut("METSiglepInvis>12", Cut::CutType::CutFloat, m_doFullSkimming);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            CR1ResolvedCutFlow.push_back(JetPt);
            CR1ResolvedCutFlow.push_back(DeltaRJJ);
            CR1ResolvedCutFlow.push_back(bJetVeto);  // bJetVeto: reject events with more than 2 central b-jets
                                                     // -> check if they are central
            CR1ResolvedCutFlow.push_back(HtCut);
            CR1ResolvedCutFlow.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));
            CR1ResolvedCutFlow.push_back(AbsDeltaPhiJJ);
            CR1ResolvedCutFlow.push_back(AbsDeltaPhiMetJJ);
        }

        else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            if (!METSig12_lepInvis->initialize("MetTST_Significance_noPUJets_noSoftTerm_lepInvis", Form(">=%f", 12.)))
                return StatusCode::FAILURE;
            CR1ResolvedCutFlow.push_back(METSig12_lepInvis);
            CR1ResolvedCutFlow.push_back(ptjj100->combine(ptjjcorr100, Cut::Combine::OR));
            CR1ResolvedCutFlow.push_back(mT_METclosestBJet);
            CR1ResolvedCutFlow.push_back(mT_METfurthestBJet);
        }

        ATH_CHECK(AddToCutFlows(CR1ResolvedCutFlow));

        // --------------------------------------------------------------------
        // 1L_CR_Merged cut flow
        // --------------------------------------------------------------------

        CutFlow CR1MergedCutFlow("1L_CR_Merged");
        if (m_doPFlowElecCleaning) CR1MergedCutFlow.push_back(PFlowElecCleaning);

        // skimming cuts
        // --------------------------------------------------------------------
        if (m_doMETTrigger && !(m_doMETTriggerCalibration)) { CR1MergedCutFlow.push_back(IsMETTrigPassed); }
        if (m_doSingleMuonTrigger && m_doMETTriggerCalibration) { CR1MergedCutFlow.push_back(IsSingleMuonTrigPassed); }
        CR1MergedCutFlow.push_back(BaselineElectronVeto);
        CR1MergedCutFlow.push_back(OneMuon);
        CR1MergedCutFlow.push_back(TauVeto);
        CR1MergedCutFlow.push_back(Extended_TauVeto_Merged);
        CR1MergedCutFlow.push_back(MetTST150lepInvis);
        CR1MergedCutFlow.push_back(OneFatJet);
        CR1MergedCutFlow.push_back(largeR_btagging);
        CR1MergedCutFlow.push_back(HiggsMassMerged->combine(HiggsMassMergedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        CR1MergedCutFlow.push_back(HiggsMassWindowMerged);

        // TODO: Check if we need that anti-QCD cut in the merged region
        CR1MergedCutFlow.push_back(DeltaPhiMin);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            // bJet veto merged -> no b-tagged track jets outside dR=1.0 wrt leading fat
            // jet
            CR1MergedCutFlow.push_back(BJetVeto_trkJets);
            // HTcut
            CR1MergedCutFlow.push_back(HtRatioCut_merged);
        }

        ATH_CHECK(AddToCutFlows(CR1MergedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_Ele_Resolved cut flow
        // --------------------------------------------------------------------

        CutFlow CR2EleResolvedCutFlow("2L_CR_Ele_Resolved");

        if (m_doPFlowElecCleaning) CR2EleResolvedCutFlow.push_back(PFlowElecCleaning);
        // skimming cuts
        // --------------------------------------------------------------------
        // Require single electron trigger
        Cut* IsSingleElecTrigPassed = NewCut("IsSingleElecTrigPassed", Cut::CutType::CutChar, m_doproduction);
        Cut* IsSingleElecTrigMatched = NewCut("IsSingleElecTrigMatched", Cut::CutType::CutChar, m_doproduction);
        if (m_doSingleElecTrigger) {
            if (!IsSingleElecTrigPassed->initialize("IsSingleElecTrigPassed", "==1")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(IsSingleElecTrigPassed);
            if (!IsSingleElecTrigMatched->initialize("IsSingleElecTrigMatched", "==1")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(IsSingleElecTrigMatched);
        }

        // 2 signal electrons with leading electron pt > 27 GeV
        Cut* TwoBaselineElectrons = NewCut("== 2 baseline electrons", Cut::CutType::CutInt, m_doproduction);
        if (!TwoBaselineElectrons->initialize("N_BaselineElectrons", "==2")) return StatusCode::FAILURE;
        Cut* TwoSignalElectrons = NewCut("== 2 signal electrons", Cut::CutType::CutInt, m_doproduction);
        if (!TwoSignalElectrons->initialize("N_SignalElectrons", "==2")) return StatusCode::FAILURE;
        Cut* LeadingElectronPt27 = NewCut("pt_e1 > 27 GeV", Cut::CutType::CutFloat, m_doproduction);
        if (!LeadingElectronPt27->initialize("pt_e1", ">=27000.")) return StatusCode::FAILURE;
        Cut* TwoElectrons = TwoBaselineElectrons->combine(TwoSignalElectrons, Cut::Combine::AND)
                                ->combine(LeadingElectronPt27, Cut::Combine::AND)
                                ->combine(BaselineMuonVeto, Cut::Combine::AND);
        CR2EleResolvedCutFlow.push_back(TwoElectrons);

        // baseline muon veto
        CR2EleResolvedCutFlow.push_back(BaselineMuonVeto);

        // tau veto
        CR2EleResolvedCutFlow.push_back(TauVeto);
        CR2EleResolvedCutFlow.push_back(Extended_TauVeto);

        CR2EleResolvedCutFlow.push_back(TwoJets);
        CR2EleResolvedCutFlow.push_back(MoreThanOneBJet);
        CR2EleResolvedCutFlow.push_back(HiggsMassResolved->combine(HiggsMassResolvedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------
        CR2EleResolvedCutFlow.push_back(HiggsMassWindowResolved);

        // -------------------------------------------
        // Definition of 2-lepton CR cuts used in CONF
        // -------------------------------------------
        Cut* pt_ll_gt150 = NewCut("pt_ll_gt150", Cut::CutType::CutFloat, m_doproduction);
        Cut* pt_ll_lt500 = NewCut("pt_ll_lt500", Cut::CutType::CutFloat, m_doproduction);
        Cut* m_ll_gt83 = NewCut("m_ll>=83", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_ll_lt99 = NewCut("m_ll<=99", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_eeWindow;
        Cut* m_ll_gt71 = NewCut("m_ll>=71", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_ll_lt106 = NewCut("m_ll<=106", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_mumuWindow;
        Cut* METSignificance_lt35 = NewCut("METsig_lt35", Cut::CutType::CutFloat, m_doFullSkimming);

        // ----------------------------------
        // Definition of new 2-lepton CR cuts
        // ----------------------------------
        Cut* m_ll_gt82 = NewCut("m_ll>=82", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_ll_lt102 = NewCut("m_ll<=102", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* m_ZWindow;
        Cut* METSigLower5 = NewCut("METSig<=5", Cut::CutType::CutFloat, m_doFullSkimming);
        Cut* oppositeCharge = NewCut("opposite charge", Cut::CutType::CutBool, m_doFullSkimming);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            if (!pt_ll_gt150->initialize("pt_ll", ">=150000.")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(pt_ll_gt150);
            if (!pt_ll_lt500->initialize("pt_ll", "<=500000.")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(pt_ll_lt500);
            // dielectron invariant mass window around Z mass
            if (!m_ll_gt83->initialize("m_ll", ">=83000.")) return StatusCode::FAILURE;
            if (!m_ll_lt99->initialize("m_ll", "<=99000.")) return StatusCode::FAILURE;
            m_eeWindow = m_ll_gt83->combine(m_ll_lt99, Cut::Combine::AND);
            CR2EleResolvedCutFlow.push_back(m_eeWindow);

            // MET significance cut
            if (!METSignificance_lt35->initialize("METsig", "<=3.5")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(METSignificance_lt35);

            // common resolved event selection cuts
            CR2EleResolvedCutFlow.push_back(JetPt);
            CR2EleResolvedCutFlow.push_back(DeltaRJJ);
            CR2EleResolvedCutFlow.push_back(bJetVeto);  // bJetVeto: reject events with more than 2 central b-jets
                                                        // -> check if they are central
            CR2EleResolvedCutFlow.push_back(HtCut);
        } else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            // MetTSTlepInvis > 150 GeV
            CR2EleResolvedCutFlow.push_back(MetTST150lepInvis);

            if (!oppositeCharge->initialize("isOppositeCharge", "==true")) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(oppositeCharge);
            // dielectron invariant mass window around Z mass window: 91.1876 GeV +- 10 GeV
            if (!m_ll_gt82->initialize("m_ll", ">=81187.6")) return StatusCode::FAILURE;
            if (!m_ll_lt102->initialize("m_ll", "<=101187.6.")) return StatusCode::FAILURE;
            m_ZWindow = m_ll_gt82->combine(m_ll_lt102, Cut::Combine::AND);
            CR2EleResolvedCutFlow.push_back(m_ZWindow);

            CR2EleResolvedCutFlow.push_back(ptjj100->combine(ptjjcorr100, Cut::Combine::OR));
            CR2EleResolvedCutFlow.push_back(DeltaPhiMin);
            CR2EleResolvedCutFlow.push_back(METSig12_lepInvis);
            CR2EleResolvedCutFlow.push_back(mT_METclosestBJet);
            CR2EleResolvedCutFlow.push_back(mT_METfurthestBJet);

            // Object-based METlepInvis cut applied to reduce ttbar background
            if (!METSigLower5->initialize("MetTST_Significance_noPUJets_noSoftTerm", Form("<=%f", 5.))) return StatusCode::FAILURE;
            CR2EleResolvedCutFlow.push_back(METSigLower5);
        }

        ATH_CHECK(AddToCutFlows(CR2EleResolvedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_Muo_Resolved cut flow
        // --------------------------------------------------------------------

        CutFlow CR2MuoResolvedCutFlow("2L_CR_Muo_Resolved");

        if (m_doPFlowElecCleaning) CR2MuoResolvedCutFlow.push_back(PFlowElecCleaning);
        // skimming cuts
        // --------------------------------------------------------------------
        // lowest unprescaled single muon trigger (already defined in 1 muon control region)
        if (m_doSingleMuonTrigger) {
            CR2MuoResolvedCutFlow.push_back(IsSingleMuonTrigPassed);
            CR2MuoResolvedCutFlow.push_back(IsSingleMuonTrigMatched);
        }

        // baseline electron veto
        CR2MuoResolvedCutFlow.push_back(BaselineElectronVeto);

        // 2 signal muons with opposite charge and with leading signal muon pt > 25 GeV
        Cut* TwoBaselineMuons = NewCut("== 2 baseline muons", Cut::CutType::CutInt, m_doproduction);
        if (!TwoBaselineMuons->initialize("N_BaselineMuons", "==2")) return StatusCode::FAILURE;
        Cut* TwoSignalMuons = NewCut("== 2 signal muons", Cut::CutType::CutInt, m_doproduction);
        if (!TwoSignalMuons->initialize("N_SignalMuons", "==2")) return StatusCode::FAILURE;
        // LeadingMuonPt25 already defined in 1 lepton CR
        Cut* TwoMuons = TwoBaselineMuons->combine(TwoSignalMuons, Cut::Combine::AND)
                            ->combine(LeadingMuonPt25, Cut::Combine::AND)
                            ->combine(BaselineElectronVeto, Cut::Combine::AND);
        CR2MuoResolvedCutFlow.push_back(TwoMuons);

        // tau veto
        CR2MuoResolvedCutFlow.push_back(TauVeto);
        CR2MuoResolvedCutFlow.push_back(Extended_TauVeto);

        // 2 or more small-R jets
        CR2MuoResolvedCutFlow.push_back(TwoJets);

        // at least 1 b-jet
        CR2MuoResolvedCutFlow.push_back(MoreThanOneBJet);

        // m_jj > 40 GeV
        CR2MuoResolvedCutFlow.push_back(HiggsMassResolved->combine(HiggsMassResolvedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        CR2MuoResolvedCutFlow.push_back(HiggsMassWindowResolved);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            CR2MuoResolvedCutFlow.push_back(pt_ll_gt150);
            CR2MuoResolvedCutFlow.push_back(pt_ll_lt500);
            if (!oppositeCharge->initialize("isOppositeCharge", "==true")) return StatusCode::FAILURE;
            CR2MuoResolvedCutFlow.push_back(oppositeCharge);
            // dimuon invariant mass window around Z mass
            if (!m_ll_gt71->initialize("m_ll", ">=71000.")) return StatusCode::FAILURE;
            if (!m_ll_lt106->initialize("m_ll", "<=106000.")) return StatusCode::FAILURE;
            m_mumuWindow = m_ll_gt71->combine(m_ll_lt106, Cut::Combine::AND);
            CR2MuoResolvedCutFlow.push_back(m_mumuWindow);

            // MET significance cut
            CR2MuoResolvedCutFlow.push_back(METSignificance_lt35);

            // common resolved event selection cuts
            CR2MuoResolvedCutFlow.push_back(JetPt);
            CR2MuoResolvedCutFlow.push_back(DeltaRJJ);
            CR2MuoResolvedCutFlow.push_back(bJetVeto);  // bJetVeto: reject events with more than 2 central b-jets
                                                        // -> check if they are central
            CR2MuoResolvedCutFlow.push_back(HtCut);
        } else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            CR2MuoResolvedCutFlow.push_back(MetTST150lepInvis);
            CR2MuoResolvedCutFlow.push_back(oppositeCharge);
            // dimuon invariant mass window +- 10 GeV around Z mass (91.1876 GeV)
            CR2MuoResolvedCutFlow.push_back(m_ZWindow);

            CR2MuoResolvedCutFlow.push_back(ptjj100->combine(ptjjcorr100, Cut::Combine::OR));
            CR2MuoResolvedCutFlow.push_back(DeltaPhiMin);
            CR2MuoResolvedCutFlow.push_back(METSig12_lepInvis);
            CR2MuoResolvedCutFlow.push_back(mT_METclosestBJet);
            CR2MuoResolvedCutFlow.push_back(mT_METfurthestBJet);

            // Object based METlepInvis cut applied to reduce ttbar background
            // Maybe value will be changed to 6
            CR2MuoResolvedCutFlow.push_back(METSigLower5);
        }

        ATH_CHECK(AddToCutFlows(CR2MuoResolvedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_Ele_Merged cut flow
        // --------------------------------------------------------------------

        CutFlow CR2EleMergedCutFlow("2L_CR_Ele_Merged");
        if (m_doPFlowElecCleaning) CR2EleMergedCutFlow.push_back(PFlowElecCleaning);
        // skimming cuts
        // --------------------------------------------------------------------
        if (m_doSingleElecTrigger) {
            CR2EleMergedCutFlow.push_back(IsSingleElecTrigPassed);
            CR2EleMergedCutFlow.push_back(IsSingleElecTrigMatched);
        }
        CR2EleMergedCutFlow.push_back(TwoElectrons);
        CR2EleMergedCutFlow.push_back(BaselineMuonVeto);
        CR2EleMergedCutFlow.push_back(TauVeto);
        CR2EleMergedCutFlow.push_back(Extended_TauVeto_Merged);
        // METlepInvis > 150 GeV, i.e. MET with either mumu or ee pair set as invisible in MET calculation
        CR2EleMergedCutFlow.push_back(MetTST150lepInvis);
        CR2EleMergedCutFlow.push_back(OneFatJet);
        // at least 1 b-tagged track jets
        CR2EleMergedCutFlow.push_back(largeR_btagging);
        CR2EleMergedCutFlow.push_back(HiggsMassMerged->combine(HiggsMassMergedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        CR2EleMergedCutFlow.push_back(HiggsMassWindowMerged);


        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            CR2EleMergedCutFlow.push_back(m_eeWindow);
            CR2EleMergedCutFlow.push_back(BJetVeto_trkJets);
            CR2EleMergedCutFlow.push_back(HtRatioCut_merged);
        } else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            CR2EleMergedCutFlow.push_back(oppositeCharge);
            CR2EleMergedCutFlow.push_back(m_ZWindow);
            CR2EleMergedCutFlow.push_back(DeltaPhiMin);
        }

        ATH_CHECK(AddToCutFlows(CR2EleMergedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_Muo_Merged cut flow
        // --------------------------------------------------------------------

        CutFlow CR2MuoMergedCutFlow("2L_CR_Muo_Merged");
        if (m_doPFlowElecCleaning) CR2MuoMergedCutFlow.push_back(PFlowElecCleaning);

        // skimming cuts
        // --------------------------------------------------------------------
        if (m_doSingleMuonTrigger) {
            CR2MuoMergedCutFlow.push_back(IsSingleMuonTrigPassed);
            CR2MuoMergedCutFlow.push_back(IsSingleMuonTrigMatched);
        }
        CR2MuoMergedCutFlow.push_back(BaselineElectronVeto);
        CR2MuoMergedCutFlow.push_back(TwoMuons);
        CR2MuoMergedCutFlow.push_back(TauVeto);
        CR2MuoMergedCutFlow.push_back(Extended_TauVeto_Merged);
        // METlepInvis > 150 GeV, i.e. MET with either mumu or ee pair set as invisible in MET calculation
        CR2MuoMergedCutFlow.push_back(MetTST150lepInvis);
        CR2MuoMergedCutFlow.push_back(OneFatJet);
        CR2MuoMergedCutFlow.push_back(largeR_btagging);
        CR2MuoMergedCutFlow.push_back(HiggsMassMerged->combine(HiggsMassMergedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        CR2MuoMergedCutFlow.push_back(HiggsMassWindowMerged);


        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            CR2MuoMergedCutFlow.push_back(oppositeCharge);
            CR2MuoMergedCutFlow.push_back(m_mumuWindow);
            CR2MuoMergedCutFlow.push_back(BJetVeto_trkJets);
            CR2MuoMergedCutFlow.push_back(HtRatioCut_merged);
        } else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            CR2MuoMergedCutFlow.push_back(oppositeCharge);
            CR2MuoMergedCutFlow.push_back(m_ZWindow);
            CR2MuoMergedCutFlow.push_back(DeltaPhiMin);
        }

        ATH_CHECK(AddToCutFlows(CR2MuoMergedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_EMu_Resolved cut flow
        // --------------------------------------------------------------------

        CutFlow CREMuResolvedCutFlow("2L_CR_EMu_Resolved");

        // skimming cuts
        // --------------------------------------------------------------------
        // If we use PFlow, we clean PFlow :)
        if (m_doPFlowElecCleaning) CREMuResolvedCutFlow.push_back(PFlowElecCleaning);
        CREMuResolvedCutFlow.push_back(IsMETTrigPassed);

        // Use the same muon selection as the 1L CR
        CREMuResolvedCutFlow.push_back(OneMuon);

        // Prepare the same electron selection as the 2L CR, just require only
        // one of them
        Cut* OneBaselineElectron = NewCut("== 1 baseline electron", Cut::CutType::CutInt, m_doproduction);
        if (!OneBaselineElectron->initialize("N_BaselineElectrons", "==1")) return StatusCode::FAILURE;
        Cut* OneSignalElectron = NewCut("== 1 signal electron", Cut::CutType::CutInt, m_doproduction);
        if (!OneSignalElectron->initialize("N_SignalElectrons", "==1")) return StatusCode::FAILURE;
        Cut* OneElectron =
            OneBaselineElectron->combine(OneSignalElectron, Cut::Combine::AND)->combine(LeadingElectronPt27, Cut::Combine::AND);
        CREMuResolvedCutFlow.push_back(OneElectron);
        CREMuResolvedCutFlow.push_back(oppositeCharge);

        // From here, basically copy the 1L region
        // Just imagine if there were just a vector of these cuts to extend with
        // ;) Maybe that can be a QoL TODO item
        //
        // tau veto
        CREMuResolvedCutFlow.push_back(TauVeto);
        CREMuResolvedCutFlow.push_back(Extended_TauVeto);

        // We can't necessarily say which MET value we're cutting on here, but
        // we're definitely cutting on one of them... :)
        CREMuResolvedCutFlow.push_back(MetTST150lepInvis);
        CREMuResolvedCutFlow.push_back(TwoJets);
        // This variable now cuts on >= 2 b-jets
        CREMuResolvedCutFlow.push_back(MoreThanOneBJet);
        CREMuResolvedCutFlow.push_back(HiggsMassResolved->combine(HiggsMassResolvedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        CREMuResolvedCutFlow.push_back(HiggsMassWindowResolved);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            // Common cuts for resolved selection
            CREMuResolvedCutFlow.push_back(JetPt);
            CREMuResolvedCutFlow.push_back(DeltaRJJ);
            CREMuResolvedCutFlow.push_back(bJetVeto);  // bJetVeto: reject events with more than 2 central b-jets
                                                       // -> check if they are central
            CREMuResolvedCutFlow.push_back(HtCut);

            // anti-QCD cuts for resolved selection
            CREMuResolvedCutFlow.push_back(AbsDeltaPhiJJ);
            CREMuResolvedCutFlow.push_back(AbsDeltaPhiMetJJ);
        }

        else {
            // ****************************************
            // Initialize and add new cuts to cutflow
            // ****************************************
            CREMuResolvedCutFlow.push_back(ptjj100->combine(ptjjcorr100, Cut::Combine::OR));

            // NOTE: No cuts on mT variables or MET significance added yet, to be studied whether it makes sense to apply them
        }

        ATH_CHECK(AddToCutFlows(CREMuResolvedCutFlow));

        // --------------------------------------------------------------------
        // 2L_CR_EMu_Merged cut flow
        // --------------------------------------------------------------------

        CutFlow CREMuMergedCutFlow("2L_CR_EMu_Merged");

        // skimming cuts
        // --------------------------------------------------------------------
        // If we use PFlow, we clean PFlow :)
        if (m_doPFlowElecCleaning) CREMuMergedCutFlow.push_back(PFlowElecCleaning);

        CREMuMergedCutFlow.push_back(IsMETTrigPassed);

        // Use the same lepton selections as resolved
        CREMuMergedCutFlow.push_back(OneMuon);
        CREMuMergedCutFlow.push_back(OneElectron);
        CREMuMergedCutFlow.push_back(oppositeCharge);

        // From here, basically copy the 1L region
        // Just imagine if there were just a vector of these cuts to extend with
        // ;) Maybe that can be a QoL TODO item
        //
        CREMuMergedCutFlow.push_back(TauVeto);
        // extended tau veto merged
        CREMuMergedCutFlow.push_back(Extended_TauVeto_Merged);
        // Same as above
        CREMuMergedCutFlow.push_back(MetTST150lepInvis);
        CREMuMergedCutFlow.push_back(OneFatJet);
        CREMuMergedCutFlow.push_back(largeR_btagging);
        CREMuMergedCutFlow.push_back(HiggsMassMerged->combine(HiggsMassMergedCorr, Cut::Combine::OR));

        // non-skimming cuts
        // --------------------------------------------------------------------

        // mass window used in fit
        CREMuMergedCutFlow.push_back(HiggsMassWindowMerged);

        if (m_doCONFselection) {
            // ****************************************
            // Initialize and add CONF cuts to cutflow
            // ****************************************
            // common merged cuts
            // bJet veto merged -> no b-tagged track jets outside dR=1.0 wrt leading fat
            // jet
            CREMuMergedCutFlow.push_back(BJetVeto_trkJets);
            // HTcut
            CREMuMergedCutFlow.push_back(HtRatioCut_merged);
        }

        ATH_CHECK(AddToCutFlows(CREMuMergedCutFlow));

        return StatusCode::SUCCESS;
    }

    MonoHTruthAnalysisConfig::MonoHTruthAnalysisConfig(std::string Analysis) : TruthAnalysisConfig(Analysis) {}

    StatusCode MonoHTruthAnalysisConfig::initializeCustomCuts() {
        CutFlow MonoHiggsbb_merged("Mono_Higgs_bb_merged");

        std::cout << "***********************************************" << std::endl;
        std::cout << "Starting with the Mono_Higgs_bb_merged cutflow!" << std::endl;
        std::cout << "***********************************************" << std::endl;

        Cut* N_Lep = NewCut("N_Lep==0", Cut::CutType::CutInt, false);
        if (!N_Lep->initialize("N_Lep", "==0")) return StatusCode::FAILURE;
        MonoHiggsbb_merged.push_back(N_Lep);

        Cut* MET_gt500 = NewCut("MET>500", Cut::CutType::CutXAMPPmet, false);
        if (!MET_gt500->initialize("TruthMET", ">=500000")) return StatusCode::FAILURE;
        MonoHiggsbb_merged.push_back(MET_gt500);

        Cut* N_FatJets = NewCut("N_FatJets>=1", Cut::CutType::CutInt, false);
        if (!N_FatJets->initialize("N_FatJets", ">=1")) return StatusCode::FAILURE;
        MonoHiggsbb_merged.push_back(N_FatJets);

        Cut* minDPhi = NewCut("minDPhi>20", Cut::CutType::CutFloat, false);
        if (!minDPhi->initialize("abs_mindPhi", Form(">=%f", TMath::Pi() * 1. / 9.))) return StatusCode::FAILURE;
        MonoHiggsbb_merged.push_back(minDPhi);

        Cut* dphi_metJ = NewCut("dphi_metJ>120", Cut::CutType::CutFloat, false);
        if (!dphi_metJ->initialize("dphi_metJ", ">120")) return StatusCode::FAILURE;
        MonoHiggsbb_merged.push_back(dphi_metJ);

        // Cut* N_BJetsAssoc02 = NewCut("N_BJetsAssoc02=2", Cut::CutType::CutInt, false);
        // if (!N_BJetsAssoc02->initialize("N_BJetsAssoc02", "==2")) return StatusCode::FAILURE;
        // MonoHiggsbb_merged.push_back(N_BJetsAssoc02);

        // Cut* N_BJetsNonAssoc02 = NewCut("N_BJetsNonAssoc02=0", Cut::CutType::CutInt, false);
        // if (!N_BJetsNonAssoc02->initialize("N_BJetsNonAssoc02", "==0")) return StatusCode::FAILURE;
        // MonoHiggsbb_merged.push_back(N_BJetsNonAssoc02);

        ATH_CHECK(AddToCutFlows(MonoHiggsbb_merged));

        CutFlow MonoHiggsbb_resolved("Mono_Higgs_bb_resolved");

        std::cout << "*************************************************" << std::endl;
        std::cout << "Starting with the Mono_Higgs_bb_resolved cutflow!" << std::endl;
        std::cout << "*************************************************" << std::endl;

        MonoHiggsbb_resolved.push_back(N_Lep);

        Cut* MET_gt150 = NewCut("MET>150", Cut::CutType::CutXAMPPmet, false);
        if (!MET_gt150->initialize("TruthMET", ">150000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved.push_back(MET_gt150);

        Cut* MET_lt500 = NewCut("MET<=500", Cut::CutType::CutXAMPPmet, false);
        if (!MET_lt500->initialize("TruthMET", "<=500000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved.push_back(MET_lt500);

        // minDPhi cut already defined within merged event selection
        MonoHiggsbb_resolved.push_back(minDPhi);

        Cut* N_Jets = NewCut("N_Jets>=2", Cut::CutType::CutInt, false);
        if (!N_Jets->initialize("N_Jets", ">=2")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved.push_back(N_Jets);

        Cut* jetpt0 = NewCut("jet0>45", Cut::CutType::CutFloat, false);
        if (!jetpt0->initialize("LeadingjetPt", ">45000")) return StatusCode::FAILURE;

        Cut* jetpt1 = NewCut("jet1>45", Cut::CutType::CutFloat, false);
        if (!jetpt1->initialize("SubLeadingjetPt", ">45000")) return StatusCode::FAILURE;
        jetpt0->SetName("jet0or1>45");
        MonoHiggsbb_resolved.push_back(jetpt0->combine(jetpt1, Cut::Combine::OR));

        Cut* N_TwoJets = NewCut("N_Jets=2", Cut::CutType::CutInt, false);
        if (!N_TwoJets->initialize("N_Jets", "==2")) return StatusCode::FAILURE;
        Cut* N_SigSumTwoJets = NewCut("sigjet012ptsum>120(150)", Cut::CutType::CutFloat, false);
        if (!N_SigSumTwoJets->initialize("sigjet012ptsum", ">120000")) return StatusCode::FAILURE;
        Cut* N_SigSum120 = N_SigSumTwoJets->combine(N_TwoJets, Cut::Combine::AND);
        Cut* MoreJets = NewCut("N_Jets>2", Cut::CutType::CutInt, false);
        if (!MoreJets->initialize("N_Jets", ">2")) return StatusCode::FAILURE;
        Cut* N_SigSumMoreJets = NewCut("sigjet012ptsum>150", Cut::CutType::CutFloat, false);
        if (!N_SigSumMoreJets->initialize("sigjet012ptsum", ">150000")) return StatusCode::FAILURE;
        Cut* N_SigSum150 = N_SigSumMoreJets->combine(MoreJets, Cut::Combine::AND);
        N_SigSum150->SetName("sigjet012ptsum>120(150)");
        MonoHiggsbb_resolved.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));

        Cut* dphi_jj = NewCut("dphi_jj<140", Cut::CutType::CutFloat, false);
        if (!dphi_jj->initialize("dphi_jj", "<140")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved.push_back(dphi_jj);

        Cut* dphi_metjj = NewCut("dphi_metjj>120", Cut::CutType::CutFloat, false);
        if (!dphi_metjj->initialize("dphi_metjj", ">120")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved.push_back(dphi_metjj);

        // Cut* N_TwoBJets = NewCut("N_BJets04=2", Cut::CutType::CutInt, false);
        // if (!N_TwoBJets->initialize("N_BJets04", "==2")) return StatusCode::FAILURE;
        // MonoHiggsbb_resolved.push_back(N_TwoBJets);

        ATH_CHECK(AddToCutFlows(MonoHiggsbb_resolved));

        CutFlow MonoHiggsbb_resolved_MET_150_200("Mono_Higgs_bb_resolved_MET_150_200");

        std::cout << "*************************************************************" << std::endl;
        std::cout << "Starting with the Mono_Higgs_bb_resolved_MET_150_200 cutflow!" << std::endl;
        std::cout << "*************************************************************" << std::endl;

        MonoHiggsbb_resolved_MET_150_200.push_back(N_Lep);

        MonoHiggsbb_resolved_MET_150_200.push_back(MET_gt150);

        Cut* MET_lt200 = NewCut("MET<=200", Cut::CutType::CutXAMPPmet, false);
        if (!MET_lt200->initialize("TruthMET", "<=200000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved_MET_150_200.push_back(MET_lt200);

        // minDPhi cut already defined within merged event selection
        MonoHiggsbb_resolved_MET_150_200.push_back(minDPhi);

        MonoHiggsbb_resolved_MET_150_200.push_back(N_Jets);

        MonoHiggsbb_resolved_MET_150_200.push_back(jetpt0->combine(jetpt1, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_150_200.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_150_200.push_back(dphi_jj);

        MonoHiggsbb_resolved_MET_150_200.push_back(dphi_metjj);

        // MonoHiggsbb_resolved_MET_150_200.push_back(N_TwoBJets);

        ATH_CHECK(AddToCutFlows(MonoHiggsbb_resolved_MET_150_200));

        CutFlow MonoHiggsbb_resolved_MET_200_350("Mono_Higgs_bb_resolved_MET_200_350");

        std::cout << "*************************************************************" << std::endl;
        std::cout << "Starting with the Mono_Higgs_bb_resolved_MET_200_350 cutflow!" << std::endl;
        std::cout << "*************************************************************" << std::endl;

        MonoHiggsbb_resolved_MET_200_350.push_back(N_Lep);

        Cut* MET_gt200 = NewCut("MET>200", Cut::CutType::CutXAMPPmet, false);
        if (!MET_gt200->initialize("TruthMET", ">200000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved_MET_200_350.push_back(MET_gt200);

        Cut* MET_lt350 = NewCut("MET<=350", Cut::CutType::CutXAMPPmet, false);
        if (!MET_lt350->initialize("TruthMET", "<=350000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved_MET_200_350.push_back(MET_lt350);

        // minDPhi cut already defined within merged event selection
        MonoHiggsbb_resolved_MET_200_350.push_back(minDPhi);

        MonoHiggsbb_resolved_MET_200_350.push_back(N_Jets);

        MonoHiggsbb_resolved_MET_200_350.push_back(jetpt0->combine(jetpt1, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_200_350.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_200_350.push_back(dphi_jj);

        MonoHiggsbb_resolved_MET_200_350.push_back(dphi_metjj);

        // MonoHiggsbb_resolved_MET_200_350.push_back(N_TwoBJets);

        ATH_CHECK(AddToCutFlows(MonoHiggsbb_resolved_MET_200_350));

        CutFlow MonoHiggsbb_resolved_MET_350_500("Mono_Higgs_bb_resolved_MET_350_500");

        std::cout << "*************************************************************" << std::endl;
        std::cout << "Starting with the Mono_Higgs_bb_resolved_MET_350_500 cutflow!" << std::endl;
        std::cout << "*************************************************************" << std::endl;

        MonoHiggsbb_resolved_MET_350_500.push_back(N_Lep);

        Cut* MET_gt350 = NewCut("MET>350", Cut::CutType::CutXAMPPmet, false);
        if (!MET_gt350->initialize("TruthMET", ">350000")) return StatusCode::FAILURE;
        MonoHiggsbb_resolved_MET_350_500.push_back(MET_gt350);

        MonoHiggsbb_resolved_MET_350_500.push_back(MET_lt500);

        // minDPhi cut already defined within merged event selection
        MonoHiggsbb_resolved_MET_350_500.push_back(minDPhi);

        MonoHiggsbb_resolved_MET_350_500.push_back(N_Jets);

        MonoHiggsbb_resolved_MET_350_500.push_back(jetpt0->combine(jetpt1, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_350_500.push_back(N_SigSum120->combine(N_SigSum150, Cut::Combine::OR));

        MonoHiggsbb_resolved_MET_350_500.push_back(dphi_jj);

        MonoHiggsbb_resolved_MET_350_500.push_back(dphi_metjj);

        // MonoHiggsbb_resolved_MET_350_500.push_back(N_TwoBJets);

        ATH_CHECK(AddToCutFlows(MonoHiggsbb_resolved_MET_350_500));

        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
