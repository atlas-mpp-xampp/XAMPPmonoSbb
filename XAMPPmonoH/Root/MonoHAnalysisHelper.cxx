// clang-format off
#include <XAMPPmonoH/MonoHAnalysisHelper.h>
#include <XAMPPmonoH/MonoHTruthSelector.h>
#include <XAMPPmonoH/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>
#include <XAMPPbase/SUSYTriggerTool.h>
#include <MetTriggerSFUncertainties/IMetTriggerSF.h>
#include <SMShapeUncertainties/ISMShapeWeights.h>
#include <cmath>
// clang-format on

// Tool includes
#include <PathResolver/PathResolver.h>
#include <PileupReweighting/TPileupReweighting.h>
#include <fstream>
#include <iostream>
#include <boost/format.hpp>

// Put helper functions into the anonymous namespace
// The anonymous namespace is special - it has only internal linkage so will not
// affect anything in any other translation unit.
namespace {

    /**
     * @brief Helper output class for a dijet pair
     */
    class DijetXAMPPOutput : public XAMPP::XAMPPOutput {
    public:
        /// Create the output branches.
        static StatusCode createBranches(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix) {
            return XAMPP::P4XAMPPOutput::createBranches(evtInfo, prefix, "_jj" + postfix);
        }

        /// Get the output branches
        DijetXAMPPOutput(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix) :
            m_dijet(evtInfo, prefix, "_jj" + postfix) {}

        /// Set the output dijet and MET
        void setDijetP4(const TLorentzVector& j1, const TLorentzVector& j2) {
            m_j1 = j1;
            m_j2 = j2;
            m_dijet.setP4(j1 + j2);
        }

        StatusCode push() override {
            ATH_CHECK(m_dijet.push());
            return StatusCode::SUCCESS;
        }

    private:
        XAMPP::P4XAMPPOutput m_dijet;
        TLorentzVector m_j1;
        TLorentzVector m_j2;
    };  //> end class DijetXAMPPOutput

    // Map jet calibration states to muon correction names
    std::map<std::string, std::string> resolvedCalibStateToMuonCorr{{"OneMuon", "corr"}};
    std::map<std::string, std::string> mergedCalibStateToMuonCorr{{"TwoMuons", "corr"}};

    // Create the corresponding output helpers
    std::map<std::string, DijetXAMPPOutput> makeResolvedMuonCorrDijets(XAMPP::EventInfo* evtInfo) {
        std::map<std::string, DijetXAMPPOutput> outputs;
        for (const auto& namePair : resolvedCalibStateToMuonCorr)
            outputs.emplace(std::piecewise_construct, std::forward_as_tuple(namePair.first),
                            std::forward_as_tuple(evtInfo, "", "_" + namePair.second));
        return outputs;
    }
    std::map<std::string, XAMPP::P4XAMPPOutput> makeMergedMuonCorrLRJets(XAMPP::EventInfo* evtInfo) {
        std::map<std::string, XAMPP::P4XAMPPOutput> outputs;
        for (const auto& namePair : mergedCalibStateToMuonCorr)
            outputs.emplace(std::piecewise_construct, std::forward_as_tuple(namePair.first),
                            std::forward_as_tuple(evtInfo, "", "_J_" + namePair.second));
        return outputs;
    }

    // Helper functions to make the branches in XAMPP itself
    StatusCode makeResolvedMuonCorrBranches(XAMPP::EventInfo* evtInfo) {
        for (const auto& namePair : resolvedCalibStateToMuonCorr)
            if (!DijetXAMPPOutput::createBranches(evtInfo, "", "_" + namePair.second).isSuccess()) return StatusCode::FAILURE;
        return StatusCode::SUCCESS;
    }
    StatusCode makeMergedMuonCorrBranches(XAMPP::EventInfo* evtInfo) {
        for (const auto& namePair : mergedCalibStateToMuonCorr)
            if (!XAMPP::P4XAMPPOutput::createBranches(evtInfo, "", "_J_" + namePair.second).isSuccess()) return StatusCode::FAILURE;
        return StatusCode::SUCCESS;
    }

    using MuonLink_t = ElementLink<xAOD::MuonContainer>;
    using acc_VecMuonLink_t = SG::AuxElement::Accessor<std::vector<MuonLink_t>>;
    acc_VecMuonLink_t acc_muonLinks("DRAssociatedMuons");

}  // namespace

namespace XAMPP {
    static IntAccessor acc_HadronConeExclTruthLabelID("HadronConeExclTruthLabelID");
    static IntAccessor acc_n_MuonInJet("n_MuonInJet");
    static FloatAccessor acc_XbbScoreHiggs("XbbScoreHiggs");
    static FloatAccessor acc_XbbScoreQCD("XbbScoreQCD");
    static FloatAccessor acc_XbbScoreTop("XbbScoreTop");
    static FloatAccessor acc_NNXbbScoreHiggs("SubjetBScore_Higgs");
    static FloatAccessor acc_NNXbbScoreQCD("SubjetBScore_QCD");
    static FloatAccessor acc_NNXbbScoreTop("SubjetBScore_Top");
    static FloatAccessor acc_NNXbb_CombScore_25("NNXbb_CombScore_25");
    static BoolAccessor acc_IsTaggedByNNXbb_25_70("IsTaggedByNNXbb_25_70");
    static CharAccessor acc_isAssociated("isAssociated");
    static CharAccessor acc_passDRcut("passDRcut");
    static CharAccessor acc_DFCommonCrackVetoCleaning("DFCommonCrackVetoCleaning");
    MonoHAnalysisHelper::~MonoHAnalysisHelper() {}
    MonoHAnalysisHelper::MonoHAnalysisHelper(const std::string& myname) :
        SUSYAnalysisHelper(myname),
        m_met_trigger_sf_tool(""),
        m_sm_shape_weights_tool_0lep(""),
        m_sm_shape_weights_tool_1lep(""),
        m_sm_shape_weights_tool_2lep(""),
        m_corrs_syst_weights(),
        m_useSystematicGroups(false),
        m_storeExtendedTriggerInfo(true),
        m_ORfatjetElectronDR(1.2),
        m_useTTBarMETFiltered0LepSamples(true),
        m_useVZqqVZbb(true),
        m_useZnunuBfilPTV0LepSamples(true),
        m_doTruthWjets(false),
        m_doTruthTtbar(false),
        m_prw_tool(""),
        m_cached_sum_w() {
        declareProperty("useTTBarMETFiltered0LepSamples", m_useTTBarMETFiltered0LepSamples);
        declareProperty("useVZqqVZbb", m_useVZqqVZbb);
        declareProperty("useZnunuBfilPTV0LepSamples", m_useZnunuBfilPTV0LepSamples);
        declareProperty("doDiagnosticVariables", m_doDiagnosticVariables = false);
        declareProperty("doMetTriggerSF", m_metTriggerSF = false);
        declareProperty("doSMShapeWeights", m_SMShapeWeights = false);
        declareProperty("useSystematicGroups", m_useSystematicGroups = false);
        declareProperty("storeExtendedTriggerInfo", m_storeExtendedTriggerInfo = true);
        declareProperty("ORfatjetElectronDR", m_ORfatjetElectronDR = 1.2);
        declareProperty("doPFlowCleaning", m_doPFlowCleaning = false);
        declareProperty("doCONFvariables", m_doCONFvariables = true);
        declareProperty("doXbbScore", m_doXbbScore = false);
        declareProperty("doNNXbbScore", m_doNNXbbScore = false);
        declareProperty("doBDT", m_doBDT = false);
        declareProperty("doTruthWjets", m_doTruthWjets);
        declareProperty("doTruthTtbar", m_doTruthTtbar);
        declareProperty("WriteMuonsInJets", m_writeMuonsInJets = true);

        m_met_trigger_sf_tool.declarePropertyFor(this, "MetTriggerSF", "The MetTriggerSF tool");
        m_sm_shape_weights_tool_0lep.declarePropertyFor(this, "SMShapeWeights0L", "The SMShapeWeights tool 0 lep");
        m_sm_shape_weights_tool_1lep.declarePropertyFor(this, "SMShapeWeights1L", "The SMShapeWeights tool 1 lep");
        m_sm_shape_weights_tool_2lep.declarePropertyFor(this, "SMShapeWeights2L", "The SMShapeWeights tool 2 lep");
    }

    StatusCode MonoHAnalysisHelper::initializeAnalysisTools() {
        ATH_CHECK(SUSYAnalysisHelper::initializeAnalysisTools());
        m_prw_tool = GetCPTool<CP::IPileupReweightingTool>("PileupReweightingTool");
        if (m_metTriggerSF) {
            if (!m_met_trigger_sf_tool.isUserConfigured()) {
                m_met_trigger_sf_tool.setTypeAndName("CP::MetTriggerSF/MetTriggerSF");
                ATH_CHECK(m_met_trigger_sf_tool.retrieve());
            }
            ToolHandleSystematics<CP::IMetTriggerSF>* MetTriggerSF = new ToolHandleSystematics<CP::IMetTriggerSF>(
                m_met_trigger_sf_tool, XAMPP::SelectionObject::Other, XAMPP::SelectionObject::MissingET);
            ATH_CHECK(MetTriggerSF->initialize());
        }

        if (m_SMShapeWeights && !isData()) {
            // TODO: we probably need to instanciate 3 of these tools. read further below why this is the case
            if (!m_sm_shape_weights_tool_0lep.isUserConfigured()) {
                m_sm_shape_weights_tool_0lep.setTypeAndName("CP::SMShapeWeights/SMShapeWeights0L");
                ATH_CHECK(m_sm_shape_weights_tool_0lep.retrieve());
            }
            if (!m_sm_shape_weights_tool_1lep.isUserConfigured()) {
                m_sm_shape_weights_tool_1lep.setTypeAndName("CP::SMShapeWeights/SMShapeWeights1L");
                ATH_CHECK(m_sm_shape_weights_tool_1lep.retrieve());
            }
            if (!m_sm_shape_weights_tool_2lep.isUserConfigured()) {
                m_sm_shape_weights_tool_2lep.setTypeAndName("CP::SMShapeWeights/SMShapeWeights2L");
                ATH_CHECK(m_sm_shape_weights_tool_2lep.retrieve());
            }
            ToolHandleSystematics<CP::ISMShapeWeights>* SMShapeWeights0L = new ToolHandleSystematics<CP::ISMShapeWeights>(
                m_sm_shape_weights_tool_0lep, XAMPP::SelectionObject::Other, XAMPP::SelectionObject::EventWeight);
            ATH_CHECK(SMShapeWeights0L->initialize());

            ToolHandleSystematics<CP::ISMShapeWeights>* SMShapeWeights1L = new ToolHandleSystematics<CP::ISMShapeWeights>(
                m_sm_shape_weights_tool_1lep, XAMPP::SelectionObject::Other, XAMPP::SelectionObject::EventWeight);
            ATH_CHECK(SMShapeWeights1L->initialize());

            ToolHandleSystematics<CP::ISMShapeWeights>* SMShapeWeights2L = new ToolHandleSystematics<CP::ISMShapeWeights>(
                m_sm_shape_weights_tool_2lep, XAMPP::SelectionObject::Other, XAMPP::SelectionObject::EventWeight);
            ATH_CHECK(SMShapeWeights2L->initialize());

            // TODO: here we  need to initialise three versions of
            // corrsandsyst, meaning we need to have three shape weight tools
            // (one for 0, one for 1, one for 2 lepton. this is the price we
            // have to pay for using corrsandsysts with its structure
            // #thankscorrsandsysts)
            ATH_CHECK(m_sm_shape_weights_tool_0lep->SetLeptonMultiplicity(0));
            ATH_CHECK(m_sm_shape_weights_tool_0lep->InitializeCorrsAndSysts());
            ATH_CHECK(m_sm_shape_weights_tool_1lep->SetLeptonMultiplicity(1));
            ATH_CHECK(m_sm_shape_weights_tool_1lep->InitializeCorrsAndSysts());
            ATH_CHECK(m_sm_shape_weights_tool_2lep->SetLeptonMultiplicity(2));
            ATH_CHECK(m_sm_shape_weights_tool_2lep->InitializeCorrsAndSysts());
        }

#if USE_XGBOOST
        if (m_doBDT) {
            m_XGB_resolved.LoadXGBoostModel("resolved");
            m_XGB_merged.LoadXGBoostModel("merged");
        }
#else
        if (m_doBDT) {
            ATH_MSG_ERROR("Analysis code must be compiled with -DUSE_XGBOOST for the BDT to be available!");
            return StatusCode::FAILURE;
        }
#endif

        return StatusCode::SUCCESS;
    }
    StatusCode MonoHAnalysisHelper::initialize() {
        ATH_MSG_INFO("Initialising MonoH AnalysisHelper...");
        ATH_CHECK(SUSYAnalysisHelper::initialize());
        return StatusCode::SUCCESS;
    }

    StatusCode MonoHAnalysisHelper::RemoveOverlap() {
        ATH_MSG_DEBUG("OverlapRemoval (non-large-R-jet objects)...");
        if (m_systematics->AffectsOnlyMET(m_systematics->GetCurrent())) return StatusCode::SUCCESS;
        ATH_CHECK(m_susytools->OverlapRemoval(m_electron_selection->GetPreElectrons(), m_muon_selection->GetPreMuons(),
                                              m_jet_selection->GetPreJets(),  // small-R jets
                                              m_photon_selection->GetPrePhotons(), m_tau_selection->GetPreTaus()));

        // Since no large-R jet container was passed in the command above (instead a
        // nullpointer is passed), no large-R jet to electron and no jet to large-R
        // jet overlap removal is performed within SUSYTools. The overlap removal
        // between large-R jets and electrons (with preference to electrons) is
        // performed in the following line. The sequence of ORs is in accordance with
        // the sequence in the OR tool
        // https://gitlab.cern.ch/atlas/athena/blob/release/21.2.23/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/OverlapRemovalTool.cxx

        ATH_MSG_DEBUG(
            "OverlapRemoval (large-R-jets, electron), with preference to "
            "electron...");
        // RemoveOverlap(xAOD::IParticleContainer* RemFrom, xAOD::IParticleContainer*
        // RemWith, float dR, bool UseRapidity);
        ATH_CHECK(
            XAMPP::RemoveOverlap(m_jet_selection->GetPreJets(10), m_electron_selection->GetPreElectrons(), m_ORfatjetElectronDR, true));

        return StatusCode::SUCCESS;
    }
    bool MonoHAnalysisHelper::AcceptEvent() {
        if (!SUSYAnalysisHelper::AcceptEvent()) return false;
        if (isData()) return true;
        static const std::vector<int> DSID_ttbarMETsliced{345935, 407345, 407346, 407347};

        // Sherpa di boson samples
        static const std::vector<int> DSID_VZqq{363355, 363356, 363489};  // ZqqZvv, ZqqZll, ZqqWlv
        static const std::vector<int> DSID_VZbb{345043, 345044, 345045};  // ZbbZvv, ZbbZll, ZbbWlv

        // Sherpa Znunu+Bjets samples
        static const std::vector<int> DSID_ZnunuB_MAXHTPTV{364144, 364147, 364150, 364153, 364154, 364155};
        static const std::vector<int> DSID_ZnunuB_PTV{366010, 366011, 366012, 366013, 366014, 366015, 366016, 366017, 364222, 364223};

        static XAMPP::Storage<float>* dec_gen_filt_met = m_XAMPPInfo->GetVariableStorage<float>("GenFiltMET");
        static XAMPP::Storage<double>* dec_gen_weight_merge = m_XAMPPInfo->GetVariableStorage<double>("GenWeightMCSampleMerging");
        /// Basic check
        if (!dec_gen_filt_met->isAvailable()) {
            ATH_MSG_FATAL("GenFiltMET is not available in the sample.... We should run");
            return false;
        }

        int dsid = m_XAMPPInfo->mcChannelNumber();
        int run = m_XAMPPInfo->runNumber();

        enum MCCampaign { mc16a = 284500, mc16d = 300000, mc16e = 310000, UNDEFINED };

        if (m_useTTBarMETFiltered0LepSamples && dsid == 410470 && dec_gen_filt_met->GetValue() > 200.e3) {
            return false;
        } else if (!m_useTTBarMETFiltered0LepSamples &&
                   std::find(DSID_ttbarMETsliced.begin(), DSID_ttbarMETsliced.end(), dsid) != DSID_ttbarMETsliced.end())
            return false;


        // Making sure to remove overlapping events for the ttbar PowHw inclusive and met filtered samples
        static const std::vector<int> DSID_ttbarMETsliced_PowHw{407357, 407358, 407359};
        if (m_useTTBarMETFiltered0LepSamples && (dsid == 410557 || dsid==410558) && dec_gen_filt_met->GetValue() > 200.e3) {
            return false;
        } else if (!m_useTTBarMETFiltered0LepSamples &&
                   std::find(DSID_ttbarMETsliced_PowHw.begin(), DSID_ttbarMETsliced_PowHw.end(), dsid) != DSID_ttbarMETsliced_PowHw.end())
            return false;


        else if (!m_useZnunuBfilPTV0LepSamples && run != MCCampaign::mc16e &&
                 std::find(DSID_ZnunuB_PTV.begin(), DSID_ZnunuB_PTV.end(), dsid) != DSID_ZnunuB_PTV.end())
            return false;

        // Try to find an appropiate weight to combine the samples in one
        double W = 1;

        // these samples are going to get half of the event weight because we want to merge them together in the end.
        // Take one half of the cross-section from sample a and the other one from sample b... It's straightforward to apply those weights
        if (m_useTTBarMETFiltered0LepSamples && ((dsid == 410470 && dec_gen_filt_met->GetValue() > 100.e3) || dsid == 345935)) {
            W = 0.5;
        }

        else if (m_useZnunuBfilPTV0LepSamples && run != MCCampaign::mc16e &&
                 ((std::find(DSID_ZnunuB_PTV.begin(), DSID_ZnunuB_PTV.end(), dsid) != DSID_ZnunuB_PTV.end()) ||
                  (std::find(DSID_ZnunuB_MAXHTPTV.begin(), DSID_ZnunuB_MAXHTPTV.end(), dsid) != DSID_ZnunuB_MAXHTPTV.end()))) {
            W = 0.5;
        }

        if (!m_useVZqqVZbb) {
            if (!dec_gen_weight_merge->ConstStore(W).isSuccess()) throw std::runtime_error("Failed to store 'gen_weight_merge!'");
            return true;
        }

        std::vector<int>::const_iterator VZbb_itr = std::find(DSID_VZbb.begin(), DSID_VZbb.end(), dsid);
        std::vector<int>::const_iterator VZqq_itr = std::find(DSID_VZqq.begin(), DSID_VZqq.end(), dsid);

        // define the two quark anti-quark pairs
        const xAOD::TruthParticle* b_quark = nullptr;
        const xAOD::TruthParticle* b_anti_quark = nullptr;
        if (VZqq_itr != DSID_VZqq.end()) {
            for (const auto truth_part : *m_truth_selection->GetTruthInContainer()) {
                if (truth_part->isZ() && truth_part->status() == 3) {
                    for (unsigned int c = 0; c < truth_part->nChildren(); ++c) {
                        const xAOD::TruthParticle* child = truth_part->child(c);
                        if (!child || child->absPdgId() != 5) continue;
                        if (!b_quark)
                            b_quark = child;
                        else if (!b_anti_quark)
                            b_anti_quark = child;
                        else
                            ATH_MSG_WARNING("A z decay with 3 b quarks? That violates charge conservation");
                    }
                }

                // We've selected everything we need. bail out of the loop
                if (b_quark && b_anti_quark) break;
            }
        }
        // Apply BR to bb only if the sample is exclusive
        static const double BR_VVbb = 0.2161;
        // Ensure that two b-quarks exist or that the dsid belongs to the exclusive sample
        if ((b_quark && b_anti_quark && b_quark->pdgId() + b_anti_quark->pdgId() == 0 && b_quark->prodVtx() == b_anti_quark->prodVtx()) ||
            VZbb_itr != DSID_VZbb.end()) {
            int dsid_bb = VZbb_itr != DSID_VZbb.end() ? dsid : (*DSID_VZbb.begin() + VZqq_itr - DSID_VZqq.begin());
            int dsid_qq = VZbb_itr == DSID_VZbb.end() ? dsid : (*DSID_VZqq.begin() + VZbb_itr - DSID_VZbb.begin());
            W = (VZqq_itr != DSID_VZqq.end() ? BR_VVbb : 1) * getSumW(dsid, run) /
                (getSumW(dsid_bb, run) + BR_VVbb * getSumW(dsid_qq, run));
        }

        if (!dec_gen_weight_merge->ConstStore(W).isSuccess()) throw std::runtime_error("Failed to store 'gen_weight_merge'!");
        return true;
    }
    StatusCode MonoHAnalysisHelper::initializeEventVariables() {
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_0L_SR_Resolved"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_0L_SR_Merged"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_1L_CR_Resolved"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_1L_CR_Merged"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_Ele_Resolved"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_Ele_Merged"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_Muo_Resolved"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_Muo_Merged"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_EMu_Resolved"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("passed_2L_CR_EMu_Merged"));
    
        // Event weights
        // --------------------------------------------------------------------------
        // If you add a variable with false, you can cut on that but it will not be
        // written to the tree
        if (m_SMShapeWeights && !isData()) {
            for (const auto& set : m_systematics->GetWeightSystematics(XAMPP::SelectionObject::EventWeight)) {
                if (set == m_systematics->GetNominal()) continue;
                std::string weight_name = "CorrsAndSysts_SF" + (set->name().empty() ? "" : "_" + set->name());
                if (!ToolIsAffectedBySystematic(m_sm_shape_weights_tool_0lep.getHandle(), set)) continue;
                ATH_CHECK(m_XAMPPInfo->NewEventVariable<double>(weight_name, true, set == m_systematics->GetNominal()));
                XAMPP::Storage<double>* weight = m_XAMPPInfo->GetVariableStorage<double>(weight_name);
                if (!weight) return StatusCode::FAILURE;
                m_corrs_syst_weights.insert(std::pair<const CP::SystematicSet*, XAMPP::Storage<double>*>(set, weight));
            }
        }
        if (!isData()) {
            ATH_CHECK(m_XAMPPInfo->NewCommonEventVariable<double>("GenWeightMCSampleMerging"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("GenFiltMET", false));
        }
        // Event variables cleanup (this removes some event variables that are
        // included by default from the ntuple output to improve performance)
        // The lines that are commented out are NOT removed and therefore
        // included by default in the output ntuple.
        if (!m_doDiagnosticVariables) {
            m_XAMPPInfo->RemoveVariableFromOutput<char>("passLArTile");
            m_XAMPPInfo->RemoveVariableFromOutput<char>("HasVtx");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("pixelFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("sctFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("trtFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("larFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("tileFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("muonFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("forwardDetFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("coreFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("backgroundFlags");
            m_XAMPPInfo->RemoveVariableFromOutput<unsigned int>("lumiFlags");
        }

        if (!m_storeExtendedTriggerInfo) {
            m_XAMPPInfo->RemoveVariableFromOutput<char>("Trigger");
            m_XAMPPInfo->RemoveVariableFromOutput<char>("TrigMatching");

            // remove all trigger variables and trigger matching variables
            for (auto trig : m_triggers->GetActiveTriggers()) {
                m_XAMPPInfo->RemoveVariableFromOutput<char>(trig->StoreName());
                m_XAMPPInfo->RemoveVariableFromOutput<char>(trig->MatchStoreName());
            }
        }

        ////////////////////////////////////////
        // event variable to remove event when
        // electron or photon entering MET calculation
        // has DFCommonCrackVeto set to false.
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("DFCommonCut", false));

        //////////////////////////////////////
        // Light lepton and tau multiplicities
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BaselineLeptons", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_SignalLeptons", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BaselineElectrons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_SignalElectrons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BaselineMuons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_SignalMuons"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_SignalTaus"));

        /////////////////////
        // jet multiplicities
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_Jets04"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_ForwardJets04"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_AllJets04", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BJets_04"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BTags_associated_02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_BTags_not_associated_02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_associated_Jets02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_not_associated_Jets02"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_Jets10"));

        ////////////////////////////////////////////////////////////////////////////////
        // information about the two Higgs candidate small-R jets in the resolved region
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Jet_BLight1BPt"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("Jet_BLight2BPt"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("Jet_BLight1isBjet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("Jet_BLight2isBjet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Jet_BLight1TruthLabel"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("Jet_BLight2TruthLabel"));

        /////////////////////////////////////////////////////////////////////////////
        // information about the two Higgs candidate track jets in the merged region
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("TrackJet_1isBjet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("TrackJet_2isBjet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("TrackJet_1passOR"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("TrackJet_2passOR"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("TrackJet_1TruthLabel"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("TrackJet_2TruthLabel"));

        ///////////////////////
        // Higgs candidate mass
        // corr refers to the m_jj variable caluclated with the muon-in-jet correction
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("m_jj"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("m_J"));

        ////////////////////////////
        // Resolved Higgs kinematics
        // Only write the resolved here as the merged candidate's kinematics are
        // already written out. As before, 'corr' refers to the muon-in-jet
        // correction
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("pt_jj"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("eta_jj"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("phi_jj"));

        //////////////////////////
        // Muon corrected variables.
        // For historical reasons, 'corr' refers to a correction using the
        // single muon closest in DR to the jet axis (two closest for merged).
        ATH_CHECK(makeResolvedMuonCorrBranches(m_XAMPPInfo));
        ATH_CHECK(makeMergedMuonCorrBranches(m_XAMPPInfo));

        /////////////////////////////////////////////
        // leading lepton pt (not written to output)
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("pt_mu1", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("pt_e1", false));

        /////////////////////////////////////////
        // dilepton variables used in 2-lepton CR
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("m_ll"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("pt_ll"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("isOppositeCharge"));

        // anti-QCD variable
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("AbsDeltaPhiMin3", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("DeltaPhiMin3"));

        ////////////////////////////
        // ttbar reduction variables
        // Extended tau veto in resolved region; calculated using the number of tracks in a jet
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_TausExtended_Resolved"));
        // Extended tau veto in merged region; calculated using the number of tracks in a jet
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("N_TausExtended_Merged"));
        // transverse mass calculated with MET and closest / furthest b-jet (if the lepton in semi-leptonic ttbar decays is not
        // reconstructed, then it contributes to MET --> mT should have a cutoff around the top mass)
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("mT_METclosestBJet"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("mT_METfurthestBJet"));

        /////////////////////////////////////////////////////////////////////////////////
        // variables related to ttbar decay topologies calculated with truth information
        if (m_doTruthWjets && !isData()) ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("WjetsDecayCategory"));
        if (m_doTruthTtbar && !isData()) {
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("topDecayCategory"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("antiDecayCategory"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("ttbarDecayCategory"));
        }

        // ****************************************
        // CONF variables that are not used anymore
        // ****************************************
        if (m_doCONFvariables) {
            // multijet reduction variables
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("AbsDeltaPhiJJ", false));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("DeltaPhiJJ"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("AbsDeltaPhiMetJJ", false));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("DeltaPhiMetJJ"));
            // angular separation between the two leading jets used for ttbar reduction
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("DeltaRJJ"));
            // event based MET significance variable
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("METsig"));
            // HtRatioResolved: The two Higgs candidate jets + the highest ISR jet should carry the largest fraction of the total jet pT in
            // an event --> reduce ttbar
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("HtRatioResolved"));
            // HtRatioMerged: The small-R jets in the event should carry a much smaller fraction of the total jet pT compared to the leading
            // large-R jet
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("HtRatioMerged"));
            // pT sum of leading 2/3 jets; cut due to some trigger related issue inherited from run 1
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("sigjet012ptsum"));
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////
        // topological cut for ttbar reduction in resolved region; calculated using likelihood ordering
        // this cut was studied for 2015-16 analysis
        // it is ***NOT*** applied in current analysis (code left here if somebody wants to take a look into it again)
        // ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("ttbarTopoResolved"));

        ///////////////////////////////////////////////////////////////////////////////
        // Training variables and XGBoost BDT outputs
#ifdef USE_XGBOOST
        if (m_doBDT) {
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("XGB_resolved"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("XGB_merged"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("PCSj1"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("PCSj2"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("PCStj1"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("PCStj2"));
        }
#endif

        //////////////////////////
        // Higgs tagging variables
        if (m_doXbbScore) {
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("XbbScoreHiggs"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("XbbScoreQCD"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("XbbScoreTop"));
        }

        if (m_doNNXbbScore) {
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("NNXbbScoreHiggs"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("NNXbbScoreQCD"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("NNXbbScoreTop"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<float>("NNXbb_CombScore_25"));
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<bool>("IsTaggedByNNXbb_25_70"));
        }

        ///////////////////////////////////////////////////////////////////////////////
        // Introduce systematic groups to re-use nominal information for groups of objects
        // when running with systematics to reduce ntupel size
        if (m_useSystematicGroups) {
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("Jet", SelectionObject::Jet));
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("Muon", SelectionObject::Muon));
            ATH_CHECK(m_XAMPPInfo->createSystematicGroup("Electron", SelectionObject::Electron));
        }

        //////////////////////////////////////////////////////////////////////////
        // write the containers (+decorators) for central jets to the output trees
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Jet", true));
        XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("Jet");
        ATH_CHECK(JetStore->SaveVariable<char>(std::vector<std::string>{"bjet"}));
        ATH_CHECK(JetStore->SaveVariable<double>("BTagScore"));
        ATH_CHECK(JetStore->SaveVariable<int>("n_MuonInJet"));
        // truth information only available for MC
        if (!isData()) ATH_CHECK(JetStore->SaveVariable<int>("HadronConeExclTruthLabelID"));
        if (m_useSystematicGroups) {
            ATH_CHECK(JetStore->setSystematicGroup("Jet"));
            ATH_CHECK(JetStore->SaveVariable<char>(std::vector<std::string>{"passOR"}));
            JetStore->pipeVariableToAllTrees(std::vector<std::string>{"passOR"});
        }

        /////////////////////////////////////////////////////////////////////
        // write the containers for central jets and taus to the output trees
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("ForwardJet", true));

        //////////////////////////////////////////////////////////////////////////
        // write the containers (+decorators) for large-R jets to the output trees
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("FatJet", true));
        XAMPP::ParticleStorage* FatJetStore = m_XAMPPInfo->GetParticleStorage("FatJet");
        ATH_CHECK(FatJetStore->SaveVariable<int>("n_matchedasstrkjets"));

        //////////////////////////////////////////////////////////////////////////
        // write the containers (+decorators) for track jets to the output trees
        ATH_CHECK(m_XAMPPInfo->BookCommonParticleStorage("TrackJet", true));
        XAMPP::ParticleStorage* TrackJetStore = m_XAMPPInfo->GetParticleStorage("TrackJet");
        ATH_CHECK(TrackJetStore->SaveVariable<double>("BTagScore"));
        ATH_CHECK(TrackJetStore->SaveVariable<char>(std::vector<std::string>{"bjet", "isAssociated", "passDRcut"}));
        // truth information only available for MC
        if (!isData()) ATH_CHECK(TrackJetStore->SaveVariable<int>("HadronConeExclTruthLabelID"));

        //////////////////////////////////////////////////////////////////////////
        // write the containers (+decorators) for light leptons to the output trees
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Electron", true));
        XAMPP::ParticleStorage* ElectronStore_Base = m_XAMPPInfo->GetParticleStorage("Electron");
        ATH_CHECK(ElectronStore_Base->SaveVariable<float>("charge"));
        if (m_useSystematicGroups) {
            ATH_CHECK(ElectronStore_Base->SaveVariable<char>(std::vector<std::string>{"passOR", "signal"}));
            ATH_CHECK(ElectronStore_Base->setSystematicGroup("Electron"));
            ElectronStore_Base->pipeVariableToAllTrees(std::vector<std::string>{"passOR", "signal"});
        }

        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("Muon", true));
        XAMPP::ParticleStorage* MuonStore_Base = m_XAMPPInfo->GetParticleStorage("Muon");
        ATH_CHECK(MuonStore_Base->SaveVariable<float>("charge"));
        if (m_useSystematicGroups) {
            ATH_CHECK(MuonStore_Base->SaveVariable<char>(std::vector<std::string>{"passOR", "signal"}));
            ATH_CHECK(MuonStore_Base->setSystematicGroup("Muon"));
            MuonStore_Base->pipeVariableToAllTrees(std::vector<std::string>{"passOR", "signal"});
        }
        if (m_writeMuonsInJets) {
            for (const std::string& name : {"Jet1Muons", "Jet2Muons", "FatJetMuons"}) {
                ATH_CHECK(m_XAMPPInfo->BookParticleStorage(name, true));
                XAMPP::ParticleStorage* store = m_XAMPPInfo->GetParticleStorage(name);
                ATH_CHECK(store->SaveVariable<float>("EnergyLoss"));
            }
        }
        return StatusCode::SUCCESS;
    }

    StatusCode MonoHAnalysisHelper::ComputeEventVariables() {
        // define MET choice dependent on type of region: MET used in 0 lepton, METlepInvis used in 1 lepton region and 2 lepton
        static XAMPP::Storage<XAMPPmet>* dec_XAMPPmetTST = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTST");
        static XAMPP::Storage<XAMPPmet>* dec_XAMPPmetTSTlepInvis = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTSTlepInvis");
        // check if 1 lepton region lepton requirement is satisfied
        bool doOneLepton = (m_muon_selection->GetSignalMuons()->size() == 1 && m_muon_selection->GetBaselineMuons()->size() == 1);
        bool doTwoLepton =
            ((m_muon_selection->GetSignalMuons()->size() == 2 && m_muon_selection->GetBaselineMuons()->size() == 2) ||
             (m_electron_selection->GetSignalElectrons()->size() == 2 && m_electron_selection->GetBaselineElectrons()->size() == 2));
        bool doEMuLepton =
            ((m_muon_selection->GetSignalMuons()->size() == 1 && m_muon_selection->GetBaselineMuons()->size() == 1) &&
             (m_electron_selection->GetSignalElectrons()->size() == 1 && m_electron_selection->GetBaselineElectrons()->size() == 1));
        xAOD::MissingET* MetTSTProxy;
        if (doOneLepton || doTwoLepton || doEMuLepton)
            MetTSTProxy = dec_XAMPPmetTSTlepInvis->GetValue();
        else
            MetTSTProxy = dec_XAMPPmetTST->GetValue();

        // check if MET is nan - can occur in MET systematics for events without hard MET term (fixed in 21.2.67+)
        if (std::isnan(MetTSTProxy->met())) {
            ATH_MSG_ERROR("METTST is not a number, setting it to 0 and therefore dropping event with skimming cuts");
            MetTSTProxy->setMpx(0.);
            MetTSTProxy->setMpy(0.);
            MetTSTProxy->setSumet(0.);
        }

        static XAMPP::Storage<int>* dec_NBJets04 = m_XAMPPInfo->GetVariableStorage<int>("N_BJets_04");  // all b-tags of central small-R
        static XAMPP::Storage<int>* dec_N_BTags_associated_02 =
            m_XAMPPInfo->GetVariableStorage<int>("N_BTags_associated_02");  // leading to associated track-jets
        static XAMPP::Storage<int>* dec_BJetVetoMerged =
            m_XAMPPInfo->GetVariableStorage<int>("N_BTags_not_associated_02");  // adding BJetVeto merged

        static XAMPP::Storage<int>* dec_NLeps = m_XAMPPInfo->GetVariableStorage<int>("N_SignalLeptons");
        static XAMPP::Storage<int>* dec_NBaseLeps = m_XAMPPInfo->GetVariableStorage<int>("N_BaselineLeptons");

        static XAMPP::Storage<int>* dec_NElec = m_XAMPPInfo->GetVariableStorage<int>("N_SignalElectrons");
        static XAMPP::Storage<int>* dec_NBaseElec = m_XAMPPInfo->GetVariableStorage<int>("N_BaselineElectrons");
        static XAMPP::Storage<int>* dec_NMuon = m_XAMPPInfo->GetVariableStorage<int>("N_SignalMuons");
        static XAMPP::Storage<int>* dec_NBaseMuon = m_XAMPPInfo->GetVariableStorage<int>("N_BaselineMuons");
        static XAMPP::Storage<bool>* dec_isOppositeCharge = m_XAMPPInfo->GetVariableStorage<bool>("isOppositeCharge");

        static XAMPP::Storage<int>* dec_NTaus = m_XAMPPInfo->GetVariableStorage<int>("N_SignalTaus");
        // adding extended tau veto resolved
        static XAMPP::Storage<int>* dec_ExtendedTauVetoRes = m_XAMPPInfo->GetVariableStorage<int>("N_TausExtended_Resolved");
        // adding extended tau veto merged
        static XAMPP::Storage<int>* dec_ExtendedTauVetoMerged = m_XAMPPInfo->GetVariableStorage<int>("N_TausExtended_Merged");

        static XAMPP::Storage<int>* dec_NJets04 = m_XAMPPInfo->GetVariableStorage<int>("N_Jets04");
        static XAMPP::Storage<int>* dec_NAllJets04 = m_XAMPPInfo->GetVariableStorage<int>("N_AllJets04");
        static XAMPP::Storage<int>* dec_NForwardJets04 = m_XAMPPInfo->GetVariableStorage<int>("N_ForwardJets04");
        static XAMPP::Storage<int>* dec_NJetsassociated02 = m_XAMPPInfo->GetVariableStorage<int>("N_associated_Jets02");
        static XAMPP::Storage<int>* dec_NJetsnotassociated02 = m_XAMPPInfo->GetVariableStorage<int>("N_not_associated_Jets02");
        static XAMPP::Storage<int>* dec_NJets10 = m_XAMPPInfo->GetVariableStorage<int>("N_Jets10");

        static XAMPP::Storage<float>* dec_JetBLight1BPt = m_XAMPPInfo->GetVariableStorage<float>("Jet_BLight1BPt");
        static XAMPP::Storage<float>* dec_JetBLight2BPt = m_XAMPPInfo->GetVariableStorage<float>("Jet_BLight2BPt");

        static XAMPP::Storage<bool>* dec_JetBLight1isBjet = m_XAMPPInfo->GetVariableStorage<bool>("Jet_BLight1isBjet");
        static XAMPP::Storage<bool>* dec_JetBLight2isBjet = m_XAMPPInfo->GetVariableStorage<bool>("Jet_BLight2isBjet");

        static XAMPP::Storage<int>* dec_JetBLight1TruthLabel = m_XAMPPInfo->GetVariableStorage<int>("Jet_BLight1TruthLabel");
        static XAMPP::Storage<int>* dec_JetBLight2TruthLabel = m_XAMPPInfo->GetVariableStorage<int>("Jet_BLight2TruthLabel");

        static XAMPP::Storage<bool>* dec_TrackJetBLight1isBjet = m_XAMPPInfo->GetVariableStorage<bool>("TrackJet_1isBjet");
        static XAMPP::Storage<bool>* dec_TrackJetBLight2isBjet = m_XAMPPInfo->GetVariableStorage<bool>("TrackJet_2isBjet");

        static XAMPP::Storage<bool>* dec_TrackJetBLight1passOR = m_XAMPPInfo->GetVariableStorage<bool>("TrackJet_1passOR");
        static XAMPP::Storage<bool>* dec_TrackJetBLight2passOR = m_XAMPPInfo->GetVariableStorage<bool>("TrackJet_2passOR");

        static XAMPP::Storage<int>* dec_TrackJetBLight1TruthLabel = m_XAMPPInfo->GetVariableStorage<int>("TrackJet_1TruthLabel");
        static XAMPP::Storage<int>* dec_TrackJetBLight2TruthLabel = m_XAMPPInfo->GetVariableStorage<int>("TrackJet_2TruthLabel");

        static XAMPP::Storage<float>* dec_m_jj = m_XAMPPInfo->GetVariableStorage<float>("m_jj");
        static XAMPP::Storage<float>* dec_m_J = m_XAMPPInfo->GetVariableStorage<float>("m_J");

        static XAMPP::Storage<float>* dec_pt_jj = m_XAMPPInfo->GetVariableStorage<float>("pt_jj");
        static XAMPP::Storage<float>* dec_eta_jj = m_XAMPPInfo->GetVariableStorage<float>("eta_jj");
        static XAMPP::Storage<float>* dec_phi_jj = m_XAMPPInfo->GetVariableStorage<float>("phi_jj");

        static XAMPP::Storage<float>* dec_pt_mu1 = m_XAMPPInfo->GetVariableStorage<float>("pt_mu1");
        static XAMPP::Storage<float>* dec_pt_e1 = m_XAMPPInfo->GetVariableStorage<float>("pt_e1");
        static XAMPP::Storage<float>* dec_m_ll = m_XAMPPInfo->GetVariableStorage<float>("m_ll");
        static XAMPP::Storage<float>* dec_pt_ll = m_XAMPPInfo->GetVariableStorage<float>("pt_ll");

        // multijet reduction variables
        static XAMPP::Storage<float>* dec_AbsDeltaPhiMin3 = m_XAMPPInfo->GetVariableStorage<float>("AbsDeltaPhiMin3");
        static XAMPP::Storage<float>* dec_DeltaPhiMin3 = m_XAMPPInfo->GetVariableStorage<float>("DeltaPhiMin3");

        // mT calculated with MET and closest / furthest b-jet
        static XAMPP::Storage<float>* dec_mT_METclosestBJet = m_XAMPPInfo->GetVariableStorage<float>("mT_METclosestBJet");
        static XAMPP::Storage<float>* dec_mT_METfurthestBJet = m_XAMPPInfo->GetVariableStorage<float>("mT_METfurthestBJet");

        // Output helpers for all our muon corrected branches
        static std::map<std::string, DijetXAMPPOutput> muonCorrDijets = makeResolvedMuonCorrDijets(m_XAMPPInfo);
        static std::map<std::string, P4XAMPPOutput> muonCorrLargeRJets = makeMergedMuonCorrLRJets(m_XAMPPInfo);

        /////////////////////////
        // variables used in CONF
        // adding HtRatio variables for resolved channel
        static XAMPP::Storage<float>* dec_JetHtRatioResolved;
        // adding HtRatioMerged
        static XAMPP::Storage<float>* dec_JetHtRatioMerged;
        // sum of jet pts, cut due to some trigger related issue inherited from run 1
        static XAMPP::Storage<float>* dec_sigjet012ptsum;
        // event based METsig
        static XAMPP::Storage<float>* dec_METsig;

        static XAMPP::Storage<float>* dec_AbsDeltaPhiJJ;
        static XAMPP::Storage<float>* dec_DeltaPhiJJ;
        static XAMPP::Storage<float>* dec_AbsDeltaPhiMetJJ;
        static XAMPP::Storage<float>* dec_DeltaPhiMetJJ;
        static XAMPP::Storage<float>* dec_DeltaRJJ;
        if (m_doCONFvariables) {
            dec_JetHtRatioResolved = m_XAMPPInfo->GetVariableStorage<float>("HtRatioResolved");
            dec_JetHtRatioMerged = m_XAMPPInfo->GetVariableStorage<float>("HtRatioMerged");
            dec_sigjet012ptsum = m_XAMPPInfo->GetVariableStorage<float>("sigjet012ptsum");
            dec_METsig = m_XAMPPInfo->GetVariableStorage<float>("METsig");
            dec_AbsDeltaPhiJJ = m_XAMPPInfo->GetVariableStorage<float>("AbsDeltaPhiJJ");
            dec_DeltaPhiJJ = m_XAMPPInfo->GetVariableStorage<float>("DeltaPhiJJ");
            dec_AbsDeltaPhiMetJJ = m_XAMPPInfo->GetVariableStorage<float>("AbsDeltaPhiMetJJ");
            dec_DeltaPhiMetJJ = m_XAMPPInfo->GetVariableStorage<float>("DeltaPhiMetJJ");
            dec_DeltaRJJ = m_XAMPPInfo->GetVariableStorage<float>("DeltaRJJ");
        }

        static XAMPP::Storage<float>* dec_XbbScoreHiggs;
        static XAMPP::Storage<float>* dec_XbbScoreQCD;
        static XAMPP::Storage<float>* dec_XbbScoreTop;
        if (m_doXbbScore) {
            dec_XbbScoreHiggs = m_XAMPPInfo->GetVariableStorage<float>("XbbScoreHiggs");
            dec_XbbScoreQCD = m_XAMPPInfo->GetVariableStorage<float>("XbbScoreQCD");
            dec_XbbScoreTop = m_XAMPPInfo->GetVariableStorage<float>("XbbScoreTop");
        }

        static XAMPP::Storage<float>* dec_NNXbbScoreHiggs;
        static XAMPP::Storage<float>* dec_NNXbbScoreQCD;
        static XAMPP::Storage<float>* dec_NNXbbScoreTop;
        static XAMPP::Storage<float>* dec_NNXbb_CombScore_25;
        static XAMPP::Storage<bool>* dec_IsTaggedByNNXbb_25_70;
        if (m_doNNXbbScore) {
            dec_NNXbbScoreHiggs = m_XAMPPInfo->GetVariableStorage<float>("NNXbbScoreHiggs");
            dec_NNXbbScoreQCD = m_XAMPPInfo->GetVariableStorage<float>("NNXbbScoreQCD");
            dec_NNXbbScoreTop = m_XAMPPInfo->GetVariableStorage<float>("NNXbbScoreTop");
            dec_NNXbb_CombScore_25 = m_XAMPPInfo->GetVariableStorage<float>("NNXbb_CombScore_25");
            dec_IsTaggedByNNXbb_25_70 = m_XAMPPInfo->GetVariableStorage<bool>("IsTaggedByNNXbb_25_70");
        }
#ifdef USE_XGBOOST
        // training variables and BDT scores
        static XAMPP::Storage<float>* dec_XGB_resolved = 0;
        static XAMPP::Storage<float>* dec_XGB_merged = 0;
        static XAMPP::Storage<int>* dec_PCSj1 = 0;
        static XAMPP::Storage<int>* dec_PCSj2 = 0;
        static XAMPP::Storage<int>* dec_PCStj1 = 0;
        static XAMPP::Storage<int>* dec_PCStj2 = 0;
        if (m_doBDT && !dec_XGB_resolved) {
            dec_XGB_resolved = m_XAMPPInfo->GetVariableStorage<float>("XGB_resolved");
            dec_XGB_merged = m_XAMPPInfo->GetVariableStorage<float>("XGB_merged");
            dec_PCSj1 = m_XAMPPInfo->GetVariableStorage<int>("PCSj1");
            dec_PCSj2 = m_XAMPPInfo->GetVariableStorage<int>("PCSj2");
            dec_PCStj1 = m_XAMPPInfo->GetVariableStorage<int>("PCStj1");
            dec_PCStj2 = m_XAMPPInfo->GetVariableStorage<int>("PCStj2");
        }
#endif  //> !USE_XGBOOST

        xAOD::JetContainer* FatJets = m_jet_selection->GetBaselineJets(10);
        xAOD::JetContainer* TrackJets = m_jet_selection->GetBaselineJets(2);
        xAOD::MuonContainer* BaselineMuons = m_muon_selection->GetBaselineMuons();
        xAOD::MuonContainer* SignalMuons = m_muon_selection->GetSignalMuons();
        xAOD::ElectronContainer* PreElectrons = m_electron_selection->GetPreElectrons();  // added for PFlowJets
        xAOD::ElectronContainer* BaselineElectrons = m_electron_selection->GetBaselineElectrons();
        xAOD::ElectronContainer* SignalElectrons = m_electron_selection->GetSignalElectrons();
        xAOD::JetContainer* SignalJets = m_jet_selection->GetSignalJets();
        xAOD::JetContainer* allJets = m_jet_selection->GetCustomJets("centralForward");  // should be: central + forward jets (each sorted
                                                                                         // by pt)
        xAOD::JetContainer* BtagLightJets =
            m_jet_selection->GetCustomJets("btaggedLight");  // should correspond to JetsForHbbReco in CxAOD be: btag
                                                             // + non-b-tag jets (each sorted by pt)
        xAOD::JetContainer* ForwardJets = m_jet_selection->GetCustomJets("forward");
        xAOD::JetContainer* BJets = m_jet_selection->GetBJets();

        m_jet1Muons.clear();
        m_jet2Muons.clear();
        m_fatJetMuons.clear();

        // check lepton multiplicities
        int NBaseMuons = BaselineMuons->size();
        int NSignalMuons = SignalMuons->size();
        int NBaseElectrons = BaselineElectrons->size();
        int NSignalElectrons = SignalElectrons->size();

        int NBaseLeps = NBaseMuons + NBaseElectrons;
        int NLeps = NSignalMuons + NSignalElectrons;

        float pt_mu1 = -1;
        float pt_e1 = -1;
        if (NSignalMuons > 0) pt_mu1 = SignalMuons->at(0)->pt();
        if (NSignalElectrons > 0) pt_e1 = SignalElectrons->at(0)->pt();

        bool isOppositeCharge = false;
        float m_ll = -1.;
        float pt_ll = -1.;
        if (NSignalMuons == 2 && NBaseElectrons == 0) {
            isOppositeCharge = OppositeSign(SignalMuons->at(0), SignalMuons->at(1));
            m_ll = InvariantMass(SignalMuons->at(0), SignalMuons->at(1));
            pt_ll = (SignalMuons->at(0)->p4() + SignalMuons->at(1)->p4()).Pt();
        }

        if (NSignalElectrons == 2 && NBaseMuons == 0) {
            isOppositeCharge = OppositeSign(SignalElectrons->at(0), SignalElectrons->at(1));
            m_ll = InvariantMass(SignalElectrons->at(0), SignalElectrons->at(1));
            pt_ll = (SignalElectrons->at(0)->p4() + SignalElectrons->at(1)->p4()).Pt();
        }

        if (NSignalMuons == 1 && NBaseMuons == 1 && NSignalElectrons == 1 && NBaseElectrons == 1) {
            isOppositeCharge = OppositeSign(SignalElectrons->at(0), SignalMuons->at(0));
        }

        // check tau multiplicities
        int NTaus = m_tau_selection->GetSignalTaus()->size();

        // check jet multiplicities
        int NJets04 = SignalJets->size();
        int NAllJets04 = allJets->size();
        int NForwardJets04 = ForwardJets->size();
        int NJets10 = FatJets->size();
        int NBJets04 = BJets->size();

        ///////////////////////////////////////////////////////////////////////////
        // get number of associated track jets and non-associated (b-tagged) track jets
        int N_associated_02 = 0;
        int N_BTags_associated_02 = 0;
        int N_not_associated_02 = 0;
        int N_BTags_not_associated_02 = 0;
        for (const auto& ijet : *TrackJets) {
            char isBJet = false;
            GetJetDecorations()->isBJet.get(ijet, isBJet);

            if (acc_isAssociated(*ijet)) {
                ++N_associated_02;
                if (isBJet && N_associated_02 < 3) ++N_BTags_associated_02;
            } else {
                ++N_not_associated_02;
                if (isBJet) ++N_BTags_not_associated_02;
            }
        }

        //////////////////////////////////////////////////////
        // Debug messages for all physics objects in the event
        if (msgLevel(MSG::DEBUG)) {
            ATH_MSG_INFO("*************************************************");
            ATH_MSG_INFO("Check event: " << m_XAMPPInfo->eventNumber());
            ATH_MSG_INFO("Check systematic: " << m_XAMPPInfo->GetSystematic()->name());
            // MET
            ATH_MSG_INFO("MET: " << MetTSTProxy->met() / 1000.);
            // Leptons
            ATH_MSG_INFO("NBaseMuons: " << NBaseMuons << " NSignalMuons: " << NSignalMuons << " NBaseElectrons: " << NBaseElectrons
                                        << " NSignalElectrons: " << NSignalElectrons);
            ATH_MSG_INFO("Baseline muons:");
            for (const auto& baseMu : *BaselineMuons) { PromptParticle(baseMu); }
            ATH_MSG_INFO("Signal muons:");
            for (const auto& signalMu : *SignalMuons) { PromptParticle(signalMu); }
            ATH_MSG_INFO("Baseline electrons:");
            for (const auto& baseE : *BaselineElectrons) { PromptParticle(baseE); }
            ATH_MSG_INFO("Signal electrons:");
            for (const auto& signalE : *SignalElectrons) { PromptParticle(signalE); }
            // Taus
            ATH_MSG_INFO("NTaus: " << NTaus);
            ATH_MSG_INFO("Signal taus:");
            for (const auto& tau : *m_tau_selection->GetSignalTaus()) { PromptParticle(tau); }
            // Jets
            ATH_MSG_INFO("NJetsCentral: " << NJets04 << " NBJets: " << NBJets04 << " NForwardJets: " << NForwardJets04
                                          << " NJets10: " << NJets10);
            ATH_MSG_INFO("Signal jets:");
            for (const auto& ijet : *SignalJets) {
                char isBJet = false;
                double bTagWeight = -1e9;
                GetJetDecorations()->isBJet.get(ijet, isBJet);
                GetJetDecorations()->BTagWeight.get(ijet, bTagWeight);
                std::string addInfo = (boost::format("Signal jet is b-jet: %1% bTagWeight: %2%") % isBJet % bTagWeight).str();
                PromptParticle(ijet, addInfo);
            }
            ATH_MSG_INFO("Forward jets:");
            for (const auto& ijet : *ForwardJets) { PromptParticle(ijet); }
            ATH_MSG_INFO("Large-R jets:");
            for (const auto& ijet : *FatJets) { PromptParticle(ijet); }
            // Track jets
            ATH_MSG_INFO("Track jets:");
            for (const auto& ijet : *TrackJets) {
                char isBJet = false;
                double bTagWeight = -1e9;
                char isAssociated = acc_isAssociated(*ijet);
                char passDRcut = acc_passDRcut(*ijet);
                GetJetDecorations()->isBJet.get(ijet, isBJet);
                GetJetDecorations()->BTagWeight.get(ijet, bTagWeight);
                std::string addInfo = (boost::format("Track jet is b-jet: %1% bTagWeight: %2% is associated: %3% passDRcut: %4%") % isBJet %
                                       bTagWeight % isAssociated % passDRcut)
                                          .str();
                PromptParticle(ijet, addInfo);
            }
            ATH_MSG_INFO("*************************************************");
        }

        /////////////////////////
        // bug fix for PFlow jets
        if (m_doPFlowCleaning) {
            // check for the decorators
            static XAMPP::Storage<int>* dec_DFCommonCut = m_XAMPPInfo->GetVariableStorage<int>("DFCommonCut");
            int DfCommon = 1;

            for (const auto& elec : *PreElectrons) {
                if (acc_DFCommonCrackVetoCleaning.isAvailable(*elec) && !acc_DFCommonCrackVetoCleaning(*elec)) {
                    DfCommon = 0;  // set the flag to false for this event
                    break;
                }
            }

            ATH_CHECK(dec_DFCommonCut->Store(DfCommon));
        }

        ///////////////////////////////////////////
        // get properties of leading two track jets
        bool TrackJetBLight1isBjet = false;
        bool TrackJetBLight2isBjet = false;

        bool TrackJetBLight1passOR = true;
        bool TrackJetBLight2passOR = true;

        int TrackJetBLight1TruthLabel = 0;
        int TrackJetBLight2TruthLabel = 0;

        std::vector<const xAOD::Jet*> LeadingTwoAssoTrackJets;
        LeadingTwoAssoTrackJets.clear();

        // this variable is used in the calculation of other variables and is not saved to the ntuple
        int NAssTrackJets = 0;

        for (const auto& ijet : *TrackJets) {
            if (!acc_isAssociated(*ijet)) continue;
            if (NAssTrackJets > 1) break;
            LeadingTwoAssoTrackJets.push_back(ijet);

            char isBJet = false;
            GetJetDecorations()->isBJet.get(ijet, isBJet);

            if (NAssTrackJets == 0) {
                TrackJetBLight1isBjet = isBJet;
                if (acc_HadronConeExclTruthLabelID.isAvailable(*ijet) && !isData())
                    TrackJetBLight1TruthLabel = (int)(acc_HadronConeExclTruthLabelID(*ijet));
                if (acc_passDRcut.isAvailable(*ijet)) TrackJetBLight1passOR = acc_passDRcut(*ijet);
            }
            if (NAssTrackJets == 1) {
                TrackJetBLight2isBjet = isBJet;
                if (acc_HadronConeExclTruthLabelID.isAvailable(*ijet) && !isData())
                    TrackJetBLight2TruthLabel = (int)(acc_HadronConeExclTruthLabelID(*ijet));
                if (acc_passDRcut.isAvailable(*ijet)) TrackJetBLight2passOR = acc_passDRcut(*ijet);
            }
            ++NAssTrackJets;
        }

        ///////////////////////////////////////////////////////////////////////////////
        // Compute min dPhi between the leading three (central and fwd) jets and MET
        // //
        float DeltaPhiMin3 = ComputeDPhiMin(allJets, MetTSTProxy, 3);
        if (allJets->size() && DeltaPhiMin3 == -FLT_MAX) return StatusCode::FAILURE;

        ////////////////////////////////////////////

        float DeltaPhiJJ = -99.;
        float DeltaPhiMetJJ = -99.;
        float DeltaRJJ = -99.;

        if (BtagLightJets->size() > 1) {
            ////////////////////////////////////////////////////////////////////////////////////////////////
            // Compute dPhi between the two higgs candidate jets (one might b-tagged the
            // other might not) //
            DeltaPhiJJ = xAOD::P4Helpers::deltaPhi(BtagLightJets->at(0), BtagLightJets->at(1));
            //////////////////////////////////////////////////
            // Compute dPhi between MET and higgs candidate //
            DeltaPhiMetJJ = xAOD::P4Helpers::deltaPhi((BtagLightJets->at(0)->p4() + BtagLightJets->at(1)->p4()).Phi(), MetTSTProxy->phi());
            ////////////////////////////////////////////////////////////////
            // Compute dR between the two jets making the higgs candidate //
            DeltaRJJ = xAOD::P4Helpers::deltaR(BtagLightJets->at(0), BtagLightJets->at(1), false);
        }

        ////////////////////////////////////////////////////////////
        // Get properties of the two jets making the higgs candidate //
        float JetBLight1BPt = -1.;
        float JetBLight2BPt = -1.;
        if (BtagLightJets->size() > 0) JetBLight1BPt = BtagLightJets->at(0)->pt();
        if (BtagLightJets->size() > 1) JetBLight2BPt = BtagLightJets->at(1)->pt();

        bool JetBLight1isBjet = false;
        bool JetBLight2isBjet = false;

        int JetBLight1TruthLabel = 0;
        int JetBLight2TruthLabel = 0;

        if (BtagLightJets->size() > 0) {
            char isBJet = false;
            GetJetDecorations()->isBJet.get(BtagLightJets->at(0), isBJet);

            JetBLight1isBjet = isBJet;
            if (acc_HadronConeExclTruthLabelID.isAvailable(*BtagLightJets->at(0)) && !isData())
                JetBLight1TruthLabel = (int)(acc_HadronConeExclTruthLabelID(*BtagLightJets->at(0)));

            if (m_writeMuonsInJets)
                for (MuonLink_t& link : acc_muonLinks(*BtagLightJets->at(0)))
                    // cast here is unavoidable. ElementLinks give you const
                    // pointers but XAMPP for some reason reads from non-const
                    // objects when writing to ntuples.
                    m_jet1Muons.push_back(const_cast<xAOD::Muon*>(*link));
        }
        if (BtagLightJets->size() > 1) {
            char isBJet = false;
            GetJetDecorations()->isBJet.get(BtagLightJets->at(1), isBJet);
            JetBLight2isBjet = isBJet;
            if (acc_HadronConeExclTruthLabelID.isAvailable(*BtagLightJets->at(1)) && !isData())
                JetBLight2TruthLabel = (int)(acc_HadronConeExclTruthLabelID(*BtagLightJets->at(1)));
            if (m_writeMuonsInJets)
                for (MuonLink_t& link : acc_muonLinks(*BtagLightJets->at(1))) m_jet2Muons.push_back(const_cast<xAOD::Muon*>(*link));
        }

        //////////////////////////////////////////////////////////////////////
        // Compute invariant mass of the higgs candidate in resolved regime //
        float m_jj = -1;
        float pt_jj = -1;
        float eta_jj = -1;
        float phi_jj = -1;
        for (auto& storePair : muonCorrDijets) storePair.second.setDijetP4(TLorentzVector(), TLorentzVector());
        if (BtagLightJets->size() > 1) {
            TLorentzVector p4_jj = BtagLightJets->at(0)->p4() + BtagLightJets->at(1)->p4();
            pt_jj = p4_jj.Pt();
            eta_jj = p4_jj.Eta();
            phi_jj = p4_jj.Phi();
            m_jj = p4_jj.M();
            for (auto& storePair : muonCorrDijets)
                storePair.second.setDijetP4(
                    JetBLight1isBjet ? GetTLV(BtagLightJets->at(0)->jetP4(storePair.first)) : BtagLightJets->at(0)->p4(),
                    JetBLight2isBjet ? GetTLV(BtagLightJets->at(1)->jetP4(storePair.first)) : BtagLightJets->at(1)->p4());
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////
        // Get mass of leading large-R jet -> higgs candidate in merged regime; get XbbScore variables //
        float m_J = -1;
        float XbbScoreHiggs = -1;
        float XbbScoreQCD = -1;
        float XbbScoreTop = -1;
        float NNXbbScoreHiggs = -1;
        float NNXbbScoreQCD = -1;
        float NNXbbScoreTop = -1;
        float NNXbb_CombScore_25 = -1;
        bool IsTaggedByNNXbb_25_70 = false;
        for (auto& storePair : muonCorrLargeRJets) storePair.second.setP4(TLorentzVector());
        if (NJets10 > 0) {
            m_J = FatJets->at(0)->m();
            if (m_doXbbScore) {
                if (acc_XbbScoreHiggs.isAvailable(*FatJets->at(0))) XbbScoreHiggs = acc_XbbScoreHiggs(*FatJets->at(0));
                if (acc_XbbScoreQCD.isAvailable(*FatJets->at(0))) XbbScoreQCD = acc_XbbScoreQCD(*FatJets->at(0));
                if (acc_XbbScoreTop.isAvailable(*FatJets->at(0))) XbbScoreTop = acc_XbbScoreTop(*FatJets->at(0));
            }
            if (m_doNNXbbScore) {
                if (acc_NNXbbScoreHiggs.isAvailable(*FatJets->at(0))) NNXbbScoreHiggs = acc_NNXbbScoreHiggs(*FatJets->at(0));
                if (acc_NNXbbScoreQCD.isAvailable(*FatJets->at(0))) NNXbbScoreQCD = acc_NNXbbScoreQCD(*FatJets->at(0));
                if (acc_NNXbbScoreTop.isAvailable(*FatJets->at(0))) NNXbbScoreTop = acc_NNXbbScoreTop(*FatJets->at(0));
                if (acc_NNXbb_CombScore_25.isAvailable(*FatJets->at(0))) NNXbb_CombScore_25 = acc_NNXbb_CombScore_25(*FatJets->at(0));
                if (acc_IsTaggedByNNXbb_25_70.isAvailable(*FatJets->at(0)))
                    IsTaggedByNNXbb_25_70 = acc_IsTaggedByNNXbb_25_70(*FatJets->at(0));
            }
            for (auto& storePair : muonCorrLargeRJets) storePair.second.setP4(GetTLV(FatJets->at(0)->jetP4(storePair.first)));
            if (m_writeMuonsInJets)
                for (MuonLink_t& link : acc_muonLinks(*FatJets->at(0))) m_fatJetMuons.push_back(const_cast<xAOD::Muon*>(*link));
        }

        ////////////////////////////////
        // Extended tau veto resolved //
        int NTausExtended_Reso = 0;

        if (BtagLightJets->size() > 0) {
            for (const auto& ijet : *BtagLightJets) {
                // first condition on number of tracks inside the 'jet' (= tauish jet)
                int NTrks = 0;
                GetJetDecorations()->nTracks.get(ijet, NTrks);
                if (NTrks < 1) continue;
                if (NTrks > 4) continue;

                // second condition
                float AbsdPhi_MET_jet = fabs(xAOD::P4Helpers::deltaPhi(ijet->phi(), MetTSTProxy->phi()));  // already rad
                if (AbsdPhi_MET_jet <= TMath::Pi() / 8.) NTausExtended_Reso++;
            }
        }

        ///////////////////////////
        // Extended tau veto merged
        float Ht_nonAssJets = 0;
        int N_TausExtended_Merged = 0;
        int NonAssJets = 0;

        if (NJets10 > 0 && allJets->size() > 0) {
            // loop over all jets
            for (const auto& ijet : *allJets) {
                // calculate dR of the leadingFatJet and each small-R jet
                // third arcgument of xAOD::P4Helpers::deltaR() is need to use
                // pseudorapidity instead of rapidity
                if (xAOD::P4Helpers::deltaR(FatJets->at(0), ijet, false) < 1.0) continue;
                NonAssJets++;
                if (NonAssJets > 0) Ht_nonAssJets += ijet->pt();
                // Extended Tau Veto
                int NTrks = 0;
                GetJetDecorations()->nTracks.get(ijet, NTrks);
                float AbsdPhi_MET_nonass_jet = fabs(xAOD::P4Helpers::deltaPhi(ijet->phi(), MetTSTProxy->phi()));  // already rad
                if (NTrks >= 1 && NTrks <= 4 && AbsdPhi_MET_nonass_jet <= TMath::Pi() / 8.) N_TausExtended_Merged++;
            }
        }
        //////////////////////////////////////////////////////////////////////////////
        // Transverse mass calculated with MET and closest / furthest small-R b-jet //
        const xAOD::IParticle* closestBJet = nullptr;
        float mT_METclosestBJet = 0.;
        const xAOD::IParticle* furthestBJet = nullptr;
        float mT_METfurthestBJet = 0.;
        if (NBJets04 > 0) {
            closestBJet = GetClosestParticle(BJets, MetTSTProxy);
            mT_METclosestBJet = ComputeMt(closestBJet, MetTSTProxy);
            furthestBJet = GetFarestParticle(BJets, MetTSTProxy);
            mT_METfurthestBJet = ComputeMt(furthestBJet, MetTSTProxy);
        }

        ////////////////////////////////////////
        // Variables used only in CONF selection
        float sigjet012ptsum = -99.;
        float Ht_ratio_resolved = 0;
        float Ht_ratio_merged = 0;
        float METsig = -1;

        if (m_doCONFvariables) {
            ///////////////////////////////////////////////////
            // Compute pt sum of the leading 2/3 signal jets //
            if (allJets->size() == 2)
                sigjet012ptsum = allJets->at(0)->pt() + allJets->at(1)->pt();
            else if (allJets->size() > 2)
                sigjet012ptsum = allJets->at(0)->pt() + allJets->at(1)->pt() + allJets->at(2)->pt();

            ///////////////////////////
            // Ht ratio cut resolved //
            int iter_jet = 0;      // iterater to stop at 3 jets
            float Ht = 0;          // Ht total of all small-R jets
            float Ht_noHiggs = 0;  // Ht without Higgs candidate

            Ht = CalculateHt(allJets, 0.);
            if (allJets->size() > 3) {
                for (const auto& jets_ht : *allJets) {
                    iter_jet++;
                    if (iter_jet < 4) continue;
                    Ht_noHiggs += jets_ht->pt();  // starting from the 4th jet in the event: sum (4,\infty)
                }
            }
            if (Ht != 0) Ht_ratio_resolved = Ht_noHiggs / Ht;
            ATH_MSG_DEBUG("Ht_ratio_resolved: " << Ht_ratio_resolved);

            float sumET = 0;
            sumET += Ht;

            if (NSignalMuons == 2)
                sumET += CalculateHt(SignalMuons, 0.);
            else if (NSignalElectrons == 2)
                sumET += CalculateHt(SignalElectrons, 0.);

            // classic MET significance used in CR2 for CONF. Needs normal MET as input and not MET proxy
            if (sumET != 0) METsig = (dec_XAMPPmetTST->GetValue()->met() / 1000.) / sqrt((sumET / 1000.));

            /////////////////////////
            // Ht ratio cut merged //
            if (NJets10 > 0) Ht_ratio_merged = Ht_nonAssJets / (Ht_nonAssJets + FatJets->at(0)->pt());
            ATH_MSG_DEBUG("Ht_ratio_merged: " << Ht_ratio_merged);
        }

        /////////////
        // training varialbes
        int PCSj1 = -999;
        int PCSj2 = -999;
        int PCStj1 = -999;
        int PCStj2 = -999;
        float fatjet_Pt = -999;
        float fatjet_Eta = -999;
        float DeltaPhiMetJ10 = -999;
        float XGB_resolved = -999;
        float XGB_merged = -999;
#ifdef USE_XGBOOST
        if (m_doBDT) {
            if (BtagLightJets->size() >= 2) {
                TLorentzVector dijet = BtagLightJets->at(0)->p4() + BtagLightJets->at(1)->p4();
                double jet1_BTagWeight = -1e9;
                double jet2_BTagWeight = -1e9;
                GetJetDecorations()->BTagWeight.get(BtagLightJets->at(0), jet1_BTagWeight);
                GetJetDecorations()->BTagWeight.get(BtagLightJets->at(1), jet2_BTagWeight);
                PCSj1 = PCS(jet1_BTagWeight);
                PCSj2 = PCS(jet2_BTagWeight);
            }
            if (N_associated_02 >= 2) {
                double trackjet1_BTagWeight = -1e9;
                double trackjet2_BTagWeight = -1e9;
                GetJetDecorations()->BTagWeight.get(TrackJets->at(0), trackjet1_BTagWeight);
                GetJetDecorations()->BTagWeight.get(TrackJets->at(1), trackjet2_BTagWeight);
                PCStj1 = PCS_tj(trackjet1_BTagWeight);
                PCStj2 = PCS_tj(trackjet2_BTagWeight);
            }
            if (NJets10 > 0) {
                fatjet_Pt = FatJets->at(0)->pt();
                fatjet_Eta = FatJets->at(0)->eta();
                DeltaPhiMetJ10 = xAOD::P4Helpers::deltaPhi(FatJets->at(0)->phi(), MetTSTProxy->phi());
            }

            // making XGBoost DMatrix and get the BDT score for each event
            std::vector<float> vars_resolved{
                pt_jj, eta_jj, (float)MetTSTProxy->met(), DeltaPhiMetJJ, (float)NJets04, Ht_ratio_resolved, (float)PCSj1, (float)PCSj2};
            std::vector<float> vars_merged{(float)MetTSTProxy->met(),
                                           DeltaPhiMetJ10,
                                           (float)NJets04,
                                           0,
                                           Ht_ratio_merged,
                                           (float)PCStj1,
                                           (float)PCStj2,
                                           fatjet_Pt,
                                           fatjet_Eta};
            int event_number = m_XAMPPInfo->eventNumber();
            XGB_resolved = m_XGB_resolved.get_XGBoost_Weight(vars_resolved, event_number);  // get score
            XGB_merged = m_XGB_merged.get_XGBoost_Weight(vars_merged, event_number);
        }
#endif  //> USE_XGBOOST

        // Finally store the variables they are then used by the Cut Class or just
        // written out into the trees

        ATH_CHECK(dec_AbsDeltaPhiMin3->Store(fabs(DeltaPhiMin3)));
        ATH_CHECK(dec_DeltaPhiMin3->Store(DeltaPhiMin3));
        // adding Tau Veto Reso
        ATH_CHECK(dec_ExtendedTauVetoRes->Store(NTausExtended_Reso));
        // adding Tau Veto Merged
        ATH_CHECK(dec_ExtendedTauVetoMerged->Store(N_TausExtended_Merged));

        ATH_CHECK(dec_NBJets04->Store(NBJets04));
        ATH_CHECK(dec_N_BTags_associated_02->Store(N_BTags_associated_02));
        ATH_CHECK(dec_BJetVetoMerged->Store(N_BTags_not_associated_02));  // adding BJetVeto merged
        ATH_CHECK(dec_NLeps->Store(NLeps));
        ATH_CHECK(dec_NBaseLeps->Store(NBaseLeps));
        ATH_CHECK(dec_NElec->Store(NSignalElectrons));
        ATH_CHECK(dec_NBaseElec->Store(NBaseElectrons));
        ATH_CHECK(dec_NMuon->Store(NSignalMuons));
        ATH_CHECK(dec_NBaseMuon->Store(NBaseMuons));
        ATH_CHECK(dec_NTaus->Store(NTaus));
        ATH_CHECK(dec_NJets04->Store(NJets04));
        ATH_CHECK(dec_NAllJets04->Store(NAllJets04));
        ATH_CHECK(dec_NForwardJets04->Store(NForwardJets04));
        ATH_CHECK(dec_NJetsassociated02->Store(N_associated_02));
        ATH_CHECK(dec_NJetsnotassociated02->Store(N_not_associated_02));
        ATH_CHECK(dec_NJets10->Store(NJets10));

        ATH_CHECK(dec_JetBLight1BPt->Store(JetBLight1BPt));
        ATH_CHECK(dec_JetBLight2BPt->Store(JetBLight2BPt));
        ATH_CHECK(dec_JetBLight1isBjet->Store(JetBLight1isBjet));
        ATH_CHECK(dec_JetBLight2isBjet->Store(JetBLight2isBjet));
        ATH_CHECK(dec_JetBLight1TruthLabel->Store(JetBLight1TruthLabel));
        ATH_CHECK(dec_JetBLight2TruthLabel->Store(JetBLight2TruthLabel));

        ATH_CHECK(dec_TrackJetBLight1isBjet->Store(TrackJetBLight1isBjet));
        ATH_CHECK(dec_TrackJetBLight2isBjet->Store(TrackJetBLight2isBjet));
        ATH_CHECK(dec_TrackJetBLight1TruthLabel->Store(TrackJetBLight1TruthLabel));
        ATH_CHECK(dec_TrackJetBLight2TruthLabel->Store(TrackJetBLight2TruthLabel));
        ATH_CHECK(dec_TrackJetBLight1passOR->Store(TrackJetBLight1passOR));
        ATH_CHECK(dec_TrackJetBLight2passOR->Store(TrackJetBLight2passOR));

        ATH_CHECK(dec_m_jj->Store(m_jj));
        ATH_CHECK(dec_m_J->Store(m_J));

        ATH_CHECK(dec_pt_jj->Store(pt_jj));
        ATH_CHECK(dec_eta_jj->Store(eta_jj));
        ATH_CHECK(dec_phi_jj->Store(phi_jj));

        ATH_CHECK(dec_isOppositeCharge->Store(isOppositeCharge));
        ATH_CHECK(dec_pt_mu1->Store(pt_mu1));
        ATH_CHECK(dec_pt_e1->Store(pt_e1));
        ATH_CHECK(dec_m_ll->Store(m_ll));
        ATH_CHECK(dec_pt_ll->Store(pt_ll));

        ATH_CHECK(dec_mT_METclosestBJet->Store(mT_METclosestBJet));
        ATH_CHECK(dec_mT_METfurthestBJet->Store(mT_METfurthestBJet));

        if (m_doCONFvariables) {
            ATH_CHECK(dec_METsig->Store(METsig));
            ATH_CHECK(dec_sigjet012ptsum->Store(sigjet012ptsum));
            ATH_CHECK(dec_AbsDeltaPhiJJ->Store(fabs(DeltaPhiJJ)));
            ATH_CHECK(dec_DeltaPhiJJ->Store(DeltaPhiJJ));
            ATH_CHECK(dec_AbsDeltaPhiMetJJ->Store(fabs(DeltaPhiMetJJ)));
            ATH_CHECK(dec_DeltaPhiMetJJ->Store(DeltaPhiMetJJ));
            ATH_CHECK(dec_DeltaRJJ->Store(DeltaRJJ));
            // adding Ht resolved
            ATH_CHECK(dec_JetHtRatioResolved->Store(Ht_ratio_resolved));
            // adding Ht merged
            ATH_CHECK(dec_JetHtRatioMerged->Store(Ht_ratio_merged));
        }

#ifdef USE_XGBOOST
        ///////
        // store training variables and XGBoost BDT outputs
        if (m_doBDT) {
            ATH_CHECK(dec_XGB_resolved->Store(XGB_resolved));
            ATH_CHECK(dec_XGB_merged->Store(XGB_merged));
            ATH_CHECK(dec_PCSj1->Store(PCSj1));
            ATH_CHECK(dec_PCSj2->Store(PCSj2));
            ATH_CHECK(dec_PCStj1->Store(PCStj1));
            ATH_CHECK(dec_PCStj2->Store(PCStj2));
        }
#endif  //> USE_XGBOOST

        /////////////////
        // store XbbScore
        if (m_doXbbScore) {
            ATH_CHECK(dec_XbbScoreHiggs->Store(XbbScoreHiggs));
            ATH_CHECK(dec_XbbScoreQCD->Store(XbbScoreQCD));
            ATH_CHECK(dec_XbbScoreTop->Store(XbbScoreTop));
        }

        if (m_doNNXbbScore) {
            ATH_CHECK(dec_NNXbbScoreHiggs->Store(NNXbbScoreHiggs));
            ATH_CHECK(dec_NNXbbScoreQCD->Store(NNXbbScoreQCD));
            ATH_CHECK(dec_NNXbbScoreTop->Store(NNXbbScoreTop));
            ATH_CHECK(dec_NNXbb_CombScore_25->Store(NNXbb_CombScore_25));
            ATH_CHECK(dec_IsTaggedByNNXbb_25_70->Store(IsTaggedByNNXbb_25_70));
        }
        /////////////////////////
        // Store muon corrections
        for (auto& storePair : muonCorrDijets) ATH_CHECK(storePair.second.push());
        for (auto& storePair : muonCorrLargeRJets) ATH_CHECK(storePair.second.push());

        if (m_SMShapeWeights && !isData()) {
            const FourMomentumContainer& zBosonMomenta =
                (dynamic_cast<MonoHTruthSelector*>(&*m_truth_selection))->GetTruthZbosonMomenta();  // Bad... Let's discuss better options.
                                                                                                    // PR.
            const FourMomentumContainer& topQuarkMomenta =
                (dynamic_cast<MonoHTruthSelector*>(&*m_truth_selection))->GetTruthTopQuarkMomenta();  // Bad... Let's discuss better
                                                                                                      // options. PR.
            const FourMomentumContainer& wBosonMomenta =
                (dynamic_cast<MonoHTruthSelector*>(&*m_truth_selection))->GetTruthWbosonMomenta();  // Bad... Let's discuss better options.
                                                                                                    // PR.

            float truthPt = 0.0f;
            if (zBosonMomenta.size()) truthPt = zBosonMomenta[0].Pt();
            if (wBosonMomenta.size()) truthPt = wBosonMomenta[0].Pt();
            if (topQuarkMomenta.size()) {
                TLorentzVector pTopSum = topQuarkMomenta[0];
                if (topQuarkMomenta.size() > 1) pTopSum += topQuarkMomenta[1];
                truthPt = pTopSum.Pt();
            }

            bool doZeroLepton =
                (m_electron_selection->GetBaselineElectrons()->size() == 0 && m_muon_selection->GetBaselineMuons()->size() == 0);
            // bool doOneLepton already defined above
            bool doTwoElectron =
                (m_electron_selection->GetSignalElectrons()->size() == 2 && m_muon_selection->GetBaselineMuons()->size() == 0);
            bool doTwoMuon = (m_muon_selection->GetSignalMuons()->size() == 2 && m_electron_selection->GetBaselineElectrons()->size() == 0);
            float vPT = MetTSTProxy->met();
            if (doTwoElectron || doTwoMuon) vPT = pt_ll;

            for (auto& corr_sys : m_corrs_syst_weights) {
                ATH_CHECK(m_systematics->setSystematic(corr_sys.first));
                double sf = 1;
                // Call the tool
                if (doZeroLepton)
                    sf = m_sm_shape_weights_tool_0lep->GetSMShapeWeight(vPT, dec_m_jj->GetValue(), truthPt, dec_NJets04->GetValue(),
                                                                        dec_NBJets04->GetValue());
                if (doOneLepton)
                    sf = m_sm_shape_weights_tool_1lep->GetSMShapeWeight(vPT, dec_m_jj->GetValue(), truthPt, dec_NJets04->GetValue(),
                                                                        dec_NBJets04->GetValue());
                if (doTwoElectron || doTwoMuon)
                    sf = m_sm_shape_weights_tool_2lep->GetSMShapeWeight(vPT, dec_m_jj->GetValue(), truthPt, dec_NJets04->GetValue(),
                                                                        dec_NBJets04->GetValue());
                ATH_CHECK(corr_sys.second->Store(sf));
            }
        }

        if (m_doTruthWjets && !isData()) {
            int WjetsCategory = 0;
            static XAMPP::Storage<int>* dec_WjetsDecayCat = m_XAMPPInfo->GetVariableStorage<int>("WjetsDecayCategory");
            for (const auto& particle : (*m_truth_selection->GetTruthInContainer())) {
                if (particle->isW()) WjetsCategory = m_truth_selection->classifyWDecays(particle);
            }
            ATH_CHECK(dec_WjetsDecayCat->Store(WjetsCategory));
        }
        if (m_doTruthTtbar && !isData()) {
            static XAMPP::Storage<int>* dec_topDecayCat = m_XAMPPInfo->GetVariableStorage<int>("topDecayCategory");
            static XAMPP::Storage<int>* dec_antitopDecayCat = m_XAMPPInfo->GetVariableStorage<int>("antiDecayCategory");
            static XAMPP::Storage<int>* dec_ttbarDecayCat = m_XAMPPInfo->GetVariableStorage<int>("ttbarDecayCategory");

            int ttbarCategory = 0;

            const xAOD::TruthParticle* top = nullptr;
            const xAOD::TruthParticle* antitop = nullptr;
            bool topfound = false;
            bool antitopfound = false;

            for (const auto& particle : (*m_truth_selection->GetTruthInContainer())) {
                // if the particle is by any chance a nullptr, skip it
                if (!particle) continue;
                if (particle->pdgId() == 6) {
                    topfound = true;
                    top = particle;
                }
                if (particle->pdgId() == -6) {
                    antitopfound = true;
                    antitop = particle;
                }
            }

            int topCategory = m_truth_selection->classifyTopDecays(top);
            int antitopCategory = m_truth_selection->classifyTopDecays(antitop);

            if (topfound && antitopfound) { ttbarCategory = m_truth_selection->classifyTtbarDecays(top, antitop); }

            ATH_CHECK(dec_topDecayCat->Store(topCategory));
            ATH_CHECK(dec_antitopDecayCat->Store(antitopCategory));
            ATH_CHECK(dec_ttbarDecayCat->Store(ttbarCategory));
        }

        // fill the jet and particle containers to the output trees
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("Jet");
        // if using common tree mechanism we have to write signal jets without overlap removal
        // and separately store for each jet the decorator passOR, which needs to be checked
        // in XAMPPplotting
        if (m_useSystematicGroups)
            ATH_CHECK(JetStore->Fill(m_jet_selection->GetSignalNoORJets()));
        else
            ATH_CHECK(JetStore->Fill(SignalJets));
        static XAMPP::ParticleStorage* ForwardJetStore = m_XAMPPInfo->GetParticleStorage("ForwardJet");
        ATH_CHECK(ForwardJetStore->Fill(ForwardJets));
        static XAMPP::ParticleStorage* FatJetStore = m_XAMPPInfo->GetParticleStorage("FatJet");
        ATH_CHECK(FatJetStore->Fill(FatJets));
        static XAMPP::ParticleStorage* TrackJetStore = m_XAMPPInfo->GetParticleStorage("TrackJet");
        ATH_CHECK(TrackJetStore->Fill(TrackJets));
        // if using common tree mechanism we have to write baseline leptons
        // without overlap removal (those are in XAMPP language the
        // "PreLeptons") and separately store for each lepton the decorator
        // passOR, which needs to be checked in XAMPPplotting
        static XAMPP::ParticleStorage* ElectronStore_Base = m_XAMPPInfo->GetParticleStorage("Electron");
        if (m_useSystematicGroups)
            ATH_CHECK(ElectronStore_Base->Fill(m_electron_selection->GetPreElectrons()));
        else
            ATH_CHECK(ElectronStore_Base->Fill(BaselineElectrons));
        static XAMPP::ParticleStorage* MuonStore_Base = m_XAMPPInfo->GetParticleStorage("Muon");
        if (m_useSystematicGroups)
            ATH_CHECK(MuonStore_Base->Fill(m_muon_selection->GetPreMuons()));
        else
            ATH_CHECK(MuonStore_Base->Fill(BaselineMuons));

        if (m_writeMuonsInJets) {
            static XAMPP::ParticleStorage* s_jet1Muons = m_XAMPPInfo->GetParticleStorage("Jet1Muons");
            s_jet1Muons->Fill(&m_jet1Muons);
            static XAMPP::ParticleStorage* s_jet2Muons = m_XAMPPInfo->GetParticleStorage("Jet2Muons");
            s_jet2Muons->Fill(&m_jet2Muons);
            static XAMPP::ParticleStorage* s_fatJetMuons = m_XAMPPInfo->GetParticleStorage("FatJetMuons");
            s_fatJetMuons->Fill(&m_fatJetMuons);
        }

        return StatusCode::SUCCESS;
    }

    double MonoHAnalysisHelper::getSumW(int dsid, int period_number) {
        const dsid_ident id(dsid, period_number);
        std::map<dsid_ident, double>::const_iterator itr = m_cached_sum_w.find(id);
        if (itr != m_cached_sum_w.end()) return itr->second;
        TH1* prw_histo = m_prw_tool->expert()->GetInputHistogram(dsid, period_number);
        double W = prw_histo ? prw_histo->Integral() : -1;
        m_cached_sum_w.insert(std::pair<dsid_ident, double>(id, W));
        return W;
    }
}  // namespace XAMPP
