#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPmonoH/AnalysisUtils.h>
#include <XAMPPmonoH/MonoHJetSelector.h>

#include "xAODBase/IParticleHelpers.h"

namespace {
    using MuonLink_t = ElementLink<xAOD::MuonContainer>;
    using dec_VecMuonLink_t = SG::AuxElement::Decorator<std::vector<MuonLink_t>>;
    dec_VecMuonLink_t dec_muonLinks("DRAssociatedMuons");
}  // namespace

namespace XAMPP {
    static IntDecorator dec_n_MuonInJet("n_MuonInJet");
    static IntDecorator dec_n_matchedasstrkjets("n_matchedasstrkjets");
    static CharDecorator dec_isAssociated("isAssociated");
    static DoubleDecorator dec_DL1r("DL1r");
    MonoHJetSelector::MonoHJetSelector(std::string myname) :
        SUSYJetSelector(myname),
        m_Kt04_ForwardJetMinEta(2.5),
        m_Kt04_ForwardJetMaxEta(4.5),
        m_Kt04_ForwardJetMinPt(30000.),  // MeV
        m_R10name(""),
        m_R02name(""),
        m_R02linkname(""),
        m_R10PreSelJets(nullptr),
        m_R10BaselineJets(nullptr),
        m_R02PreSelJets(nullptr),
        m_R02BtaggingJets(nullptr),
        m_ForwardJets(nullptr),
        m_CentralAndForwardJets(nullptr),
        m_BTaggedAndLightJets(nullptr),
        m_muon_selection("SUSYMuonSelector"),
        m_muon_selTool(""),
        m_dR_mu_jet(0.4),
        m_mu_min_pt(5000.),
        m_mu_quality(1),
        m_TrkJet_bTagEffTool(""),
        m_trackJetsForBtagging() {
        declareProperty("forwardJetMinEta", m_Kt04_ForwardJetMinEta);
        declareProperty("forwardJetMaxEta", m_Kt04_ForwardJetMaxEta);
        declareProperty("forwardJetMinPt", m_Kt04_ForwardJetMinPt);  // MeV
        declareProperty("LargeRJetContainer", m_R10name);
        declareProperty("TrackJetContainer", m_R02name);
        declareProperty("AssociatedObjectsContainer", m_R02linkname);
        declareProperty("TrackJetBTagEfficiencyTool", m_TrkJet_bTagEffTool);
        declareProperty("MuonSelector", m_muon_selection);
        declareProperty("MuonSelectionTool", m_muon_selTool);
        declareProperty("JetMuonDeltaR", m_dR_mu_jet);
        declareProperty("JetMuonPt", m_mu_min_pt);
        declareProperty("JetMuonQuality", m_mu_quality);
        // For meaning of the muon quality c.f.
        // https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionTool
        // {Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1, 2 and 3 (default
        // is 3 - VeryLoose quality)
    }

    xAOD::JetContainer* MonoHJetSelector::GetPreJets(int Radius) const {
        if (Radius == 2)
            return m_R02PreSelJets;
        else if (Radius == 10)
            return m_R10PreSelJets;
        return SUSYJetSelector::GetPreJets(Radius);
    }
    xAOD::JetContainer* MonoHJetSelector::GetBaselineJets(int Radius) const {
        if (Radius == 2)
            return m_R02BaselineJets;
        else if (Radius == 10)
            return m_R10BaselineJets;
        return SUSYJetSelector::GetBaselineJets(Radius);
    }
    StatusCode MonoHJetSelector::initialize() {
        ATH_CHECK(SUSYJetSelector::initialize());
        std::shared_ptr<ActiveTimer> timer;
        if (m_doTime) timer = m_initTimer.activate();

        ATH_CHECK(m_muon_selection.retrieve());
        if (m_muon_selTool.empty()) m_muon_selTool = GetCPTool<CP::IMuonSelectionTool>("MuonSelectionTool");

        ATH_CHECK(m_TrkJet_bTagEffTool.retrieve());
        ToolHandleSystematics<IBTaggingEfficiencyTool>* SystHandle =
            new ToolHandleSystematics<IBTaggingEfficiencyTool>(m_TrkJet_bTagEffTool);
        ATH_CHECK(SystHandle->initialize());

        BTagJetWeightMap BTagSFs;
        for (auto syst_set : m_systematics->GetWeightSystematics(XAMPP::SelectionObject::BTag)) {
            if (XAMPP::ToolIsAffectedBySystematic(m_TrkJet_bTagEffTool, syst_set)) {
                BTagJetWeight_Ptr BTag(new BTagJetWeight(m_TrkJet_bTagEffTool));
                BTag->SetBJetEtaCut(2.5);
                ATH_CHECK(initIParticleWeight(*BTag, "BTag", syst_set, ScaleFactorMapContains::SignalSf, false, "TrackJet"));
                JetWeightHandler_Ptr Handler(new JetWeightHandler(syst_set));
                BTagSFs[syst_set] = BTag;
                Handler->append(BTagSFs, m_systematics->GetNominal());
                Handler->setupDecorations(GetJetDecorations());
                ATH_CHECK(initIParticleWeight(*Handler, "", syst_set, ScaleFactorMapContains::SignalSf, true, "TrackJet"));
                m_TrackSF.push_back(Handler);
            }
        }

        // initialize HbbTagTool
        m_hbbTagTool = new FlavorTagDiscriminants::HbbTagTool("HbbTagTool_Example");
        ATH_CHECK(m_hbbTagTool->setProperty("nnFile",
                                            "dev/BTagging/202002/SubjetBScore/GhostVR30Rmax4Rmin02TrackJet_BTagging201903/network.json"));
        ATH_CHECK(m_hbbTagTool->initialize());

        return StatusCode::SUCCESS;
    }
    StatusCode MonoHJetSelector::InitialFill(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYJetSelector::InitialFill(systset));

        ATH_CHECK(CalibrateJets(m_R10name, m_R10PreSelJets, "FatJets", JetAlgorithm::AntiKt10));
        ATH_CHECK(CalibrateJets(m_R02name, m_R02PreSelJets, "TrackJets", JetAlgorithm::AntiKt2));

        return StatusCode::SUCCESS;
    }

    bool MonoHJetSelector::PassBaseline(const xAOD::IParticle& P) const {
        if (!SUSYJetSelector::PassBaseline(P)) return false;
        if (isFromAlgorithm(P, JetAlgorithm::AntiKt2)) {
            const xAOD::Jet* jet = dynamic_cast<const xAOD::Jet*>(&P);
            if (!jet) {
                ATH_MSG_ERROR("Something went wrong casting a track-jet");
                return false;
            }
            return (jet->numConstituents() >= 2);
        }
        if (isFromAlgorithm(P, JetAlgorithm::AntiKt4)) {
            return ((fabs(P.eta()) <= m_Kt04_ForwardJetMinEta) ||
                    ((fabs(P.eta()) > m_Kt04_ForwardJetMinEta) && (fabs(P.eta()) < m_Kt04_ForwardJetMaxEta) &&
                     (P.pt() > m_Kt04_ForwardJetMinPt)));
        }
        return true;
    }
    StatusCode MonoHJetSelector::FillJets(const CP::SystematicSet& systset) {
        ATH_CHECK(SUSYJetSelector::FillJets(systset));
        std::shared_ptr<ActiveTimer> timer;
        if (m_doTime) timer = m_fillTimer.activate();
        m_BJets->sort(XAMPP::ptsorter);  // also sort b-jets by their pt
        // fill the forward jets + calculate muon-in-jet correction
        ATH_CHECK(ViewElementsContainer("forward", m_ForwardJets));

        // Get the muons that will be used for the correction
        std::vector<const xAOD::Muon*> nonIsoMuons;
        for (const xAOD::Muon* imu : *m_muon_selection->GetMuons())
            // basic selections on our muons
            if (imu->pt() > m_mu_min_pt && fabs(imu->eta()) < 2.7 &&
                // Reminder - 'lower' quality value => tighter selection
                m_muon_selTool->getQuality(*imu) <= m_mu_quality)
                nonIsoMuons.push_back(imu);

        for (const auto& ijet : *m_PreJets) {
            if ((fabs(ijet->eta()) > 2.5) && (fabs(ijet->eta()) < 4.5)) m_ForwardJets->push_back(ijet);
            dec_n_MuonInJet(*ijet) = 0.;
            dec_DL1r(*ijet) = recalculate_DL1r(*ijet);
            if (!PassSignal(ijet)) continue;
            // count muons in central jets and decorate jet with this number
            int nMuons_closeBy = 0;
            if (m_dR_mu_jet > 0.) {
                double DRCut = m_dR_mu_jet;
                for (const xAOD::Muon* imuon : nonIsoMuons) {
                    // Count number of muons in a jet
                    // apply the a DRCut changing with muon pT, just as in the Overlap
                    // Removal procedure
                    DRCut = std::min((double)m_dR_mu_jet, 0.04 + 10000.0 / imuon->pt());
                    // Overlaps(): include the pseudo rapidity in calculation or it's
                    // rapidity? false -> use pseudo rapidity
                    if (Overlaps(imuon, ijet, DRCut, false)) ++nMuons_closeBy;
                    // end nMuons_closeBy
                }
            }
            dec_n_MuonInJet(*ijet) = nMuons_closeBy;

            // Calculate a few versions of a muon collection.
            // The versions depend on how you select the muons to include in the
            // correction (all muons must fall within DR)
            // - Pick the closest (in DR) muon
            // - Pick the highest pT muon
            // - Pick all muons
            const xAOD::Muon* closestMuon = nullptr;
            float minDR = 999;
            const xAOD::Muon* highestPtMuon = nullptr;
            std::vector<const xAOD::Muon*> allContainedMuons;
            for (const xAOD::Muon* imu : nonIsoMuons) {
                float DR = xAOD::P4Helpers::deltaR(ijet, imu, false);
                if (DR > 0.4)
                    // Skip any outside of the envelope
                    continue;
                allContainedMuons.push_back(imu);
                if (DR < minDR) {
                    minDR = DR;
                    closestMuon = imu;
                }
                // The muons are ordered in pT so the first is the highest in pT
                if (!highestPtMuon) highestPtMuon = imu;
            }
            // Now apply the corrections
            ijet->setJetP4("OneMuon",
                           GetJetFourMom(ijet->p4() + (closestMuon ? (closestMuon->p4() - getELossTLV(closestMuon)) : TLorentzVector())));
            ijet->setJetP4("OneMuonHighPt", GetJetFourMom(ijet->p4() + (highestPtMuon ? (highestPtMuon->p4() - getELossTLV(highestPtMuon))
                                                                                      : TLorentzVector())));
            TLorentzVector allMuCorrection;
            for (const xAOD::Muon* imu : allContainedMuons) allMuCorrection += (imu->p4() - getELossTLV(imu));
            ijet->setJetP4("AllNonIsoMuons", GetJetFourMom(ijet->p4() + allMuCorrection));
            std::vector<MuonLink_t>& links = dec_muonLinks(*ijet);
            links.assign(allContainedMuons.size(), {});
            for (std::size_t ii = 0; ii < allContainedMuons.size(); ++ii) links.at(ii).setElement(allContainedMuons.at(ii));
        }

        // pt-sorted central and forward jets get joined without resorting (for
        // DeltaPhiMin3 cut)
        ATH_CHECK(ViewElementsContainer("centralForward", m_CentralAndForwardJets));
        for (const auto& ijet : *m_SignalJets) m_CentralAndForwardJets->push_back(ijet);
        for (const auto& ijet : *m_ForwardJets) m_CentralAndForwardJets->push_back(ijet);

        // pt-sorted b-tagged and light jets get joined without resorting
        ATH_CHECK(ViewElementsContainer("btaggedLight", m_BTaggedAndLightJets));
        for (const auto& ijet : *m_BJets) m_BTaggedAndLightJets->push_back(ijet);
        for (const auto& ijet : *m_LightJets) m_BTaggedAndLightJets->push_back(ijet);

        ATH_CHECK(ViewElementsContainer("R02BaselineJets", m_R02BaselineJets));
        for (const auto& ijet : *m_R02PreSelJets) {
            dec_DL1r(*ijet) = recalculate_DL1r(*ijet);
            if (!PassBaseline(*ijet)) continue;
            m_R02BaselineJets->push_back(ijet);
            dec_isAssociated(*ijet) = false;
            double bTagWeight = BtagBDT(*ijet);
            GetJetDecorations()->BTagWeight.set(ijet, bTagWeight);
            // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21
            char isB = m_susytools->IsTrackBJet(*ijet);
            GetJetDecorations()->isBJet.set(ijet, isB);
            ATH_MSG_DEBUG("Track-jets pt " << ijet->pt() << " eta: " << ijet->eta() << " bTagWeight " << bTagWeight << " IsBJet "
                                           << (int)isB);
        }

        ATH_CHECK(ViewElementsContainer("R10BaselineJets", m_R10BaselineJets));
        for (const auto ijet : *m_R10PreSelJets) {
            // Also do a muon-in jet correction here
            std::vector<const xAOD::Muon*> allContainedMuons;
            for (const xAOD::Muon* imu : nonIsoMuons)
                if (xAOD::P4Helpers::deltaR(ijet, imu, false) < 1.0) allContainedMuons.push_back(imu);
            TLorentzVector allMuCorrection;
            for (const xAOD::Muon* imu : allContainedMuons) allMuCorrection += (imu->p4() - getELossTLV(imu));
            ijet->setJetP4("AllNonIsoMuons", GetJetFourMom(ijet->p4() + allMuCorrection));
            std::vector<MuonLink_t>& links = dec_muonLinks(*ijet);
            links.assign(allContainedMuons.size(), {});
            for (std::size_t ii = 0; ii < allContainedMuons.size(); ++ii) links.at(ii).setElement(allContainedMuons.at(ii));

            // The muons are already sorted by pT.
            TLorentzVector highestMuCorrection;
            for (std::size_t ii = 0; ii < 2 && ii < allContainedMuons.size(); ++ii)
                highestMuCorrection += (allContainedMuons.at(ii)->p4() - getELossTLV(allContainedMuons.at(ii)));
            ijet->setJetP4("TwoMuonsHighPt", GetJetFourMom(ijet->p4() + highestMuCorrection));

            // Sort by DR
            std::sort(allContainedMuons.begin(), allContainedMuons.end(), [ijet](const xAOD::Muon* lhs, const xAOD::Muon* rhs) -> bool {
                return ijet->p4().DeltaR(lhs->p4()) < ijet->p4().DeltaR(rhs->p4());
            });
            TLorentzVector closestMuCorrection;
            for (std::size_t ii = 0; ii < 2 && ii < allContainedMuons.size(); ++ii)
                closestMuCorrection += (allContainedMuons.at(ii)->p4() - getELossTLV(allContainedMuons.at(ii)));
            ijet->setJetP4("TwoMuons", GetJetFourMom(ijet->p4() + closestMuCorrection));

            if (!PassBaseline(*ijet)) continue;
            m_R10BaselineJets->push_back(ijet);
        }

        // decorate the fatjet with NN Xbb tagger scores
        m_hbbTagTool->decorate(*m_R10BaselineJets);

        // decorate the scores and the tagging info to fatjets
        ATH_CHECK(FillBtaggingForFatJetWithNNXbb(m_R10BaselineJets));

        std::vector<const xAOD::Jet*> AssociatedTrackJets;
        AssociatedTrackJets.clear();

        ATH_CHECK(ViewElementsContainer("TrackJetsForBtagging", m_R02BtaggingJets));

        // note that although here a loop is defined, the bottom line is a "break" command.
        // this results in only the leading large-R jet being iterated over and the associated
        // track jets only refer to the leading large-r jet.
        for (const auto ijet : *m_R10BaselineJets) {
            // associate track jets to large-R jet
            dec_n_matchedasstrkjets(*ijet) = 0;
            int n_matchedasstrkjets = 0;
            const xAOD::Jet* ParentJet = nullptr;
            ParentJet = GetParentJet(ijet);
            if (!ParentJet)
                continue;
            if (!ParentJet->getAssociatedObjects<xAOD::Jet>(m_R02linkname, AssociatedTrackJets)) {
                ATH_MSG_ERROR("No ghost-matched track-jets found on parent jet.");
                return StatusCode::FAILURE;
            }
            for (const auto& tk02 : *m_R02BaselineJets) {
                if (!PassBaseline(*tk02)) {
                    continue;
                    ATH_MSG_ERROR("Check your track-jet selection : pt: " << tk02->pt() << " eta: " << tk02->eta()
                                                                          << " nConst: " << tk02->numConstituents());
                }
                for (const auto& iAssJet : AssociatedTrackJets) {
                    if (!iAssJet) continue;
                    if (IsSame(tk02, iAssJet, true)) {
                        dec_isAssociated(*tk02) = true;
                        ++n_matchedasstrkjets;
                        m_R02BtaggingJets->push_back(tk02);
                    }
                }
            }
            if (m_R02BtaggingJets->size() > m_R02BaselineJets->size())
                ATH_MSG_FATAL("Association found more trackjets than in original container");
            dec_n_matchedasstrkjets(*ijet) = n_matchedasstrkjets;
            break;
        }

        // this is a bug-fix for a derivation version in which the
        // HadronConeExclTruthLabelID is missing:
        // the information is calculated from the other decorators
        if (m_R02name == "AntiKtVR30Rmax4Rmin02TrackJets" && !isData()) {
            for (const auto& ijet : *m_R02BaselineJets) {
                static IntAccessor acc_TurthLabel_track_jets("HadronConeExclTruthLabelID");
                if (acc_TurthLabel_track_jets.isAvailable(*ijet))
                    continue;
                else {
                    static IntDecorator dec_TurthLabel_track_jets("HadronConeExclTruthLabelID");
                    static IntAccessor acc_GhostBHadron("GhostBHadronsFinalCount");
                    static IntAccessor acc_GhostCHadron("GhostCHadronsFinalCount");
                    if (acc_GhostBHadron.isAvailable(*ijet) && acc_GhostBHadron(*ijet) > 0) {
                        dec_TurthLabel_track_jets(*ijet) = 5;
                    } else if (acc_GhostCHadron.isAvailable(*ijet) && acc_GhostCHadron(*ijet) > 0) {
                        dec_TurthLabel_track_jets(*ijet) = 4;
                    } else {
                        dec_TurthLabel_track_jets(*ijet) = 0;
                    }
                }
            }
        }

        m_trackJetsForBtagging.clear();
        if (m_R02BtaggingJets->size() > 0) m_trackJetsForBtagging.push_back(m_R02BtaggingJets->at(0));
        if (m_R02BtaggingJets->size() > 1) m_trackJetsForBtagging.push_back(m_R02BtaggingJets->at(1));
        for (const auto& ijet : *m_R02BaselineJets) {
            if (!dec_isAssociated(*ijet)) m_trackJetsForBtagging.push_back(ijet);
        }
        return StatusCode::SUCCESS;
    }

    double MonoHJetSelector::BtagDL1r_pu(const xAOD::Jet& jet) const {
        double jet_pu(-999.);
        if (jet.btagging() != nullptr) jet.btagging()->pu("DL1r", jet_pu);
        return jet_pu;
    }

    double MonoHJetSelector::BtagDL1r_pb(const xAOD::Jet& jet) const {
        double jet_pb(-999.);
        if (jet.btagging() != nullptr) jet.btagging()->pb("DL1r", jet_pb);
        return jet_pb;
    }

    double MonoHJetSelector::BtagDL1r_pc(const xAOD::Jet& jet) const {
        double jet_pc(-999.);
        if (jet.btagging() != nullptr) jet.btagging()->pc("DL1r", jet_pc);
        return jet_pc;
    }

    double MonoHJetSelector::recalculate_DL1r(const xAOD::Jet& jet) const {
        double DL1r_pb = BtagDL1r_pb(jet);
        double DL1r_pc = BtagDL1r_pc(jet);
        double DL1r_pu = BtagDL1r_pu(jet);
        double DL1r;
        ATH_MSG_DEBUG("pu: " << DL1r_pu << " pb: " << DL1r_pb << " pc: " << DL1r_pc);
        DL1r = log(DL1r_pb / (0.018 * DL1r_pc + (1. - 0.018) * DL1r_pu));
        ATH_MSG_DEBUG("dl1r: " << DL1r);
        return DL1r;
    }

    double MonoHJetSelector::calculate_CombinedNNXbbDiscriminant(float pHiggs, float pTop, float pQCD, float AntiTopness) {
        // more details here
        // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/BoostedHiggsToBBTagging#Using_the_tagger
        ATH_MSG_DEBUG("pHiggs: " << pHiggs << " pTop: " << pTop << " pQCD: " << pQCD);
        return log(pHiggs / (AntiTopness * pTop + (1 - AntiTopness) * pQCD));
    }

    bool MonoHJetSelector::IsTaggedByNNXbb(float CombScore, int AntiTopness, int wp) {
        // cut value from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BoostedHiggsToBBTagging#AntiKt10LCTopoTrimmedPtFrac5Smal
        bool supported_AntiTopness_wp = ((AntiTopness == 0) || (AntiTopness == 25) || (AntiTopness == 50) || (AntiTopness == 100)) &&
                                        ((wp == 60) || (wp == 70) || (wp == 80));
        if (!supported_AntiTopness_wp) ATH_MSG_ERROR("The AntiTopness and working point is not support!!!");
        if (AntiTopness == 0) {
            if (wp == 60)
                return CombScore > 3.36;
            else if (wp == 70)
                return CombScore > 3.05;
            else if (wp == 80)
                return CombScore > 2.34;
        } else if (AntiTopness == 25) {  // We'll most likely recommend a tagger with f = 0.25
            if (wp == 60)
                return CombScore > 2.55;
            else if (wp == 70)
                return CombScore > 1.92;
            else if (wp == 80)
                return CombScore > 1.20;
        } else if (AntiTopness == 50) {
            if (wp == 60)
                return CombScore > 2.11;
            else if (wp == 70)
                return CombScore > 1.47;
            else if (wp == 80)
                return CombScore > 0.76;
        } else if (AntiTopness == 100) {
            if (wp == 60)
                return CombScore > 1.61;
            else if (wp == 70)
                return CombScore > 0.98;
            else if (wp == 80)
                return CombScore > 0.32;
        }
        return false;
    }

    StatusCode MonoHJetSelector::FillBtaggingForFatJetWithNNXbb(const xAOD::JetContainer* fatjets) {
        // loop the fatjet
        for (const auto& ijet : *fatjets) {
            // firstly, let's get the scores
            static FloatAccessor acc_NNXbbScore_Higgs("SubjetBScore_Higgs");
            static FloatAccessor acc_NNXbbScore_Top("SubjetBScore_Top");
            static FloatAccessor acc_NNXbbScore_QCD("SubjetBScore_QCD");

            float NNXbbScore_Higgs = -1;
            float NNXbbScore_Top = -1;
            float NNXbbScore_QCD = -1;

            if (acc_NNXbbScore_Higgs.isAvailable(*ijet))
                NNXbbScore_Higgs = acc_NNXbbScore_Higgs(*ijet);
            else
                ATH_MSG_ERROR("Can't get SubjetBScore_Higgs!!!");

            if (acc_NNXbbScore_Top.isAvailable(*ijet))
                NNXbbScore_Top = acc_NNXbbScore_Top(*ijet);
            else
                ATH_MSG_ERROR("Can't get SubjetBScore_Top!!!");

            if (acc_NNXbbScore_QCD.isAvailable(*ijet))
                NNXbbScore_QCD = acc_NNXbbScore_QCD(*ijet);
            else
                ATH_MSG_ERROR("Can't get SubjetBScore_QCD!!!");

            // secondly, let's get the combined score and decorate it to fat jet
            static FloatDecorator dec_CombScore_0("NNXbb_CombScore_0");
            static FloatDecorator dec_CombScore_25("NNXbb_CombScore_25");
            static FloatDecorator dec_CombScore_50("NNXbb_CombScore_50");
            static FloatDecorator dec_CombScore_100("NNXbb_CombScore_100");

            // dec_CombScore_0(*ijet) = -1;
            // dec_CombScore_25(*ijet) = -1;
            // dec_CombScore_50(*ijet) = -1;
            // dec_CombScore_100(*ijet) = -1;

            float CombScore_0 = calculate_CombinedNNXbbDiscriminant(NNXbbScore_Higgs, NNXbbScore_Top, NNXbbScore_QCD, 0.);
            float CombScore_25 = calculate_CombinedNNXbbDiscriminant(NNXbbScore_Higgs, NNXbbScore_Top, NNXbbScore_QCD, 0.25);
            float CombScore_50 = calculate_CombinedNNXbbDiscriminant(NNXbbScore_Higgs, NNXbbScore_Top, NNXbbScore_QCD, 0.5);
            float CombScore_100 = calculate_CombinedNNXbbDiscriminant(NNXbbScore_Higgs, NNXbbScore_Top, NNXbbScore_QCD, 1.);

            dec_CombScore_0(*ijet) = CombScore_0;
            dec_CombScore_25(*ijet) = CombScore_25;
            dec_CombScore_50(*ijet) = CombScore_50;
            dec_CombScore_100(*ijet) = CombScore_100;

            // finally, decorate the tagging info
            static BoolDecorator dec_IsTaggedByNNXbb_25_70("IsTaggedByNNXbb_25_70");
            dec_IsTaggedByNNXbb_25_70(*ijet) = IsTaggedByNNXbb(CombScore_25, 25, 70);

            // bool NNXbbTagged_0_60 = IsTaggedByNNXbb( CombScore_0, 0, 60 );
            // bool NNXbbTagged_0_70 = IsTaggedByNNXbb( CombScore_0, 0, 70 );
            // bool NNXbbTagged_0_80 = IsTaggedByNNXbb( CombScore_0, 0, 80 );

            // bool NNXbbTagged_25_60 =  IsTaggedByNNXbb( CombScore_25, 25, 60 );
            // bool NNXbbTagged_25_70 =  IsTaggedByNNXbb( CombScore_25, 25, 70 );
            // bool NNXbbTagged_25_80 =  IsTaggedByNNXbb( CombScore_25, 25, 80 );

            // bool NNXbbTagged_50_60 =  IsTaggedByNNXbb( CombScore_50, 50, 60 );
            // bool NNXbbTagged_50_70 =  IsTaggedByNNXbb( CombScore_50, 50, 70 );
            // bool NNXbbTagged_50_80 =  IsTaggedByNNXbb( CombScore_50, 50, 80 );

            // bool NNXbbTagged_100_60 = IsTaggedByNNXbb( CombScore_100, 100, 60 );
            // bool NNXbbTagged_100_70 = IsTaggedByNNXbb( CombScore_100, 100, 70 );
            // bool NNXbbTagged_100_80 = IsTaggedByNNXbb( CombScore_100, 100, 80 );
        }
        return StatusCode::SUCCESS;
    }

    StatusCode MonoHJetSelector::SaveScaleFactor() {
        ATH_CHECK(SUSYJetSelector::SaveScaleFactor());
        for (auto& SF : m_TrackSF) {
            if (SF->systematic() != m_systematics->GetNominal() && m_XAMPPInfo->GetSystematic() != m_systematics->GetNominal()) continue;
            ATH_CHECK(m_systematics->setSystematic(SF->systematic()));
            for (auto ijet : m_trackJetsForBtagging) {
                ATH_CHECK(SF->saveSF(*ijet));
                char isB = false;
                double bTagWeight = -1e9;
                GetJetDecorations()->isBJet.get(ijet, isB);
                GetJetDecorations()->BTagWeight.get(ijet, bTagWeight);
                ATH_MSG_DEBUG("Track-jets used for BTagging:  pt: " << ijet->pt() << " eta: " << ijet->eta() << " bTagWeight " << bTagWeight
                                                                    << " IsBJet " << (int)isB << " isAssociated: "
                                                                    << (int)dec_isAssociated(*ijet) << " jetSF: " << SF->getSF(*ijet));
            }
            ATH_CHECK(SF->applySF());
        }
        return StatusCode::SUCCESS;
    }
    StatusCode MonoHJetSelector::preCalibCorrection(xAOD::JetContainer* /*Container*/) {
        // Here some pre calibration corrections, such as mitigating for a missing
        // detector eta variable could be applied.

        return StatusCode::SUCCESS;
    }
    TLorentzVector MonoHJetSelector::getELossTLV(const xAOD::Muon* muon) {
        float eLoss = 0.0;
        muon->parameter(eLoss, xAOD::Muon::EnergyLoss);
        ATH_MSG_DEBUG("xAOD::Muon energ loss in calo = " << eLoss);

        TLorentzVector m = muon->p4();
        double eLossX = eLoss * sin(m.Theta()) * cos(m.Phi());
        double eLossY = eLoss * sin(m.Theta()) * sin(m.Phi());
        double eLossZ = eLoss * cos(m.Theta());
        return TLorentzVector(eLossX, eLossY, eLossZ, eLoss);
    }
}  // namespace XAMPP
