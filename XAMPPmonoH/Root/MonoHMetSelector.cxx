#include <MetTriggerSFUncertainties/IMetTriggerSF.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/Defs.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/IElectronSelector.h>
#include <XAMPPbase/IJetSelector.h>
#include <XAMPPbase/IMuonSelector.h>
#include <XAMPPbase/IPhotonSelector.h>
#include <XAMPPbase/ISystematics.h>
#include <XAMPPbase/ITauSelector.h>
#include <XAMPPmonoH/MonoHMetSelector.h>

namespace XAMPP {
    MonoHMetSelector::MonoHMetSelector(std::string myname) :
        SUSYMetSelector(myname),
        m_store_significance_lepInvis(false),
        m_MetTSTlepInvisible(nullptr),
        m_sto_MetTSTlepInvis(nullptr),
        m_metSignif_noPUJets_noSoftTerm_lepInvis(nullptr),
        m_met_trigger_sf_tool("MetTriggerSF"),
        m_SFs() {
        declareProperty("WriteMetSignificanceLepInvis", m_store_significance_lepInvis = false);

        m_met_trigger_sf_tool.declarePropertyFor(this, "MetTriggerSF", "The MetTriggerSF tool");
    }
    MonoHMetSelector::~MonoHMetSelector() {}
    StatusCode MonoHMetSelector::initialize() {
        if (isInitialized()) { return StatusCode::SUCCESS; }
        std::shared_ptr<ActiveTimer> timer;
        if (m_doTime) timer = m_initTimer.activate();

        // initialization of object based MET significance with muon added back
        // to MET definition
        if (m_store_significance_lepInvis) {
            m_metSignif_noPUJets_noSoftTerm_lepInvis = newSignificanceTool("noPUJets_noSoftTerm_lepInvis");
            if (!m_metSignif_noPUJets_noSoftTerm_lepInvis->isUserConfigured()) {
                m_metSignif_noPUJets_noSoftTerm_lepInvis->setMissingEt(m_MetTSTlepInvisible);
                ATH_CHECK(m_metSignif_noPUJets_noSoftTerm_lepInvis->setProperty("SoftTermParam", 0));
                ATH_CHECK(m_metSignif_noPUJets_noSoftTerm_lepInvis->setProperty("TreatPUJets", false));
                ATH_CHECK(m_metSignif_noPUJets_noSoftTerm_lepInvis->setProperty("SoftTermReso", 0));
                ATH_CHECK(m_metSignif_noPUJets_noSoftTerm_lepInvis->setProperty("IsDataJet", false));
            }
        }

        // initialize method of parent class needs to be called after
        // initialization of noPUJets_noSoftTerm_lepInvis, otherwise it won't be
        // possible to define a new met significance tools since those can only
        // defined in an un-initialized class
        ATH_CHECK(SUSYMetSelector::initialize());

        // MET containers with muon added back
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<XAMPPmet>("MetTSTlepInvis"));
        m_sto_MetTSTlepInvis = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTSTlepInvis");

        // met trigger scale factors
        ATH_CHECK(m_met_trigger_sf_tool.retrieve());
        for (const auto& set : m_systematics->GetWeightSystematics(XAMPP::SelectionObject::MissingET)) {
            std::string weight_name = "MET_TriggerSF" + (set->name().empty() ? "" : "_" + set->name());
            if (!ToolIsAffectedBySystematic(m_met_trigger_sf_tool.getHandle(), set)) continue;
            ATH_CHECK(m_XAMPPInfo->NewEventVariable<double>(weight_name, true, set == m_systematics->GetNominal()));
            XAMPP::Storage<double>* weight = m_XAMPPInfo->GetVariableStorage<double>(weight_name);
            if (!weight) return StatusCode::FAILURE;
            m_SFs.insert(std::pair<const CP::SystematicSet*, XAMPP::Storage<double>*>(set, weight));
        }

        return StatusCode::SUCCESS;
    }

    StatusCode MonoHMetSelector::FillMet(const CP::SystematicSet& systset) {
        std::shared_ptr<ActiveTimer> timer;
        if (m_doTime) timer = m_fillTimer.activate();
        if (!m_systematics->ProcessObject(XAMPP::SelectionObject::MissingET)) {
            ATH_MSG_DEBUG("Do not process the missing ET...");
            return StatusCode::SUCCESS;
        }

        ATH_MSG_DEBUG("MonoHMetSelector::Fill MetTSTlepInvis...");
        ATH_CHECK(SetSystematic(systset));

        xAOD::MuonContainer* BaselineMuons = m_muon_selection->GetBaselineMuons();
        xAOD::MuonContainer* SignalMuons = m_muon_selection->GetSignalMuons();
        xAOD::ElectronContainer* BaselineElectrons = m_elec_selection->GetBaselineElectrons();
        xAOD::ElectronContainer* SignalElectrons = m_elec_selection->GetSignalElectrons();

        // emu Control region. Only marking the muon as invisible
        if (SignalMuons->size() == 1 && BaselineMuons->size() == 1 && SignalElectrons->size() == 1 && BaselineElectrons->size() == 1) {
            ATH_CHECK(addToInvisible(SignalMuons->at(0), "TSTmuInvis"));
        // Events with exactly only one muon and no electron lie in the 1-muon CR
        // --> Marking the leading signal muon as invisible
        } else if (SignalMuons->size() == 1 && BaselineMuons->size() == 1 && SignalElectrons->size() == 0 && BaselineElectrons->size() == 0) {
            ATH_MSG_DEBUG("SignalMuon pt: " << SignalMuons->at(0)->pt() / 1000. << " eta: " << SignalMuons->at(0)->eta()
                                            << " phi: " << SignalMuons->at(0)->phi());
            ATH_CHECK(addToInvisible(SignalMuons->at(0), "TSTmuInvis"));
            // Events with exactly two muons and no electron lie in the mumu channel of the 2-lepton CR
            // --> Marking the leading two signal muons as invisible
        } else if (SignalMuons->size() == 2 && BaselineMuons->size() == 2 && SignalElectrons->size() == 0 &&
                   BaselineElectrons->size() == 0) {
            ATH_CHECK(addToInvisible(SignalMuons->at(0), "TSTmuInvis"));
            ATH_CHECK(addToInvisible(SignalMuons->at(1), "TSTmuInvis"));
            // Events with exactly two electrons and no muons lie in the ee channel of the 2-lepton CR
            // --> Marking the leading two signal electrons as invisible
        } else if (SignalMuons->size() == 0 && BaselineMuons->size() == 0 && SignalElectrons->size() == 2 &&
                   BaselineElectrons->size() == 2) {
            ATH_CHECK(addToInvisible(SignalElectrons->at(0), "TSTmuInvis"));
            ATH_CHECK(addToInvisible(SignalElectrons->at(1), "TSTmuInvis"));
        }

        ATH_CHECK(buildMET(m_MetTSTlepInvisible, "TSTmuInvis", softTrackTerm(), m_elec_selection->GetPreElectrons(),
                           m_muon_selection->GetPreMuons(), nullptr, nullptr, true));
        ATH_CHECK(m_sto_MetTSTlepInvis->Store(XAMPP::GetMET_obj("Final", m_MetTSTlepInvisible)));

        // parent class method needs to be called last because it also calls the
        // fill function for the object based MET significance (if defined) and
        // this depends on the previosly calculated TSTlepInvis which in turn
        // needs to be calculated earlier.
        ATH_CHECK(SUSYMetSelector::FillMet(systset));

        return StatusCode::SUCCESS;
    }
    StatusCode MonoHMetSelector::SaveScaleFactor() {
        ATH_CHECK(SUSYMetSelector::SaveScaleFactor());
        // if there is exactly 1 signal muon (i.e. the event belongs to the 1L
        // control region), use MetTSTlepInvis to evaluate the MET trigger SF
        bool doOneLepton = (m_muon_selection->GetSignalMuons()->size() == 1 && m_muon_selection->GetBaselineMuons()->size() == 1);
        static XAMPP::Storage<XAMPPmet>* dec_MetTST = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTST");
        static XAMPP::Storage<XAMPPmet>* dec_MetTSTlepInvis = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("MetTSTlepInvis");

        xAOD::MissingET* met = doOneLepton ? dec_MetTSTlepInvis->GetValue() : dec_MetTST->GetValue();

        for (auto syst_sf : m_SFs) {
            if (syst_sf.first != m_systematics->GetNominal() && m_XAMPPInfo->GetSystematic() != m_systematics->GetNominal()) continue;
            ATH_CHECK(m_systematics->setSystematic(syst_sf.first));
            ATH_CHECK(syst_sf.second->Store(m_met_trigger_sf_tool->getMetTriggerSF(met, m_XAMPPInfo->GetEventInfo())));
        }

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
