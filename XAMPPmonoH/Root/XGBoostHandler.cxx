#ifdef USE_XGBOOST
#include <PathResolver/PathResolver.h>
#include <XAMPPmonoH/XGBoostHandler.h>
#include <xgboost/c_api.h>

namespace XAMPP {
    XGBoostHandler::XGBoostHandler() { initialize(); }

    XGBoostHandler::~XGBoostHandler() {
        XGBoosterFree(m_xgboost);
        XGBoosterFree(m_xgboost_r);
    }

    bool XGBoostHandler::initialize() {
        m_xgboost = BoosterHandle();
        m_xgboost_r = BoosterHandle();
        return true;
    }

    float XGBoostHandler::get_XGBoost_Weight(const std::vector<float>& vars, int event_number) {
        DMatrixHandle dmat;
        XGDMatrixCreateFromMat(vars.data(), 1, vars.size(), -1, &dmat);
        bst_ulong out_len;
        const float* f;
        if (event_number % 100 < 50) {
            XGBoosterPredict(m_xgboost_r, dmat, 0, 0, &out_len, &f);
        } else {
            XGBoosterPredict(m_xgboost, dmat, 0, 0, &out_len, &f);
        }
        XGDMatrixFree(dmat);
        return 1. - *f;
    }

    bool XGBoostHandler::LoadXGBoostModel(const std::string& region) {
        std::string path = "XAMPPmonoH/" + region + ".h5";
        std::string path_r = "XAMPPmonoH/" + region + "_r.h5";
        XGBoosterCreate(0, 0, &m_xgboost);
        XGBoosterLoadModel(m_xgboost, PathResolverFindCalibFile(path).c_str());
        XGBoosterCreate(0, 0, &m_xgboost_r);
        XGBoosterLoadModel(m_xgboost_r, PathResolverFindCalibFile(path_r).c_str());
        return true;
    }
}  // namespace XAMPP
#endif  //> USE_XGBOOST
