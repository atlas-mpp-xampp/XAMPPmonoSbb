#ifndef XAMPPmonoH_AnalysisUtils_H
#define XAMPPmonoH_AnalysisUtils_H

#include <XAMPPbase/AnalysisUtils.h>

namespace XAMPP {

    /**
     * @brief      Helper function to get parent jet using an element link
     *             to the parent jet container.
     *
     *
     * @param[in]  jet   The jet
     *
     * @return     The parent jet.
     */
    const xAOD::Jet* GetParentJet(const xAOD::Jet* jet);

    /**
     * @brief      Helper function to get parent jet using an element link
     *             to the parent jet container.
     *
     *
     * @param[in]  jet   The jet
     *
     * @return     The parent jet.
     */
    const xAOD::Jet* GetParentJet(const xAOD::Jet& jet);

    /**
     * @brief      Helper function to convert xAOD JetFourMom_t datatype to ROOT
     *             TLorentzVector.
     *
     * @param[in]  tmp   The jet four momentum as xAOD::JetFourMom_t
     *
     * @return     The jet four momentum as TLorentzVector
     */
    TLorentzVector GetTLV(xAOD::JetFourMom_t tmp);

    /**
     * @brief      Helper function to convert ROOT
     *             TLorentzVector datatype to xAOD JetFourMom_t.
     *
     * @param[in]  tmp   The jet four momentum as TLorentzVector
     *
     * @return     The jet four momentum as xAOD::JetFourMom_t
     */
    xAOD::JetFourMom_t GetJetFourMom(TLorentzVector tmp);

    // Helper class connecting input values to output branches
    class XAMPPOutput {
    public:
        /// Push everything this object is repsonsible for to the store
        virtual StatusCode push() = 0;
    };  //> end class XAMPPOutput

    /**
     * @brief Helper output class for something that writes a single set of
     * pt/eta/phi/m values.
     *
     * Writes things to branches like <prefix>pt<postfix> + the same for eta,
     * phi and m
     */
    class P4XAMPPOutput : public virtual XAMPPOutput {
    public:
        /// Create the output branches.
        static StatusCode createBranches(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix);

        /// Get the output branches
        P4XAMPPOutput(XAMPP::EventInfo* evtInfo, const std::string& prefix, const std::string& postfix);

        /// Set the output
        void setP4(const TLorentzVector& p4) { m_p4 = p4; }
        const TLorentzVector& p4() { return m_p4; }

        StatusCode push() override;

    private:
        XAMPP::Storage<float>* m_storePt;
        XAMPP::Storage<float>* m_storeEta;
        XAMPP::Storage<float>* m_storePhi;
        XAMPP::Storage<float>* m_storeM;
        /// The p4 value to store
        TLorentzVector m_p4;

    };  //> end class P4XAMPPOutput

    /**
     * @brief      Helper function to convert a given MV2c10 scores for calo jet 04
     *             to a pseudo continuous b-tagging scores
     *
     * @param[in]  MV2c10  The MV2c10 scores
     *
     * @return     The pseudo continious b-tagging score as int
     */
    int PCS(double MV2c10);

    /**
     * @brief      Helper function to convert a given MV2c10 scores for VR track-jet
     *             to a pseudo continuous b-tagging scores
     *
     * @param[in]  MV2c10  The MV2c10 scores
     *
     * @return     The pseudo continious b-tagging score as int
     */
    int PCS_tj(double MV2c10);

    enum score { default_score = 0, wp_85 = 1, wp_77 = 2, wp_70 = 3, wp_60 = 4 };
}  // namespace XAMPP
#endif
