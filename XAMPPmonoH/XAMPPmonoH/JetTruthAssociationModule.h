#ifndef XAMPPmonoH_JetTruthAssociationModule_H
#define XAMPPmonoH_JetTruthAssociationModule_H

#include <memory>

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "XAMPPbase/EventInfo.h"
#include "XAMPPbase/IAnalysisModule.h"
#include "XAMPPbase/IJetSelector.h"
#include "XAMPPmonoH/AnalysisUtils.h"

namespace XAMPP {
    class JetTruthAssociationModule : virtual public IAnalysisModule, public asg::AsgTool {
        ASG_TOOL_CLASS(JetTruthAssociationModule, IAnalysisModule)
    public:
        /// Constructor
        JetTruthAssociationModule(const std::string& name);
        /// Destructor
        ~JetTruthAssociationModule() override;

        StatusCode initialize() override;
        StatusCode bookVariables() override;
        StatusCode fill() override;

    private:
        // properties
        /// The jet selection handle
        ToolHandle<IJetSelector> m_jetSelector;
        /// The XAMPP event info handle
        ToolHandle<IEventInfo> m_evtInfoHdl;
        /// The truth jet collection
        std::string m_truthJetName;
        /// The truth large-R jet collection
        std::string m_truthLRJetName;
        /// The truth particle collection
        std::string m_truthParticlesName;
        /// The DR between reco and truth jets to count as a match
        float m_DR;
        /// The DR between reco and truth large-R jets to count as a match
        float m_LRDR;

        // Internals
        EventInfo* m_XAMPPInfo;

        /// Correct the truth jets
        std::map<const xAOD::Jet*, TLorentzVector> correctedJetP4s(const xAOD::JetContainer* jets,
                                                                   const xAOD::TruthParticleContainer* truthParticles, float dr);

        // Output branches
        // Output p4 for the truth jets associated to the large-R jet and the
        // leading two btag/light jets. Output both uncorrected and corrected
        // truth jets.
        std::unique_ptr<P4XAMPPOutput> m_j1Truth;
        std::unique_ptr<P4XAMPPOutput> m_j1TruthCorr;
        std::unique_ptr<P4XAMPPOutput> m_j2Truth;
        std::unique_ptr<P4XAMPPOutput> m_j2TruthCorr;
        std::unique_ptr<P4XAMPPOutput> m_lrjTruth;
        std::unique_ptr<P4XAMPPOutput> m_lrjTruthCorr;

    };  //> end class JetTruthAssociationModule
}  // namespace XAMPP

#endif  //> !XAMPPmonoH_JetTruthAssociationModule_H
