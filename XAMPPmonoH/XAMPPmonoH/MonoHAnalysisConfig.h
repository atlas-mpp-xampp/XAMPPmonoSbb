#ifndef MonoHAnalysisConfig_h
#define MonoHAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {

    /**
     * @brief      Class to specify (on detector level, typical data/MC comparison)
     *             the event selection of the Mono-h(bb) analysis for multiple channels:
     *             \li 0 lepton signal region,
     *             \li 1 lepton control region,
     *             \li 2 lepton control region.
     */
    class MonoHAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(MonoHAnalysisConfig, XAMPP::IAnalysisConfig)
        /**
         * @brief Create a proper constructor for Athena
         *
         * @param[in]  Analysis  The name of the analysis (default: MonoH)
         */
        MonoHAnalysisConfig(std::string Analysis = "MonoH");

        /**
         * @brief      Default destructor
         */
        virtual ~MonoHAnalysisConfig() {}

        /** @brief Toggles whether a certain set cuts are applied as skimming cuts (True, for production)
         or applied as cutflow cuts (False, for testing purposes, creates much larger output files) */
        bool m_doproduction;
        /** @brief Toggles whether the mass window from 70 GeV to 140 GeV should be written to the ntuples (False: no blinding)
                   or not (True: apply blinding). This only affects data samples, MC samples will never be blinded. */
        bool m_blinding;
        /** @brief Toggles whether the MET trigger cut should be initialized (True) or not (False) */
        bool m_doMETTrigger;
        /** @brief Toggles whether the single electron trigger cut should be initialized (True) or not (False) */
        bool m_doSingleElecTrigger;
        /** @brief Toggles whether the single muon trigger cut should be initialized (True) or not (False) */
        bool m_doSingleMuonTrigger;
        /** @brief     Toggles whether the single lepton trigger cut should be
         *             used in the 1 lepton channel (True) or not (False) For
         *             the MET trigger calibration it is required to set this to
         *             true, for the analysis it should be set to false.
         */

        bool m_doMETTriggerCalibration;
        /** @brief     When we use particle flow jets we need to reject the events where the
         *             electrons or the photons that are entering the MET calculation
         *             have the decoration DFCommonCrackVetoCleaning set to false.
         *             This flag is set to true when using PFLow jets to allow rejecting those events.
         */
        bool m_doPFlowElecCleaning;

        /** @brief Toggles whether the CONF event selection cuts or the new cuts should be used in the MonoHAnalysisConfig cutflows */
        bool m_doCONFselection;

        bool m_doFullSkimming;

    protected:
        /**
         * @brief      Initialize cutflows for Mono-h(bb) analysis code.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initializeCustomCuts); to check if the method
         *             was successful or failed.
         */
        virtual StatusCode initializeCustomCuts();
    };

    /**
     * @brief      Class to specify (on truth level, typical for signal MC studies)
     *             the event selection of the Mono-h(bb) analysis for the channels:
     *             \li 0 lepton signal region (truth).
     */
    class MonoHTruthAnalysisConfig : public TruthAnalysisConfig {
    public:
        ASG_TOOL_CLASS(MonoHTruthAnalysisConfig, XAMPP::IAnalysisConfig)
        /**
         * @brief Create a proper constructor for Athena
         *
         * @param[in]  Analysis  The name of the analysis (default: MonoHTruth)
         */
        MonoHTruthAnalysisConfig(std::string Analysis = "MonoHTruth");
        /**
         * @brief      Default destructor
         */
        virtual ~MonoHTruthAnalysisConfig() {}

    protected:
        /**
         * @brief      Initialize cutflows for Mono-h(bb) truth analysis code.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initializeCustomCuts); to check if the method
         *             was successful or failed.
         */
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
