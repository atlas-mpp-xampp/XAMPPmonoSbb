#ifndef MonoHAnalysisHelper_H
#define MonoHAnalysisHelper_H

#include <XAMPPbase/SUSYAnalysisHelper.h>
#include <XAMPPbase/SUSYTruthAnalysisHelper.h>
#include <XAMPPmonoH/XGBoostHandler.h>

namespace CP {

    /**
     * @brief      Interface class for met trigger scale factors.
     */
    class IMetTriggerSF;

    /**
     * @brief      Interface class for background modeling systematics.
     */
    class ISMShapeWeights;

    class IPileupReweightingTool;
}  // namespace CP

namespace XAMPP {

    /**
     * @brief      Class for mono h analysis helper.
     */
    class MonoHAnalysisHelper : public SUSYAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(MonoHAnalysisHelper, XAMPP::IAnalysisHelper)

        /**
         * @brief      Constructor
         *
         * @param[in]  myname  Name of the class instance.
         */
        MonoHAnalysisHelper(const std::string& myname);
        /**
         * @brief      Default destructor.
         */
        virtual ~MonoHAnalysisHelper();
        /**
         * @brief      Initialize MonoHAnalysisHelper: currently only calls the
         *             initialize method of the parent class SUSYAnalysisHelper
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initialize); to check if the method
         *             was successful or failed.
         */
        virtual StatusCode initialize();

        /**
         * @brief      Re-implementation of the overlap removal method of the
         *             parent class. The difference to the standard overlap
         *             removal the large-R jets are excluded from the standard
         *             overlap removal and a custom electron - large-R jet
         *             overlap removal is performed to make sure that it is done
         *             properly (i.e. in the correct sequence).
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(RemoveOverlap); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode RemoveOverlap();

        /**
         * @brief      Re-implementation of parent class method. Always returns
         *             true to ensure that all events are contained in the
         *             ntuple. This is done in order to add SUSYTools trigger
         *             check.
         *
         * @return     Returns boolean value: true means accept event, false
         *             means discard event
         */
        virtual bool AcceptEvent();

    protected:
        /**
         @brief      Toggles if variables used for initial skimming and event
                     quality cuts are written out to the ntuple or not
        */
        bool m_doDiagnosticVariables;

        /**
         * @brief      Boolean to store if MET trigger scale factor tool should be
         *             set up and applied (true) or not (false).
         */
        bool m_metTriggerSF;

        /**
         * @brief      Boolean to store if SM shape weight tool should be
         *             set up and applied (true) or not (false).
         */
        bool m_SMShapeWeights;

        /**
         * @brief      Re-implements method of parent class. Calls parent
         *             class-method and in addition sets up MET trigger SF tool
         *             and SM shape weights tool (only for MC) + configures them
         *             accordingly.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initializeAnalysisTools); to check if the
         *             method was successful or failed.
         */
        virtual StatusCode initializeAnalysisTools();

        /**
         * @brief      This method defines all event-level variables that should
         *             be written to the ntuple and initializes them. If you
         *             plan to add a new variable, you need to first initialize
         *             it with
         *
         *             ATH_CHECK(m_XAMPPInfo->NewEventVariable<int/float/double/bool>("name"));
         *
         *             Please note that this will only initialize the storage.
         *             You still need to write the values, for this you must
         *             implement some code in the
         *             MonoHAnalysisHelper::ComputeEventVariables method.
         *
         *             In MonoHAnalysisHelper::ComputeEventVariables you need
         *             first to define a XAMPP::Storage object
         *
         *             static XAMPP::Storage<int/float/double/bool>*
         *             dec_variable =
         *             m_XAMPPInfo->GetVariableStorage<int/float/double/bool>("variable");
         *
         *             Then you need to compute the variable and assign it to a
         *             variable of the desired type:
         *
         *             int variable = 42;
         *
         *             Finally you can store it using the storage element:
         *
         *             ATH_CHECK(dec_variable->Store(variable));
         *
         *
         *             You can also initialize storage for particles or jets.
         *             This will store the four-momenta of the particles/jets
         *             and also all decorators that you list. You can do this
         *             with
         *
         *             ATH_CHECK(m_XAMPPInfo->BookParticleStorage("particle",
         *             true));
         *
         *             XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->SaveInteger("some_integer_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveFloat("some_float_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveDouble("some_double_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveChar("some_boolean_value_decorator"));
         *
         *             Note that this will only initialize the storage objects.
         *             You still need to specify what to fill in the
         *             MonoHAnalysisHelper::ComputeEventVariables method. There
         *             you also need to define a JetContainer or a
         *             ParticleContainer that should be filled in the particle
         *             storage.
         *
         *             You can add a piece of code to the
         *             MonoHAnalysisHelper::ComputeEventVariables method like
         *             this (particle here is a placeholder for either Muon,
         *             Electron, Tau or Jet):
         *
         *             xAOD::ParticleContainer* particles =
         *             m_particle_selection->GetSignalParticles();
         *
         *             static XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->Fill(particles));
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initializeEventVariables); to check if the
         *             method was successful or failed.
         */
        virtual StatusCode initializeEventVariables();

        /**
         * @brief      Calculates the event variables and fills them to the
         *             storage elements defined in
         *             MonoHAnalysisHelper::initializeEventVariables.
         *
         *             You need to first initialize the storage in
         *             MonoHAnalysisHelper::initializeEventVariables by adding a
         *             piece of code to
         *             MonoHAnalysisHelper::initializeEventVariables like this:
         *
         *             ATH_CHECK(m_XAMPPInfo->NewEventVariable<int/float/double/bool>("name"));
         *
         *             Then you can compute the event level variable in the
         *             MonoHAnalysisHelper::ComputeEventVariables method and
         *             store the result.
         *
         *             Here is a generic example, how to calculate and store
         *             event level variables:
         *
         *             First you need to define a XAMPP::Storage element to
         *             access the storage defined in
         *             MonoHAnalysisHelper::initializeEventVariables.
         *
         *             static XAMPP::Storage<int/float/double/bool>*
         *             dec_variable =
         *             m_XAMPPInfo->GetVariableStorage<int/float/double/bool>("variable");
         *
         *             Then you need to compute the variable and assign it to a
         *             variable of the desired type:
         *
         *             int variable = 42;  // just an example, in reality you
         *             would compute here e.g. the angular separation between
         *             two jets or retrieve a tagger score.
         *
         *             Finally you can store it using the storage element
         *             defined earlier.
         *
         *             ATH_CHECK(dec_variable->Store(variable));
         *
         *
         *             It is also possible to write the four-vector content of
         *             particle or jet containers with selected decorator
         *             information to the ntuples. This will store the
         *             four-momenta of the particles/jets and also all
         *             decorators that you list. Before you can do this, you
         *             need to initialize in
         *             MonoHAnalysisHelper::initializeEventVariables the storage
         *             with with
         *
         *             ATH_CHECK(m_XAMPPInfo->BookParticleStorage("particle",
         *             true));
         *
         *             XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->SaveInteger("some_integer_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveFloat("some_float_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveDouble("some_double_value_decorator"));
         *             ATH_CHECK(ParticleStore->SaveChar("some_boolean_value_decorator"));
         *
         *             Note that this will only initialize the storage objects.
         *
         *             You then can specify what to fill in the
         *             MonoHAnalysisHelper::ComputeEventVariables method. Here
         *             you also need to define a JetContainer or a
         *             ParticleContainer that should be filled in the particle
         *             storage.
         *
         *             You can add a piece of code to the
         *             MonoHAnalysisHelper::ComputeEventVariables method like
         *             this (particle here is a placeholder for either Muon,
         *             Electron, Tau or Jet):
         *
         *             xAOD::ParticleContainer* particles =
         *             m_particle_selection->GetSignalParticles();
         *
         *             static XAMPP::ParticleStorage* ParticleStore =
         *             m_XAMPPInfo->GetParticleStorage("particle");
         *             ATH_CHECK(ParticleStore->Fill(particles));
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(ComputeEventVariables); to check if the method
         *             was successful or failed.
         */
        virtual StatusCode ComputeEventVariables();

        /**
         * @brief      Tool handle for the MET trigger scale factor tool.
         */
        asg::AnaToolHandle<CP::IMetTriggerSF> m_met_trigger_sf_tool;

        /**
         * @brief      Tool handle for the standard model background modeling
         *             uncertainties tool for 0 lepton selection.
         */
        asg::AnaToolHandle<CP::ISMShapeWeights> m_sm_shape_weights_tool_0lep;

        /**
         * @brief      Tool handle for the standard model background modeling
         *             uncertainties tool for 1 lepton selection.
         */
        asg::AnaToolHandle<CP::ISMShapeWeights> m_sm_shape_weights_tool_1lep;

        /**
         * @brief      Tool handle for the standard model background modeling
         *             uncertainties tool for 2 lepton selection.
         */
        asg::AnaToolHandle<CP::ISMShapeWeights> m_sm_shape_weights_tool_2lep;

        /**
         * @brief      Storage element for the standard model background
         *             modeling uncertainties (stored as weights, named
         *             CorrsAndSysts according to the name used for SM
         *             background modeling uncertainties by the VHbb group)
         */
        std::map<const CP::SystematicSet*, XAMPP::Storage<double>*> m_corrs_syst_weights;

        /**
         * @brief      Boolean variable to enable storing objects without
         *             overlap removal criterium, needs to be active when running
         *             with common tree mechanism for systematics.
         *             If active, write output objects in ntuple for which systematic
         *             groups are active (should be electrons, muons, default jets)
         *             with and without overlap removal and store passOR decorator
         *             if not active, only write objects with passOR == True and do
         *             not store passOR decorator in ntuple (since it would be always True).
         */
        bool m_useSystematicGroups;

        /**
         * @brief      Store trigger information in ntuple output.
         */
        bool m_storeExtendedTriggerInfo;

        /**
         * @brief      Float value of the maximum delta R used for the electron -
         *             large-R jet overlap removal (can be set in job option
         *             file).
         */
        float m_ORfatjetElectronDR;

        /**
         * @brief      Boolean set to true when using PFlow Jets.
         */
        bool m_doPFlowCleaning;

        /**
         * @brief      Store also variables which were used only in CONF selections
         */

        bool m_doCONFvariables;
        /**
         * @brief      Store XbbScore variables
         */
        bool m_doXbbScore;

        /**
         * @brief      Store NN Xbb Score variables
         */
        bool m_doNNXbbScore;

        bool m_useTTBarMETFiltered0LepSamples;
        bool m_useVZqqVZbb;
        bool m_useZnunuBfilPTV0LepSamples;

        /**
         * @brief      If set to True the W boson decay modes will be classified.
         *
         */
        bool m_doTruthWjets;
        /**
         * @brief      If set to True the single decay modes of the top and the anti-top quark will be classified as well as the combined
         * ttbar decay mode.
         *
         */
        bool m_doTruthTtbar;

        // Write out extra muons in the Higgs candidate jets
        bool m_writeMuonsInJets;

        xAOD::MuonContainer m_jet1Muons{SG::VIEW_ELEMENTS};
        xAOD::MuonContainer m_jet2Muons{SG::VIEW_ELEMENTS};
        xAOD::MuonContainer m_fatJetMuons{SG::VIEW_ELEMENTS};

        ToolHandle<CP::IPileupReweightingTool> m_prw_tool;
        typedef std::pair<int, int> dsid_ident;
        std::map<dsid_ident, double> m_cached_sum_w;

        double getSumW(int dsid, int period_number);

        bool m_doBDT;
#ifdef USE_XGBOOST
        XGBoostHandler m_XGB_resolved;
        XGBoostHandler m_XGB_merged;
#endif  //> USE_XGBOOST
    };
}  // namespace XAMPP

#endif  // MonoHAnalysisHelper_H //
