#ifndef XAMPPmonoH_MonoHJetSelector_H
#define XAMPPmonoH_MonoHJetSelector_H

#include <FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h>
#include <FTagAnalysisInterfaces/IBTaggingSelectionTool.h>
#include <MuonSelectorTools/MuonSelectionTool.h>
#include <XAMPPbase/IElectronSelector.h>
#include <XAMPPbase/IMuonSelector.h>
#include <XAMPPbase/SUSYJetSelector.h>

#include "FlavorTagDiscriminants/HbbTagTool.h"
#include "JetAnalysisInterfaces/IJetSelectorTool.h"

namespace XAMPP {
    class MonoHJetSelector : public SUSYJetSelector {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(MonoHJetSelector, XAMPP::IJetSelector)

        /**
         * @brief      Default constructor
         *
         * @param[in]  myname  The name of the class instance
         */
        MonoHJetSelector(std::string myname);

        /**
         * @brief      Default destructor
         */
        virtual ~MonoHJetSelector() {}

        /**
         * @brief      Re-implementation of parent class method. Calls parent
         *             class method. Calibrates large-R jets and track jets.
         *
         * @param[in]  systset  The current systematic set
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(InitialFill); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode InitialFill(const CP::SystematicSet& systset);

        /**
         * @brief      Re-implementation of the parent class FillJets method.
         *             Calls parent class method. Then sorts these jet
         *             collections by their jet pt to ensure that the jets are
         *             ordered:
         *
         *             - m_BJets
         *             - m_LightJets
         *             - m_R10PreSelJets
         *             - m_R02PreSelJets
         *
         *             Creates view container for forward jets and fills it with
         *             jets satisfying 2.5 < |eta| < 4.5
         *
         *             Applies muon in jet correction to small-R jets.
         *
         *             Creates view container for "centralforward" jets that
         *             contains in this order the pt-sorted central jets and the
         *             pt-sorted forward jets (used for min dPhi cut).
         *
         *             Creates view container for "btaggedLight" jets that
         *             containes in this order the pt-sorted b-tagged central
         *             jets and the pt-sorted light (i.e. non b-tagged) central
         *             jets.
         *
         *             Fills baseline large-R jets and baseline track jets.
         *
         *             Creates view container for track jets associated to
         *             large-R jet.
         *
         *             The track jet containers used for b-tagging are filled.
         *
         * @param[in]  systset  The current systematic set
         *
         * @return     StatusCode, needs to be checked with ATH_CHECK(FillJets);
         *             to check if the method was successful or failed.
         */
        virtual StatusCode FillJets(const CP::SystematicSet& systset);

        /**
         * @brief      Re-implementation of the parent class method. Returns the
         *             pre jets. Depending on the radius parameter different jet
         *             collections can be obtained:
         *
         *             - radius = 10: large-R jets (usually anti-kt 10)
         *             - radius = 2: track jets (usually anti-kt 02)
         *             - radius = 4: small-R jets (usually anti-kt 04) (via
         *               parent class method)
         *
         * @param[in]  Radius  The radius parameter of the desired jet
         *                     collection
         *
         * @return     The pre jet container of a desired jet collection
         *             (selected by the radius parameter).
         */

        virtual xAOD::JetContainer* GetPreJets(int Radius = 99) const;

        /**
         * @brief Function that uses the b-tagging tool to retrieve the DL1r_pb variable
         *
         * @return  "pb" variable of DL1r (DL1r_pb)
         *
         */

        double BtagDL1r_pb(const xAOD::Jet& jet) const;

        /**
         * @brief Function that uses the b-tagging tool to retrieve the DL1r_pc variable
         *
         * @return  "pc" variable of DL1r (DL1r_pc)
         *
         */

        double BtagDL1r_pc(const xAOD::Jet& jet) const;

        /**
         * @brief Function that uses the b-tagging tool to retrieve the DL1r_pu variable
         *
         * @return  "pu" variable of DL1r (DL1r_pu)
         *
         */

        double BtagDL1r_pu(const xAOD::Jet& jet) const;

        /**
         * @brief      Function that computes the final DL1r tagger.
         *             The DL1r is a b-tagging algorithm based on a DeepNeuralNetwork.
         *             With respect the previous DL1 tagger, DL1r uses a Recurrent Neural Networks (RNN)
         *	       version of the low-level b-tagging algorithm and is expected
         *	       to have a better performance. The output of DL1r tagger (as for the DL1)
         *             comes in three variants: DL1r_pb, DL1r_pc, DL1rpu, corresponding to the probability
         *	       for the jet to a b, c or light-flavour jet, respectively.
         *
         *             The final DL1r discriminant can be constructed, through the recalculate_DL1r
         *		       function, as:
         *		       DL1r =  ln(DL1r_pb/(DL1r_pc*f + (1-f)DL1r_pu))
         *		       where f represents the fraction of c-jets.
         *
         * @return     The final DL1r discriminant, given the output of BtagDL1r_pb,BtagDL1r_pc and BtagDL1r_pu
         *
         */

        double recalculate_DL1r(const xAOD::Jet& jet) const;

        /**
         * @brief      Function that computes the final NNXbb tagger.
         *
         *             The final NNXbb discriminant can be constructed, through the calculate_CombinedNNXbbDiscriminant
         *		       function, as:
         *		       Discriminant =  ln( pHiggs / ( pTop * f + (1-f) * pQCD ) )
         *		       where f represents the anti-topness in the discriminant.
         *
         * @return     The final NNXbb discriminant, given the output of pHiggs, pTop and pQCD.
         *
         */

        double calculate_CombinedNNXbbDiscriminant(float pHiggs, float pTop, float pQCD, float AntiTopness);

        /**
         * @brief  Funtion that performs the tagging for the fatjets with NN Xbb tagger.
         *
         * @return true if the fajet pass the given wp at give antiTopness.
         */

        bool IsTaggedByNNXbb(float CombScore, int AntiTopness, int wp);

        /**
         * @brief  Funtion that decorates the fatjet with the combined scores, and the tagging info
         *
         * @return StatusCode, needs to be checked with
         *         ATH_CHECK(FillBtaggingForFatJetWithNNXbb); to check if the method.
         *         successful or failed.
         */

        StatusCode FillBtaggingForFatJetWithNNXbb(const xAOD::JetContainer* fatjets);

        /**
         * @brief      Re-implementation of the parent class method. Returns the
         *             baseline jets. Depending on the radius parameter
         *             different jet collections can be obtained:
         *
         *             - radius = 10: large-R jets (usually anti-kt 10)
         *             - radius = 2: track jets (usually anti-kt 02)
         *             - radius = 4: small-R jets (usually anti-kt 04) (via
         *               parent class method)
         *
         * @param[in]  Radius  The radius parameter of the desired jet
         *                     collection
         *
         * @return     The baseline jet container of a desired jet collection
         *             (selected by the radius parameter).
         */
        virtual xAOD::JetContainer* GetBaselineJets(int Radius = 99) const;

        /**
         * @brief      Re-implementation of the parent class initialize method,
         *             calls parent class initialize method. Sets up muon tool
         *             (for muon in jet correction). Sets up b-tagging tool for
         *             track jets and prepares weights for storing b-tagging
         *             scale factors.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(initialize); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode initialize();

        /**
         * @brief      Re-implementation of the parent class SaveScaleFactor
         *             method. Calls parent class method, then loops over track
         *             jets to store track jet b-tagging scale factors.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(SaveScaleFactor); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode SaveScaleFactor();

        /**
         * @brief      HbbTagTool  for the new NN Xbb tagger for the fat jet
         *
         */
        FlavorTagDiscriminants::HbbTagTool* m_hbbTagTool;  //!

    protected:
        /**
         * @brief      Checks if jet passes baseline jet selection. This is
         *             currently implemented in a horribly hard-coded manner
         *             which could be improved.
         *
         *             - track jets: need to have at least 2 constituents
         *             - small-R jets: either pt > 20 GeV and |eta| <= 2.5 or pt
         *               > 30 GeV and  2.5 |eta| <= 4.5 GeV
         *             - otherwise: always accepted
         *
         * @param[in]  P     Particle (in this case a jet) to be checked
         *
         * @return     True if jet passes the baseline requirements, False
         *             otherwise
         */
        virtual bool PassBaseline(const xAOD::IParticle& P) const;

        /**
         * @brief      This function allows to add a correction to the jets
         *             (e.g. remedy a missing variable by calculating on the
         *             fly) before they are calibrated.
         *
         * @param      Container  The jet container for which the correction
         *                        should be applied.
         *
         * @return     StatusCode, needs to be checked with
         *             ATH_CHECK(preCalibCorrection); to check if the method was
         *             successful or failed.
         */
        virtual StatusCode preCalibCorrection(xAOD::JetContainer* Container);

        /**
         * @brief      Minimal eta of forward jets
         **/
        float m_Kt04_ForwardJetMinEta;

        /**
         * @brief      Maximal eta of forward jets
         **/
        float m_Kt04_ForwardJetMaxEta;

        /**
         * @brief      Minimal pt of forward jets
         **/

        float m_Kt04_ForwardJetMinPt;

        /**
         * @brief      String storing the name of the large-R jet container in
         *             the xAOD input file
         */
        std::string m_R10name;

        /**
         * @brief      String storing the name of the track jet container in the
         *             xAOD input file
         */
        std::string m_R02name;

        /**
         * @brief      String storing the name of the element links of the track
         *             jets to the associated large-R jets
         */
        std::string m_R02linkname;

        /**
         * @brief      Jet container for pre large-R jets
         */
        xAOD::JetContainer* m_R10PreSelJets;

        /**
         * @brief      Jet container for baseline large-R jets
         */
        xAOD::JetContainer* m_R10BaselineJets;

        /**
         * @brief      Jet container for pre track jets
         */
        xAOD::JetContainer* m_R02PreSelJets;

        /**
         * @brief      Jet container for baseline track jets
         */
        xAOD::JetContainer* m_R02BaselineJets;

        /**
         * @brief      Jet container for track jets used for b-tagging
         */
        xAOD::JetContainer* m_R02BtaggingJets;

        /**
         * @brief      Jet container for forward jets (those are defined by 2.5 < |eta| < 4.5)
         */
        xAOD::JetContainer* m_ForwardJets;

        /**
         * @brief      Jet container for pt-sorted central jets followed by
         *             pt-sorted forward jets. Those get joined without
         *             resorting afterwards, resulting in an order of
         *
         *             leading central jet, subleading central jet, ..., leading
         *             forward jet, subleading forward jet, ...
         *
         *             Those jets are used for the DeltaPhiMin3 cut.
         */
        xAOD::JetContainer* m_CentralAndForwardJets;
        /**
         * @brief      Jet container for pt-sorted b-tagged central jets
         *             followed by light / non-btagged central jets. Those get
         *             joined without resorting afterwards, resulting in an
         *             order of
         *
         *             leading b-jet, subleading b-jet, ..., leading light jet,
         *             subleading light jet, ...
         *
         *             Those jets are used for reconstructing the Higgs
         *             candidate mass and pt cuts in the resolved selection.
         */
        xAOD::JetContainer* m_BTaggedAndLightJets;

        /**
         * @brief      Tool handle for XAMPP muon selector (required to retrieve
         *             the muon for the muon in jet correction)
         */
        ToolHandle<XAMPP::IMuonSelector> m_muon_selection;

        /**
         * @brief      Tool handle for muon selection tool (required to check
         *             the quality of muons used for the muon in jet correction)
         */
        ToolHandle<CP::IMuonSelectionTool> m_muon_selTool;

        /**
         * @brief      Float storing maximum delta R for muon to jet matching
         *             (required for muon in jet correction)
         */
        float m_dR_mu_jet;

        /**
         * @brief      Float storing minimum pt of muons matched to jets for muon to jet
         *             correction. (required for muon in jet correction)
         */
        float m_mu_min_pt;

        /**
         * @brief      Integer storing minimum quality of muons matched to jets
         *             for muon to jet correction. (required for muon in jet
         *             correction)
         *
         *             For meaning of the muon quality c.f.
         *             https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionTool
         *             {Tight, Medium, Loose, VeryLoose}, corresponding to 0, 1,
         *             2 and 3 (default is 3 - VeryLoose quality)
         */
        int m_mu_quality;

        /**
         * @brief      Tool handle for track jet b-tagging efficiency tool
         */
        ToolHandle<IBTaggingEfficiencyTool> m_TrkJet_bTagEffTool;

        /**
         * @brief      Vector of JetWeightHandler_Ptr, required to handle and
         *             store b-tagging scale factors for track jets
         */
        std::vector<JetWeightHandler_Ptr> m_TrackSF;

        /**
         * @brief      Helper function to get energy loss of muon in the
         *             calorimeter as a ROOT TLorentzVector
         *
         * @param[in]  mu    pointer to xAOD::Muon for which the energy loss in
         *                   the calorimeter should be retrieved
         *
         * @return     The energy loss of the muon in the calorimeter as a ROOT
         *             TLorentzVector
         */
        TLorentzVector getELossTLV(const xAOD::Muon* mu);

        /**
         * @brief      Vector of track jets used for b-tagging in the merged
         *             selection
         */
        std::vector<const xAOD::Jet*> m_trackJetsForBtagging;
    };
}  // namespace XAMPP
#endif
