# Checklist for Launching a Ntuple Production

A new ntuple production should be launched when all items below are fulfilled.

## Checklist

* Get the latest version of XAMPPbase (AnalysisTools/XAMPPplotting) and update the submodule (or some tag created for production)
  * If you checkout a tag, please make sure that you use a recent version of git. The commands 'setupATLAS' and 'lsetup git' are good for that.
    If you don't do so, you might use the wrong submodules.
* Check that the master of the framework compiles (hopefully it does already see CI) against the latest AthAnalysis release
  * Checking available releases: `showVersions athena | grep AthAnalysis-21 | sort -nk3 -t.`   
  * The main differences of the releases can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AnalysisBaseReleaseNotes21_2)
  * The status (missing/available) of the CP recommendations in the release can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AtlasPhysicsRun2Planning#Status_of_CP_recommendations_for)
* Check the sample list you want to run on (located [here](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/tree/master/XAMPPmonoH/data/SampleLists), and Sorting the list by DSID makes it easier)
  * Use the latest version of the datasets MC/data (check the p-tag via AMI)
  * Check for duplicated datasets in the list (especially important for duplicated runs of data, e.g. different m/f/p-tags and if the runs are on the GRL)
  * Full production requires a check of data (15, 16, 17), bkg MC and signal MC
  * Check also before the lifetime of your derivations, for MC [here](http://adc-mon.cern.ch/DAODProduction//DAILYCHECK_MC/) and for data [here](http://adc-mon.cern.ch/DAODProduction//DAILYCHECK_DATA/)
* Check for the latest [GRL, ilumicalc files](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase/tree/master/XAMPPbase/data/GRL) and [PRW files](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase/tree/master/XAMPPbase/data/PRWFiles) (and setup [here](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPbase/blob/master/XAMPPbase/share/BaseToolSetup.py))
    * The PRW got recently a bit complicated, when running on mc16a/c/d. Therefore, one has to [check the correct configuration, e.g. data15/16 or data17]([https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/share/MonoHToolSetup.py#L102)
* Check that all of the desired triggers (MET and single lepton) are included in the 3 job option files
* Update the 1 b-tagging CDI file:
  * For small-R jets [here](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/data/SUSYTools_MonoH.conf)
  * For track-jets the same as for small-R jets is used.
* Check the large-R uncertainties config, e.g. look for `Jet.LargeRuncConfig` in [SUSYTools_MonoH.conf](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/data/SUSYTools_MonoH.conf)
* Check that all 4 job options files have the right configuration: [MonoHToolSetup.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/share/MonoHToolSetup.py),
[runMonoH_0lep.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/share/runMonoH_0lep.py), [runMonoH_0lep.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/share/runMonoH_1lep.py) and [runMonoH_0lep.py](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/share/runMonoH_2lep.py)
* Check that the [SUSYTools file](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/data/SUSYTools_MonoH.conf) is correctly configured
* Check that the framework is locally running for all lepton channels (0/1/2) using MC and data (6 test jobs to run!!!)
  For MC it is reasonable to use the ttbar sample as it contributes in all analysis channels.
* Check that a file exists which assigns scattering process names to DSIDs properly. Normally this should be available under
  XAMPPmonoH/data, but running python XAMPPmonoH/scripts/assignDSIDtoProc.py again would be nice to make things safe.

If everythings works out fine create a new tag, announce it to the mailing list and keep the fingers crossed that all jobs finish fine.
