======================================
Scripts
======================================

The project contains some scripts to make the analysis easier by automating as work much as possible in steering the XAMPP framework. Most scripts are just simple wrappers for XAMPPplotting scripts. It is not required to use them but it can make your life a bit easier.

The scripts are located in `XAMPPmonoH/python <https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/python/>`_ and in `XAMPPmonoH/util <https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/util/>`_

You first have to make sure that you have an Athena TestArea set up (e.g. by executing the `setup script <https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/scripts/setup.sh>`_). Then you can run the steering scripts to produce histograms and plots.


---------------
Common tasks
---------------

These scripts automate the most common tasks like histogram creation and plotting.

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Download the XAMPP ntuples of a production
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script helps to download all samples that match a common identifier, e.g.

* `group.phys-exotics.*.0L0604*`
* `group.phys-exotics.*.1L0604*`
* `group.phys-exotics.*.2L0604*`


**Note:** You need to have set up `lsetup rucio` before using this script.

.. argparse::
   :module: XAMPPmonoH.downloadXAMPPntuples
   :func: getArgumentParser
   :prog: downloadXAMPPntuples

**Example:**
Download all ntuples in the 0lepton region with tag `0604` in the current directory::

  python MonoHSteeringScripts/getXAMPPntuples.py group.phys-exotics.*.0L0604*XAMPP


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Check that download/production of XAMPP ntuples of a production was complete
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The script ``validateXAMPPntuples.py`` is a wrapper for XAMPPplotting's ``CheckMetaData.py`` script. It compares the metadata with information stored in AMI to see if the local set of XAMPP ntuples is complete. It also checks the PRW file information stored in the ntuples to the information in AMI (MC only).

It is assumed that you will have stored the XAMPP ntuples in a separate folder for 0 lepton, 1 lepton and 2 lepton ntuples, so you have to run the script for each lepton multiplicity, which is specified by ``-l 0L/1L/2L``.

The script will check all 4 groups of ntuples, data1516, mc16a, data17, mc16d and generate latex/pdf files documenting the completeness as tables. The script takes quite some time to run, over 10 min usually.
It will also provide text files in which the luminosity in data files is stored. You can look at those most efficiently using `tail lumi_15.txt` (only the last line is relevant). The three numbers are "lumi in local ntuples", "reference lumi according to GRL for local ntuples" and the difference between those two numbers.


In case you want to run the ``XAMPPplotting/python/CheckMetaData.py`` script without wrapper, you please make sure that you include *both* input config and sample list containing the names of the DxAODs used to create the samples listed in the input config.
Otherwise it is not clear what should be compared on AMI.
Currently the `EXOT` derivation sample lists are the reference. In case you want to use a set of sample lists belonging to another derivation, you have to specify it using the ``--deriv`` argument.

**Note:** You need to have set up ``lsetup rucio pyami`` before using this script. Best practice is to use this command before the AthAnalysis setup is chosen. Otherwise conflicts may arise during execution.

**Note:** Also make sure that you use the appropriate datasets in ``XAMPPmonoH/data/SampleLists`` for the production you are checking (e.g. by checking out the appropriate tag for the production before running this script)

**Note** It does not hurt to look at the script in a text editor before executing it. For example, if you only want to validate mc16a and not data, you can comment out the lines that would call ``XAMPPplotting/python/CheckMetaData.py`` for data ntuples.


.. argparse::
   :module: XAMPPmonoH.validateXAMPPntuples
   :func: getArgumentParser
   :prog: validateXAMPPntuples


**Example:**
Check the completeness of the downloaded ntuples in the 0lepton region with tag ``0604`` in the directory ``XAMPPmonoH-00-06-04.0lep`̀`::

  python MonoHSteeringScripts/validateXAMPPntuples.py -d XAMPPmonoH-00-06-04.0lep/ validation_0604_0lep -l 0L --deriv HIGG


^^^^^^^^^^^^^^^^^^^^^
Create Input Configs
^^^^^^^^^^^^^^^^^^^^^

This is a script to create input configs for the Mono-h(bb) analysis based on the settings defined in ``XAMPPmonoH/data/inputConfig_config.json``. There are two ways two run this script: 

1) Create input configs with ntuples lying in a local directory:
   Use the option -i to point to the input directory.
2) Create input configs with ntuples located on a localgroupdisk:
   Do NOT use -i anymore, but specify a pattern, which all datasets of interest have in common. With -e one can specify, which patterns should not be part of the dataset name. Note that the pattern elements must be separated with " ". If the ntuples are not located at the own localgroupdisk, specify the groupdisk by using -r.
   Example:

   ``
   python createInputConfig.py -o /project/etp5/amatic/MonoH/XAMPP/source/XAMPPplotting/data/InputConf/MonoH/ZeroLepton/0L0601/mc16d --addPRWFiles -p "group.phys-exotics mc16 1L0601 r10201 XAMPP" -e "0L0601a" -r LRZ-LMU_LOCALGROUPDISK
   ``

**Note**: The script supports the creation of input configs for different campaigns. Use the ``--campaigns`` argument to select the campaign(s) among possible choices of ``mc16a,mc16d,mc16e`` which should be filtered out from the set of input files.


.. argparse::
   :module: XAMPPmonoH.createInputConfig
   :func: getArgumentParser
   :prog: createInputConfig

**Example:**
Create input config for all 0 lepton ntuples::

  /python MonoHSteeringScripts/createInputConfig.py -i /ptmp/mpp/pgadow/monoH/data/0700/0lep/ -o XAMPPplotting/data/InputConf/MonoH/MPPMU/XAMPPmonoH-00-07-00.SR


^^^^^^^^^^^^^^^^^^^^^
Create DSConfigs
^^^^^^^^^^^^^^^^^^^^^

Automatically create DS configs to be used in XAMPPplotting to create plots out of the histograms.

The names of the histograms are defined in the configuration file ``XAMPPmonoH/data/DSConfig_config.json``. This is also the file that defines the colour scheme of the plots. Usually only the background samples are defined in this file.

If additional samples, e.g. some signals, should be appended to the DS config, the option ``--additional`` can be used to add the content of other files (e.g. those stored in ``XAMPPmonoH/data/DSConfig_signal``) to the DSConfig.

**Note:** There is the ``--inputConfigs`` option to add the path to the folder with your data input configs to the DSconfig (*absolute path required !!!*) to automatically calculate the luminosity for scaling the MC correctly for the plot. If this option is chosen, the luminosity set by the `-l` option will be overwritten.


.. argparse::
   :module: XAMPPmonoH.createDSConfig
   :func: getArgumentParser
   :prog: createDSConfig

**Example:**
Create DSConfig for background ntuples and include signals defined in ``XAMPPmonoH/data/DSConfig_signal/signal.conf``::

  python XAMPPmonoH/python/createDSConfig.py -a XAMPPmonoH/data/DSConfig_signal/signal.conf -i /ptmp/mpp/pgadow/Cluster/OUTPUT/2018-11-22/monoHbb_output/

^^^^^^^^^^^^^^
Produce plots
^^^^^^^^^^^^^^

This script allows to produce plots from a collection of XAMPP ntuples almost automatically.

The script automatically scans the input ntuples and creates the required input configs to create histograms and creates the required DSConfigs to create plots from the histograms.

To allow for quick identification of the plotting output it is possible to define a job name using the option ``-n``.
All other settings are configured using a configuration file. The default configuration file is ``XAMPPmonoH/data/runHistos_config.json``. It is possible to edit this file before running the script using the option ``-e``.

The default settings of this file are::

    "inputBasePath": "XAMPPplotting/data/InputConf/MonoH/MPPMU",
    "outputBasePath": "/ptmp/mpp/pgadow/monoH/plottingOutput/",
    "tag": "00-08-00",
    "ntuplesPath": {
        "SR": "/ptmp/mpp/pgadow/monoH/data/XAMPPmonoH-00-07-00.0lep/",
        "CR1": "",
        "CR2ee": "/ptmp/mpp/pgadow/monoH/data/XAMPPmonoH-00-07-01.2lep/",
        "CR2mumu": "/ptmp/mpp/pgadow/monoH/data/XAMPPmonoH-00-07-01.2lep/",
        "PH": ""
    },
    "regions": ["SR"],
    "samples": ["data15", "data16", "data17", "data18", "stops", "stopt", "stopWt", "ttbar", "VHbb", "Wenu_cl", "Wenu_hf", "Wenu_hpt", "Wenu_l", "Wmunu_cl", "Wmunu_hf", "Wmunu_hpt", "Wmunu_l", "Wtaunu_cl", "Wtaunu_hf", "Wtaunu_hpt", "Wtaunu_l", "WW", "WZ", "Zee_cl", "Zee_hf", "Zee_hpt", "Zee_l", "Zmumu_cl", "Zmumu_hf", "Zmumu_hpt", "Zmumu_l", "Ztautau_cl", "Ztautau_hf", "Ztautau_hpt", "Ztautau_l", "Znunu_cl", "Znunu_hf", "Znunu_hpt", "Znunu_l", "ZZ"],
    "signals": ["zp2hdm", "zphxxbb", "shxxbb", "2HDMa", "monoSbb"],
    "doBlinding": true,
    "doSystematics": false,
    "doPRW": true,
    "doFitInputs": false,
    "skipInputConfigCreation": false,
    "campaigns": ["mc16a", "mc16d"],
    "luminosity": "auto",
    "engine": "LOCAL"


**Explanation of settings:**

+-------------------------+--------------------------------------------------------+
| Settings name           | explanation                                            |
+=========================+========================================================+
| inputBasePath           | Input configs created on-the-fly will be stored here   |
+-------------------------+--------------------------------------------------------+
| outputBasePath          | Histogram and plot output will be stored here          |
+-------------------------+--------------------------------------------------------+
| tag                     | Input config filename: XAMPPmonoH-<tag>.<identifier>   |
+-------------------------+--------------------------------------------------------+
| regions                 | Regions to be processed (SR, CR1, CR2ee, CR2mumu, PH)  |
+-------------------------+--------------------------------------------------------+
| samples                 | Process these background samples (filters input conf.) |
+-------------------------+--------------------------------------------------------+
| signals                 | Process these signal samples (filters input configs)   |
+-------------------------+--------------------------------------------------------+
| doBlinding              | Blind signal region mass window? true / false          |
+-------------------------+--------------------------------------------------------+
| doSystematics           | Run with systematics? true / false                     |
+-------------------------+--------------------------------------------------------+
| doPRW                   | Run with PRW? true / false (should be always true)     |
+-------------------------+--------------------------------------------------------+
| doFitInputs             | Create strongly reduced set of output histograms       |
+-------------------------+--------------------------------------------------------+
| campaigns               | Process these MC campaigns (match dataXX in samples!)  |
+-------------------------+--------------------------------------------------------+
| luminosity              | Which luminosity to scale MC ("auto": calc from input) |
+-------------------------+--------------------------------------------------------+
| engine                  | Which batch system (or "LOCAL") for processing?        |
+-------------------------+--------------------------------------------------------+

.. argparse::
   :module: XAMPPmonoH.runHistos
   :func: getArgumentParser
   :prog: runHistos


**Example:**
Create plots and view + edit configuration file before running script::

  python XAMPPmonoH/python/runHistos.py -e


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get yields from XAMPPplotting histograms
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script allows to calculate event yields from any ROOT histogram, e.g. also those created by XAMPPplotting's ``WriteDefaultHistos``.

.. argparse::
   :module: XAMPPmonoH.getYields
   :func: getArgumentParser
   :prog: getYields



^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Convert XAMPPplotting output histograms to WSMaker input format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In order to run WSMaker easily we have a script to convert the XAMPPplotting output format into WSMaker input format. 

.. argparse::
   :module: XAMPPmonoH.XAMPPplottingToWSMaker
   :func: getArgumentParser
   :prog: XAMPPplottingToWSMaker

Currently it expects an input file format that matches the output of `runHistos.py` (i.e. file names like WW.root).

When systematics jobs are run, it is important to switch on the jet flavour splitting for W and Z+jets samples (pick the dedicated RunConfig region files).

Special care must be taken in case of W+jets and Z+jets input! Currently this script works only if beforehand all W+jets and Z+jets ROOT files are merged, respectively.
So for instance, if the W+jets background is split into Wenu_l.root, Wenu_cl.root, Wenu_hf.root, Wenu_hpt.root, [enu -> munu], [enu -> munu], all this files must be merged into W.root using hadd first.
A similar condition must be respected for Z+jets splittings. Then only the resulting W.root and Z.root files must be considered by this script.


Also data input files must be prepared before running this script. Assuming there is one folder with data15.root and data16.root files and another folder with a data17.root file, where this script
is run twice accordingly, the data15.root and data16.root files first need to be merged using hadd where the result is called data.root. The data17.root file must just be renamed to data.root.::

  # data1516
  hadd data.root data{15,16}.root
  rm data{15,16}.root

  # data17
  mv data17.root data.root

  # W/Z+jets
  hadd W.root W{enu,munu,taunu}_{cl,hf,hpt,l}.root
  hadd Z.root Z{ee,mumu,tautau,nunu}_{cl,hf,hpt,l}.root
  rm W{enu,munu,taunu}_{cl,hf,hpt,l}.root
  rm Z{ee,mumu,tautau,nunu}_{cl,hf,hpt,l}.root


Often it occurs that some data runs are missing (sorry ATLAS operations). In this case, the integrated luminosity of the data at hand must be determined. One way to do this conveniently is to just run the script
https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting/blob/master/python/CheckMetaData.py
with the --lumi option. The outcome must then be passed to this script here via the -l option.

**What next? **

In case some systematics still need to be renamed, an example is provided with ``XAMPPmonoH/utils/formatSystematicHistos.C``.

Currently the fitting code is developed here:

https://gitlab.cern.ch/atlas-phys/exot/jdm/ANA-EXOT-2018-46/fitting-and-limits/WSMaker_MonoH


---------------
Bookkeeping
---------------

The scripts in ``XAMPPmonoH/python/bookkeeping`` are designed to simplify the bookkeeping of

- sample lists: these lists are stored in ``XAMPPmonoH/data/SampleLists`` and contain the DxAODs used for ntuple creation
- ntuple lists: these lists are circulated to the analysis team once a new ntuple production is finished and contain the XAMPP ntuples required for analysis with XAMPPplotting



^^^^^^^^^^^^^^^^^^^^^^^^^
Create new MC sample list
^^^^^^^^^^^^^^^^^^^^^^^^^

This script works both for data and MC and creates sample lists for both AODs and all derivations that are defined in the configuration files:

- ``XAMPPmonoH/data/samplelist.txt``: list of all dsids + sample names with option to put a veto (will remove sample from DAOD lists) + option to limit sample only to a specific derivation (if multiple derivations are used)
- ``XAMPPmonoH/data/samplelist_config.json``: configuration file with settings for sample list creation, e.g. output folder, good run lists, all derivations for which sample lists should be created, rtags, MC campaign, signal keywords, blacklisted samples, and fastsim / fullsim switches 

.. argparse::
   :filename: ../python/bookkeeping/CreateSampleList.py
   :func: getArguments
   :prog: CreateSampleList


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Check sample list for completeness
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script can be used to check a sample list for completeness.

A sample list containing data is checked against the good run list (GRL) for completeness.

A sample list containing MC is checked against the content of ``XAMPPmonoH/data/samplelist.txt`` for completeness.

.. argparse::
   :filename: ../python/bookkeeping/CheckSampleList.py
   :func: getArgParser
   :prog: CheckSampleList


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Check XAMPP ntuple list for completeness
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script can be used to check if the content of a filelist with XAMPP ntuples is complete with respect to a set of sample lists (e.g. those sample lists that were used to create the ntuples during production).

.. argparse::
   :filename: ../python/bookkeeping/CheckNtupleList.py
   :func: getArgParser
   :prog: CheckNtupleList


---------------
Systematics
---------------

There exist some scripts that allow to plot the systematic up/down variations in support-note-plot quality and create tables listing the magnitude of the up/down variations:

https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/tree/master/XAMPPmonoH/python/studysystematics

---------------
Other
---------------

^^^^^^^^^^^^^^^^^^^^^^
Update DSID proc list 
^^^^^^^^^^^^^^^^^^^^^^

The background modeling uncertainties are assigned based on a process list that matches DSIDs to processes:
https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/data/mc16_dsid_proc.txt

If a sample is not included on this list, the framework will crash for processing this sample.

To simplify the task of updating this list, a very basic script that extracts the samples and DSIDs from the sample lists exists:

https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/blob/master/XAMPPmonoH/python/assignDSIDtoProc.py


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Signal significance studies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are some scripts that can be used to study the signal significance:

https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPmonoH/tree/master/XAMPPmonoH/python/studysignalsignificance
