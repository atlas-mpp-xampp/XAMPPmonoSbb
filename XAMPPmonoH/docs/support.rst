=======
Support
=======

The easiest way to get help with the project is to write to the mailing list ``atlas-sw-pat-xampp-framework@cern.ch``.
If you have specific questions about the Mono-h(bb) analysis, you can also write to ``atlas-phys-exotics-monowzh-hadronic@cern.ch``.

--------------------
Indices and tables
--------------------

* :ref:`genindex`
