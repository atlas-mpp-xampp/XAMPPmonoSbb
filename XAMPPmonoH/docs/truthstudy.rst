=============
Truth studies
=============

For several cases it can be interesting to study simulated events (Monte Carlo) on truth level.
This means that the direct output from the event generator is analysed, without any detector simulation.
If the events would be processed by a detector simulation one speaks of analysis on reconstruction level.

The most common use case might be to study the properties of a new signal model and scan a large range of 
model parameters since the event generation is rather fast compared to the detector simulation.

-----------------
Event generation
-----------------

If you are unfamiliar with ATLAS MC software, please have a look into the tutorial:

- `General overview (slides) <https://indico.cern.ch/event/757797/contributions/3141899/attachments/1737197/2810424/2018-10-19_SoftTutorial_MC_mcfayden.pdf>`_ 
- `Hands-on tutorial (TWiki) <https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialMonteCarlo>`_

A collection of scripts and job options for MC generation is linked here:

https://gitlab.cern.ch/pgadow/monoHbbSignalStudy/


---------------
Bookkeeping
---------------

These scripts simplify the bookkeeping of generated samples and keeping track of the generated cross-sections.
They are designed to work with the scripts stored in https://gitlab.cern.ch/pgadow/monoHbbSignalStudy/

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get cross-sections from AMI
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script allows you to query the `AMI database <https://ami.in2p3.fr/>`_ for cross-sections.

.. argparse::
   :filename: ../python/studysignaltruth/getXSectionFromAMI.py 
   :func: getArgs
   :prog: getXSectionFromAMI


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Get cross-sections from log files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script allows you to search a large amount of log files from the event generation for the cross-section and other interesting information.

The cross-sections are either printed to the screen or can be saved to a JSON file using the ``-o`` option.
For further analysis it is required to store the cross-section information to a file.

.. argparse::
   :filename: ../python/studysignaltruth/getXSectionFromLog.py 
   :func: getArgs
   :prog: getXSectionFromLog


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Analyse cross section files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script allows you to analyse the content of a cross-section information JSON file created with ``getXSectionFromLog.py``.

This script is more or less something like a swiss army knife. It can print the cross-sections in a nice format, write them to a csv file, plot the cross-sections or even generate snippets for the XAMPPmonoH input config steering script.
Check out the options of the script:

.. argparse::
   :filename: ../python/studysignaltruth/analyseXSection.py
   :func: getArgs
   :prog: analyseXSection

It may well be that you have to modify this script if you want to study a specific signal, so don't be afraid to look in the source code!


^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Create TRUTH sample lists
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This script allows you to generate sample lists for TRUTH DxAODs (or other samples) to be stored in ``XAMPPmonoH/data/GroupDiskLists/<RSE>``

.. argparse::
   :filename: ../python/studysignaltruth/createTruthSampleList.py
   :func: getArgs
   :prog: createTruthSampleList
