#! /usr/bin/env python
import os
import argparse
import sys
from XAMPPbase.runAthena import applyZnunuSampleFix
from ClusterSubmission.Utils import ReadListFromFile, CheckRemainingProxyTime
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def GetoutDSName(DS, ProdName, names):
    """Split DS name and ensure that i follows ATLAS conventions for data filenames."""
    parts = DS.replace("/", "").strip().split('.')
    project = parts[0]
    dsid = parts[1]
    name = 'dataset'
    if 'data' in project:
        name = 'data'
    elif dsid in names.keys():
        name = names[dsid]
    derivation = parts[-2].replace('DAOD_', '')
    tags = parts[-1]

    outDS = project + '.' + dsid + '.' + name + '.' + ProdName + '.' + derivation + '.' + tags

    if len('group.phys-exotics.' + outDS +
           '_XAMPP') > 120:  # actually the limit is 135, but a safety margin of 15 characters for the production name is never bad
        logging.warning('dataset name too long. Please choose a production name with at most 15 characters.')
        logging.warning("outDS name: {outname}, length: {length}".format(outname=outDS, length=len(outDS)))

    return outDS


def getNameDict(nameDictPath):
    """Read text file with DSIDs and sample names to parse those
       into a dictionary. This dictionary will be used to
       write human-readable short sample names to the filename
       based on the DSID of the sample."""
    nameDict = {}
    with open(os.path.expandvars(nameDictPath)) as file:
        for line in file:
            if line.startswith('#') or len(line.strip()) == 0:
                continue
            splitline = line.split()
            nameDict[splitline[0].strip()] = splitline[1].strip()
    return nameDict


def setupGridSubmitArgParser():
    """Set up argument parser to process command line input for specifying job options."""
    parser = argparse.ArgumentParser(
        description='This script submits the analysis code to the grid. For more help type \"python XAMPPmonoH/python/SubmitToGrid.py -h\"',
        prog='SubmitToGrid',
        conflict_handler='resolve',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--noSyst', help='No systematics will be submitted', action="store_true", default=False)
    parser.add_argument("--list", required=True, help="List of datasets given")
    parser.add_argument("--jobOptions", help="The athena jobOptions file to be executed", default="XAMPPmonoH/share/runMonoH.py")
    parser.add_argument("--production", help="Name of the production, must be less than 15 characters", required=True)
    parser.add_argument("--filesPerJob",
                        type=int,
                        default=-1,
                        help="Files per job to be submitted (warning: small value might drive job into exhausted state)")
    parser.add_argument("--productionRole", action="store_true", help="Use exotics production role for submitting jobs")
    return parser


def SubmitToGrid():
    """Submit jobs to grid using XAMPPmonoH framework."""
    RunOptions = setupGridSubmitArgParser().parse_args()
    while CheckRemainingProxyTime() < 100:
        continue

    names = getNameDict('$TestArea/XAMPPmonoH/data/samplelist.txt')

    DataSetsToSubmit = ReadListFromFile(RunOptions.list)
    for DS in DataSetsToSubmit:
        # check for and if required apply bug fix for Znunu samples with wrong dsid in metadata
        HFFilter = applyZnunuSampleFix(DS)
        Cmd = "python $TestArea/XAMPPbase/XAMPPbase/python/SubmitToGrid.py --jobOptions {jo} --inputDS {inDS} --outDS {outDS} {syst} {filesPerJob} {productionRole} {znunubugfix}".format(
            jo=RunOptions.jobOptions,
            inDS=DS,
            outDS=GetoutDSName(DS, RunOptions.production, names),
            syst="--noSyst" if RunOptions.noSyst else "",
            filesPerJob="--nFilesPerJob " + str(RunOptions.filesPerJob) if RunOptions.filesPerJob > 0 else "",
            productionRole="--productionRole phys-exotics" if RunOptions.productionRole else "",
            znunubugfix="--dsidBugFix {hffilter}".format(hffilter=HFFilter) if HFFilter else "")
        logging.info("Executing " + Cmd)
        os.system(Cmd)


if __name__ == '__main__':
    SubmitToGrid()
