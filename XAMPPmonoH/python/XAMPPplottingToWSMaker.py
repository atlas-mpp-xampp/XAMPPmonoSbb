############################
#
# Author : Efe Yigitbasi <efe.yigitbasi@cern.ch>
#
############################
#!/bin/env python
import os
import sys
import ROOT
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

ROOT.PyConfig.IgnoreCommandLineOptions = True

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None

if TestArea is None:
    TestArea = './'


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Script for converting XAMPPplotting output to WSMaker input",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='input_dir', help='Folder with XAMPPplotting output files', default="./")
    parser.add_argument('-o',
                        '--output',
                        dest='output_dir',
                        help='Output directory to put WSMaker input file',
                        default='./WSMakerInput.root')
    parser.add_argument('-l',
                        '--lumi',
                        dest='lumi',
                        default=138.96,
                        help='Integrated luminosity to be applied as signal and background scaling.')
    parser.add_argument('-c', '--charge', action='store_true', help='Use muon charge instead of higgs candidate mass.')
    parser.add_argument('--mee', action='store_true', help='Use dielectron mass instead of higgs candidate mass.')
    parser.add_argument('--mmumu', action='store_true', help='Use dimuon mass instead of higgs candidate mass.')
    parser.add_argument('-n', '--nominal', action='store_true', help='Do nominal only.')
    parser.add_argument('-r', '--region', action='store', help='Do specific region')
    parser.add_argument('-p', '--pattern', action='store', nargs='+', help='Do specific file')
    return parser


class XAMPPplottingToWSMaker:

    input_directory = ""
    output_directory = ""
    file_list = []
    histos = {}

    def __init__(self, options):
        self.input_directory = options.input_dir
        self.output_directory = options.output_dir
        self.lumi = float(options.lumi)
        self.useCharge = bool(options.charge)
        self.useMee = bool(options.mee)
        self.useMmuMu = bool(options.mmumu)
        self.doNominal = bool(options.nominal)
        self.region = options.region
        self.pattern = options.pattern

        self.var = "mBB"
        if self.useCharge:
            self.var = "Charge"
        elif self.useMee:
            self.var = "mEE"
        elif self.useMmuMu:
            self.var = "mMuMu"

    def mBB(self, region_name):

        if "Resolved" in region_name:
            mBB = "m_jj_corr"
        else:
            mBB = "m_J_corr"

        if self.useCharge:
            mBB = "mu_charge"
        elif self.useMee:
            mBB = "m_ee"
        elif self.useMmuMu:
            mBB = "m_mumu"

        return mBB

    def sample_name(self, file_name):

        file_name = file_name.split("/")[-1].split(".")[0]
        if any(Zjets in file_name for Zjets in ["Zee", "Zmumu", "Ztautau", "Znunu"]): sample_name = "Z"
        elif any(Wjets in file_name for Wjets in ["Wenu", "Wmunu", "Wtaunu"]): sample_name = "W"
        elif any(VHbb in file_name for VHbb in ["VHbb"]): sample_name = "VHbb"
        elif any(ttbar in file_name for ttbar in ["ttbar"]): sample_name = "ttbar"
        elif any(data in file_name for data in ["data"]): sample_name = "data"
        else:
            sample_name = file_name.replace("_Resolved", "").replace("_Merged", "")

        return sample_name

    def get_outname(self, region_name, sample_name, syst_name):

        if sample_name.startswith("2HDMa"):
            sample_name = sample_name.replace("2HDMa", "a2HDM")

        if "Resolved" in region_name:
            nfatjet = "0p"
            if region_name.endswith("j"):
                njtag = region_name.split("_")[-1].replace("j", "")
                if njtag.endswith("m"):
                    if "p" in njtag:
                        njtag = njtag.replace("p", "").replace("m", "")
                    else:
                        njtag = "2" + njtag.replace("m", "")
                is_njtag = 1
            else:
                njtag = "2p"
                is_njtag = 0
            btag = region_name.split("_")[-1 - is_njtag].replace("b", "")
            if region_name.split("_")[-2 - is_njtag] == "Resolved":
                met = "150_500ptv"
            elif region_name.split("_")[-3 - is_njtag] == "Resolved": # This is for recycling region SR_Resolved_500_2b
                met = region_name.split("_")[-2 - is_njtag] + "ptv"
            else:
                met = region_name.split("_")[-3 - is_njtag] + "_" + region_name.split("_")[-2 - is_njtag] + "ptv"
        else:
            njtag = "0p"
            nfatjet = "1p"
            btag = region_name.split("_")[-1].replace("b", "").replace("w", "")
            if region_name.split("_")[-2] == "Merged":
                met = "500ptv"
            elif region_name.split("_")[-3] == "Merged":
                met = region_name.split("_")[-2] + "ptv"
            elif region_name.split("_")[-4] == "Merged":
                met = region_name.split("_")[-3] + "_" + region_name.split("_")[-2] + "ptv"
            else:
                raise Exception("Can't identify met bin for merged region! ({})".format(region_name))

        reg = region_name.split("_")[0]

        outname = "{sample_name}_{btag}tag{nfatjet}fat{njtag}jet_{met}_{reg}_{var}{syst_name}".format(sample_name=sample_name,
                                                                                                      btag=btag,
                                                                                                      nfatjet=nfatjet,
                                                                                                      njtag=njtag,
                                                                                                      met=met,
                                                                                                      reg=reg,
                                                                                                      var=self.var,
                                                                                                      syst_name=syst_name)

        return outname

    def convert(self):
        for item in os.listdir(self.input_directory):
            if self.pattern and (not any(pattern in item for pattern in self.pattern)): continue
            if not item.endswith("root"): continue
            path = os.path.join(self.input_directory, item)
            if not os.path.isdir(path):
                self.file_list.append(path)

        for file_name in self.file_list:
            sample_name = self.sample_name(file_name)
            if sample_name in ["Z", "W"]:
                is_Vjets=True
                flavor_list=["V_ll", "V_cl", "V_cc", "V_bl", "V_bc", "V_bb"]
            else:
                is_Vjets=False
                flavor_list=[""]


            fin = ROOT.TFile(file_name)
            print("Reading file: " + file_name)
            for i in range(ROOT.gDirectory.GetListOfKeys().GetSize()):
                dir_name = ROOT.gDirectory.GetListOfKeys().At(i).GetName()
                if dir_name.split("MonoH")[1] == "_Nominal":
                    if "ttbar_PowHw" in file_name:
                        syst_name = "_SysttbarPowHw"
                    elif "ttbar_aMcPy8" in file_name:
                        syst_name = "_SysttbaraMcPy8"
                    else:
                        syst_name = ""
                else:
                    if self.doNominal: continue
                    syst_name = '_Sys' + dir_name.split("MonoH")[1][1:]

                ROOT.gDirectory.cd(dir_name)
                for j in range(ROOT.gDirectory.GetListOfKeys().GetSize()):
                    region_name = ROOT.gDirectory.GetListOfKeys().At(j).GetName()
                    if self.region and region_name != self.region: continue
                    if not (region_name.endswith("b") or region_name.endswith("j")): continue
                    mBB = self.mBB(region_name)

                    for flavor_name in flavor_list:
                        if is_Vjets:
                            sample_outname = sample_name[0] + flavor_name.split("_")[-1].replace('ll', 'l')
                            hist_name = dir_name + "/" + region_name + "/" + flavor_name + "/" + mBB
                            out_name = self.get_outname(region_name, sample_outname, syst_name)
                        else:
                            hist_name=dir_name + "/" + region_name + "/" + mBB
                            out_name = self.get_outname(region_name, sample_name, syst_name)

                        h1 = fin.Get(hist_name)
                        if not h1:
                            raise Exception("ERROR: Can't find {hname} in {fname}".format(hname=hist_name, fname=fin.GetName()))

                        h1.SetNameTitle(out_name, out_name)
                        if sample_name != "data":
                            h1.Scale(self.lumi)

                        if out_name not in self.histos:
                            self.histos[out_name] = h1.Clone()
                            self.histos[out_name].SetDirectory(0)
                        else:
                            self.histos[out_name].Add(h1)

                ROOT.gDirectory.cd("../")

            fin.Close()

    def write(self):

        if os.path.dirname(self.output_directory) and not os.path.isdir(os.path.dirname(self.output_directory)):
            os.makedirs(os.path.dirname(self.output_directory))

        fout = ROOT.TFile(self.output_directory, "RECREATE")
        fout.mkdir("Systematics/")

        for out_name in self.histos:
            if not "_Sys" in out_name:
                fout.cd()
                self.histos[out_name].Write()
            else:
                fout.cd("Systematics")
                self.histos[out_name].Write()

        fout.Close()


def main():
    options = getArgumentParser().parse_args()
    converter = XAMPPplottingToWSMaker(options)
    converter.convert()
    converter.write()


if __name__ == "__main__":
    main()
