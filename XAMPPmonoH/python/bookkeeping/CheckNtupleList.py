#! /usr/bin/env python
import os
from argparse import ArgumentParser
from collections import Counter
from ClusterSubmission.Utils import ResolvePath
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None

if TestArea is None:
    TestArea = '.'


def getArgParser():
    """Get arguments from command line."""
    parser = ArgumentParser(
        description='This script checks if a text file containing a list with XAMPP ntuples (i.e. the output of XAMPPmonoH) \
        contains all samples specified on a set of sample lists with the names of DxAODs that went into the ntuple production. \
        Typical use case: The analysis team just finished a ntuple production on the grid and compiled a text file \
        containing all ntuples for download using "rucio download --ndownloader 5 `cat textfile.txt`". \
        Before sending the list to the team you want to check using this script if this text file is complete with respect \
        to the sample list containing the DxAODs that went into the ntuple production.')
    parser.add_argument('inputFile', help='your input file listing XAMPP ntuples')
    parser.add_argument(
        '--sampleLists',
        nargs='*',
        default=[],
        help='sample lists with DxAODs used as a reference to check the ntuple file list for completeness (be sure that those are complete!)'
    )
    return parser


def checkForDuplicates(infile):
    logging.info('<checkForDuplicates> Checking input file {inputfile} for duplicated runs/DSIDs!'.format(inputfile=infile))
    fileToOpen = open(str(infile), "r")
    nfiles = 0
    identifiers = []
    for line in fileToOpen:
        logging.debug("scanning line {line}".format(line=line))
        line = line.strip()
        # ignore comments
        if line.startswith('#') or len(line) == 0:
            logging.debug("line commented out -> ignoring it")
            continue
        # group.phys-exotics.data/mc.DSID.[...].tag
        dsid = line.split('.')[3] if 'XAMPP' in line else line.split('.')[1]
        tag = line.split('.')[-1].replace('_XAMPP', '')
        identifiers.append('DSID:' + dsid + '-TAG:' + tag)
        nfiles += 1

    # check for duplicates
    checkedList = [k for k, v in Counter(identifiers).items() if v > 1]
    if len(checkedList) > 0:
        logging.info('There are {nfiles} files in your sample list, and {nduplicates} duplicates have been found!'.format(
            nfiles=nfiles, nduplicates=len(checkedList)))
        for dup in checkedList:
            logging.warning('Please check the following [run/DSID, tag] combinations : ' + dup)
            for i in fileToOpen:
                logging.warning(i)
                if dup in i:
                    logging.warning('Please check the following files:')
                    logging.warning(i)
    else:
        logging.info('There are {nfiles} files in your sample list, and no duplicates have been found!'.format(nfiles=nfiles))
    return identifiers


def main():
    """Check XAMPP ntuple lists for completeness and consistency with sample lists."""
    RunOptions = getArgParser().parse_args()

    logging.info('# -------------------------------------------------------------')
    logging.info('# PREPARE SAMPLE LISTS WITH DxAODs (USED AS REFERENCE)         ')
    logging.info('# -------------------------------------------------------------')
    logging.info(
        "Checking now the reference sample lists with the DxAODs that were used as input for ntuple production contain no duplicate samples."
    )
    # reference sample list
    listOfSamples = []

    for sampleList in RunOptions.sampleLists:
        logging.info('Checking {samplelist}'.format(samplelist=sampleList))
        tempList = checkForDuplicates(sampleList)
        listOfSamples.extend(tempList)

    # check combined reference sample list again for duplicates
    checkedList = [k for k, v in Counter(listOfSamples).items() if v > 1]
    if len(checkedList) > 0:
        logging.warning('In your combined sample lists' + str(len(checkedList)) + 'duplicates have been found!')
        for dup in checkedList:
            logging.warning('Please check the following [run/DSID, tag] combinations :' + dup)

    logging.info('# -------------------------------------------------------------')
    logging.info('# DUPLICATE CHECK FOR NTUPLE LIST                              ')
    logging.info('# -------------------------------------------------------------')
    logging.info(
        "Checking now that the ntuple list with the output from the ntuple production is complete with respect to the sample lists that contain the DxAODs that were used as input for ntuple production."
    )
    listOfNtuples = checkForDuplicates(RunOptions.inputFile)

    # now finally check ntuple list for completeness against reference sample list
    logging.info('# -------------------------------------------------------------')
    logging.info('# MISSING SAMPLES                                              ')
    logging.info('# -------------------------------------------------------------')
    if len(listOfSamples) > 0:
        for i in listOfNtuples:
            if i not in listOfSamples:
                logging.warning(
                    "You have included the dsid/rtag combination {comb} but this is not on any reference sample list!".format(comb=i))

        missingOnSampleList = False
        for i in listOfSamples:
            if i not in listOfNtuples:
                missingOnSampleList = True
                logging.warning("You are missing dsid/rtag combination {miss}".format(miss=i))
        if missingOnSampleList is False:
            logging.info("You are not missing any run of the sample lists, that is good!")


if __name__ == "__main__":
    main()
