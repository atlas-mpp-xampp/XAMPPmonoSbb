#!/usr/bin/env python

from argparse import ArgumentParser
parser = ArgumentParser(description='Check if there are ntuples that were produced with multiple tags')
parser.add_argument("-i", "--Input", help="give path to directory with ntuples", default="")
parser.add_argument("-t", "--Text", help="give path to text file listing all available ntuples", default="")
args = parser.parse_args()

import os

# assume structure of samples like:
# group.phys-exotics.mc16_13TeV.364185.WtaunuC_Sh221.1202e0Syst.EXOT27.e5340_s3126_r10724_p3947_XAMPP


def getTag(ntuplename):
    return ntuplename.split(".")[5]


# tag number: always the first four numbers of the tag
def getTagNumber(tag):
    return tag[:3]


def getMCcampaign(tag):
    return tag[4]


def getRetryNumber(tag):
    return tag[5]


def getNameBeforeProdTag(ntuplename):
    return ntuplename.split(getTag(ntuplename))[0]


def getNameAfterProdTag(ntuplename):
    return ntuplename.split(getTag(ntuplename))[1]


def doCheck(ntup_i, ntup_j):
    tag_i = getTag(ntup_i)
    mc_i = getMCcampaign(tag_i)
    retry_i = getRetryNumber(tag_i)
    beforeTag_i = getNameBeforeProdTag(ntup_i)
    afterTag_i = getNameBeforeProdTag(ntup_i)
    tag_j = getTag(ntup_j)
    mc_j = getMCcampaign(tag_j)
    retry_j = getRetryNumber(tag_j)
    beforeTag_j = getNameBeforeProdTag(ntup_j)
    afterTag_j = getNameBeforeProdTag(ntup_j)
    if (beforeTag_i == beforeTag_j and afterTag_i == afterTag_j and not tag_i == tag_j):
        if (mc_i == mc_j and not retry_i == retry_j):
            print "found ntuple file with multiple production tags:"
            print ntup_i
            print ntup_j


if not args.Input == "":
    for i, ntup_i in enumerate(os.listdir(args.Input)):
        for j, ntup_j in enumerate(os.listdir(args.Input)):
            if j > i: continue
            doCheck(ntup_i, ntup_j)

if not args.Text == "":
    openedTextFile = open(args.Text)
    ntupLines = openedTextFile.readlines()
    for i, ntup_i in enumerate(ntupLines):
        for j, ntup_j in enumerate(ntupLines):
            if j < i: continue
            doCheck(ntup_i, ntup_j)
