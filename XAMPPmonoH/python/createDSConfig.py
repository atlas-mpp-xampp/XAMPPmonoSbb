#!/usr/bin/env python
"""
This scripts creates DSConfig files used for XAMPPplotting histogram plot creation
automatically based on the ROOT files containing the histograms needed for plotting
in the input directory and the configuration in the config file (json format).

The script is meant to be used to create DSConfigs for histograms created with XAMPPplotting's WriteDefaultHistos.
These DSConfigs can then be used for XAMPPplotting/python/DataMCPlots.py or MCPlots.py
"""
import os
import json
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from ClusterSubmission.Utils import ResolvePath

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None

if TestArea is None:
    TestArea = '../'


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Script for creating DSconfigs for XAMPPplotting", formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i',
                        '--input',
                        dest='input',
                        help='Folder with root files from WriteDefaultHistos (must be an absolute path)',
                        required=True)
    parser.add_argument('-o', '--output', dest='output', help='Output file for the DSConfig', default='./DSConfig_MonoH.py')
    parser.add_argument('-c',
                        '--config',
                        dest='config',
                        help='Configuration file containing information about MC samples.',
                        default=ResolvePath('XAMPPmonoH/DSConfig_config.json'))
    parser.add_argument('-a',
                        '--additional',
                        dest='additional',
                        nargs='+',
                        help='Path to file(s) containing additional entries for DSConfig file, e.g. data-driven background estimates.')
    parser.add_argument('-l',
                        '--lumi',
                        dest='lumi',
                        help='Luminosity (overwritten by --inputConfigs and auto lumi calculation)',
                        default='80.')
    parser.add_argument(
        '--inputConfigs',
        nargs='*',
        dest='inputconfigs',
        help=
        'Path to folder containing input configs or input config(s) to calculate luminosity automatically. Need to give absolute paths here (no ../)',
        default=[])
    return parser


def makeHeader(basepath):
    """Return a string containing a universal header for the DSConfig file."""
    header = """
#!/usr/bin/env python
import os
from XAMPPplotting.Defs import *
ROOT.gStyle.SetOptStat(0)

## --------------------------------------------------------
## General options
## --------------------------------------------------------
BasePath = '{basepath}'

""".format(basepath=basepath)
    return header


def makeDataEntry(data, lumi, inputConfigs=''):
    """Return a string containing an entry about data for the DSConfig file."""
    # collect data files
    samplelist = ""
    if len(data) == 0:
        print("ERROR: No data found!")
        return ""

    for d in data[:-1]:
        samplelist += "os.path.join(BasePath, \"{d}.root\"), ".format(d=d)
    # treat last item in list differently (no comma at the end)
    samplelist += "os.path.join(BasePath, \"{d}.root\")".format(d=data[-1])

    dataEntry = """
## --------------------------------------------------------
## Specify data and samples
## --------------------------------------------------------

"""
    # if input configs are avaliable, calculate luminosity automatically
    if inputConfigs:
        dataEntry += """
# calculate luminosity automatically from data input configs
from XAMPPplotting.FileUtils import ReadInputConfig, ResolvePath
from XAMPPplotting.CheckMetaData import GetNormalizationDB
from XAMPPplotting.CalculateLumiFromIlumicalc import CalculateRecordedLumi
# automatically calculate lumi
ConfigPaths = "{InputConfigs}".split()
dataConfigs = set()

for configpath in ConfigPaths:
    # individual input configs
    if configpath.strip().endswith(".conf") and 'data' in config.split('/')[-1]:
        dataConfigs.add(os.path.join(configpath, config))
    else:
        # path to folder
        temp = [os.path.join(configpath, Cfg) for Cfg in os.listdir(configpath) if 'data' in Cfg]
        dataConfigs.update(temp)

      
ROOT_Files = []
for config in dataConfigs: 
    ROOT_Files += ReadInputConfig( "%s" % (config) )

lumi = 0.

for Run in GetNormalizationDB( ROOT_Files ).GetRunNumbers():
    lumi += CalculateRecordedLumi(Run)

""".format(InputConfigs=" ".join(inputConfigs))

    # otherwise specify lumi by hand
    else:
        dataEntry += """
lumi = {lumi}

""".format(lumi=lumi)

    dataEntry += """
ds_data = [{samplelist}]
data = DSconfig(name="Data",
                label="Data",
                colour=ROOT.kBlack,
                filepath=ds_data,
                lumi=lumi,
                sampletype=SampleTypes.Data)

""".format(samplelist=samplelist)

    return dataEntry


def makeSampleEntry(sample, sample_inputs, label, colour, sampletype):
    """Return a string containing an entry about a MC sample for the DSConfig file."""
    # collect data files
    samplelist = ""
    if len(sample_inputs) == 0:
        print("ERROR: no samples provided for {sample}".format(sample=sample))
        return ""
    for s in sample_inputs[:-1]:
        samplelist += "os.path.join(BasePath, \"{s}.root\"), ".format(s=s)
    # treat last item in list differently (no comma at the end)
    samplelist += "os.path.join(BasePath, \"{s}.root\")".format(s=sample_inputs[-1])

    sampleEntry = """
ds_{sample} = [{samplelist}]
{sample} = DSconfig(name="{sample}",
                   label="{label}",
                   colour={colour},
                   filepath=ds_{sample},
                   sampletype={sampletype})

""".format(sample=sample, samplelist=samplelist, colour=colour, label=label, sampletype=sampletype)

    return sampleEntry


def makeEntryFromFile(inputFile):
    """Read contents of a file containing an entry for the DS and add it to the DSConfig file."""
    return open(inputFile, 'r').read()


def writeDSConfig(outFilePath, basepath, inputs_data, inputs_mc, samples_mc, lumi=1., additional=[], inputconfigs=[]):
    """Create a DSConfig file containing entries for data and MC."""
    with open(outFilePath, 'w') as outFile:
        # make header
        outFile.write(makeHeader(basepath))
        # add data entry
        outFile.write(makeDataEntry(inputs_data, lumi, inputconfigs))
        # add MC sample entries
        for sample, info in samples_mc.items():
            sample_inputs = []
            for i in inputs_mc:
                if i in info['samples']:
                    sample_inputs.append(i)
            label = info['label']
            colour = info['colour']
            sampletype = info['sampletype']
            outFile.write(makeSampleEntry(sample, sample_inputs, label, colour, sampletype))
        # add additional entries directly from files (if there are any)
        if additional:
            for addSample in additional:
                outFile.write(makeEntryFromFile(addSample))


def scanDirectory(inputDirectory):
    """Scans input directory for ROOT files containing histograms created with WriteDefaultHistos."""
    inputs_data = []
    inputs_mc = []

    for file in os.listdir(inputDirectory):
        # only use root files and skip everything else
        if not file.endswith('.root'):
            continue
        samplename = file.replace('.root', '')
        if 'data' in samplename:
            inputs_data.append(samplename)
        else:
            inputs_mc.append(samplename)
    return inputs_data, inputs_mc


def readConfig(configPath):
    """Read configuration file formated in json to extract information about samples' plotting options."""
    try:
        stream = open(configPath, 'r')
        config = json.loads(stream.read())
        return config
    except Exception:
        print("Error reading configuration '{config}'".format(config=configPath))
        print("Try default config. This might work but is definitely not recommended. Use a config file instead!")
        samples_mc = {
            "ttbar": {
                "samples": ["ttbar"],
                "label": "t#bar{t}",
                "colour": "ROOT.kGreen"
            },
            "stop": {
                "samples": ["stops", "stopt", "stopWt"],
                "label": "Single top",
                "colour": "ROOT.kGreen - 9"
            },
            "diboson": {
                "samples": ["WW", "WZ", "ZZ"],
                "label": "Diboson",
                "colour": "ROOT.kOrange + 7"
            },
            "wjets": {
                "samples": ["Wenu", "Wmunu", "Wtaunu"],
                "label": "W+jets",
                "colour": "ROOT.kCyan"
            },
            "zjets": {
                "samples": ["Zee", "Zmumu", "Ztautau", "Znunu"],
                "label": "Z+jets",
                "colour": "ROOT.kYellow"
            }
        }
        return samples_mc


def main():
    """Write DS configs for XAMPPplotting."""
    args = getArgumentParser().parse_args()

    # read config file to get MC sample grouping and plotting attributes
    samples_mc = readConfig(args.config)

    # scan input directory for root files containing histograms
    inputs_data, inputs_mc = scanDirectory(args.input)

    writeDSConfig(args.output, args.input, inputs_data, inputs_mc, samples_mc, args.lumi, args.additional, args.inputconfigs)


if __name__ == '__main__':
    main()
