#!/usr/bin/env python
"""
This scripts creates InputConfig files from a path containing the XAMPP ntuples of the grid-output of a production
"""
import json
import os
import re
import time
from pprint import pprint, pformat
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from ClusterSubmission.Utils import ResolvePath, CreateDirectory
from ClusterSubmission.RucioListBuilder import *

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None

if TestArea is None:
    TestArea = './'


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Script for creating Data input configs", formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i',
                        '--input',
                        dest='indir',
                        help='Folder with files (must be an absolute path); to be used only when reading local directory',
                        default="")
    parser.add_argument('-o', '--output', dest='outdir', help='Output directory to put file list(s) into', default='./InputConfigs/MonoH/')
    parser.add_argument('-c',
                        '--config',
                        dest='config',
                        help='Path to configuration file in JSON format.',
                        default=ResolvePath('XAMPPmonoH/inputConfig_config.json'))
    parser.add_argument('--campaigns',
                        nargs='+',
                        default=['mc16a', 'mc16d', 'mc16e'],
                        choices=['mc16a', 'mc16d', 'mc16e', 'truth'],
                        help='Choose which data and mc campaigns should be processed for input config creation')
    parser.add_argument('-v', '--verbose', help='Print out in verbose mode', action='store_true', default=False)
    parser.add_argument('--excludeExtensions', help='Exclude files with multiple e-tags or s-tags', action='store_true', default=False)
    # arguments to read from a groupdisk
    parser.add_argument('-p',
                        '--pattern',
                        help='specify a pattern which is part of dataset name; to be used only when reading RSE',
                        default="")
    parser.add_argument('-e',
                        '--exclude',
                        nargs='*',
                        help='specify a pattern which must not be part of dataset name. argument 0 is used for RSE.',
                        default=[])
    parser.add_argument('-r', '--RSE', help='specify RSE storage element which should be read', default=RSE)
    return parser


RSE = os.getenv("RUCIO_RSE")
RUCIO_ACC = os.getenv("RUCIO_ACCOUNT")


def getDSID(filename, useRSE=False):
    """Get DSID from ATLAS standard dataset file name assuming filename has the structure "/<basepath>/<datasetname: group.namespace.campaign.->DSID<-.otherstuff...>/<rootfile.root>"""
    if not useRSE:
        assert ".root" in filename
        dsname = os.path.dirname(filename)
        try:
            # assuming name to be group.namespace.campaign.->DSID<-.otherstuff...>/<rootfile.root>
            return dsname.split('/')[-1].split('.')[3]
        except IndexError:
            # assuming name to be ->DSID<-.otherstuff...>/<rootfile.root>
            dsid = dsname.split('/')[-1].split('.')[0]
            try:
                int(dsid)
            # if not, assuming name to be ->DSID<-.otherstuff.root>
            except ValueError:
                dsid = os.path.basename(filename).split('.')[0]
            return dsid
    else:
        # when reading RSE: Input string from txt file is already the dsname
        dsname = filename
        return dsname.split(':')[-1].split('.')[3]


def getRtag(sample, useRSE=False):
    """Get rtag from a file belonging to one sample.
    It is assumed that the filename has the structure "/<basepath>/<datasetname: group.namespace.campaign.DSID.otherstuff[...].->[ftags]<-_XAMPP>/<rootfile.root>"""
    rtag = 'no_rtag'
    if useRSE:
        try:
            dsname = sample
            # rtag may be either 4 or 5 characters long (5 for mc16d/e, 4 for mc16a)
            if not (re.search(r'r\d{5}', dsname) is None):
                rtag = re.search(r'r\d{5}', dsname).group(0)
            elif not (re.search(r'r\d{4}', dsname) is None):
                rtag = re.search(r'r\d{4}', dsname).group(0)
            # if rtag neither 4 nor 5 character long, let it crash and throw exception
            else:
                rtag = re.search(r'r\d{4}', dsname).group(0)
        except Exception:
            print("could not get rtag for sample {sample}".format(sample=sample))
    else:
        try:
            filename = sample
            assert ".root" in filename
            dsname = os.path.dirname(filename)
            # rtag may be either 4 or 5 characters long (5 for mc16d/e, 4 for mc16a)
            if not (re.search(r'r\d{5}', dsname) is None):
                rtag = re.search(r'r\d{5}', dsname).group(0)
            elif not (re.search(r'r\d{4}', dsname) is None):
                rtag = re.search(r'r\d{4}', dsname).group(0)
            # if rtag neither 4 nor 5 character long, let it crash and throw exception
            else:
                rtag = re.search(r'r\d{4}', dsname).group(0)
        except Exception:
            print("could not get rtag for sample {sample}".format(sample=sample))
    return rtag


def isExtension(f):
    """Check if file has multiple e-tags or s-tags (like those: e5340_e5984_s3126)."""
    if re.subn('e[0-9]{4}', '', f)[1] > 1 or re.subn('s[0-9]{4}', '', f)[1] > 1:
        return True
    return False


class InputSample(object):
    """Class for individual input sample, stores files associated to sample"""
    def __init__(self, samplename='', files=[]):
        super(InputSample, self).__init__()
        self.samplename = ''
        self.files = []
        self.isData = False
        if samplename:
            self.samplename = samplename
            if 'data' in samplename:
                self.isData = True
        if files:
            self.addFile(files)

    def __len__(self):
        return len(self.files)

    def __repr__(self):
        return "sample: {sample}\nassociated files:\n".format(sample=self.samplename) + pformat(sorted(self.files))

    def addFile(self, file):
        """Add file to file list"""
        if isinstance(file, list):
            self.files.extend(file)
        else:
            self.files.append(file)

    def writeToFile(self, outputFolder, rtag=[], useRSE=False, whichRSE=RSE):
        """Write input config for sample to file."""
        # only write input config if there are files for sample
        if not self.files: return

        with open(os.path.join(outputFolder, self.samplename + '.conf'), 'w') as fout:
            # write header
            fout.write("SampleName {sample}\n".format(sample=self.samplename))
            # write LHE weight variation input for systematic variations
            if self.samplename in [
                    "Wenu_l", "Wenu_cl", "Wenu_hf", "Wenu_hpt", "Wmunu_l", "Wmunu_cl", "Wmunu_hf", "Wmunu_hpt", "Wtaunu_l", "Wtaunu_cl",
                    "Wtaunu_hf", "Wtaunu_hpt", "Zee_l", "Zee_cl", "Zee_hf", "Zee_hpt", "Zmumu_l", "Zmumu_cl", "Zmumu_hf", "Zmumu_hpt",
                    "Ztautau_l", "Ztautau_cl", "Ztautau_hf", "Ztautau_hpt", "Znunu_l", "Znunu_cl", "Znunu_hf", "Znunu_hpt", "WW", "WZ", "ZZ"
            ]:
                fout.write("Import XAMPPplotting/Misc/TruthSystematics/Sherpa_WeightVariations.conf\n")
            if self.samplename in ["ttbar", "stops", "stopt", "stopWt", "VHbb"]:
                fout.write("Import XAMPPplotting/Misc/TruthSystematics/PowHeg_WeightVariations.conf\n")

            # add individual inputs
            if (self.samplename.startswith("zp2hdm") or self.samplename.startswith("2HDMa")):
                fout.write("xSecDir XAMPPplotting/xSecFiles/MonoH\n")
                fout.write("xSecDirBkg XAMPPplotting/xSecFiles/MonoH/signal_crosssections_monoHbb_dummy.txt\n")
            else:
                fout.write("xSecDir XAMPPplotting/xSecFiles/MonoH\n")
                PMGfile = "dev/PMGTools/2020-10-01/PMGxsecDB_mc16.txt"
                fout.write("xSecDirBkg {}\n".format(PMGfile))
            if useRSE:
                rselist = []
                for s in sorted(self.files):
                    if not self.isData and rtag and getRtag(s, useRSE) not in rtag: continue
                    # get root path to files on RSE
                    GetDataSetFiles(s, whichRSE, "root")
                    rselist.extend(GetDataSetFiles(s, whichRSE, "root"))
                for r in rselist:
                    fout.write("Input {r}\n".format(r=r))
            else:
                for s in sorted(self.files):
                    print s
                    if not self.isData and rtag and getRtag(s, useRSE) not in rtag: continue
                    fout.write("Input {s}\n".format(s=s))


class SampleContainer(object):
    """docstring for SampleContainer"""
    def __init__(self):
        super(SampleContainer, self).__init__()
        self.samples = {}

    def __getitem__(self, key):
        try:
            return self.samples[key]
        except KeyError as e:
            print("Error, sample {sample} not in sample container!".format(sample=key))

    def __repr__(self):
        output = ''
        for samplename, sample in sorted(self.samples.items()):
            output += 'sample {sample} with {nfiles} files\n'.format(sample=samplename, nfiles=len(sample))
        return output

    def addSample(self, sample):
        try:
            assert isinstance(sample, InputSample)
            self.samples[sample.samplename] = sample
        except AssertionError as e:
            print("Error, sample not an InputSample")
            print(e)

    def addSample(self, samplename, files):
        try:
            self.samples[samplename] = InputSample(samplename, files)
        except Exception as e:
            print("Error, could not add sample {sample}".format(sample=samplename))
            print(e)

    def writeInputConfigs(self, outputFolder, rtag=[], data=[], useRSE=False, whichRSE=RSE):
        for sample in self.samples.values():
            # if "data" contains list of selected data periods, only write out selected data periods
            if 'data' in sample.samplename and data and sample.samplename not in data: continue
            sample.writeToFile(outputFolder, rtag, useRSE=useRSE, whichRSE=whichRSE)


class InputConfigMaker:
    """Class to create input configs for data and MC for XAMPPplotting in the Mono-h(bb) 2015+2016+2017 search."""
    def __init__(self, config, RSE):
        #  data
        self.periods = {}
        self.excludeFromAllLists = []

        #  mc
        self.samples = {}
        self.rtags = {}
        self.datamc = {}

        # internal variables
        self._files = []
        self._dsnames = []
        self._campaigns = set()
        self._output = SampleContainer()

        self._RSE = RSE

        # member variables: will be read from config file
        self.readConfig(config)

    def addCampaigns(self, campaign):
        """Add data taking period or mc campaign to list of campaigns to process."""
        if isinstance(campaign, list):
            for c in campaign:
                self._campaigns.add(c)
        else:
            self._campaigns.add(campaign)

    def resetCampaigns(self):
        """Clear campaigns to be processed."""
        self._campaigns.clear()

    def getFiles(self, directory):
        """Get ROOT files in directory as list recursively."""
        # os.listdir only for locally downloaded files
        for item in os.listdir(directory):
            path = os.path.join(directory, item)
            if not os.path.isdir(path) and ".root" in path:
                self._files.append(path)
            elif os.path.isdir(path):
                self.getFiles(path)
        return self._files

    def getDSNamesRSE(self, pattern, exclude):
        # create a text file with all datasets on the groupdisk with a pattern p
        # pattern elements separated by " "
        # e.g. p = "group.phys-exotics.mc16_13TeV. NNPDF30NNLO_W 0L0500a _XAMPP"
        Today = time.strftime("%Y-%m-%d")
        rseExclude = ""
        if len(exclude):
            rseExclude = exclude[0]
        if not (rseExclude == ""):
            os.system("python " + ResolvePath("ClusterSubmission/python/ListDisk.py") + " -p " + pattern + " -e " + rseExclude + " -r " +
                      self._RSE)
            dsListName = self._RSE + "_" + Today + "_" + pattern.replace(" ", "_") + "_exl_" + rseExclude.replace(" ", "_") + ".txt"
        else:
            os.system("python " + ResolvePath("ClusterSubmission/python/ListDisk.py") + " -p " + pattern + " -r " + self._RSE)
            dsListName = self._RSE + "_" + Today + "_" + pattern.replace(" ", "_") + ".txt"

        dslist = open(dsListName)
        for line in dslist.readlines():
            self._dsnames.append(line.strip("\n"))
        return self._dsnames

    def readConfig(self, configPath):
        """Read configuration file formated in json to extract information to fill TemplateMaker variables."""
        try:
            stream = open(configPath, 'r')
            config = json.loads(stream.read())
            member_variables = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("_")]
            # check if member variable name is in config file. if it is, update member variable to value of config file
            for member in config.keys():
                if member in member_variables:
                    setattr(self, member, config[member])
        except Exception:
            print("Error reading configuration '{config}'".format(config=configPath))

    def printConfig(self):
        """Print current configuration of this class."""
        # data
        print("# DATA - periods ---------------------------------")
        print(json.dumps(self.periods, indent=4))
        print("# DATA - excludeFromAllLists ---------------------")
        print(json.dumps(self.excludeFromAllLists, indent=4))
        # mc
        print("#   MC - samples ---------------------------------")
        print(json.dumps(self.samples, indent=4))
        print("#   MC - rtags -----------------------------------")
        print(json.dumps(self.rtags, indent=4))

    def makeConfigs(self, exclude, outputFolder, excludeExtensions=False, useRSE=False, whichRSE=RSE):
        """Loop over files in inputdir and match them to the periods/names stored inf the config files. Report unmatched files."""

        # prepare data containers
        self._output.addSample('data15', [])
        self._output.addSample('data16', [])
        self._output.addSample('data17', [])
        self._output.addSample('data18', [])

        # prepare MC containers
        for sample in self.samples.keys():
            self._output.addSample(sample, [])

        # loop over files / dsnames found before by calling getFiles() / getDSNamesRSE() and try to match to patterns in config files
        missing_files = []

        if useRSE:
            # loop over DSnames found from getDSNamesRSE()
            loopDir = self._dsnames
        else:
            # loop over root input files
            loopDir = self._files

        # create dictionary of dsid+rtag combinations and count how many of them are there
        numberOfDSIDs = {}
        if excludeExtensions:
            for f in loopDir:
                f = os.path.basepath(f)
                if useRSE:
                    dsid = getDSID(f, useRSE=True)
                else:
                    dsid = getDSID(f)
                # only relevant for MC
                if len(dsid) == 6:
                    for sample, sampledict in self.samples.items():
                        rtag = getRtag([f], useRSE=useRSE)
                        key = dsid + '_' + rtag
                        if dsid in sampledict.keys() and rtag in self.rtags.values():
                            if key in numberOfDSIDs.keys():
                                numberOfDSIDs[key] += 1
                            else:
                                numberOfDSIDs[key] = 1
            print("DSID + rtag occurence in sample list:")

        # now process samples
        for f in loopDir:
            isDesired = True
            for thisExclude in exclude:
                if thisExclude in f:
                    isDesired = False
                    break
            if not isDesired:
                continue
            dsid = getDSID(f, useRSE=useRSE)

            # data - this is also done in a shitty way at the moment: if you can improve the len() comparison to separate data/mc, do so!
            if len(dsid) == 8:
                if dsid in self.excludeFromAllLists:
                    print("Skipping file {f} because it is in `excludeFromAllLists`".format(f=f))
                    continue
                elif int(dsid) in range(self.periods['2015_All'][0], self.periods['2015_All'][1] + 1):
                    self._output['data15'].addFile(f)
                elif int(dsid) in range(self.periods['2016_All'][0], self.periods['2016_All'][1] + 1):
                    self._output['data16'].addFile(f)
                elif int(dsid) in range(self.periods['2017_All'][0], self.periods['2017_All'][1] + 1):
                    self._output['data17'].addFile(f)
                elif int(dsid) in range(self.periods['2018_All'][0], self.periods['2018_All'][1] + 1):
                    self._output['data18'].addFile(f)
                else:
                    missing_files.append(f)

            # mc
            elif len(dsid) == 6:
                fileUsed = False
                for sample, sampledict in self.samples.items():
                    if not dsid in sampledict.keys(): continue
                    if excludeExtensions and numberOfDSIDs[dsid + '_' + rtag] > 1 and isExtension(f):
                        print(
                            '---> Extension (:= sample with multiple e-tags and s-tags which ) found: {name} - will be ignored because setting excludeExtension is {exclude}'
                            .format(name=f, exclude=excludeExtensions))
                        continue
                    self._output[sample].addFile(f)
                    fileUsed = True

                if (not fileUsed):
                    missing_files.append(f)

            elif 'period' in dsid and 'data' in f:
                if 'data15' in f: self._output['data15'].addFile(f)
                elif 'data16' in f: self._output['data16'].addFile(f)
                elif 'data17' in f: self._output['data17'].addFile(f)
                elif 'data18' in f: self._output['data18'].addFile(f)
                else: missing_files.append(f)

            else:
                missing_files.append(f)

        # write inputConfig files
        data = []
        for campaign in self._campaigns:
            try:
                data.extend(self.datamc[campaign])
            except Exception as e:
                print("Could not add data periods to MC campaign")
        rtags = [self.rtags[r] for r in self._campaigns if self.rtags[r]]
        self._output.writeInputConfigs(outputFolder, rtag=rtags, data=data, useRSE=useRSE, whichRSE=self._RSE)
        # print missing files
        print("# INFO - files not matched to patterns in config files-------")
        print(json.dumps(missing_files, indent=4))


def main():
    """Write input configs."""
    options = getArgumentParser().parse_args()
    RSE = os.getenv("RUCIO_RSE") if not options.RSE else options.RSE
    maker = InputConfigMaker(options.config, RSE)
    maker.addCampaigns(options.campaigns)
    if options.verbose:
        maker.printConfig()

    # create output directory
    CreateDirectory(options.outdir)

    # make sure to run either on a local directory OR on the RSE
    if not (options.pattern == "") and not (options.indir == ""):
        print "ERROR! Run either on local input directory or RSE!"
        exit()

    #reading from local directory
    if (options.pattern == ""):
        # get files from input directory
        files = maker.getFiles(options.indir)

        if options.verbose:
            print("# INFO - put these files in lists---------------")
            print(json.dumps(files, indent=4))

        # write configuration files to output folder
        maker.makeConfigs(options.exclude, options.outdir, options.excludeExtensions)

    # reading from RSE
    else:
        dsnames = maker.getDSNamesRSE(options.pattern, options.exclude)
        # (for PRW files and actual Mu use reverse logic to enforce it by default)
        maker.makeConfigs(options.exclude, options.outdir, options.excludeExtensions, useRSE=True, whichRSE=RSE)


if __name__ == '__main__':
    main()
