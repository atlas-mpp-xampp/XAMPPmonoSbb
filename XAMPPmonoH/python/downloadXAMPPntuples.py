#!/bin/env python
import os
import sys
import commands
from argparse import ArgumentParser
from pprint import pprint


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("identifier", help="Identifier for dataset, e.g. `group.phys-exotics.*.0L0500a.*XAMPP`")
    parser.add_argument("-d", "--destination", default="./", help="Directory where datasets should be downloaded to.")
    parser.add_argument("-l", "--listOnly", action='store_true', help="Only list datasets and do not download.")
    parser.add_argument("-w", "--writeFile", default=None, help="Write dataset list to file located at this path.")

    return parser


def listDataSets(identifier):
    """Create a list of datasets matching the identifier."""
    DSReplicas = commands.getoutput("rucio list-dids {searchphrase}".format(searchphrase=identifier))
    DS = []
    failed = []
    for line in DSReplicas.split("\n"):
        # skip lines used to format the result for nice screen output
        if (10 * '-') in line or 'SCOPE' in line:
            continue
        try:
            ds = line.split('|')[1]
            DS.append(ds)
        except Exception:
            failed.append(line)
    if len(failed) > 0:
        print("ERROR: These datasets failed to be retrieved:")
        pprint(sorted(failed))
    return DS


def checkDownload(statusLog):
    """Check log of download if it was successful and return number of failed file transfers."""
    failedTotal = 0

    for line in statusLog:
        if 'Files that cannot be downloaded :' in line:
            failedFile = line.split(':')[1]
            failedTotal += int(failedFile)

    return failedTotal


def getDataSets(datasets, destination):
    """Download datasets from a list of dataset names and check if download was successfull."""
    failed_datasets = {}
    for dataset in datasets:
        statusLog = commands.getoutput("rucio download --ndownloader 5 {dataset} --dir {destination}".format(dataset=dataset,
                                                                                                             destination=destination))
        failed = checkDownload(statusLog)
        if failed > 0:
            failed_datasets[dataset] = failed

    # Print summary
    print('Summary of failed datasets')
    pprint(failed_datasets)


def main():
    options = getArgumentParser().parse_args()

    # check if rucio is set up
    try:
        if not os.environ['RUCIO_ACCOUNT']:
            raise Exception
    except Exception:
        print("RUCIO was not set up properly! Please set up rucio first: `lsetup rucio`.")
        sys.exit(1)

    # create a list of datasets
    DS = listDataSets(options.identifier)

    # write dataset list to file
    if options.writeFile:
        with open(options.writeFile, 'w') as outFile:
            outFile.write('\n'.join(DS))

    # list datasets
    pprint(sorted(DS))

    # exit if should only list datasets
    if options.listOnly:
        return

    # download datasets and check if download was successful
    os.system('mkdir -p {destination}'.format(destination=options.destination))
    getDataSets(DS, options.destination)


if __name__ == '__main__':
    main()
