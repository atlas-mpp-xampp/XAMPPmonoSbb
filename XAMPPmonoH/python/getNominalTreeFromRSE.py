#!/usr/bin/env python
"""
This scripts copies the nominal trees of the XAMPP ntuples located on a
RSE to a local repository and creates input configs for these.
"""

import os
from argparse import ArgumentParser
from ROOT import *


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser(
        description="This copies the nominal trees of the XAMPP ntuples on a RSE to a local repository and creating input configs for these."
    )
    parser.add_argument("-i", "--InputConfigs", default="./", help="Path to the input configs for the ntuples on the RSE.")
    parser.add_argument("-l",
                        "--LocalInputConfigs",
                        default="./",
                        help="Directory where the input configs for the nominal ntuples should be stored.")
    parser.add_argument("-o", "--Output", default="./", help="Directory where the nominal trees should be stored.")

    return parser


#TODO: Implement options to select or exclude specific samples, exception for timeouts (if possible) and slurm option


def processInputConfig(pathToInputConfig, pathToLocalConfig, pathToLocalNtuples):
    with open(pathToInputConfig) as c:
        """Start reading input config"""
        lines = c.readlines()
        for line in lines:
            print line
            if not line.startswith("Input "):
                """Copy the sample name and PRW configs to new file"""
                pathToLocalConfig.write(line + "\n")
            else:
                """Copying nominal trees to local root file and creating new input configs"""
                rsePath = line.split()[-1]
                """Get name of rootfile without path before"""
                fileName = rsePath.split("/")[-1]
                rseRootFile = TFile.Open(rsePath)
                rseNominalTree = rseRootFile.Get("MonoH_Nominal")
                rseMetaDataTree = rseRootFile.Get("MetaDataTree")
                localFilePath = os.path.join(pathToLocalNtuples, fileName)
                """Create a local root file to store the nominal tree to"""
                localRootFile = TFile(localFilePath, "recreate")
                localNominalTree = rseNominalTree.CloneTree()
                localMetaDataTree = rseMetaDataTree.CloneTree()
                localRootFile.Write()
                localRootFile.Close()
                """Write path to local root file into new input config"""
                pathToLocalConfig.write("Input " + localFilePath + "\n")
                rseRootFile.Close()


def main():
    args = getArgumentParser().parse_args()
    """Create directories for nominal ntuples and their input configs"""
    outputPath = os.path.abspath(args.Output)
    print "outputPath = ", outputPath
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)
    if not os.path.exists(args.LocalInputConfigs):
        os.makedirs(args.LocalInputConfigs)

    for config in os.listdir(args.InputConfigs):
        if not (config == "dijet.conf"):
            configPath = os.path.join(args.InputConfigs, config)
            localConfigPath = os.path.join(args.LocalInputConfigs, config)
            localConfig = open(localConfigPath, "w")
            processInputConfig(configPath, localConfig, outputPath)
            localConfig.close()


if __name__ == '__main__':
    main()
