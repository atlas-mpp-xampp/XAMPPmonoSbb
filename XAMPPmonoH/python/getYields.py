#!/usr/bin/env python
import csv
import json
from argparse import ArgumentParser
from pprint import pprint, pformat
from os import makedirs
from os.path import join, basename, exists
from rootpy.io import root_open
from ROOT import Double as Double_t
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('inputFiles', nargs='*', help='input files')
    # optional arguments
    parser.add_argument('--useOverflow', action='store_true', help='take into account overflow and underflow bins')
    parser.add_argument('--printAllVariables', action='store_true', help='print all variables')
    parser.add_argument('--csv', action='store_true', help='save output as csv file')
    # default settings
    parser.add_argument('-o', '--outputPath', default='yields', help='name of the directory with output yields')
    parser.add_argument('-l', '--luminosity', default=36.1, type=float, help='Luminosity for backgrounds to be scaled to')
    parser.add_argument('--resolvedVariable', default='m_jj', help='name of the variable for the resolved fit input histogram')
    parser.add_argument('--mergedVariable', default='m_J', help='name of the variable for the resolved fit input histogram')
    return parser


def main():
    """Check yields and statistical uncertainties of histograms in ROOT file, iterate recursively through all directories."""
    args = getArgumentParser().parse_args()
    results = {}
    for inputFile in args.inputFiles:
        with root_open(inputFile) as f:
            results[inputFile] = []
            # recursively walk through the file
            for path, dirs, objects in f.walk():
                # print yields and statistical uncertainty
                for obj in objects:
                    # check that only the resolved/merged variable is obtained for the respective region
                    if 'Merged' in path and args.mergedVariable not in obj and not args.printAllVariables:
                        continue
                    if 'Resolved' in path and args.resolvedVariable not in obj and not args.printAllVariables:
                        continue
                    # obtain integral yield and statistical uncertainty
                    histo = getattr(getattr(f, path), obj)
                    if 'data' not in basename(inputFile):
                        histo.Scale(args.luminosity)
                    uncertainty = Double_t(-1.)
                    integral = histo.IntegralAndError(1, histo.GetNbinsX(),
                                                      uncertainty) if not args.useOverflow else histo.IntegralAndError(
                                                          0,
                                                          histo.GetNbinsX() + 1, uncertainty)
                    results[inputFile].append([path, obj, integral, uncertainty])

    for inputFileName, result in results.items():
        logging.info(inputFileName)
        for line in sorted(result):
            logging.info(pformat(line))

        if args.csv:
            if not exists(args.outputPath):
                makedirs(args.outputPath)
            with open(join(args.outputPath, 'yields_' + basename(inputFileName).split('.root', 1)[0] + '.csv'), 'w') as fout:
                fout.write('region,histogram,eventyield,statuncertainty\n')
                for line in sorted(result):
                    fout.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + ',' + str(line[3]) + '\n')
                logging.info('Wrote ' + join(args.outputPath, 'yields_' + basename(inputFileName).split('.root', 1)[0] + '.csv'))
    logging.info('Used luminosity of {lumi} for scaling the backgrounds. Was this correct?'.format(lumi=args.luminosity))


if __name__ == '__main__':
    main()
