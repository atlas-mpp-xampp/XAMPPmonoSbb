#! /usr/bin/env python
import argparse
from XAMPPbase.runAthena import ExecuteAthena, AssembleAthenaOptions, applyZnunuSampleFix
from XAMPPbase.AthArgParserSetup import SetupArgParser
from pprint import pprint


def getArgs():
    """Get arguments from command line."""
    parser = SetupArgParser()
    parser.set_defaults(jobOptions="XAMPPmonoH/runMonoH.py")
    parser.set_defaults(evtMax=10000)
    return parser


def runAthena():
    """Run Athena with options defined via the command line arguments.
       This runs the XAMPPmonoH analysis code locally and processes
       DxAODs to generate XAMPP ntuples."""
    parser = getArgs()
    RunOptions = parser.parse_args()
    AthenaArgs = AssembleAthenaOptions(RunOptions, parser)
    pprint(AthenaArgs)

    # automatic configuration of fix for buggy Znunu samples
    applyZnunuSampleFix(RunOptions, AthenaArgs)

    ExecuteAthena(RunOptions, AthenaArgs)


if __name__ == "__main__":
    runAthena()
