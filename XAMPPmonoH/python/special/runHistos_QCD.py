#!/usr/bin/env python
import os
from argparse import ArgumentParser
from datetime import datetime

# environment variable for system independent setup
try:
    MONOH_OUTPUTPATH = os.environ['MONOH_OUTPUTPATH']
except Exception:
    MONOH_OUTPUTPATH = None

if MONOH_OUTPUTPATH is None:
    MONOH_OUTPUTPATH = './output/'


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Produce histogram input and control plots for QCD estimation for mono-h(bb) analysis.")
    parser.add_argument('-p', '--onlyPlots', action='store_true', help='Only produce plots (histograms already produced before).')
    return parser.parse_args()


def main():
    """Run local histogram production for Mono-h(bb) analysis."""
    args = getArgs()
    base = os.path.join(os.environ['TestArea'], 'XAMPPplotting')
    steeringscripts = os.path.join(os.environ['TestArea'], 'MonoHSteeringScripts')
    date = datetime.now().strftime("%Y-%m-%d")

    # prepare histograms
    script = os.path.join(base, 'python/SubmitToLocal.py')
    Output = "{path}/plotting/QCD/{date}/".format(path=MONOH_OUTPUTPATH, date=date)
    InputConf = os.path.join(base, 'data/InputConf/MonoH/XAMPPmonoH-QCD0700.0lep/')
    RunConf = os.path.join(base, 'data/RunConf/MonoH/RunConfig_QCD_Resolved.conf') + " " + os.path.join(
        base, 'data/RunConf/MonoH/RunConfig_QCD_Merged.conf')
    HistoConf = os.path.join(base, 'data/HistoConf/MonoH/Histos_QCD.conf')

    if not args.onlyPlots:
        os.system("mkdir -p {output}".format(output=Output))
        command = """python {script} -O  {output} -I {inputConf} -R {runConf} -H {histoConf} -N 16""".format(script=script,
                                                                                                             output=Output,
                                                                                                             inputConf=InputConf,
                                                                                                             runConf=RunConf,
                                                                                                             histoConf=HistoConf)
        os.system(command)

    # # create new DSConfig
    # DSConf = os.path.join(base, 'python/DSConfigs/MonoH_QCD_autogen.py')
    # os.system('python {steeringscripts}/createDSConfig.py -i {plottinginput} -o {dsconfig}'.format(steeringscripts=steeringscripts, plottinginput=Output, dsconfig=DSConf))

    # # create plots
    # script = os.path.join(base, 'python/DataMCPlots.py')
    # Output = "{path}/plotting/QCD/{date}/Plots".format(path=MONOH_OUTPUTPATH, date=date)

    # os.system("mkdir -p {output}".format(output=Output))
    # command = """python {script} -o  {output} -c {DSConf} --noSyst --noSignal""".format(
    #     script=script, output=Output, DSConf=DSConf)
    # os.system(command)


if __name__ == "__main__":
    main()
