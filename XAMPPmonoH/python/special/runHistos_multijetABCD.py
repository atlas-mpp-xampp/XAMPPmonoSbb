#!/usr/bin/env python
import os, sys
import random
from argparse import ArgumentParser
from datetime import datetime

# environment variable for system independent setup
try:
    MONOH_OUTPUTPATH = os.environ['MONOH_OUTPUTPATH']
except Exception:
    MONOH_OUTPUTPATH = None

try:
    INSTITUTE = os.environ['INSTITUTE']
except Exception:
    INSTITUTE = ''

if MONOH_OUTPUTPATH is None:
    MONOH_OUTPUTPATH = '${TestArea}/../run/output/'


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser(description="Produce histograms for mono-h(bb) analysis.")
    parser.add_argument('-l', '-L', '--leptonRegion', help='Choose the lepton region (0L, 1L or 2L)', required=True)
    parser.add_argument('-p', '--onlyPlots', action='store_true', help='Only produce plots (histograms already produced before).')
    parser.add_argument('--plotInput', default=None, help='Path to plot input histograms (to be used when calling -p/--onlyPlots option).')
    parser.add_argument('-f',
                        '--doFitInputs',
                        action='store_true',
                        help='Run with reduced histogram output needed for fit on local batch farm')
    parser.add_argument('--do1516', action='store_true', help='Run for data1516/mc16a')
    parser.add_argument('--do17', action='store_true', help='Run for data17/mc16d')
    parser.add_argument('--samples', nargs='*', default=[], help='Names of samples to be processed')
    parser.add_argument('--institute', default=INSTITUTE, help='Names of samples to be processed')
    parser.add_argument('--doSyst', action='store_true', help='Try to plot with systematics')

    return parser.parse_args()


def main():
    """Run local histogram production for Mono-h(bb) analysis."""
    args = getArgs()
    base = os.path.join(os.environ['TestArea'], 'XAMPPplotting')
    steeringscripts = os.path.join(os.environ['TestArea'], 'MonoHSteeringScripts')
    date = datetime.now().strftime("%Y-%m-%d-%H-%M")

    institute = args.institute

    if args.leptonRegion == '0L':
        region = 'SR'
    else:
        print "Error: You have to assign the lepton region! i.e. 0L or 1L"
        exit(1)

    # can add sample list here to only process these samples
    samples = args.samples
    # samples = ['WW', 'WZ', 'Wenu_cl', 'Wenu_hf', 'Wenu_hpt', 'Wenu_l', 'Wmunu_cl', 'Wmunu_hf', 'Wmunu_hpt', 'Wmunu_l', 'Wtaunu_cl', 'Wtaunu_hf', 'Wtaunu_hpt', 'Wtaunu_l', 'Zee_cl', 'Zee_hf', 'Zee_hpt', 'Zee_l', 'Zmumu_cl', 'Zmumu_hf', 'Zmumu_hpt', 'Zmumu_l', 'Znunu_cl', 'Znunu_hf', 'Znunu_hpt', 'Znunu_l', 'Ztautau_cl', 'Ztautau_hf', 'Ztautau_l', 'data15', 'data16', 'data17', 'dijet', 'stopWt', 'stops', 'ttbar']

    # prepare histograms
    script = os.path.join(base, 'python/SubmitToLocal.py')
    Output = "{path}/{plotting}/{region}-ABCD/{date}/".format(path=MONOH_OUTPUTPATH,
                                                              region=region,
                                                              date=date,
                                                              plotting='fitting' if args.doFitInputs else 'plotting')
    InputConf = os.path.join(base, 'data/InputConf/MonoH/{institute}/XAMPPmonoH-00-07-00.{region}/'.format(region=region,
                                                                                                           institute=institute))

    if args.do1516:
        Output = "{path}/{plotting}/{region}-ABCD-1516/{date}/".format(path=MONOH_OUTPUTPATH,
                                                                       region=region,
                                                                       date=date,
                                                                       plotting='fitting' if args.doFitInputs else 'plotting')
        InputConf = os.path.join(
            base, 'data/InputConf/MonoH/{institute}/XAMPPmonoH-00-07-00.data1516mc16a.{region}/'.format(region=region, institute=institute))

    if args.do17:
        Output = "{path}/{plotting}/{region}-ABCD-17/{date}/".format(path=MONOH_OUTPUTPATH,
                                                                     region=region,
                                                                     date=date,
                                                                     plotting='fitting' if args.doFitInputs else 'plotting')
        InputConf = os.path.join(
            base, 'data/InputConf/MonoH/{institute}/XAMPPmonoH-00-07-00.data17mc16d.{region}/'.format(region=region, institute=institute))

    RunConf = os.path.join(base, 'data/RunConf/MonoH/Other/RunConfig_Multijet_Study.conf')

    # if samples are provided, only process those
    if samples:
        inputconfig_base = InputConf
        InputConf = ''
        for sample in samples:
            InputConf += os.path.join(inputconfig_base, sample + '.conf') + ' '

    HistoConf = os.path.join(base, 'data/HistoConf/MonoH/Histos_MultijetABCD.conf')
    Options = "-N 16 --noSyst -O {output} ".format(output=Output)

    # run on batch system and only use J_m and m_jj histogram configuration for producing fit inputs with systematics
    if args.doFitInputs:
        script = os.path.join(base, 'python/SubmitToBatch.py')
        identifier = "{i}".format(i=random.getrandbits(16))
        Options = "-J mH_{region}_{identifier} --BaseFolder {output} --RunTime 07:59:59 --vmem 7900 --MergeTime 01:59:59 --Parallelize  --nSystPerJob 20 --nMaxCurrentJobs 72".format(
            output=Output, region=region, identifier=identifier)

    if not args.onlyPlots:
        os.system("mkdir -p {output}".format(output=Output))
        command = """python {script} -I {inputConf} -R {runConf} -H {histoConf} {options}""".format(script=script,
                                                                                                    inputConf=InputConf,
                                                                                                    runConf=RunConf,
                                                                                                    histoConf=HistoConf,
                                                                                                    options=Options)
        os.system(command)

    # finish without plotting when submitting jobs for fit inputs
    if args.doFitInputs:
        return

    # only plot special option
    if args.onlyPlots:
        if args.plotInput:
            Output = args.plotInput
        else:
            print('Which plotting input histograms folder should be used?')
            dirpath = os.path.abspath(os.path.join(Output, os.pardir))
            for d in os.listdir(dirpath):
                print(os.path.abspath(os.path.join(dirpath, d)))
            Output = raw_input('Please enter absolute path to the input histograms folder here: ')

    DSConf = os.path.join(base, 'python/DSConfigs/MonoH_{region}_autogen.py'.format(region=region))
    os.system(
        'python {steeringscripts}/createDSConfig.py -i {plottinginput} -o {dsconfig} -c {steeringscripts}/data/DSConfig/{configfile} --inputConfigs {inputconfigs} {signal}'
        .format(steeringscripts=steeringscripts,
                plottinginput=Output,
                dsconfig=DSConf,
                configfile='config.json',
                inputconfigs=InputConf,
                signal='-a ' + steeringscripts + '/data/DSConfig/signal.conf' if args.leptonRegion == '0L' else ''))

    # create plots
    script = os.path.join(base, 'python/DataMCPlots.py')
    Output = os.path.join(Output, "Plots")
    Options = ""
    Options += "--noSignalStack"
    if not args.doSyst:
        Options += " --noSyst"

    os.system("mkdir -p {output}".format(output=Output))
    command = """python {script} -o {output} -c {DSConf} {options}""".format(script=script, output=Output, DSConf=DSConf, options=Options)
    os.system(command)

    command = """python {script} -o {output} -c {DSConf} {options} --doLogY""".format(script=script,
                                                                                      output=Output,
                                                                                      DSConf=DSConf,
                                                                                      options=Options)
    os.system(command)


if __name__ == "__main__":
    main()
