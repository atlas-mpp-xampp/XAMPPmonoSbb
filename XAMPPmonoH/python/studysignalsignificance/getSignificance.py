#!/bin/env python
from argparse import ArgumentParser
from pprint import pprint, pformat
from os import listdir
from os.path import join
import csv
from math import sqrt, log
from rootpy.plotting.style import set_style
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def getArgs():
    parser = ArgumentParser()
    parser.add_argument('inputDir', help="Directory containing csv yield files created with the getYields.py script")
    parser.add_argument('-o', '--outputPath', default='significances.csv', help="Output file path")
    return parser


def readBackgrounds(inputDir):
    """Read all yield information from input directory."""
    # structure: region -> sample -> yield
    backgrounds = {}
    for filename in listdir(inputDir):
        if not filename.endswith(".csv") or not 'yields' in filename:
            continue

        logging.debug("Reading input file: " + join(inputDir, filename))
        with open(join(inputDir, filename)) as f:
            sample = filename.split('yields_')[1].replace('.csv', '')
            logging.debug("Processing " + sample)
            for line in f:
                if 'region,histogram,eventyield,statuncertainty' in line or len(line) == 0:
                    continue
                region, variable, integral, uncertainty = line.replace('\n', '').split(',')
                logging.debug("region {region} | variable {variable} | integral {integral} | uncertainty {uncertainty}".format(
                    region=region, variable=variable, integral=integral, uncertainty=uncertainty))
                if region not in backgrounds.keys():
                    backgrounds[region] = {sample: integral}
                else:
                    backgrounds[region][sample] = integral
    logging.debug("Backgrounds:")
    logging.debug(pformat(backgrounds))
    return backgrounds


def calculateSignificance(s, b):
    """Formula for calculating expected significance (ref.: https://www.pp.rhul.ac.uk/~cowan/stat/notes/SigCalcNote.pdf equation 27)"""
    try:
        return sqrt(2 * ((s + b) * log(1 + s / b) - s))
    except ValueError:
        return -1.0


def getSignificance(data, outputPath):
    """Make CSV file with significances. At the moment hard coded names, could be improved."""

    significances = {}

    for region, info in data.items():
        significances[region.replace('/', '_')] = {}
        background = 0.

        # collect yields
        for sample, integral in info.items():
            if sample in [
                    'zjets', 'Zee', 'Zee_l', 'Zee_cl', 'Zee_hf', 'Zee_hpt', 'Zmumu', 'Zmumu_l', 'Zmumu_cl', 'Zmumu_hf', 'Zmumu_hpt',
                    'Ztautau', 'Ztautau_l', 'Ztautau_cl', 'Ztautau_hf', 'Ztautau_hpt'
            ]:
                background += float(integral)
            elif sample in ['Znunu', 'Znunu_l', 'Znunu_cl', 'Znunu_hf', 'Znunu_hpt']:
                background += float(integral)
            elif sample in [
                    'wjets', 'Wenu', 'Wenu_l', 'Wenu_cl', 'Wenu_hf', 'Wenu_hpt', 'Wmunu', 'Wmunu_l', 'Wmunu_cl', 'Wmunu_hf', 'Wmunu_hpt',
                    'Wtaunu', 'Wtaunu_l', 'Wtaunu_cl', 'Wtaunu_hf', 'Wtaunu_hpt'
            ]:
                background += float(integral)
            elif sample in ['ttbar']:
                background += float(integral)
            elif sample in ['singletop', 'stops', 'stopt', 'stopWt']:
                background += float(integral)
            elif sample in ['diboson', 'WW', 'WZ', 'ZZ']:
                background += float(integral)
            elif sample in ['dijet']:
                background += float(integral)

        # calculate significances
        for sample, integral in info.items():
            if 'zp2hdm' not in sample:
                continue
            signal = float(integral)
            significance = calculateSignificance(signal, background)
            logging.debug("Calculating significance:")
            logging.debug("region: {region} | sample: {sample} | signal: {signal}".format(region=region, sample=sample, signal=signal))
            logging.debug("region: {region} | background: {background}".format(region=region, background=background))
            logging.debug("region: {region} | significance: {significance}".format(region=region, significance=significance))
            significances[region.replace('/', '_')][sample] = significance

    with open(outputPath, 'w') as fout:
        fout.write('mzp,mA,region,significance\n')
        for region, d in significances.items():
            for sample, significance in d.items():
                info = sample.split('_')
                mzp = info[-2].replace('mzp', '')
                mA = info[-1].replace('mA', '').replace('.csv', '')
                fout.write(str(mzp) + ',' + str(mA) + ',' + str(region) + ',' + str(significance) + '\n')


def main():
    """This function creates csv file with expected significances following the 
    Cowan et. al. formula for expected significance considering the background statistical uncertainty."""
    # get arguments from command line
    args = getArgs().parse_args()

    # plotting style
    set_style('ATLAS', mpl=True)

    # input file
    data = readBackgrounds(args.inputDir)

    # make csv output file with results
    getSignificance(data, args.outputPath)


if __name__ == '__main__':
    main()
