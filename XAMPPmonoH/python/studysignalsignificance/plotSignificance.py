import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap
from argparse import ArgumentParser


def reverse_colourmap(cmap, name='my_cmap_r'):
    """
    In: 
    cmap, name 
    Out:
    my_cmap_r

    Explanation:
    t[0] goes from 0 to 1
    row i:   x  y0  y1 -> t[0] t[1] t[2]
                   /
                  /
    row i+1: x  y0  y1 -> t[n] t[1] t[2]

    so the inverse should do the same:
    row i+1: x  y1  y0 -> 1-t[0] t[2] t[1]
                   /
                  /
    row i:   x  y1  y0 -> 1-t[n] t[2] t[1]
    """
    reverse = []
    k = []

    for key in cmap._segmentdata:
        k.append(key)
        channel = cmap._segmentdata[key]
        data = []

        for t in channel:
            data.append((1 - t[0], t[2], t[1]))
        reverse.append(sorted(data))

    LinearL = dict(zip(k, reverse))
    my_cmap_r = mpl.colors.LinearSegmentedColormap(name, LinearL)
    return my_cmap_r


# plt.style.use('https://raw.githubusercontent.com/beojan/atlas-mpl/master/atlas_mpl_style/stylesheets/atlas.mplstyle')


def getColourMap():
    # create colour map
    delta = 1.8
    cldict = {
        'red': ((0.0, 0.0, 0.0), (1. / delta, 221. / 256., 221. / 256.), (1.0, 1.0, 1.0)),
        'green': ((0.0, 119 / 256., 119 / 256.), (1. / delta, 222 / 256., 222 / 256.), (1.0, 1., 1.)),
        'blue': ((0.0, 117. / 256., 117. / 256.), (1. / delta, 214. / 256., 214. / 256.), (1.0, 1., 1.))
    }
    return LinearSegmentedColormap('mpp', cldict)


def getArgs():
    parser = ArgumentParser()
    parser.add_argument('-i',
                        '--inputFile',
                        default='significances.csv',
                        help='CSV file containing the significances for regions to be plotted.')
    parser.add_argument('region', help='Name of region for which significance should be plotted.')
    parser.add_argument('--regionForRatio',
                        default=None,
                        help='Name of region serving as the normalisation, instead plots a ratio of significances.')
    parser.add_argument('-t', '--title', default=None, help='Name of plot title.')
    return parser.parse_args()


def main():
    """Plot expected signal significance for certain region from CSV file."""
    args = getArgs()
    region = args.region

    df = pd.read_csv(args.inputFile)
    df = df[df.region == region]
    df = df.pivot("mA", "mzp", "significance").astype(float)

    # if second region is specified, do not plot significance but plot ratio of significances instead
    if args.regionForRatio:
        try:
            df_ref = pd.read_csv(args.inputFile)
            df_ref = df_ref[df_ref.region == args.regionForRatio]
            df_ref = df_ref.pivot("mA", "mzp", "significance").astype(float)
            df = df.divide(df_ref, 'significance')
        except ValueError:
            print('Error - the reference region does not exist or has strange numbers.')

    # create colour map
    colourmap = getColourMap()

    r_colourmap = reverse_colourmap(colourmap)
    # plot significance as heatmap
    f, ax = plt.subplots(figsize=(10, 8))

    p = sns.heatmap(df, annot=True, fmt=".2f", cmap=r_colourmap, cbar_kws={'label': 'Expected significance Z'})

    plt.xlabel("m(Z') [GeV]", position=(1., 0.), va='bottom', ha='right')
    plt.ylabel("m(A) [GeV]", position=(0., 1.), va='top', ha='right')
    ax.xaxis.set_label_coords(1., -0.10)
    ax.yaxis.set_label_coords(-0.18, 1.)
    title = args.title if args.title else args.region.replace('_', ' ')
    plt.title('Exp. significance {ratio} {title}'.format(title=title, ratio='ratio' if args.regionForRatio else ''))

    plt.gca().invert_yaxis()
    if args.regionForRatio:
        plt.savefig('significance_ratio_{region}_{regionforratio}.png'.format(region=args.region.replace('_', ''),
                                                                              regionforratio=args.regionForRatio.replace('_', '')))
    else:
        plt.savefig('significance_{region}.png'.format(region=args.region))
    plt.tight_layout()


if __name__ == '__main__':
    main()
