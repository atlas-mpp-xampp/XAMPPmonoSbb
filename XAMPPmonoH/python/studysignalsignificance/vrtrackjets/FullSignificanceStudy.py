#!/usr/bin/env python

from argparse import ArgumentParser
parser = ArgumentParser(
    description='Steering script for SignalSignificance.py and RatioSignalSignificance.py creating all significance and ratio plots.')
parser.add_argument("--VRBkgs", help="give path to VR background histograms", default="")
parser.add_argument("--VRSignals", help="give path to VR signal histograms", default="")
parser.add_argument("--FRBkgs", help="give path to FR background histograms", default="")
parser.add_argument("--FRSignals", help="give path to FR signal histograms", default="")
parser.add_argument("-v", "--VROutput", help="VR output", default="significance_VR")
parser.add_argument("-f", "--FROutput", help="FR output", default="significance_FR")
parser.add_argument("-p", "--Plots", help="directory for plots", default="significance_plots")
args = parser.parse_args()

from ROOT import *
import os

# make VR significance output
os.system("python SignalSignificance.py -o " + args.VROutput + " -b " + args.VRBkgs + " -s " + args.VRSignals + " --Debug")
# make FR significance output
os.system("python SignalSignificance.py -o " + args.FROutput + " -b " + args.FRBkgs + " -s " + args.FRSignals + " --Debug")

# VR over FR
#########
# 1b in full SR
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_1b_merged.root -d " + args.FROutput +
          "/significance_1b_merged.root --Label '1 b-tag' -o " + args.Plots)
# 1b in HiggsWindow
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_1b_HiggsWindow.root -d " + args.FROutput +
          "/significance_1b_HiggsWindow.root --Label '1 b-tag' -o " + args.Plots)
# 2b in full SR
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_2b_merged.root -d " + args.FROutput +
          "/significance_2b_merged.root --Label '2 b-tags' -o " + args.Plots)
# 2b in HiggsWindow
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_2b_HiggsWindow.root -d " + args.FROutput +
          "/significance_2b_HiggsWindow.root --Label '2 b-tags' -o " + args.Plots)
# 1b+2b in full SR
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_all_merged.root -d " + args.FROutput +
          "/significance_all_merged.root --Label '1+2 b-tags' -o " + args.Plots)
# 1b+2b in HiggsWindow
os.system("python RatioSignalSignificance.py --VRoverFR -n " + args.VROutput + "/significance_all_HiggsWindow.root -d " + args.FROutput +
          "/significance_all_HiggsWindow.root --Label '1+2 b-tags' -o " + args.Plots)

# 2b over 1b
#########
# VR in full SR
os.system("python RatioSignalSignificance.py --TwoBoverOneB -n " + args.VROutput + "/significance_2b_merged.root -d " + args.VROutput +
          "/significance_1b_merged.root --Label 'VR Track Jets' -o " + args.Plots)
# VR in HiggsWindow
os.system("python RatioSignalSignificance.py --TwoBoverOneB -n " + args.VROutput + "/significance_2b_HiggsWindow.root -d " + args.VROutput +
          "/significance_1b_HiggsWindow.root --Label 'VR Track Jets' -o " + args.Plots)
# FR in full SR
os.system("python RatioSignalSignificance.py --TwoBoverOneB -n " + args.FROutput + "/significance_2b_merged.root -d " + args.FROutput +
          "/significance_1b_merged.root --Label 'FR Track Jets' -o " + args.Plots)
# FR in HiggsWindow
os.system("python RatioSignalSignificance.py --TwoBoverOneB -n " + args.FROutput + "/significance_2b_HiggsWindow.root -d " + args.FROutput +
          "/significance_1b_HiggsWindow.root --Label 'FR Track Jets' -o " + args.Plots)

# new analysis strategy vs. old
##########
# VR 2b over FR 1b+2b in full SR
os.system("python RatioSignalSignificance.py --ZTitle 'VR 2b / FR 1b+2b' -n " + args.VROutput + "/significance_2b_merged.root -d " +
          args.FROutput + "/significance_all_merged.root --Label 'Comparison to previous analysis' -o " + args.Plots)
# VR 2b over FR 1b+2b in HiggsWindow
os.system("python RatioSignalSignificance.py --ZTitle 'VR 2b / FR 1b+2b' -n " + args.VROutput + "/significance_2b_HiggsWindow.root -d " +
          args.FROutput + "/significance_all_HiggsWindow.root --Label 'Comparison to previous analysis' -o " + args.Plots)
# 2b over 1b+2b
os.system("python RatioSignalSignificance.py --ZTitle 'VR 2b / VR 1b+2b' -n " + args.VROutput + "/significance_2b_merged.root -d " +
          args.VROutput + "/significance_all_merged.root --Label 'VR Track Jets' -o " + args.Plots)
# 2b over 1b+2b in HiggsWindow
os.system("python RatioSignalSignificance.py --ZTitle 'VR 2b / VR 1b+2b' -n " + args.VROutput + "/significance_2b_HiggsWindow.root -d " +
          args.VROutput + "/significance_all_HiggsWindow.root --Label 'VR Track Jets' -o " + args.Plots)
