#!/usr/bin/env python

from argparse import ArgumentParser
parser = ArgumentParser(description='calculate MET trigger efficiencies')
parser.add_argument("-n", "--Nominator", help="give path to background histograms", default="")
parser.add_argument("-d", "--Denominator", help="give path to signal histograms", default="")
parser.add_argument("-o", "--Output", help="How output file should be named", default="significance_plots")
parser.add_argument("--VRoverFR", help="Compare ratio between VR and FR jets", action='store_true', default=False)
parser.add_argument("--TwoBoverOneB", help="Compare ratio between 1b and 2b events", action='store_true', default=False)
parser.add_argument("--ZTitle", help="Specify which ratio should be plotted", default="")
parser.add_argument("--Label", help="Specify a label", default="")
parser.add_argument("-l", "--Lumi", help="Luminosity", default=80)
args = parser.parse_args()

from ROOT import *
import os, math

gROOT.LoadMacro("~/RootUtils/AtlasStyle.C")
gROOT.ProcessLine("SetAtlasStyle()")
gStyle.SetPalette(1)
gStyle.SetPaintTextFormat(".2f")

c = TCanvas()

rootfile_VR = TFile.Open(args.Nominator)
keys_nom = rootfile_VR.GetListOfKeys()
keyName_nom = keys_nom[0].GetName()
histo_VR = rootfile_VR.Get(keyName_nom)
rootfile_FR = TFile.Open(args.Denominator)
keys_denom = rootfile_FR.GetListOfKeys()
keyName_denom = keys_denom[0].GetName()
histo_FR = rootfile_FR.Get(keyName_denom)

histo_ratio = histo_VR.Clone("h_ratio")
histo_ratio.Divide(histo_FR)

title = "Significance Ratio "
if args.VRoverFR:
    title += "VR / FR"
elif args.TwoBoverOneB:
    title += "2b / 1b"
elif not (args.ZTitle == ""):
    title += args.ZTitle

histo_ratio.GetZaxis().SetTitle(title)
histo_ratio.Draw("COLZ TEXT")

ATLAS_label = TLatex()
ATLAS_label.SetNDC()
ATLAS_label.SetTextFont(43)
ATLAS_label.SetTextSize(19)
ATLAS_label.SetTextColor(kBlack)
ATLAS_label.SetTextAlign(11)
ATLAS_label.DrawLatex(0.15, 0.94, "#font[72]{ATLAS} Internal      #sqrt{s} = 13 TeV, " + str(args.Lumi) + " fb^{-1}")
region_label = TLatex()
region_label.SetNDC()
region_label.SetTextFont(43)
region_label.SetTextSize(18)
text = args.Label
doHiggsWindow = False
if ("HiggsWindow" in args.Nominator and "HiggsWindow" in args.Denominator):
    text += ", 70 GeV < m_{J} < 140 GeV"
    doHiggsWindow = True
elif ("HiggsWindow" not in args.Nominator):
    if ("HiggsWindow" not in args.Denominator):
        text += ""
    else:
        print "ERROR: Comparing histogram in full SR with histogram in Higgs mass window! Exiting..."
        exit()
region_label.SetTextColor(kBlack)
region_label.DrawLatex(0.15, 0.88, text)

outName = args.Label.replace(" ", "")
if args.VRoverFR:
    outName += "_VRoverFR"
elif args.TwoBoverOneB:
    outName += "_2bover1b"
elif not (args.ZTitle == ""):
    outName += "_" + args.ZTitle.replace(" ", "").replace("/", "over")
if doHiggsWindow:
    outName += "_HiggsWindow"
outName += ".pdf"

if not os.path.exists(args.Output):
    os.makedirs(args.Output)

c.SetRightMargin(0.2)
c.SetLeftMargin(0.15)
c.SetTopMargin(0.15)
c.Print(args.Output + "/" + outName)
