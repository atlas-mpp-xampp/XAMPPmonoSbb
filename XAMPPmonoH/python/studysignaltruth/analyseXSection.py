#!/bin/env python
import json
import logging
import numpy as np
import pandas as pd
from argparse import ArgumentParser
# control logging verbosity
logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("xsecFile", help="JSON file containing cross-section information.")
    parser.add_argument("--factor",
                        default=None,
                        help="CSV file with factors to be multiplied to the cross-sections.\
                                                        Structure: dsid, factor")
    parser.add_argument("-p", "--plot", action='store_true', help="Create plots of cross-sections.")
    parser.add_argument("-c", "--csv", action='store_true', help="Write cross-sections to CSV file.")
    parser.add_argument("-x", "--xsec", action='store_true', help="Write cross-sections to file with XAMPP framework structure.")
    parser.add_argument("-l", "--list", default=None, help="List samples for JSON file steering input config creation for XAMPPplotting.")
    return parser


def findBetween(s, first, last):
    """Find substring between two substrings first and last in string s."""
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return ""


def readFile(xsecFile):
    """Read JSON input file and return content as dictionary."""
    logging.info("Reading input file {f}".format(f=xsecFile))
    try:
        with open(xsecFile, 'r') as f:
            content = json.load(f)
            return content
    except Exception:
        logging.error("Could not load {f}".format(f=xsecFile))
        return {}


def averageValue(batch, key='totalxsec'):
    """Average over individual simulated samples."""
    try:
        return np.mean([float(v[key]) for v in batch.values()])
    except KeyError:
        print(batch.values())


def processData(data, factor):
    """Process data from JSON file and return pandas data frame."""

    if factor:
        logging.info("Reading in factors from {f}".format(f=factor))
        df_factor = pd.read_csv(factor, header=None)
        df_factor.columns = ['dsid', 'factor']
        df_factor.set_index('dsid', inplace=True)
        print(df_factor)

    content = []
    for dsid, batch in sorted(data.items()):
        dsid = int(dsid)
        samplename = str(batch.values()[0]['samplename'])
        mzp = int(findBetween(samplename, '_zp', '_'))
        mdm = int(findBetween(samplename, '_dm', '_'))
        try:
            mdh = int(samplename.split('_dh')[1])
        except ValueError:
            try:
                mdh = int(findBetween(samplename, '_dh', '_'))
            except ValueError:
                mdh = int(findBetween(samplename, '_dh', 'q'))
        f = df_factor.loc[dsid].values[0] if factor else 1.
        # need to read out totalxsec for xsec information, since this contains the cross-section information after all stages
        # (i.e. also from Pythia) otherwise e.g. in case of CKKWL merging where Pythia is used to correct the cross-section an
        # overly high would be used if only e.g. the MG5 information were to be propagated
        content.append((dsid, samplename, mzp, mdm, mdh, averageValue(batch, 'totalxsec'), f, averageValue(batch, 'filter')))
    return pd.DataFrame(content, columns=['dsid', 'samplename', 'mzp', 'mdm', 'mdh', 'xsec_pb', 'factor', 'filter'])


def createPlot(mdm, curves):
    """Create individual plot. This function takes responsibility
    of the matplotlib control for actually doing the plot."""

    # set up plotting
    try:
        import matplotlib as mpl
        mpl.use('Agg')  # required for batch processing without X server
        import matplotlib.pyplot as plt
    except Exception as e:
        logging.warning(e)

    # set ATLAS style
    try:
        from XAMPPmonoH.ATLASlabel import drawATLASLabel
        from rootpy.plotting.style import set_style
        set_style('ATLAS', mpl=True)
    except Exception as e:
        logging.warning(e)

    fig, ax = plt.subplots()
    for curve in sorted(curves.items()):
        mdh = curve[0]
        x = curve[1][0]
        y = curve[1][1]
        ax.plot(x, y, label=r"m(s) = {mdh} GeV".format(mdh=str(mdh)))
        ax.set_xlabel(
            "m(Z') [GeV]",
            horizontalalignment="right",
            x=1,
        )
        ax.set_ylabel('Cross section [pb]', horizontalalignment="right", y=1)
        ax.legend(loc='upper right')
        ax.get_yaxis().set_tick_params(which='both', direction='in', left='True', right='True')
        ax.get_xaxis().set_tick_params(which='both', direction='in', top='True', bottom='True')

    # additional text
    drawATLASLabel(0.10, 0.95, ax, 'int', energy='13 TeV', lumi=None)
    ax.text(0.33, 0.845, '$m_{{DM}}$ = {mdm} GeV'.format(mdm=str(mdm)), transform=ax.transAxes, fontsize=18)
    fig.savefig('plot_xsec_mdm{mdm}.eps'.format(mdm=str(mdm)))

    # log y axis
    ax.set_ylim(0.0001, 1)
    ax.set_yscale("log", nonposy='clip')
    fig.savefig('plot_xsec_mdm{mdm}_log.eps'.format(mdm=str(mdm)))

    # dump plot values to csv file
    for curve in sorted(curves.items()):
        mdh = curve[0]
        x = curve[1][0]
        y = curve[1][1]
        with open('curve_mdm{mdm}_mdh{mdh}.csv'.format(mdm=str(mdm), mdh=str(mdh)), 'w') as f:
            f.write('mzp,xsec_pb\n')
            for i, j in zip(x, y):
                f.write('{i},{j}\n'.format(i=i, j=j))


def plotData(processed_data):
    """Create plots of cross-sections from pandas dataframe.
    Create a plot showing the cross-section vs. Z' mass for different Dark Higgs masses
    for each Dark Matter particle mass value."""
    mdm_list = processed_data['mdm'].unique().tolist()
    logging.info('Creating plots for dark matter masses {mdm} GeV'.format(mdm=" GeV, ".join([str(x) for x in mdm_list])))

    # create a plot for each dark matter particle mass value
    for mdm in mdm_list:
        logging.info('--- dark matter mass {mdm} GeV:'.format(mdm=mdm))
        subset_data = processed_data.query("mdm=={mdm}".format(mdm=mdm))
        mdh_list = subset_data['mdh'].unique().tolist()
        # create a curve for each dark higgs mass value
        curves = {}
        for mdh in mdh_list:
            curve_data = subset_data.query("mdh=={mdh}".format(mdh=mdh))
            x = curve_data['mzp'].values
            y = np.multiply(curve_data['xsec_pb'].values, curve_data['factor'].values)
            curves[mdh] = (x, y)
        createPlot(mdm, curves)


def createCSV(processed_data):
    """Write CSV file from pandas data frame to file."""
    filename = 'cross_sections.csv'
    logging.info('Creating csv file {f}'.format(f=filename))
    processed_data.to_csv(filename, encoding='utf-8', index=False)


def createXAMPP(processed_data):
    """Write cross-section information in format for XAMPP framework"""
    filename = "signal_cross-sections.txt"
    logging.info('Creating XAMPP framework cross-section file {f}'.format(f=filename))
    with open(filename, 'w') as f:
        f.write("""# Signals for monoH analysis
# cross-sections (XS) are in pb
#############################################################################
# DSID  FS     XS [pb]     K-FACTOR   FILTEREFF    REL.UNCERT   # dummy 1.0 unc
""")
        for index, row in processed_data.iterrows():
            f.write("{dsid}  1   {xsec}    {kfactor}    {filtereff}   {uncertainty}\n".format(dsid=int(row['dsid']),
                                                                                              xsec=row['xsec_pb'],
                                                                                              kfactor=row['factor'],
                                                                                              filtereff=row['filter'],
                                                                                              uncertainty=1.))


def createList(processed_data, prefix):
    """Create list for input config creation JSON config file."""
    filename = "inputconfig_configsnippet.txt"
    logging.info('Creating JSON snippet to be inserted in XAMPPmonoH/data/inputConfig_config.json to file {f}'.format(f=filename))
    with open(filename, 'w') as f:
        for index, row in processed_data.iterrows():
            f.write("      \"{prefix}_dsid{dsid}_zp{zp}_dm{dm}_hs{dh}\" : {{\n        \"{dsid}\": \"{name}\"\n      }},\n\n".format(
                prefix=prefix, dsid=int(row['dsid']), name=row['samplename'], zp=row['mzp'], dm=row['mdm'], dh=row['mdh']))


def main():
    """Analyse cross-sections from file, print table and produce output"""
    args = getArgs().parse_args()
    data = readFile(args.xsecFile)
    processed_data = processData(data, args.factor)

    # print cross-sections
    print(processed_data)

    # create plots
    if args.plot:
        plotData(processed_data)

    # create csv file
    if args.csv:
        createCSV(processed_data)

    # create cross-section file
    if args.xsec:
        createXAMPP(processed_data)

    # list samples in JSON format
    if args.list:
        createList(processed_data, args.list)


if __name__ == '__main__':
    main()
