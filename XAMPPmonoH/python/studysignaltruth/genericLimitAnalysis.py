#!/bin/usr/env python
"""
This script uses the generic limits for a MET + h(bb) signature published within the
auxiliary material for Phys. Rev. Lett. 119 (2017) 181804, taken from
https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2016-25/

A generic introduction to this technique is given in this gimicky but educational talk:
https://indico.cern.ch/event/765582/contributions/3177897/attachments/1733728/2803399/GenericLimitsSensitivity_LH_15102018.pdf

The idea is to use these generic limits on the cross-section. These are provided inclusively for 1 and 2 b-tags (0 tag can be neglected)
and in 4 bins of missing transverse momentum [150-200, 200-350, 350-500, 500+] GeV.
In order to estimate the sensitivity for a new signal model, on needs to generate truth level samples for the 
new model and process these with the XAMPPmonoH framework (job option: share/runTruth.py).
It is important to have writing the cutflows to the output ntuple activated when creating the ntuples.
This script processes the ROOT files containing the ntuples and cutflow histograms and calculates
acceptance x efficiency for each sample provided.
Then together with the cross-section which needs to be stored in a cross-section file that
can be parsed to this script, the sensitivity is calculated as follows:

sensitivity = cross-section * (accXeff_bin150-200 / genericlimit_bin150-200 + 
                               accXeff_bin200-350 / genericlimit_bin200-350 + 
                               accXeff_bin350-500 / genericlimit_bin350-500 + 
                               accXeff_bin500+ / genericlimit_bin500+)

If the sensitivity is large than 1, then it can be assumed (*) that the model point can be excluded based on the 
generic limits derived with 36.1/fb.

(*): the caveat holds of course that the acceptance x efficiency in the truth selection does not capture
     the precision of the cutflow employed on reco-level samples (i.e. those with the full ATLAS detector simulation)
     since not all information is avaliable at truth level (i.e. without full ATLAS detector simulation) and
     therefore some cuts employed in the analysis cutflow were omitted.

One should check definitely before producing the ntuples + cutflows that the cutflow defined in the 
MonoHTruthAnalysisConfig (you need to scroll down, it is not the class MonoHAnalysisConfig) in the file
XAMPPmonoH/ROOT/MonoHAnalysisConfig.cxx is matching the cutflow for the signal region (SR) defined in 
https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2016-25/tabaux_03.png


Example usage of script:
> python XAMPPmonoH/python/studysignaltruth/genericLimitAnalysis.py /ptmp/mpp/pgadow/Cluster/OUTPUT/2018-12-13/monoSbb_grid/*.root

After using this script, one can plot the sensitivity on a 2D plot.

Author: paul.philipp.gadow@cern.ch
"""
from argparse import ArgumentParser
import os
import pandas as pd
from rootpy.io import root_open
from collections import OrderedDict

import logging
# output verbosity
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

# environment variable for system independent setup
try:
    TestArea = os.environ['TestArea']
except Exception:
    TestArea = None
if TestArea is None:
    TestArea = './'


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument(nargs='+', dest="files", help="Path to root files with ntuples.")
    parser.add_argument("--xsecFile",
                        default=os.path.join(TestArea, 'XAMPPplotting/data/xSecFiles/MonoH/signal_crosssections_monoSbb.txt'),
                        help="Path to crossection files")
    parser.add_argument("-o", "--output", default="sensitivity.csv", help="Output csv file with results.")
    return parser


def getEfficiency(file, region, dsid):
    """Get efficiency from cutflow histograms."""
    hist = file.Get('Histos_{region}_Nominal/InfoHistograms/DSID_{dsid}_CutFlow_weighted'.format(region=region, dsid=dsid))

    initial = float(hist.GetBinContent(1))
    final = float(hist.GetBinContent(hist.GetNbinsX()))

    # calculate efficiencies
    eff = final / initial
    return eff


def getDSID(file):
    """Crawl file for cutflow histogram, which does (hopefully) contain the DSID."""
    for base, folder, files in file.walk():
        if files:
            if 'DSID' in files[0]: return files[0].split('_')[1]


def getName(filename):
    """Get name of signal from filename."""
    return ".".join(os.path.basename(filename).split('.')[0:-1])


def main():
    # get paths to log files from command line input
    args = getArgs().parse_args()
    # assume cutflow file cutflow information starts in 4th row and has 6 columns: "dsid", "fs", "xsec_pb", "kfactor", "filter", "uncertainty"
    # if this does not hold, this will break. but it won't be hard to fix that by editing the line below, right?
    xsecs = pd.read_csv(args.xsecFile,
                        delim_whitespace=True,
                        skiprows=4,
                        usecols=[0, 1, 2, 3, 4, 5],
                        names=["dsid", "fs", "xsec_pb", "kfactor", "filter", "uncertainty"],
                        index_col=0)
    output = {}

    # generic limits in fb (digitized from https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/EXOT-2016-25/figaux_04.png)
    # make sure that the keys in this dictionary match the names of the cutflows defined in
    generic_limits = {
        'Mono_Higgs_bb_resolved_MET_150_200': 19.1253,
        'Mono_Higgs_bb_resolved_MET_200_350': 13.1181,
        'Mono_Higgs_bb_resolved_MET_350_500': 2.5003,
        'Mono_Higgs_bb_merged': 1.7932
    }

    for file in args.files:
        with root_open(file, 'r') as f:
            logging.info("Processing file {f}".format(f=file))
            dsid = getDSID(f)
            data = OrderedDict()
            data['name'] = getName(file)
            data['xsec_fb'] = float(xsecs['xsec_pb'][int(dsid)]) * float(xsecs['filter'][int(dsid)]) * 1000.  # conversion to fb
            for a in sorted(generic_limits.keys()):
                data[a] = getEfficiency(f, a, dsid)

            # compute sensitivity base on generic limits
            sensitivity = 0.
            for a in sorted(generic_limits.keys()):
                local_sensitivity = data[a] * data['xsec_fb'] / generic_limits[a]
                sensitivity += local_sensitivity
                data[a + '_sensitivity'] = local_sensitivity
            data['sensitivity'] = sensitivity

            # add everything to final result dictionary
            output[dsid] = data

    dataframe = pd.DataFrame.from_dict(
        output,
        orient='index',
    )
    print(dataframe)

    # write output to files
    dataframe.to_csv(args.output)


if __name__ == "__main__":
    main()
