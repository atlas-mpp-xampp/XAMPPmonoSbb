#!/usr/bin/env python
"""
This script prints out the cross sections of simulated processes obtained from AMI.
Author: paul.philipp.gadow@cern.ch
"""
import os
import sys
import commands
import json
import logging
from argparse import ArgumentParser
from collections import OrderedDict
# output verbosity
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument("DSID", nargs='+', type=int, help="DSID for which the cross-section should be retrieved")
    parser.add_argument("--campaign", default="mc16_13TeV", help="MC campaign")
    parser.add_argument("--tag", default="", help="Tag of AOD")
    parser.add_argument("-o", "--outFile", default=None, help="Write to output file (only if specified)")
    return parser


def main():
    """Read cross-section for a given DSID from AMI."""
    # initialize AMI database
    from ClusterSubmission.AMIDataBase import getAMIDataBase
    amiDB = getAMIDataBase()

    # get arguments from command line
    args = getArgs().parse_args()

    # get cross-sections from AMI for each DSID
    data = {}
    amiDB.getMCDataSets(channels=args.DSID, campaign=args.campaign)

    # loop over dsids
    for dsid in args.DSID:
        logging.info("Getting info for: {dsid}".format(dsid=dsid))

        info = amiDB.getMCchannel(dsid, args.campaign)
        datasetnameAMI = str(info.name())
        neventsAMI = float(info.nEvents(tag=args.tag))

        crosssectionAMI = float(info.xSection())
        filtereffAMI = float(info.filtereff())

        data[dsid] = OrderedDict()
        data[dsid]['name'] = datasetnameAMI
        data[dsid]['events'] = neventsAMI
        data[dsid]['xsection'] = crosssectionAMI
        data[dsid]['filtereff'] = filtereffAMI

    # print output
    print(json.dumps(data, indent=4, sort_keys=True))

    # write output to file
    if args.outFile:
        with open(args.outFile, 'w') as outFile:
            outFile.write(json.dumps(data, indent=4, sort_keys=True))


if __name__ == '__main__':
    main()
