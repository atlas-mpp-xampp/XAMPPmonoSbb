#! /bin/sh
# @Author: Paul Philipp Gadow
# @Date:   2017-07-04 16:06:43
# @Last Modified by:   Paul Philipp Gadow
# @Last Modified time: 2017-07-04 18:58:12

# ------------
# set up
# ------------
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup root xrootd

FITINPUT=root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-exotics/jdm/monoHiggs/2018/fitInput/07-00

# ------------
# 0 lepton
# ------------

# get input file from EOS
# enter here the hadded root file containing fit inputs
INPUTFILE=monoH_13TeV_0lep.root
if [ ! -f ${INPUTFILE} ]; then
    echo "File not found! Downloading it from EOS. Make sure that you did kinit ${USER}@CERN.CH to get access before runing this script."
    xrdcp -f ${FITINPUT}/${INPUTFILE} .
fi

PRUNINGOPTION=False

# systematics in four regions
python StudySystematicsFull.py -i $INPUTFILE -s zp2hdm_bb_mzp2800_mA300 zp2hdm_bb_mzp800_mA300 -l 0lep -r 2tag0pfat2pjet_150_200ptv_SR --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s zp2hdm_bb_mzp2800_mA300 zp2hdm_bb_mzp800_mA300 -l 0lep -r 2tag0pfat2pjet_200_350ptv_SR --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s zp2hdm_bb_mzp2800_mA300 zp2hdm_bb_mzp800_mA300 -l 0lep -r 2tag0pfat2pjet_350_500ptv_SR --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s zp2hdm_bb_mzp2800_mA300 zp2hdm_bb_mzp800_mA300 -l 0lep -r 2tag1pfat0pjet_500ptv_SR --pruning ${PRUNINGOPTION}


# ------------
# 1 lepton
# ------------

# get input file from EOS
# enter here the hadded root file containing fit inputs
INPUTFILE=monoH_13TeV_1lep.root
if [ ! -f ${INPUTFILE} ]; then
    echo "File not found! Downloading it from EOS. Make sure that you did kinit ${USER}@CERN.CH to get access before runing this script."
    xrdcp -f ${FITINPUT}/${INPUTFILE} .
fi

PRUNINGOPTION=False

# systematics in four regions
python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag0pfat2pjet_150_200ptv_CR1 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag0pfat2pjet_200_350ptv_CR1 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag0pfat2pjet_350_500ptv_CR1 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag1pfat0pjet_500ptv_CR1 --pruning ${PRUNINGOPTION}


# ------------
# 2 lepton
# ------------

# get input file from EOS
# enter here the hadded root file containing fit inputs
INPUTFILE=monoH_13TeV_2lep.root
if [ ! -f ${INPUTFILE} ]; then
    echo "File not found! Downloading it from EOS. Make sure that you did kinit ${USER}@CERN.CH to get access before runing this script."
    xrdcp -f ${FITINPUT}/${INPUTFILE} .
fi

PRUNINGOPTION=False

# systematics in four regions
python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag0pfat2pjet_150_200ptv_CR2 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag0pfat2pjet_200_350ptv_CR2 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag0pfat2pjet_350_500ptv_CR2 --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag1pfat0pjet_500ptv_CR2 --pruning ${PRUNINGOPTION}
