#!/bin/env python
import os
import sys
import random
from argparse import ArgumentParser
from shutil import copyfile
from ClusterSubmission.Utils import CheckRucioSetup

# set up latex first
if os.path.isdir("/afs/cern.ch/sw/XML/texlive/latest/bin/x86_64-linux"):
    os.system("export PATH=/afs/cern.ch/sw/XML/texlive/latest/bin/x86_64-linux:$PATH")

derivations = {'EXOT': {'0L': 'EXOT24', '1L': 'EXOT25', '2L': 'EXOT26'}, 'HIGG': {'0L': 'HIGG5D1', '1L': 'HIGG5D2', '2L': 'HIGG2D4'}}


def getArgumentParser():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('-l',
                        '-L',
                        '--leptonRegion',
                        help='Choose the lepton region (0L, 1L or 2L)',
                        choices=["0L", "1L", "2L"],
                        required=True)
    parser.add_argument("--deriv",
                        default='EXOT',
                        choices=["EXOT", "HIGG"],
                        help="Derivation which should be used for checking. Possible choices: EXOT, HIGG")
    parser.add_argument("-d",
                        "--destination",
                        default=None,
                        help="Directory where datasets are stored which should be checked. Input config is automatically created.")
    parser.add_argument("-i",
                        "--inputConfigs",
                        nargs='*',
                        default=[],
                        help="Input configs which should be used instead of automatically creating input config for files in directory")
    parser.add_argument("-o", "--outputPath", default='result_NtupleValidation', help="Directory where result will be stored.")
    parser.add_argument("-t", "--nolatex", action='store_true', help="Do not create a latex file")

    return parser


def validateDataSets(leptonRegion, destination, inputConfigs, outputPath, derivation='EXOT', latex=True):
    """Compare downloaded data sets to samples (DxAODs) in SampleList."""

    # ensure we don't mess things up
    identifier = "{i}".format(i=random.getrandbits(16))
    resultdir = 'validationresult_' + leptonRegion + '_' + identifier
    os.makedirs(resultdir)
    os.chdir(resultdir)

    # create output path
    os.system('mkdir -p {outputPath}'.format(outputPath=outputPath))

    # paths
    basepath = os.path.join(os.environ['TestArea'], 'XAMPPmonoH')
    basepathPlotting = os.path.join(os.environ['TestArea'], 'XAMPPplotting')
    basepathScripts = os.path.join(os.environ['TestArea'], 'XAMPPmonoH', 'python')

    tempdir = './InputConfigsMonoHValidation/{identifier}'.format(identifier=identifier)
    os.system('mkdir -p {tempdir}'.format(tempdir=tempdir))
    if destination:
        os.system('python {basepathScripts}/createInputConfig.py -i {destination} -o {tempdir}'.format(basepathScripts=basepathScripts,
                                                                                                       destination=destination,
                                                                                                       tempdir=tempdir))
    elif len(inputConfigs) > 0:
        print(
            "Now copying input configs given as argument to temporary dir {tempdir}. Please ensure that the input configs containing data are called data15.conf, data16.conf or data17.conf for the script to work."
            .format(tempdir=tempdir))
        for inputConfig in inputConfigs:
            copyfile(os.path.abspath(inputConfig), os.path.join('{tempdir}'.format(tempdir=tempdir), os.path.basename(inputConfig)))

    # common arguments to CheckMetaData
    arguments = ' '

    if leptonRegion == '0L':

        # 0lep - data15
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data15.conf  -l  {basepath}/data/SampleLists/data15_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.SR-15-data '
        command += '--lumi | tee lumi_15.txt'
        print(command)
        os.system(command)

        # 0lep - data16
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data16.conf  -l  {basepath}/data/SampleLists/data16_{deriv}.txt {arguments} --lumi ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.SR-16-data '
        command += '--lumi | tee lumi_16.txt'
        print(command)
        os.system(command)

        # 0lep - data17
        command = 'python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data17.conf  -l  {basepath}/data/SampleLists/data17_{deriv}.txt {arguments} --lumi '.format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.SR-17-data '
        command += '--lumi | tee lumi_17.txt'
        print(command)
        os.system(command)

        #  0lep - mc16a
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16a_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.SR-1516-mc '
        print(command)
        os.system(command)

        # 0lep - mc16d
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16d_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.SR-17-mc '
        print(command)
        os.system(command)

    if leptonRegion == '1L':
        # 1lep - data15
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data15.conf  -l  {basepath}/data/SampleLists/data15_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR1-15-data '
        command += '--lumi | tee lumi_15.txt'
        print(command)
        os.system(command)

        # 1lep - data16
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data16.conf  -l  {basepath}/data/SampleLists/data16_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR1-16-data '
        command += '--lumi | tee lumi_16.txt'
        print(command)
        os.system(command)

        # 1lep - data17
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data17.conf  -l  {basepath}/data/SampleLists/data17_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR1-17-data '
        command += '--lumi | tee lumi_17.txt'
        print(command)
        os.system(command)

        #  1lep - mc16a
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16a_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR1-1516-mc '
        print(command)
        os.system(command)

        # 1lep - mc16d
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16d_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR1-17-mc '
        print(command)
        os.system(command)

    if leptonRegion == '2L':
        # 2lep - data15
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data15.conf  -l  {basepath}/data/SampleLists/data15_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR2-15-data '
        command += '--lumi | tee lumi_15.txt'
        print(command)
        os.system(command)

        # 2lep - data16
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data16.conf  -l  {basepath}/data/SampleLists/data16_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR2-16-data '
        print(command)
        command += '--lumi | tee lumi_16.txt'
        os.system(command)

        # 2lep - data17
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  {inputconfFolder}/data17.conf  -l  {basepath}/data/SampleLists/data17_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR2-17-data '
        command += '--lumi | tee lumi_17.txt'
        print(command)
        os.system(command)

        #  2lep - mc16a
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16a_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR2-1516-mc '
        print(command)
        os.system(command)

        # 2lep - mc16d
        command = "python {basepathPlotting}/python/CheckMetaData.py -i  `find {inputconfFolder} -type f ! -name 'data*.conf'` -l {basepath}/data/SampleLists/mc16d_bkgs_{deriv}.txt {arguments} ".format(
            basepathPlotting=basepathPlotting,
            inputconfFolder=tempdir,
            basepath=basepath,
            arguments=arguments,
            deriv=derivations[derivation][leptonRegion])
        if latex:
            command += '-t summary_XAMPPmonoH.CR2-17-mc '
        print(command)
        os.system(command)

    # merge to one pdf output
    if latex:
        os.system('mv summary_*.tex {outputPath}/'.format(outputPath=outputPath))
        os.system('mv summary_*.pdf {outputPath}/'.format(outputPath=outputPath))
        os.system('gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile={outputFile}.pdf {outputPath}/*.pdf'.format(
            outputPath=outputPath, outputFile=outputPath.rstrip('/')))


def main():
    options = getArgumentParser().parse_args()
    if len(options.inputConfigs) == 0 and not options.destination:
        print("Please define either a directory with all samples to be scanned or directly give input config(s).")
        sys.exit()

    CheckRucioSetup()

    if options.leptonRegion == '0L':
        region = 'SR'
    elif options.leptonRegion == '1L':
        region = 'CR1'
    elif options.leptonRegion == '2L':
        region = 'CR2'
    else:
        print "Error: You have to assign the lepton region! i.e. 0L, 1L or 2L"
        sys.exit()
    validateDataSets(options.leptonRegion, options.destination, options.inputConfigs, options.outputPath, options.deriv,
                     not options.nolatex)

    print(
        'Validation finished! Used "{deriv}" filelists in XAMPPmonoH/data/SampleLists to check with AMI. Was that correct? For all productions < 0700 you should use the script with the argument --deriv HIGG'
        .format(deriv=options.deriv))


if __name__ == '__main__':
    main()
