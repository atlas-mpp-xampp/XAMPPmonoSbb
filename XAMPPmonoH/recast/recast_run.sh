#!/bin/bash

# Usage:
# source recast_run.sh <path to DxAOD> <DSID>

# Output:
# The script will steer XAMPPmonoH and XAMPPplotting to produce histograms for the signal process
# normalised to 1pb. These are converted to the WSMaker input format convention
# adapted for the Mono-Higgs(bb) 2018 statistical analysis.

# Input:
DXAOD=${1}
DSID=${2}

# get location independent path to recast script folder
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Step 1: Process DxAOD with XAMPPmonoH -> XAMPPmonoH-ntuple
bash ${DIR}/recast_step1_runXAMPPmonoH.sh ${DXAOD} ${DSID}

# Step 2: Process XAMPPmonoH-ntuple with XAMPPplotting and create histograms
# for signal with dummy cross-section of 1 pb (assuming given DSID)
bash ${DIR}/recast_step2_runXAMPPplotting.sh ${DSID}

# Step3: Convert histograms to WSMaker convention and scale to correct luminosity of 79.8/fb
bash ${DIR}/recast_step3_convertToWSMaker.sh
