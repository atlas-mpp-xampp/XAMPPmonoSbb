#!/bin/bash

# Usage:
# bash recast_step1_runXAMPPmonoH.sh <path to signal DxAOD file> <DSID of signal>

##############################
# Setup                      #
##############################
# prepare environment (assuming this script is located in <basedir>/XAMPPmonoH/recast)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh
cd ${BASEDIR}

TESTDIR=${BASEDIR}/output_recast
TESTFILE_DXAOD=${1}
TESTRESULT=recast_signal_ntuple.root

###################################
# Add DSID to mc16_dsid_proc list #
###################################
DSID=${2}
if [[ $(grep ${DSID} ${BASEDIR}/XAMPPmonoH/data/mc16_dsid_proc.txt) ]]; then
    echo "Signal is already included in process list"
else
    echo "Adding signal with dsid ${DSID} to process list"
    echo "${DSID} recast_signal\n" >> ${BASEDIR}/XAMPPmonoH/data/mc16_dsid_proc.txt
fi

##############################
# Process DxAOD              #
##############################
# create directory for output
mkdir -p ${TESTDIR}
cd ${TESTDIR}

# clean up old job output
if [ -f ${TESTRESULT} ]; then
    rm ${TESTRESULT} 
fi

# run job to process DxAOD and get XAMPP ntuple output
python ${BASEDIR}/XAMPPmonoH/python/runAthena.py --testJob --inputFile ${TESTFILE_DXAOD} --outputFile ${TESTRESULT} --jobOptions XAMPPmonoH/runMonoH_0lep.py --testJobWithSyst --nevents -1

##############################
# Evalulate cut flows        #
##############################
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 0L_SR_Resolved | tee cutflow_data0lep_0L_SR_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a 0L_SR_Merged | tee cutflow_data0lep_0L_SR_Merged.txt

cd ${BASEDIR}
