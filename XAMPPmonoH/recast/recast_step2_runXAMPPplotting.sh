#!/bin/bash

# Usage:
# bash recast_step2_runXAMPPplotting.sh <DSID of signal>

##############################
# Setup                      #
##############################
# prepare environment (assuming this script is located in <basedir>/XAMPPmonoH/recast)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh
cd ${BASEDIR}

TESTDIR=${BASEDIR}/output_recast

# definition of run configs for XAMPPplotting
RUNCONFIG_MERGED=${BASEDIR}/XAMPPplotting/data/RunConf/MonoH/RunConfig_SR_Merged.conf
RUNCONFIG_RESOLVED=${BASEDIR}/XAMPPplotting/data/RunConf/MonoH/RunConfig_SR_Resolved.conf

# write input config for XAMPPplotting
INPUTCONFIG=${BASEDIR}/XAMPPmonoH/recast/recast_signal.conf
echo -e "SampleName recast_signal\nInput ${TESTDIR}/recast_signal_ntuple.root" > ${INPUTCONFIG}

# definition of histo config for XAMPPplotting
HISTOCONFIG=${BASEDIR}/XAMPPplotting/data/HistoConf/MonoH/Histos_Fitting.conf

# set up run config to run with systematics
sed -i 's/noSyst/#noSyst/g' ${BASEDIR}/XAMPPplotting/data/RunConf/MonoH/Common/RunConfig.conf

# ensure that the signal region is not blinded
sed -i 's/@Blinding_Merged/#@Blinding_Merged/g' ${BASEDIR}/XAMPPplotting/data/RunConf/MonoH/Common/RunConfig_common_SR_Merged.conf
sed -i 's/@Blinding_Resolved/#@Blinding_Resolved/g' ${BASEDIR}/XAMPPplotting/data/RunConf/MonoH/Common/RunConfig_common_SR_Resolved.conf

# write cross-section file entry
CROSSSECTIONFILE=${BASEDIR}/XAMPPplotting/data/xSecFiles/MonoH/signal_recast_crosssection_monoHbb.txt
DSID=${1}
echo -e "# Signals for monoHbb analysis RECAST\n# cross-sections (XS) are in pb\n#############################################################################\n# DSID  FS     XS [pb]     K-FACTOR   FILTEREFF    REL.UNCERT   #dummy 100% unc\n${DSID}  0.    1.0            1.          1.           1.        # RECAST signal" > ${CROSSSECTIONFILE}

# set environment variables
export JOB_MAIL=none
export SGE_BASEFOLDER=$TESTDIR

# definition of regions
REGION_MERGED_2B=SR_Merged_2b
REGION_RESOLVED_2B1=SR_Resolved_150_200_2b
REGION_RESOLVED_2B2=SR_Resolved_200_350_2b
REGION_RESOLVED_2B3=SR_Resolved_350_500_2b

##############################
# Process signal sample      #
##############################

echo "##### Processing recast sample 0 lepton...   #####"
python XAMPPplotting/python/SubmitToBatch.py --engine LOCAL -J RECAST -I ${INPUTCONFIG} -H ${HISTOCONFIG} -R ${RUNCONFIG_MERGED} ${RUNCONFIG_RESOLVED}

##############################
# Output from XAMPPplotting  #
##############################
DATE=`date +%Y-%m-%d`
OUTPUT="${TESTDIR}/OUTPUT/${DATE}/RECAST/recast_signal.root"
echo "Created output file ${OUTPUT}"
cp $OUTPUT ${TESTDIR}
OUTPUT="${TESTDIR}/recast_signal.root"
echo "Copying output to ${OUTPUT}"

##############################
# Create cutflows            #
##############################

#   merged cutflow 2b
echo "##### Cutflow: 0 lepton data merged 2 b-tags  (.txt + .tex)                                   #####"
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_MERGED_2B} -o ${TESTDIR}/cutflow_merged_2b
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_MERGED_2B} -o ${TESTDIR}/cutflow_merged_2b -e -f

#    resolved cutflow 2b - 150 GeV < MET < 200 GeV
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B1} -o ${TESTDIR}/cutflow_resolved_2b1
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B1} -o ${TESTDIR}/cutflow_resolved_2b1 -e -f

#    resolved cutflow 2b - 200 GeV < MET < 350 GeV
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B2} -o ${TESTDIR}/cutflow_resolved_2b2
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B2} -o ${TESTDIR}/cutflow_resolved_2b2 -e -f

#    resolved cutflow 2b - 350 GeV < MET < 500 GeV
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B3} -o ${TESTDIR}/cutflow_resolved_2C3
python ${TestArea}/XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT} -a MonoH -r ${REGION_RESOLVED_2B3} -o ${TESTDIR}/cutflow_resolved_2b3 -e -f
