#! /bin/bash
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
cd $BASEDIR
mkdir -p ${BASEDIR}/XAMPPmonoH/data/CDI
echo "Copying custom CDI file from /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/BoostedVHbb2019/Custom_CDI/boosted-CDI-20-02-2019.root"
echo "Please enter the password for your CERN account $USER (if that is not your account, try to have a look into this script and modify it a bit):"
scp ${USER}@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/BoostedVHbb2019/Custom_CDI/boosted-CDI-20-02-2019.root ${BASEDIR}/XAMPPmonoH/data/CDI/
echo "Installed custom CDI file to ${BASEDIR}/XAMPPmonoH/data/CDI/"
echo "For details please have a look into https://indico.cern.ch/event/800060/contributions/3328740/attachments/1800725/2937212/BrianMoser_CustomCDI_22022019_final.pdf"
echo "Remember that you have to run cmake again and need to compile in order for your new custom CDI file to be visible to PathResolver!"

