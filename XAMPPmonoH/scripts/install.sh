#!/bin/bash

echo "Installing XAMPPmonoH"
echo "---------------------"
echo "Parsing README.md file to create install script..."
grep "#!" "README.md" | sed -r 's/#!//g' | sed -e 's/^[ \t]*//' > INSTALL
echo "Done! Created install script here: ${PWD}/INSTALL"
cat INSTALL
echo "----"
echo "source INSTALL"
echo "Installing..."
source INSTALL && rm INSTALL
