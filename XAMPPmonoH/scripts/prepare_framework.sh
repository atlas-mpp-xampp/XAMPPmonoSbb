#!/bin/bash

# This script is sourced by the testing scripts or by the recast scripts to set the environment variables.

# ----------------------------------
# SETUP
# ----------------------------------

# absolute path to analysis code directory (assuming that this script is in "<basedir>/XAMPPmonoH/scripts")
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
BASEDIR=${DIR}/../..

# prepare AthAnalysis or build if not already done so
if [ -f /xampp/build/x86*/setup.sh ]; then
    # inside docker container: set test area by hand
    if [[ -z "${TestArea}" ]]; then
        export TestArea=/xampp/XAMPPmonoH
    fi
    # export ATLAS_LOCAL_ROOT_BASE to some path so that XAMPP's ClusterSubmission scripts do not try to use "asetup" from cvmfs
    # since the docker container has a different way of setting up the atlas software
    export ATLAS_LOCAL_ROOT_BASE=/home/atlas/
    source /xampp/build/x86*/setup.sh

    # export ROOT version (for lsetup root)
    export ALRB_rootVersion=6.14.04-x86_64-slc6-gcc62-opt
elif [ -f ../build/x86*/setup.sh ]; then
    # default structure: (top level directory) analysis -> (subdirectories) run build source
    # the analysis code is hosted in "source" (which is a "clone" of the gitlab XAMPPmonoH project) and the compiled code is hosted in "build"
    # note that for the moment in XAMPPmonoH software the "TestArea" is the "source" and not as in other projects the "build" directory
    cd ${BASEDIR}
    source ../build/x86*/setup.sh
else
    # if the build directory is not in the default position, execute the install script and build the project
    cd ${BASEDIR}
    source ${BASEDIR}/XAMPPmonoH/scripts/install.sh
fi
