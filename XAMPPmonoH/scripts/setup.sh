#!/bin/bash

echo "Preparing shell for XAMPPmonoH"
echo "------------------------------"
echo "Parsing README.md file to create setup script..."
grep "#=" "README.md" | sed -r 's/#=//g' | sed -e 's/^[ \t]*//' > SETUP
echo "Done! Created install script here: ${PWD}/SETUP"
cat SETUP
echo "----"
echo "source SETUP"
source SETUP && rm SETUP
