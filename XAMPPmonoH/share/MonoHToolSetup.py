include("XAMPPbase/BaseToolSetup.py")


def SetupMonoHJetSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYJetSelector"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHJetSelector
        JetSelector = CfgMgr.XAMPP__MonoHJetSelector("SUSYJetSelector")
        JetSelector.DoTime = True
        ToolSvc += JetSelector
    return getattr(ToolSvc, "SUSYJetSelector")


def SetupMonoHMetSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SUSYMetSelector"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHMetSelector
        MetSelector = CfgMgr.XAMPP__MonoHMetSelector("SUSYMetSelector")
        MetSelector.DoTime = True
        ToolSvc += MetSelector
    return getattr(ToolSvc, "SUSYMetSelector")


def SetupMonoHTruthAnalysisHelper(TreeName="TruthMonoH"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHTruthAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__MonoHTruthAnalysisHelper("AnalysisHelper")
        BaseHelper.AnalysisConfig = SetupMonoHTruthAnalysisConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=True,
                                                          noBtag=True,
                                                          noElectrons=True,
                                                          noMuons=True,
                                                          noTaus=True,
                                                          noDiTaus=True,
                                                          noPhotons=True)
        BaseHelper.TruthSelector = SetupMonoHTruthSelector()
        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def SetupMonoHTruthAnalysisConfig(name="TruthMonoHConfig"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHTruthAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__MonoHTruthAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig
    return getattr(ToolSvc, "AnalysisConfig")


def SetupMonoHTruthSelector():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "MonoHTruthSelector"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHTruthSelector
        TruthSelector = CfgMgr.XAMPP__MonoHTruthSelector("MonoHTruthSelector")
        ToolSvc += TruthSelector
    return getattr(ToolSvc, "MonoHTruthSelector")


def SetupMonoHAnaConfig(name="MonoH"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisConfig"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHAnalysisConfig
        AnaConfig = CfgMgr.XAMPP__MonoHAnalysisConfig(name="AnalysisConfig", TreeName=name)
        ToolSvc += AnaConfig
    return getattr(ToolSvc, "AnalysisConfig")


def SetupMetTriggerTool():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "MetTriggerSF"):
        from MetTriggerSFUncertainties.MetTriggerSFUncertaintiesConf import CP__MetTriggerSF
        TriggerTool = CfgMgr.CP__MetTriggerSF(name="MetTriggerSF")
        ToolSvc += TriggerTool
    return getattr(ToolSvc, "MetTriggerSF")

def SetupFatJetCalibTool():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    toolName = "JetFatCalibTool_AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"
    if not hasattr(ToolSvc, toolName):
        from JetCalibTools.JetCalibToolsConf import JetCalibrationTool
        calibTool = CfgMgr.JetCalibrationTool(name=toolName)
        calibTool.JetCollection="AntiKt10UFOCSSKSoftDropBeta100Zcut10"
        calibTool.ConfigFile="JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_01April2020.config" 
        calibTool.CalibSequence="EtaJES_JMS"
        calibTool.IsData=True
        ToolSvc += calibTool
    return getattr(ToolSvc, toolName)

def SetupSMShapesWeightsTool0L():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SMShapeWeights 0L"):
        from SMShapeUncertainties.SMShapeUncertaintiesConf import CP__SMShapeWeights
        SMShapeWeightsTool0L = CfgMgr.CP__SMShapeWeights(name="SMShapeWeights 0L")
        ToolSvc += SMShapeWeightsTool0L
    return getattr(ToolSvc, "SMShapeWeights 0L")


def SetupSMShapesWeightsTool1L():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SMShapeWeights 1L"):
        from SMShapeUncertainties.SMShapeUncertaintiesConf import CP__SMShapeWeights
        SMShapeWeightsTool1L = CfgMgr.CP__SMShapeWeights(name="SMShapeWeights 1L")
        ToolSvc += SMShapeWeightsTool1L
    return getattr(ToolSvc, "SMShapeWeights 1L")


def SetupSMShapesWeightsTool2L():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "SMShapeWeights 2L"):
        from SMShapeUncertainties.SMShapeUncertaintiesConf import CP__SMShapeWeights
        SMShapeWeightsTool2L = CfgMgr.CP__SMShapeWeights(name="SMShapeWeights 2L")
        ToolSvc += SMShapeWeightsTool2L
    return getattr(ToolSvc, "SMShapeWeights 2L")


def SetupMonoHAnalysisHelper(TreeName="MonoH"):
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "AnalysisHelper"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__MonoHAnalysisHelper
        BaseHelper = CfgMgr.XAMPP__MonoHAnalysisHelper("AnalysisHelper")

        BaseHelper.AnalysisConfig = SetupMonoHAnaConfig(TreeName)
        BaseHelper.SystematicsTool = SetupSystematicsTool(noJets=False,
                                                          noBtag=False,
                                                          noElectrons=False,
                                                          noMuons=False,
                                                          noTaus=False,
                                                          noDiTaus=True,
                                                          noPhotons=True)
        BaseHelper.MetTriggerSF = SetupMetTriggerTool()
        BaseHelper.SMShapeWeights0L = SetupSMShapesWeightsTool0L()
        BaseHelper.SMShapeWeights1L = SetupSMShapesWeightsTool1L()
        BaseHelper.SMShapeWeights2L = SetupSMShapesWeightsTool2L()
        BaseHelper.JetSelector = SetupMonoHJetSelector()
        BaseHelper.MetSelector = SetupMonoHMetSelector()
        BaseHelper.SUSYTools = SetupSUSYTools()
        BaseHelper.EventInfoHandler = setupEventInfo()
        if not isData(): SetupMonoHTruthSelector().doTruthParticles = True  # needed for SM bkg shape uncertainties
        SetupMonoHTruthSelector().doJets = False  # no truth jets in reconstructed samples
        BaseHelper.DoTime = True
        ToolSvc += BaseHelper
    return getattr(ToolSvc, "AnalysisHelper")


def getNameFromMCChannelNumber(mc_channel_number):
    from XAMPPbase.Utils import ResolvePath
    inFile = ResolvePath("XAMPPmonoH/mc16_dsid_proc.txt")
    sampleName = None
    with open(inFile) as lines:
        for line in lines:
            line = line.rstrip()
            if str(mc_channel_number) in line:
                sampleName = line.split()
    if not sampleName:
        print 'ERROR: Given MC channel number %s is not part of %s' % (str(mc_channel_number), inFile)
    else:
        return sampleName[-1]


def SetupJetTruthAssociation():
    from AthenaCommon.AppMgr import ToolSvc
    from AthenaCommon import CfgMgr
    if not hasattr(ToolSvc, "MonoHJetTruthAssociator"):
        from XAMPPmonoH.XAMPPmonoHConf import XAMPP__JetTruthAssociationModule
        ToolSvc += XAMPP__JetTruthAssociationModule("MonoHJetTruthAssociator")
    return ToolSvc.MonoHJetTruthAssociator


def SetupLRJTruthLabelAlg():
    from AthenaCommon.AlgSequence import AlgSequence
    from AthenaCommon.AppMgr import ToolSvc
    job = AlgSequence()
    if not hasattr(job, "DecorateLRTruthLabels"):
        from XAMPPbase.Utils import GetPropertyFromConfFile
        STFile = SetupSUSYTools().ConfigFile
        # tool = CfgMgr.JSSWTopTaggerDNN("LargeRJetTruthTool",
        #                                ConfigFile="JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat",
        #                                CalibArea="JSSWTopTaggerDNN/Rel21/")
        tool = CfgMgr.JetTruthLabelingTool("JetTruthLabelingTool")
        #    TruthLabelName="R10TruthLabel_R21Precision"
        ToolSvc += tool
        alg = CfgMgr.DecorateLRTruthLabels("DecorateLRTruthLabels",
                                           TruthLabelTool=tool,
                                           ContainerName=GetPropertyFromConfFile(STFile, "Jet.LargeRcollection"))
        job += alg
    return job.DecorateLRTruthLabels


def SetupTransferAssociatedVRJetsAlg():
    # This is needed by the NN Xbb Tagger for missing
    # GhostVR30Rmax4Rmin02TrackJet_BTagging201903 of the fatjets in current DxAOD
    from AthenaCommon.AlgSequence import AlgSequence
    from AthenaCommon.AppMgr import ToolSvc
    job = AlgSequence()
    if not hasattr(job, "TransferAssociatedVRJets"):
        alg = CfgMgr.TransferAssociatedVRJets("TransferAssociatedVRJets",
                                              InputJets="AntiKt10LCTopoJets",
                                            #   InputJets="AntiKt10UFOCSSKJets",
                                              InputLinks="GhostVR30Rmax4Rmin02TrackJet",
                                              OutputLinks="GhostVR30Rmax4Rmin02TrackJet_BTagging201903",
                                              NewLinkedContainer="AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903")
        job += alg
    return job.TransferAssociatedVRJets


def isSignal():
    from XAMPPbase.Utils import ResolvePath
    inFile = ResolvePath("XAMPPmonoH/samplelist.txt")
    with open(inFile) as samplelist:
        for line in samplelist:
            line = line.strip()
            if line.startswith('#'): continue
            if str(getMCChannelNumber()) in line:
                if ('zp2hdm' in line or '2HDMa' in line):
                    return True
    return False


def ParseCommonMonoHOptions(STFile="XAMPPmonoH/SUSYTools_MonoH.conf"):
    ParseBasicConfigsToHelper(STFile, use1516Data=True, use17Data=True, use18Data=True, SeparateSF=False)

    # Analysis helper
    # -------------------------------------------------------------------------
    SetupMonoHAnalysisHelper().TruthSelector = SetupMonoHTruthSelector()
    # option to activate the processing of truth information
    SetupMonoHAnalysisHelper().doTruth = False
    # running ttbar and W+jets decay mode studies only on corresponding samples
    if (SetupMonoHAnalysisHelper().doTruth):
        if (getMCChannelNumber() >= 364156 and getMCChannelNumber() <= 364197):
            SetupMonoHAnalysisHelper().doTruthWjets = True
        elif ((getMCChannelNumber() >= 407345 and getMCChannelNumber() <= 410470) or (getMCChannelNumber() == 345935)):
            SetupMonoHAnalysisHelper().doTruthTtbar = True
        elif getMCChannelNumber() in [407357,407358,407359,410557,410558,410654,410655,411032,411033,411034,411035,411036,411037,410464,410465,412002,412004,412005]: # These are PowhegHerwig7 and aMcNLO+Pythia8 ttbar samples
            SetupMonoHAnalysisHelper().doTruthTtbar = True
    # Common Tree Mechanism: it is possible to strongly decrease the output
    # ntuple file size by activiting BOTH of the two options createCommonTree
    # and useSystematicGroups.
    # If you only activate createCommonTree but not useSystematicGroups, then
    # only event info and track jet info will be stored in a common tree
    # but the electron/muon/jet variables will not be stored in systematic
    # groups. If you (for whatever reason) decide to activate useSystematicGroups
    # but not activate createCommonTree, then no common tree and no systematic
    # groups will be created (createCommonTree is kind of a master switch).
    #
    # This will split the output in trees storing
    # common variables not altered by certain systematics. E.g. all nominal
    # electron information is stored and used for all non-electron systematics.
    # Since however other systematics might still influence overlap removal,
    # isolation and the topology of the event, when activating
    # useSystematiGroups for muons and electrons the "Pre-" instead of the
    # "Baseline" objects and for jets the "SignalNoOR" instead of the "Signal"
    # objects are written to the output file. The objects will have additional
    # output branches that store for each systematic the "passOR", and for
    # leptons also the "signal" and "isolation" decorators.
    # When having these settings active it is of course required to check
    # for the "passOR" flag in XAMPPplotting.
    SetupMonoHAnalysisHelper().createCommonTree = False
    SetupMonoHAnalysisHelper().useSystematicGroups = False
    # write extended trigger information to output
    SetupMonoHAnalysisHelper().storeExtendedTriggerInfo = False
    # write additional event variables to output
    SetupMonoHAnalysisHelper().doDiagnosticVariables = False
    # write SM shape weights to output
    SetupMonoHAnalysisHelper().doSMShapeWeights = False
    # flag for storing BDT outputs
    SetupMonoHAnalysisHelper().doBDT = False
    # flag for storing XbbScore variables
    SetupMonoHAnalysisHelper().doXbbScore = False
    # flag for storing NN XbbScore variables
    SetupMonoHAnalysisHelper().doNNXbbScore = True
    # Flag for processing variables which were only used in the CONF selection
    SetupMonoHAnalysisHelper().doCONFvariables = False
    if SetupMonoHAnalysisHelper().doBDT:
        SetupMonoHAnalysisHelper().doCONFvariables = True
    # fill LHE weights for signal events
    # (allows to re-map nominal weight from buggy non-nominal weight at zero-th position to weight at correct position)
    SetupMonoHAnalysisHelper().fillLHEWeights = isSignal()
    SetupMonoHAnalysisHelper().StoreLHEByName = isSignal()
    # option to save additional variables for the the muon-in-jet correction
    SetupMonoHAnalysisHelper().WriteMuonsInJets = False

    # use setting for EleFatJet overlap removal from susytools file
    SetupSUSYTools().ConfigFile = STFile
    from XAMPPbase.Utils import GetPropertyFromConfFile
    SetupMonoHAnalysisHelper().ORfatjetElectronDR = float(GetPropertyFromConfFile(STFile, "OR.EleFatJetDR"))
    if not isData() and "zp2hdm" in str(getNameFromMCChannelNumber(getMCChannelNumber())):
        SetupAnalysisHelper().STCrossSectionDB = "XAMPPplotting/xSecFiles/MonoH/"

    SetupMonoHAnalysisHelper().OutputLevel = INFO
    ServiceMgr.MessageSvc.warningLimit = 10
    ServiceMgr.MessageSvc.errorLimit = 10

    ###################################################################################
    # if True, they are applied already,
    # if False, you have to apply them by hand (branch will be created) in XAMPPplotting
    SetupMonoHAnalysisHelper().doPRW = True
    SetupMonoHAnalysisHelper().EventCleaning = True
    SetupMonoHAnalysisHelper().BadMuonCleaning = True
    SetupMonoHAnalysisHelper().CosmicMuonCleaning = True
    SetupMonoHAnalysisHelper().BadJetCleaning = True
    ###################################################################################

    # Analysis config
    # -------------------------------------------------------------------------
    # Skimming cuts: if True, not all events will be written to the ntuples but
    # a certain number of cuts has to be satisfied before an event is written to
    # the ntuple output
    SetupMonoHAnaConfig().doProduction = True
    # Blinding: if True, in the signal region the events with 70 GeV < m_J/jj < 140 GeV will
    # not be written to the XAMPP ntuples for data. MC is unaffected by this option.
    # WARNING!!! Only set this to False if you are told by the analysis coordinators
    # that you are allowed to unblind the analysis.
    SetupMonoHAnaConfig().doBlinding = True
    # flag to activate single muon trigger in CR1 to do the trigger calibration (Default: False)
    SetupMonoHAnaConfig().doMETTriggerCalibration = False
    # flag to activate CONF event selection in MonoHAnalysisConfig
    SetupMonoHAnaConfig().doCONFselection = False
    SetupMonoHAnaConfig().doFullSkimming = True

    # Jets and B-Tagging
    # -------------------------------------------------------------------------
    try:
        JetType = int(GetPropertyFromConfFile(STFile, "Jet.InputType"))
    except:
        recoLog.warning("Could not find the Jet.InputType")

    if JetType == 1:
        JetCollectionName = "AntiKt4EMTopoJets_BTagging" + str(GetPropertyFromConfFile(STFile, "Btag.TimeStamp"))
    elif JetType == 9:
        JetCollectionName = "AntiKt4EMPFlowJets_BTagging" + str(GetPropertyFromConfFile(STFile, "Btag.TimeStamp"))
        #only when we use PFlow jets do the DFCommonCrackVeto
        SetupMonoHAnaConfig().doPFlowElecCleaning = True
        SetupMonoHAnalysisHelper().doPFlowCleaning = True
        PFlow_JVT_WP = GetPropertyFromConfFile(STFile, "Jet.JvtWP")
        if not PFlow_JVT_WP == "Tight":
            print "ERROR: Selected %s JVT WP for PFlow jets! You should use tight WP instead." % (PFlow_JVT_WP)
            exit(1)
    else:
        recoLog.error("ERROR: I've no idea what JetCollection you're aiming for. But it will not work")
        exit(1)
    SetupSUSYJetSelector().AntiKt4BTagContainer = JetCollectionName

    SetupMonoHJetSelector().AntiKt10_BaselinePt = 200.e3
    SetupMonoHJetSelector().AntiKt10_BaselineEta = 2.
    SetupMonoHJetSelector().AntiKt02_BaselinePt = 10.e3
    SetupMonoHJetSelector().AntiKt02_BaselineEta = 2.5

    SetupMonoHJetSelector().TrackJetContainer = str(GetPropertyFromConfFile(STFile, "TrackJet.Coll")) + "_BTagging" + str(GetPropertyFromConfFile(STFile, "BtagTrkJet.TimeStamp"))
    if SetupMonoHJetSelector().TrackJetContainer == 'AntiKtVR30Rmax4Rmin02TrackJets':
        SetupMonoHJetSelector().AssociatedObjectsContainer = 'GhostVR30Rmax4Rmin02TrackJet'
    elif 'AntiKtVR30Rmax4Rmin02TrackJets_BTagging' in SetupMonoHJetSelector().TrackJetContainer:
        SetupMonoHJetSelector().AssociatedObjectsContainer = "GhostVR30Rmax4Rmin02TrackJet_BTagging" + str(GetPropertyFromConfFile(STFile, "BtagTrkJet.TimeStamp"))
    elif SetupMonoHJetSelector().TrackJetContainer == 'AntiKt2PV0TrackJets':
        SetupMonoHJetSelector().AssociatedObjectsContainer = 'GhostAntiKt2TrackJet'
    else:
        print 'INFO: MonoHToolSetup.py - please provide a valid track-jet container!'
        exit(1)
    SetupMonoHJetSelector().LargeRJetContainer = str(GetPropertyFromConfFile(STFile, "Jet.LargeRcollection"))

    # using same calib path and SF strategy for track jets and small-R jets
    CalibPath = str(GetPropertyFromConfFile(STFile, "Btag.CalibPath"))
    SystStrategy = str(GetPropertyFromConfFile(STFile, "Btag.SystStrategy"))
    EigenvecRedB = "Medium"
    EigenvecRedC = "Medium"
    EigenvecRedLight = "Medium"

    WorkingPoint = GetPropertyFromConfFile(STFile, "Btag.WP")
    Tagger = GetPropertyFromConfFile(STFile, "Btag.Tagger")
    MinPt = GetPropertyFromConfFile(STFile, "Btag.MinPt")

    SetupMonoHJetSelector().BTagEfficiencyTool = SetupBtaggingEfficiencyTool(JetCollectionName, WorkingPoint, Tagger, CalibPath,
                                                                             SystStrategy, EigenvecRedB, EigenvecRedC, EigenvecRedLight,
                                                                             str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                             str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                             str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                             str(GetBTagShowerDSID(applyMCMCSFs=True)), MinPt)

    # tagger and WP for track jets
    TrkTagger = str(GetPropertyFromConfFile(STFile, "BtagTrkJet.Tagger"))
    TrkWorkingPoint = str(GetPropertyFromConfFile(STFile, "BtagTrkJet.WP"))
    TrkMinPt = GetPropertyFromConfFile(STFile, "BtagTrkJet.MinPt")

    SetupMonoHJetSelector().TrackJetBTagEfficiencyTool = SetupBtaggingEfficiencyTool(str(GetPropertyFromConfFile(STFile, "TrackJet.Coll")) + "_BTagging" + str(GetPropertyFromConfFile(STFile, "BtagTrkJet.TimeStamp")),
                                                                                     TrkWorkingPoint, TrkTagger, CalibPath, SystStrategy,
                                                                                     EigenvecRedB, EigenvecRedC, EigenvecRedLight,
                                                                                     str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                                     str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                                     str(GetBTagShowerDSID(applyMCMCSFs=True)),
                                                                                     str(GetBTagShowerDSID(applyMCMCSFs=True)), TrkMinPt)
    # set susytools tools to the same tools as in XAMPPmonoH
    SetupSUSYTools().BTaggingEfficiencyTool = SetupMonoHJetSelector().BTagEfficiencyTool
    SetupSUSYTools().BTaggingEfficiencyTool_trkJet = SetupMonoHJetSelector().TrackJetBTagEfficiencyTool

    # TODO: 22.01.2021 Janik: The following two lines are needed because the in-situ calibration for UFO jets is not yet available.
    # This should be removed once the data calibration is ready.
    if isData() and str(GetPropertyFromConfFile(STFile, "Jet.LargeRcollection")) == "AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets":
        SetupSUSYTools().FatJetCalibTool = SetupFatJetCalibTool()

    #SetupSUSYJetSelector().SignalDecorator = 'signal_less_JVT'
    SetupSUSYJetSelector().bJetEtaCut = 2.5
    SetupSUSYJetSelector().SeparateSF = True
    #SetupSUSYJetSelector().ApplyJVTSF = True
    #SetupSUSYJetSelector().ApplyBTagSF = True

    # Tau
    # -------------------------------------------------------------------------
    SetupSUSYTauSelector().ORUtilsSelectionFlag = 2

    # MET
    # -------------------------------------------------------------------------
    SetupMonoHMetSelector().DoTrackMet = False
    SetupMonoHMetSelector().DoMetCST = False
    SetupMonoHMetSelector().IncludeTaus = False
    SetupMonoHMetSelector().IncludePhotons = False

    SetupMonoHMetSelector().WriteMetSignificance = True
    SetupMonoHMetSelector().WriteMetSignificanceLepInvis = True
    # choose only default MET significance definition and object based MET
    # significance without PU jets resolution and without MET soft term
    # resolution entering the MET significance calculation
    SetupMonoHMetSelector().DoMetSign = True
    SetupMonoHMetSelector().DoMetSign_noPUJets_noSoftTerm = True
    # disable all other object based MET significance definitions (not used in
    # the analysis)
    SetupMonoHMetSelector().DoMetSign_noPUJets = False
    SetupMonoHMetSelector().DoMetSign_dataJER = False
    SetupMonoHMetSelector().DoMetSign_dataJER_noPUJets = False
    SetupMonoHMetSelector().DoMetSign_phireso_noPUJets = False

    # Before we set up the XAMPP algorithm, run a preliminary sequence to
    # truth-tag the large-R jets
    SetupLRJTruthLabelAlg()

    # Set up XAMPP algorithm
    # -------------------------------------------------------------------------
    SetupAlgorithm().RunCutFlow = True
    SetupAlgorithm().DoTime = True
    #SetupAlgorithm().printInterval = 0
