// XAMPPmonoH includes
#include "DecorateLRTruthLabels.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

DecorateLRTruthLabels::DecorateLRTruthLabels(const std::string& name, ISvcLocator* pSvcLocator) : AthAnalysisAlgorithm(name, pSvcLocator) {
    declareProperty("TruthLabelTool", m_truthLabelTool);
    declareProperty("ContainerName", m_containerName);
}

DecorateLRTruthLabels::~DecorateLRTruthLabels() {}

StatusCode DecorateLRTruthLabels::initialize() {
    ATH_MSG_INFO("Initializing " << name() << "...");

    ATH_CHECK(m_truthLabelTool.retrieve());

    return StatusCode::SUCCESS;
}

StatusCode DecorateLRTruthLabels::execute() {
    const xAOD::EventInfo* evtInfo(nullptr);
    ATH_CHECK(evtStore()->retrieve(evtInfo, "EventInfo"));

    if (!evtInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) return StatusCode::SUCCESS;

    const xAOD::JetContainer* jets(nullptr);
    ATH_CHECK(evtStore()->retrieve(jets, m_containerName));
    for (const xAOD::Jet* ijet : *jets) ATH_CHECK(m_truthLabelTool->modifyJet(*ijet));

    return StatusCode::SUCCESS;
}
