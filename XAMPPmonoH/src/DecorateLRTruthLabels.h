#ifndef XAMPPMONOH_DECORATELRTRUTHLABELS_H
#define XAMPPMONOH_DECORATELRTRUTHLABELS_H 1

#include "AsgTools/ToolHandle.h"
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "ParticleJetTools/JetTruthLabelingTool.h"

class DecorateLRTruthLabels : public ::AthAnalysisAlgorithm {
public:
    DecorateLRTruthLabels(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~DecorateLRTruthLabels();

    // IS EXECUTED:
    virtual StatusCode initialize();  // once, before any input is loaded
    virtual StatusCode execute();     // per event
private:
    /// The truth labelling tool
    ToolHandle<JetTruthLabelingTool> m_truthLabelTool;
    /// The name of the input container
    std::string m_containerName;
};

#endif  //> !XAMPPMONOH_DECORATELRTRUTHLABELS_H
