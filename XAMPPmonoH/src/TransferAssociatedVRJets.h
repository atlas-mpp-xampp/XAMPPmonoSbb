#ifndef XAMPPMONOH_TRANSFERASSOCIATEDVRJETS_H
#define XAMPPMONOH_TRANSFERASSOCIATEDVRJETS_H 1

#include <experimental/optional>

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"
#include "xAODBase/IParticleContainer.h"

class TransferAssociatedVRJets : public ::AthAnalysisAlgorithm {
public:
    TransferAssociatedVRJets(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~TransferAssociatedVRJets();

    virtual StatusCode initialize();  // once, before any input is loaded
    virtual StatusCode execute();     // per event
private:
    // Properties
    std::string m_inputJets;
    std::string m_inputLinkName;
    std::string m_outputLinkName;
    std::string m_newLinkedContainer;

    // Internals
    using link_t = ElementLink<xAOD::IParticleContainer>;
    using veclink_t = std::vector<link_t>;
    template <typename T> using CAcc = SG::AuxElement::ConstAccessor<T>;
    template <typename T> using Dec = SG::AuxElement::Decorator<T>;
    std::experimental::optional<CAcc<veclink_t>> m_inputLink;
    std::experimental::optional<Dec<veclink_t>> m_outputLink;
};

#endif  //> !XAMPPMONOH_TRANSFERASSOCIATEDVRJETS_H
