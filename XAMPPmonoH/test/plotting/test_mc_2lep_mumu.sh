#!/bin/bash

##############################
# Setup                      #
##############################
# prepare environment (assuming that this script is located in <basedir>/XAMPPmonoH/test/plotting)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh
cd ${BASEDIR}

# definition of folder for storing test results
TESTNAME=test_mc_2lep_mumu
TESTDIR=${BASEDIR}/test/test_mc_2lep

# definition of run config to test
RUNCONFIG_MERGED=XAMPPplotting/data/RunConf/MonoH/RunConfig_CR2mumu_Merged_fullRun2.conf
RUNCONFIG_RESOLVED=XAMPPplotting/data/RunConf/MonoH/RunConfig_CR2mumu_Resolved_fullRun2.conf

# definition of input config for test
INPUTFILE=${BASEDIR}/test/test_mc_2lep/testResult_mc.root
INPUTCONFIG=XAMPPmonoH/test/plotting/data/inputConfig/test_mc_2lep_mumu.conf
mkdir -p XAMPPmonoH/test/plotting/data/inputConfig/
echo -e "SampleName test_mc_2lep_mumu\nInput ${INPUTFILE}" > ${INPUTCONFIG}

# definition of histo config for test
HISTOCONFIG=XAMPPmonoH/test/plotting/data/histoConfig/histoConfig_test.conf

# definition of regions to be tested in the cutflow
REGION_MERGED_2B=CR2_mumu_Merged_2w0b
REGION_RESOLVED_150_200_2B=CR2_mumu_Resolved_150_200_2b
REGION_RESOLVED_200_350_2B=CR2_mumu_Resolved_200_350_2b
REGION_RESOLVED_350_500_2B=CR2_mumu_Resolved_350_500_2b

# set environment variables
export JOB_MAIL=none
export SGE_BASEFOLDER=$TESTDIR

##############################
# Process test samples       #
##############################

echo "##### Processing test sample 2 lepton mumu mc...   #####"
python XAMPPplotting/python/SubmitToBatch.py --engine LOCAL -J ${TESTNAME} -I ${INPUTCONFIG} -H ${HISTOCONFIG} -R ${RUNCONFIG_MERGED} ${RUNCONFIG_RESOLVED}

##############################
# Output from XAMPPplotting  #
##############################
DATE=`date +%Y-%m-%d`
OUTPUT="${TESTDIR}/OUTPUT/${DATE}/${TESTNAME}/${TESTNAME}.root"
echo "Created output file ${OUTPUT}"

##############################
# Read yield from histograms #
##############################
# merged
echo "##### Yields in fit input histograms: 2 lepton mumu mc merged                                    #####"
python XAMPPmonoH/python/getYields.py ${OUTPUT} --mergedVariable m_J --resolvedVariable USENONE --csv -o ${TESTDIR}/yields_merged_mumu
echo "##### Yields in fit input histograms: 2 lepton mumu mc merged  (including under/overflow bins)   #####"
python XAMPPmonoH/python/getYields.py ${OUTPUT} --mergedVariable m_J --resolvedVariable USENONE --useOverflow --csv -o ${TESTDIR}/yields_merged_mumu_overunderflow

# resolved
echo "##### Yields in fit input histograms: 2 lepton mumu mc resolved                                  #####"
python XAMPPmonoH/python/getYields.py ${OUTPUT} --mergedVariable USENONE --resolvedVariable m_jj --csv -o ${TESTDIR}/yields_resolved_mumu
echo "##### Yields in fit input histograms: 2 lepton mumu mc resolved (including under/overflow bins)  #####"
python XAMPPmonoH/python/getYields.py ${OUTPUT} --mergedVariable USENONE --resolvedVariable m_jj --useOverflow --csv -o ${TESTDIR}/yields_resolved_mumu_overunderflow


##############################
# Create cutflows            #
##############################
#   merged cutflow 2b
echo "##### Cutflow: 2 lepton mumu mc merged 2 b-tags  (.txt + .tex)                                   #####"
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_MERGED_2B} -o ${TESTDIR}/cutflow_merged_2b_mumu
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_MERGED_2B} -o ${TESTDIR}/cutflow_merged_2b_mumu -e -f

#    resolved cutflow 2b, 1st MET bin
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_150_200_2B} -o ${TESTDIR}/cutflow_resolved_150_200_2b_mumu
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_150_200_2B} -o ${TESTDIR}/cutflow_resolved_150_200_2b_mumu -e -f

#    resolved cutflow 2b, 2nd MET bin
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_200_350_2B} -o ${TESTDIR}/cutflow_resolved_200_350_2b_mumu
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_200_350_2B} -o ${TESTDIR}/cutflow_resolved_200_350_2b_mumu -e -f

#    resolved cutflow 2b, 3rd MET bin
echo "##### Cutflow: 0 lepton data resolved 2 b-tags  (.txt + .tex)                                 #####"
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_350_500_2B} -o ${TESTDIR}/cutflow_resolved_350_500_2b_mumu
python XAMPPplotting/python/PrintCutFlow.py -i ${OUTPUT}  -a MonoH -r ${REGION_RESOLVED_350_500_2B} -o ${TESTDIR}/cutflow_resolved_350_500_2b_mumu -e -f
