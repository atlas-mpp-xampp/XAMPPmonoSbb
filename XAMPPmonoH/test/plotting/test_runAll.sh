#!/bin/bash

###############################################
# Run all scripts to test XAMPPplotting stage #
###############################################
# assuming that the plotting test scripts are in the same directory as test_runAll.sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "Running all tests."
bash ${DIR}/test_data_0lep.sh &
bash ${DIR}/test_data_1lep.sh &
bash ${DIR}/test_data_2lep_ee.sh &
bash ${DIR}/test_data_2lep_mumu.sh &

bash ${DIR}/test_mc_0lep.sh &
bash ${DIR}/test_mc_1lep.sh &
bash ${DIR}/test_mc_2lep_ee.sh &
bash ${DIR}/test_mc_2lep_mumu.sh &
echo  "Waiting for tests to finish ..."
wait
echo "Tests finished"
