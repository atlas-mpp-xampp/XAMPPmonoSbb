#!/bin/bash

############################################
# Run all scripts to test XAMPPmonoH stage #
############################################
# assuming that the XAMPPmonoH test scripts are in the same directory as test_runAll.sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

echo "Running all tests."
bash ${DIR}/test_data.sh &
bash ${DIR}/test_mc.sh &
bash ${DIR}/test_systematics.sh &
bash ${DIR}/test_truth.sh &
echo  "Waiting for tests to finish ..."
wait
echo "Tests finished"
