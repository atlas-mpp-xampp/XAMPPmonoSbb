#!/bin/bash

##############################
# Setup                      #
##############################
# prepare environment (assuming this script is located in <basedir>/XAMPPmonoH/test)
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null && pwd )"
source ${BASEDIR}/XAMPPmonoH/scripts/prepare_framework.sh

# definition of folder for storing test results
TESTDIR=test/test_truth
TESTFILE_ORIGIN="root://eoshome.cern.ch//eos/user/x/xmonoh/ci/truth/DAOD_TRUTH3.mc15_13TeV.700013.285836.root"
TESTFILE_LOCAL=test_truth_0lep.root
TESTRESULT=testResult_truth0lep.root

##############################
# Process test sample        #
##############################

# create directory for results
mkdir -p ${TESTDIR}
cd ${TESTDIR}
# copy file with xrdcp
if [ ! -f ${TESTFILE_LOCAL} ]; then
    echo "File not found! Copying it from EOS"
    # get kerberos token with service account xmonoh to access central test samples on EOS
    if [ -z ${SERVICE_PASS} ]
    then
      CERN_USER=xmonoh
      echo "Please enter the password for the service account: user ${CERN_USER} (can be obtained in the Gitlab/XAMPPmonoH/Settings/CI/Secret Variables menu)"
      echo "If you belong to the analysis team and already have your CERN USER kerberos token by entering kinit ${USER}@CERN.CH you can skip by entering ctrl + c"
      kinit ${CERN_USER}@CERN.CH
    else
      echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
    fi
    echo xrdcp ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
    xrdcp ${TESTFILE_ORIGIN} ${TESTFILE_LOCAL}
fi


# clean up old job result
if [ -f ${TESTRESULT} ]; then
    rm ${TESTRESULT} 
fi

# run job
python ${BASEDIR}/XAMPPmonoH/python/runAthena.py --evtMax 10000 --testJob --filesInput ${TESTFILE_LOCAL} --outFile ${TESTRESULT} --jobOptions XAMPPmonoH/runTruth.py | tee log.txt

# Raise error if the run did not finish successfully
tail -n 20 log.txt | grep 'successful run' || exit 1



##############################
# Evalulate cut flows        #
##############################

python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a Mono_Higgs_bb_resolved | tee cutflow_truth0lep_0L_SR_Resolved.txt
python ${BASEDIR}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${TESTRESULT} -a Mono_Higgs_bb_merged | tee cutflow_truth0lep_0L_SR_Merged.txt
